//
//  GlobalDataClass.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 08/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import Foundation
import NVActivityIndicatorView
import UIKit


// MARK:- // Language Data Class

class languageData {
    
    init() {}
    
    static let shared = languageData()
    
    //var langData = [languageDataForm]()
    var langData = NSDictionary()
    
//    public func storeLanguageData(langData : [languageDataForm]) {
//        self.langData = langData
//    }
    
    public func storeLanguageData(langData : NSDictionary) {
        self.langData = langData
    }

    
    
}

class superAgentDtls{
    init(){}
    
    static let shared = superAgentDtls()
    
    var superAgentInfo = NSDictionary()
    
    public func storeAgentInfo(superAgentinfo: NSDictionary){
        self.superAgentInfo = superAgentinfo
    }
}

// MARK:- // Country and Phone Codes Data Class

class countryPhoneCodes {
    
    init() {}
    
    static let shared = countryPhoneCodes()
    
    var countryPhnCodes = [countryAndPhoneCodes]()
    
    public func storeCountryPhoneCodesData(countryPhoneCodeData : [countryAndPhoneCodes]) {
        self.countryPhnCodes = countryPhoneCodeData
    }
}


// MARK:- // Language List Data Class

class languageList {
    
    init() {}
    
    static let shared = languageList()
    
    var languageListArray = [languageListDataFormat]()
    
    public func storeCountryPhoneCodesData(langArray : [languageListDataFormat]) {
        self.languageListArray = langArray
    }
}






// MARK:- // User Information Data Class

class userInformation {
    
    init() {}
    
    static let shared = userInformation()
    
    var searchCountryID : String! = ""
    var searchCountryName : String! = ""
    var searchCountryFlag : String! = ""
    
    var searchLanguageCode : String! = ""
    var searchLanguageName : String! = ""
    
    var searchCityID : String! = ""
    var searchCityName : String! = ""
    var searchDistrictID : String! = ""
    var searchDistrictname : String! = ""
    
    var searchCategory : String! = ""
    
    var userName : String! = ""
    var userCountryName : String! = ""
    var userCountryID : String! = ""
    var userCountryFlag : String! = ""
    var userPhoneCode : String! = ""
    var userPhoneNumber : String! = ""
    var userPassword : String! = ""
    var userID : String! = ""
    var userImageString : String! = ""
    var userEmail : String! = ""
    var userType : String! = ""
    var superAgent : String! = ""
    var userWorkPhone : String! = ""
    var userFaxNumber : String! = ""
    var userAddress : String! = ""
    var userProvinceID : String! = ""
    var userProvinceName : String! = ""
    var userDistrictID : String! = ""
    var userDistrictName : String! = ""
    var userCityID : String! = ""
    var userCityName : String! = ""
    var userLatitute : String! = ""
    var userLongitude : String! = ""
    
    var userInboxCount : String! = ""
    var userRecyclebinCount : String! = ""
    var myAdsCount : String! = ""
    var myFavouritesCount : String! = ""
    
    var userFilterDictionary : filterDictionary!
    
    var userDynamicFilterString : String! = ""
    
    var userFilterFeatureDataArray = [dynamicFilterDictionary]()
    var userFilterStatusDataArray = [dynamicFilterDictionary]()
    
    
    // Set Values for guest
    public func setValuesForGuest() {
        self.userName = ""
        self.userCountryName = ""
        self.userCountryID = ""
        self.userCountryFlag = ""
        self.userPhoneCode = ""
        self.userPhoneNumber = ""
        self.userPassword = ""
        self.userID = ""
        self.userImageString = ""
        self.userEmail = ""
        self.userType = ""
        self.superAgent = ""
        self.userWorkPhone = ""
        self.userFaxNumber = ""
        self.userAddress = ""
        self.userProvinceID = ""
        self.userProvinceName = ""
        self.userDistrictID = ""
        self.userDistrictName = ""
        self.userCityID = ""
        self.userCityName = ""
        self.userLatitute = ""
        self.userLongitude = ""
        
        self.userInboxCount = ""
        self.userRecyclebinCount = ""
        self.myAdsCount = ""
        self.myFavouritesCount = ""
        
        self.userDynamicFilterString = ""
    }
    
    
    // Set Search Country ID
    public func setSearchCountryID(id: String) {
        self.searchCountryID = id
    }
    
    // Set Search Country Name
    public func setSearchCountryName(name: String) {
        self.searchCountryName = name
    }
    
    // Set Search Country Flag
    public func setSearchCountryFlag(flag: String) {
        self.searchCountryFlag = flag
    }
    
    // Set Search Language Code
    public func setSearchLanguageCode(code: String) {
        self.searchLanguageCode = code
    }
    
    // Set Search Language Name
    public func setSearchLanguageName(name: String) {
        self.searchLanguageName = name
    }
    
    // Set Search City ID
    public func setSearchCityID(id: String) {
        self.searchCityID = id
    }
    
    // Set Search City Name
    public func setSearchCityName(name: String) {
        self.searchCityName = name
    }
    
    // Set Search District ID
    public func setSearchDistrictID(id: String) {
        self.searchDistrictID = id
    }
    
    // Set Search District Name
    public func setSearchDistrictName(name: String) {
        self.searchDistrictname = name
    }
    
    // Set Search Category
    public func setSearchCategory(category: String) {
        self.searchCategory = category
    }
    
    
    
    
    
    
    // Set User Name
    public func setUserName(name: String) {
        self.userName = name
    }
    
    // Set User Country Name
    public func setUserCountryName(countryName: String) {
        self.userCountryName = countryName
    }
    
    // Set User CountryID
    public func setUserCountryID(countryID: String) {
        self.userCountryID = countryID
    }
    
    // Set User Country Flag
    public func setUserCountryFlag(countryFlag: String) {
        self.userCountryFlag = countryFlag
    }
    
    // Set User Phone Code
    public func setUserPhoneCode(phoneCode: String) {
        self.userPhoneCode = phoneCode
    }
    
    // Set User Phone Number
    public func setUserPhoneNumber(phoneNumber: String) {
        self.userPhoneNumber = phoneNumber
    }
    
    // Set User Password
    public func setUserPassword(password: String) {
        self.userPassword = password
    }
    
    // Set User ID
    public func setUserID(id: String) {
        self.userID = id
    }
    
    // Set User Image String
    public func setUserImageString(imageString: String) {
        self.userImageString = imageString
    }
    
    // Set User Email
    public func setUserEmail(email: String) {
        self.userEmail = email
    }
    
    // Set User Type
    public func setUserType(userType: String) {
        self.userType = userType
    }
    
    // Set super agent
    public func setSuperAgent(superAgent: String) {
        self.superAgent = superAgent
    }
    
    // Set User Work Phone
    public func setUserWorkPhone(workPhone: String) {
        self.userWorkPhone = workPhone
    }
    
    // Set User Fax Number
    public func setUserFaxNumber(faxNumber: String) {
        self.userFaxNumber = faxNumber
    }
    
    // Set User Address
    public func setUserAddress(address: String) {
        self.userAddress = address
    }
    
    // Set User Province ID
    public func setUserProvinceID(provinceID: String) {
        self.userProvinceID = provinceID
    }
    
    // Set User Province Name
    public func setUserProvinceName(provinceName: String) {
        self.userProvinceName = provinceName
    }
    
    // Set User District ID
    public func setUserDictrictID(dictrictID: String) {
        self.userDistrictID = dictrictID
    }
    
    // Set User District Name
    public func setUserDictrictName(districtName: String) {
        self.userDistrictName = districtName
    }
    
    // Set User City ID
    public func setUserCityID(cityID: String) {
        self.userCityID = cityID
    }
    
    // Set User City Name
    public func setUserCityName(cityName: String) {
        self.userCityName = cityName
    }
    
    // Set User Latitude
    public func setUserLatitude(latitude: String) {
        self.userLatitute = latitude
    }
    
    // Set User Longitude
    public func setUserLongitude(longitude: String) {
        self.userLongitude = longitude
    }
    
    // Set User Inbox Count
    public func setUserInboxCount(inboxCount: String) {
        self.userInboxCount = inboxCount
    }
    
    // Set User Recyclebin Count
    public func setUserRecyclebinCount(recyclebinCount: String) {
        self.userRecyclebinCount = recyclebinCount
    }
    
    // Set User Longitude
    public func setUserAdsCount(adsCount: String) {
        self.myAdsCount = adsCount
    }
    
    // Set User Longitude
    public func setUserFavouritesCount(favouritesCount: String) {
        self.myFavouritesCount = favouritesCount
    }
    
    
    
    // Set User Filter Dictioanry
    public func setUserFilterDictionary(dict: filterDictionary) {
        self.userFilterDictionary = dict
    }
    
    // Set User Dynamic Filter String
    public func setUserDynamicFilterString(string: String) {
        self.userDynamicFilterString = string
    }

    // Set User Filter Data Arrays
    public func setUserFilterDataArrays(featureArray: [dynamicFilterDictionary] , statusArray: [dynamicFilterDictionary]) {
        self.userFilterFeatureDataArray = featureArray
        self.userFilterStatusDataArray = statusArray
    }
    
}




// MARK:- // GLobal FUnctions

class globalFunctions {
    
    init() {}
    
    static let shared = globalFunctions()
    
    // MARK:- // Get Language String
    
    public func getLanguageString(variable : String) -> String {
        
        //let langDataArray = languageData.shared.langData
        let langDataDict = languageData.shared.langData
        var langString : String!
        
//        for i in 0..<langDataArray.count {
//
//            if (langDataArray[i].variables?.elementsEqual(variable))! {
//                if (langDataArray[i].languageText?.elementsEqual(""))! {
//                    langString = langDataArray[i].defaultText
//                }
//                else {
//                    langString = langDataArray[i].languageText
//                    break
//                }
//            }
//        }
        
        if langDataDict[variable] != nil {
            
            if "\((langDataDict[variable] as! NSDictionary)["Language Text"] ?? "")".elementsEqual("") {
                langString = "\((langDataDict[variable] as! NSDictionary)["Default Text"] ?? "")"
            }
            else {
                langString = "\((langDataDict[variable] as! NSDictionary)["Language Text"] ?? "")"
            }
            
        }
        else {
            langString = ""
        }
        
        return langString ?? ""
        
    }
    
    
    // MARK:- // function for Set Font
    
    func Get_fontSize(size: Float) -> Float
    {
        var size1 = size
        
        if(GlobalViewController.Isiphone6)
        {
            size1 += 1.0
        }
        else if(GlobalViewController.Isiphone6Plus)
        {
            size1 += 2.0
        }
        else if(GlobalViewController.IsiphoneX)
        {
            size1 += 3.0
        }
        else if(GlobalViewController.IsiphoneXSMAX)
        {
            size1 += 3.0
        }
        
        return size1
    }
    
    
    // MARK:- // Check if key present in userdefaults
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    
    // MARK:- // Set Table Height
    
    func SetTableheight(table: UITableView , heightConstraint: NSLayoutConstraint) {
        
        var setheight: CGFloat  = 0
        table.frame.size.height = 3000
        
        for cell in table.visibleCells {
            setheight += cell.bounds.height
        }
        
        heightConstraint.constant = CGFloat(setheight)
    }
    
    
    
    
    // MARK:- // Change TextField Alignment
    
    func changeTextfieldAlignment(mainview: UIView, Alignment: NSTextAlignment)
    {
        for view in mainview.subviews
        {
            
            if view.isKind(of: UIView.self)
            {
                self.changeTextfieldAlignment(mainview: view, Alignment: Alignment)
            }
            
            if view.isKind(of: UITextField.self)
            {
                (view as! UITextField).textAlignment = Alignment
            }
        }
    }
    
    
    
    
    // MARK:- // Load Image from URL
    
    func loadImage(inImageview : UIImageView , url : String , indicatorInView : UIView) {
        
        var activityIndicator = UIActivityIndicatorView()
        
        activityIndicator = UIActivityIndicatorView(style: .white)
        activityIndicator.frame = inImageview.frame
        activityIndicator.hidesWhenStopped = true
        
        indicatorInView.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        
        inImageview.sd_setImage(with: URL(string: url)) { (image, error, cache, url) in
            // Your code inside completion block
            
            activityIndicator.stopAnimating()
            
        }
        
    }
    
    
    
    
    // MARK:- // Create No Records Found View
    
    func noRecordsFound(centerOf : UIView , leadingTo : UIView , trailingTo : UIView) {
        
        let containerView : UIView = {
           let containerview = UIView()
            containerview.backgroundColor = .lightGray
            containerview.translatesAutoresizingMaskIntoConstraints = false
            return containerview
        }()
        
        leadingTo.addSubview(containerView)
        
        containerView.anchor(top: nil, leading: leadingTo.leadingAnchor, bottom: nil, trailing: trailingTo.trailingAnchor)
        containerView.centerYAnchor.constraint(equalTo: centerOf.centerYAnchor).isActive = true
        
        let noRecordsFoundLBL : UILabel = {
           let norecordsfoundlbl = UILabel()
            norecordsfoundlbl.font = UIFont(name: "Lato-Bold", size: 20)
            norecordsfoundlbl.textAlignment = .center
            norecordsfoundlbl.text = globalFunctions.shared.getLanguageString(variable: "home_no_record_found")
            norecordsfoundlbl.translatesAutoresizingMaskIntoConstraints = false
            return norecordsfoundlbl
        }()
        
        containerView.addSubview(noRecordsFoundLBL)
        
        noRecordsFoundLBL.anchor(top: containerView.topAnchor, leading: containerView.leadingAnchor, bottom: containerView.bottomAnchor, trailing: containerView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 80))
        
    }
    
    
    
    // MARK:- // Create  No Network View
    
    func createNoNetworkView(addInView : UIView , noNetView : UIView) {
        
        addInView.addSubview(noNetView)
        
        noNetView.anchor(top: addInView.topAnchor, leading: addInView.leadingAnchor, bottom: addInView.bottomAnchor, trailing: addInView.trailingAnchor)
        
        noNetView.restorationIdentifier = "noInternet"
        
        addInView.bringSubviewToFront(noNetView)
        
        let contentView : UIView = {
           let contentview = UIView()
            contentview.translatesAutoresizingMaskIntoConstraints = false
            return contentview
        }()
        
        noNetView.addSubview(contentView)
        
        contentView.anchor(top: nil, leading: noNetView.leadingAnchor, bottom: nil, trailing: noNetView.trailingAnchor, padding: .init(top: 0, left: 40, bottom: 0, right: 40))
        
        contentView.centerYAnchor.constraint(equalTo: noNetView.centerYAnchor).isActive = true
        
        let noNetworkImageview : UIImageView = {
           let nonetworkimageview = UIImageView()
            nonetworkimageview.translatesAutoresizingMaskIntoConstraints = false
            nonetworkimageview.image = UIImage(named: "wifi")
            return nonetworkimageview
        }()
        
        contentView.addSubview(noNetworkImageview)
        
        noNetworkImageview.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: contentView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: (200/568)*UIScreen.main.bounds.size.height))
        
        
        let noNetworkLBL : UILabel = {
            let nonetworklbl = UILabel()
            nonetworklbl.text = "Please check your Internet connection and restart the app..."
            nonetworklbl.textAlignment = .center
            nonetworklbl.numberOfLines = 0
            nonetworklbl.font = UIFont(name: "Lato-Bold", size: 18)
            nonetworklbl.translatesAutoresizingMaskIntoConstraints = false
            return nonetworklbl
        }()
        
        contentView.addSubview(noNetworkLBL)
        
        noNetworkLBL.anchor(top: noNetworkImageview.bottomAnchor, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor)
        
    }
    
    
    
    // MARK;- // Email Validation
    
    func validateEmail(enteredEmail:String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
        
    }
    
    // MARK;- // Password Validation
    
    func validatePassword(enteredPassword:String) -> Bool {
        
        let passwordFormat = ".{6}"
        let passwordPredicate = NSPredicate(format:"SELF MATCHES %@", passwordFormat)
        return passwordPredicate.evaluate(with: enteredPassword)
        
    }
    
    
    
    // MARK:- // Load Placeholder
    
    func loadPlaceholder(onView: UIView) {
        
        let contentView : UIView = {
           let contentview = UIView()
            contentview.translatesAutoresizingMaskIntoConstraints = false
            contentview.backgroundColor = .white
            contentview.tag = 999
            return contentview
        }()
        
        onView.addSubview(contentView)
        
        let placeholderImage : UIImageView = {
           let placeholderimage = UIImageView()
            placeholderimage.translatesAutoresizingMaskIntoConstraints = false
            placeholderimage.contentMode = .scaleAspectFit
            placeholderimage.image = UIImage(named: "placeholder")
            return placeholderimage
        }()
        
        contentView.addSubview(placeholderImage)
        
        
        contentView.anchor(top: onView.topAnchor, leading: onView.leadingAnchor, bottom: onView.bottomAnchor, trailing: onView.trailingAnchor)
        
        placeholderImage.anchor(top: onView.topAnchor, leading: nil, bottom: nil, trailing: nil, padding: .init(top: (180/568)*UIScreen.main.bounds.size.height, left: 0, bottom: 0, right: 0), size: .init(width: (80/320)*UIScreen.main.bounds.size.width, height: (80/320)*UIScreen.main.bounds.size.width))
        
        placeholderImage.centerXAnchor.constraint(equalTo: onView.centerXAnchor).isActive = true
        
        
        onView.bringSubviewToFront(contentView)
    }
    
    
    // MARK:- // Remove Placeholder
    
    func removePlaceholder(fromView : UIView) {
        
        DispatchQueue.main.async {
            
            for view in fromView.subviews {
                if view.tag == 999 {
                    view.removeFromSuperview()
                    break
                }
            }
        }
    }
    
    
    
    // MARK:- // Make Asterisk (*) beside mandatory labels
    
    func asterixText(withText: String) -> NSMutableAttributedString
    {
        let passwordAttriburedString = NSMutableAttributedString(string: withText)
        let asterix = NSAttributedString(string: "*", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        passwordAttriburedString.append(asterix)
        
        return passwordAttriburedString
    }
    
    
    // MARK:- // Create Picker Toolbar
    
    func createToolbar(addOnView : UIView,bottomView : UIView) {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.tintColor = UIColor.white//"Done" button colour
        toolBar.barTintColor = UIColor.black// bar background colour
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(donePicker))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelPicker))
        
        toolBar.setItems([cancelButton, spacer, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        addOnView.addSubview(toolBar)
        toolBar.anchor(top: nil, leading: nil, bottom: bottomView.topAnchor, trailing: nil, size: .init(width: UIScreen.main.bounds.size.width, height: 40))
        
    }
    
    // MARK:- // Done Picker Action Method
    
    @objc func donePicker() {
        print("Done Clicked")
        
        doneFunction()
    }
    
    // MARK:- // Cancel Picker Action Method
    
    @objc func cancelPicker() {
        print("Cancel Clicked")
        
        cancelFunction()
    }
    
    // MARK:- // Done Function
    
    func doneFunction() {
        
        pickerviewData.shared.doneClick(doneClicked: true)
        
        if self.topViewController()?.isKind(of: AppStartSearchVC.self) == true {
            
            let instance = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "AppStartSearchVC") as! AppStartSearchVC
            
            instance.pickerView(pickerviewData.shared.selectedPickerview, didSelectRow: pickerviewData.shared.selectedPickerIndex, inComponent: 0)
            
            pickerviewData.shared.doneClick(doneClicked: false)
        }
        
        pickerviewData.shared.masterview.sendSubviewToBack(pickerviewData.shared.viewOfPicker)
        pickerviewData.shared.viewOfPicker.isHidden = true
        pickerviewData.shared.selectedPickerview.isHidden = true
        
    }
    
    // MARK:- // Cancel Function
    
    func cancelFunction() {
        pickerviewData.shared.masterview.sendSubviewToBack(pickerviewData.shared.viewOfPicker)
        pickerviewData.shared.viewOfPicker.isHidden = true
        pickerviewData.shared.selectedPickerview.isHidden = true
    }
    
    
    // MARK: - // function TopViewController
    
    
    func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
}


// MARK:- // Pickerview Data

class pickerviewData {
    
    init () {}
    
    static let shared = pickerviewData()
    
    var selectedPickerview : UIPickerView!
    
    var masterview : UIView!
    
    var viewOfPicker : UIView!
    
    var selectedPickerIndex : Int!
    
    var isNewValueSelected : Bool! = false
    
    var doneClicked : Bool! = false
    
    public func setSelectedPicker(pickerview : UIPickerView) {
        self.selectedPickerview = pickerview
    }
    
    
    public func setViews(selectedPickerview : UIPickerView , masterview : UIView , viewOfPicker : UIView) {
        
        self.selectedPickerview = selectedPickerview
        self.masterview = masterview
        self.viewOfPicker = viewOfPicker
    }
    
    public func setNewValues(selectedPickerIndex : Int) {
        
        self.selectedPickerIndex = selectedPickerIndex
    }
    
    public func doneClick(doneClicked : Bool) {
        self.doneClicked = doneClicked
    }
    
}













// MARK:- // Language Data Dictionary Format

class languageDataForm
{
    var variables : String?
    var defaultText : String?
    var languageText : String?
    
    init(variables: String, defaultText: String, languageText: String) {
        
        self.variables = variables
        self.defaultText = defaultText
        self.languageText = languageText
        
    }
}


// MARK:- // Country and Phone Codes Data Dictionary Format

class countryAndPhoneCodes
{
    var countryFlag : String?
    var countryName : String?
    var countryCode : String?
    var id : String?
    var languageCode : String?
    var phoneCode : String?
    var appSearchStatus : String?
    
    init(countryFlag: String, countryName: String, countryCode : String , id: String, languageCode : String , phoneCode: String, appSearchStatus : String) {
        
        self.countryFlag = countryFlag
        self.countryName = countryName
        self.countryCode = countryCode
        self.id = id
        self.languageCode = languageCode
        self.phoneCode = phoneCode
        self.appSearchStatus = appSearchStatus
        
    }
}


// MARK:- // Language List Data Dictionary Format

class languageListDataFormat
{
    var id : String?
    var langName : String?
    var langCode : String?
    var langImage : String?
    var langStatus : String?
    
    init(id: String, langName: String, langCode: String, langImage: String, langStatus: String) {
        
        self.id = id
        self.langName = langName
        self.langCode = langCode
        self.langImage = langImage
        self.langStatus = langStatus
        
    }
}




// MARK:- // Side Menu View Data Dictionary FOrmat

class sideMenuDataFormat
{
    var title : String?
    var imageString : String?
    var notificationShow : Bool!
    
    init(title: String, imageString: String, notificationShow: Bool) {
        
        self.title = title
        self.imageString = imageString
        self.notificationShow = notificationShow
        
    }
}


// MARK:- // Property Details Room Types Data Dictionary FOrmat

class roomTypesDataFormat
{
    var title : String?
    var count : String?
    var image : String?
    
    init(title: String, count: String, image: String) {
        
        self.title = title
        self.count = count
        self.image = image
        
    }
}


// MARK:- // Single Message Data Format

class singleMessageDataFormat
{
    var id : String?
    var name : String?
    var email : String?
    var date : String?
    var time : String?
    var readStatus : String?
    var subject : String?
    var message : String?
    var realEstateNumber : String?
    
    init(id: String, name: String, email: String, date: String, time: String, readStatus: String, subject: String, message: String, realEstateNumber: String) {
        
        self.id = id
        self.name = name
        self.email = email
        self.date = date
        self.time = time
        self.readStatus = readStatus
        self.subject = subject
        self.message = message
        self.realEstateNumber = realEstateNumber
        
    }
}




// MARK:- // Filter Data Format

class filterDataFormat
{
    var parentID : String?
    var parentName : String?
    var childArray : [filterCategoriesDataFormat]
    var isSelected : Bool!
    
    init(parentID: String, parentName: String, childArray: [filterCategoriesDataFormat], isselected: Bool) {
        
        self.parentID = parentID
        self.parentName = parentName
        self.childArray = childArray
        self.isSelected = isselected
        
    }
}



// MARK:- // Filter Categories Data Format

class filterCategoriesDataFormat
{
    var id : String?
    var categoryName : String?
    var isSelected : Bool?
    
    init(id: String, categoryName: String, isSelected: Bool) {
        
        self.id = id
        self.categoryName = categoryName
        self.isSelected = isSelected
        
    }
    
    
}



// MARK:- // Filter Dictionary

class filterDictionary
{
    
    var parentCategory : String! = ""
    var propertyType : String! = ""
    var categorySub : String! = ""
    var period : String! = ""
    var country : String! = ""
    var province : String! = ""
    var city : String! = ""
    var district : String! = ""
    var minPrice : String! = ""
    var maxPrice : String! = ""
    var minPriceID : String! = ""
    var maxPriceID : String! = ""
    var minArea : String! = ""
    var maxArea : String! = ""
    var viewDays : String! = ""
    var addedBy : String! = ""
    var negotiablePrice : String! = ""
    
    init(parentCategory: String, propertyType: String, categorySub: String, period: String, country: String, province: String, city: String, district: String, minPrice: String, maxPrice: String, minPriceID: String, maxPriceID: String, minArea: String, maxArea: String, viewDays: String, addedBy: String, negotiablePrice: String) {
        
        self.parentCategory = parentCategory
        self.propertyType = propertyType
        self.categorySub = categorySub
        self.period = period
        self.country = country
        self.province = province
        self.city = city
        self.district = district
        self.minPrice = minPrice
        self.maxPrice = maxPrice
        self.minPriceID = minPriceID
        self.maxPriceID = maxPriceID
        self.minArea = minArea
        self.maxArea = maxArea
        self.viewDays = viewDays
        self.addedBy = addedBy
        self.negotiablePrice = negotiablePrice
        
    }
    
}

// MARK:- // Dynamic Filter Dictionary

class dynamicFilterDictionary
{
    
    var fieldID : String! = ""
    var fieldName : String! = ""
    var fieldType : String! = ""
    var fieldTypeID : String! = ""
    var parameter : String! = ""
    var parameterSecond : String! = ""
    var fieldOptionArray : [StaticDictDataFormat]!
    var value : String! = ""
    
    
    init(fieldID: String, fieldName: String, fieldType: String, fieldTypeID: String, parameter: String, parameterSecond: String, fieldOptionArray: [StaticDictDataFormat], value: String) {
        
        self.fieldID = fieldID
        self.fieldName = fieldName
        self.fieldType = fieldType
        self.fieldTypeID = fieldTypeID
        self.parameter = parameter
        self.parameterSecond = parameterSecond
        self.fieldOptionArray = fieldOptionArray
        self.value = value
        
    }
    
}



// MARK:- // Filter Static Dictionary Data Format

class StaticDictDataFormat {
    
    var key : String?
    var value : String?
    var isSelected : Bool!
    
    init(key: String, value: String, isSelected: Bool) {
        
        self.key = key
        self.value = value
        self.isSelected = isSelected
        
    }
    
}




// MARK:- // Add Property Dynamic Data Format

class addPropertyDynamicDataFormat {
    
    var fieldID : String?
    var fieldName : String?
    var fieldType : String?
    var fieldTypeID : String?
    var parameter : String?
    var fieldOptionArray : [StaticDictDataFormat]!
    var value : String?
    var fieldMadatoryStatus: String?
    
    init(fieldID : String , fieldName : String , fieldType : String , fieldTypeID : String , parameter : String , fieldOptionArray : [StaticDictDataFormat] , value : String,fieldMadatoryStatus: String) {
        
        self.fieldID = fieldID
        self.fieldName = fieldName
        self.fieldType = fieldType
        self.fieldTypeID = fieldTypeID
        self.parameter = parameter
        self.fieldOptionArray = fieldOptionArray
        self.value = value
        self.fieldMadatoryStatus = fieldMadatoryStatus
        
    }
    
}



// MARK:- // Real Estate Plans Data Format

class realEstatePlansDataFormat {
    
    var planID : String?
    var planName : String?
    
    init(planID : String , planName : String) {
        self.planID = planID
        self.planName = planName
    }
    
    
}


// MARK:- // Plan Duration Data Format

class planDurationDataForamt {
    
    var planPriceID : String?
    var planCost : String?
    var planDuration : String?
    
    init(planPriceID : String , planCost : String , planDuration : String) {
        self.planPriceID = planPriceID
        self.planCost = planCost
        self.planDuration = planDuration
    }
    
}


// MARK:- // Plan Type Data Format

class planTypeDataFormat {
    
    var planID : String?
    var planName : String?
    var planValidUnit : String?
    var planPriceList : [planDurationDataForamt]
    var isSelected : Bool!
    
    var planDuration : String?
    var planCost : String?
    var planPriceID : String?
    
    init(planID : String , planName : String , planValidUnit : String , planPriceList : [planDurationDataForamt] , planDuration : String , isSelected : Bool , planCost : String , planPriceID : String) {
        self.planID = planID
        self.planName = planName
        self.planValidUnit = planValidUnit
        self.planPriceList = planPriceList
        self.planDuration = planDuration
        self.isSelected = isSelected
        self.planCost = planCost
        self.planPriceID = planPriceID
    }
    
}


// MARK:- // Sort Type Data Format

class sortTypeDataFormat {
    
    var sortID : String?
    var sortName : String?
    var isSelected : Bool!
    
    init(sortID : String , sortName : String , isSelected : Bool) {
        self.sortID = sortID
        self.sortName = sortName
        self.isSelected = isSelected
    }
    
}



// MARK:- // Location Fetch Data Format

class locationFetchDataFormat {
    
    var countryName : String?
    var countryCode : String?
    var locality : String?
    var sublocality : String?
    var throughfate : String?
    var postalCode : String?
    var subThroughfare : String?
    var administrativeArea : String?
    
    init(countryName : String , countryCode : String , locality : String , sublocality : String , throughfate : String , postalCode : String , subthroughfare : String , administrativeArea : String) {
        
        self.countryName = countryName
        self.countryCode = countryCode
        self.locality = locality
        self.sublocality = sublocality
        self.throughfate = throughfate
        self.postalCode = postalCode
        self.subThroughfare = subthroughfare
        self.administrativeArea = administrativeArea
        
    }
    
}


