//
//  ConnectivityManager.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 20/08/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import Foundation
import Alamofire

public enum NetworkConnectionStatus: String {
    case online
    case offline
}

public protocol NetworkConnectionStatusListener: class {
    func networkStatusDidChange(status: NetworkConnectionStatus)
}


class ConnectivityManager: NSObject {
    
    public override init() {
        super.init()
        configuireReachability()
    }
    
    private static let _shared = ConnectivityManager()
    
    class func shared() -> ConnectivityManager {
        return _shared
    }
    
    private let networkReachablity: NetworkReachabilityManager? = NetworkReachabilityManager()
    
    var isNetworkAvailable: Bool {
        return networkReachablity?.isReachable ?? false
    }
    
    var listeners = [NetworkConnectionStatusListener]()
    
    private func configuireReachability() {
        
        networkReachablity?.listener = {[weak self] status in
            switch status {
            case .unknown:
                self?.notifyAllListenersWith(status: .offline)
            case .notReachable:
                self?.notifyAllListenersWith(status: .offline)
            case .reachable(.ethernetOrWiFi): fallthrough
            case .reachable(.wwan):
                self?.notifyAllListenersWith(status: .online)
            }
        }
        
    }
    
    
    private func notifyAllListenersWith(status: NetworkConnectionStatus) {
        print("Network Connection Status is \(status.rawValue)")
        for listener in listeners {
            listener.networkStatusDidChange(status: status)
        }
    }
    
    
    func addListener(listener: NetworkConnectionStatusListener) {
        listeners.append(listener)
    }
    
    
    func removeListener(listener: NetworkConnectionStatusListener) {
        listeners = listeners.filter { $0 !== listener }
    }
    
    func startListening() {
        networkReachablity?.startListening()
    }
    
    func stopListening() {
        networkReachablity?.stopListening()
    }
    
}
