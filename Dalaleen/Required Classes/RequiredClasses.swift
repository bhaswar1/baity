//
//  RequiredClasses.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 05/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import Foundation
import Alamofire


// MARK:- // ShadowView class

class ShadowView: UIView {
    override var bounds: CGRect {
        didSet {
            setupShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3)
        }
    }
    
    private func setupShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1) {
        //self.layer.cornerRadius = 8
        layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = opacity
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}

class ShadowView2: UIView {
    override var bounds: CGRect {
        didSet {
            setupShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3)
        }
    }
    
    private func setupShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1) {
        //self.layer.cornerRadius = 8
        layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = opacity
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 0, height: 0)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}

// MARK:- // BottomShadowView class

class BottomShadowView: UIView {
    override var bounds: CGRect {
        didSet {
            setupShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3)
        }
    }
    
    private func setupShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1) {
        
        
//        self.layer.cornerRadius = 8
//        let shadowSize: CGFloat = 15
//        let contactRect = CGRect(x: -shadowSize, y: self.frame.size.height - (shadowSize * 0.4), width: self.frame.size.width, height: shadowSize)
//        self.layer.shadowPath = UIBezierPath(ovalIn: contactRect).cgPath
//        self.layer.shadowRadius = 5
//        self.layer.shadowOpacity = 0.4
        
        
        
//        self.layer.masksToBounds = false
//        self.layer.shadowRadius = 4
//        self.layer.shadowOpacity = 1
//        self.layer.shadowColor = UIColor.gray.cgColor
//        self.layer.shadowOffset = CGSize(width: 0 , height:2)
        
        
        
        let shadowHeight : CGFloat = 7
        let shadowPath = UIBezierPath()
        shadowPath.move(to: CGPoint(x: 0, y: self.bounds.height))
        shadowPath.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height))
        shadowPath.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height + shadowHeight))
        shadowPath.addLine(to: CGPoint(x: 0, y: self.bounds.height + shadowHeight))
        shadowPath.close()
        
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
        self.layer.shadowPath = shadowPath.cgPath
        self.layer.shadowRadius = 2
        
        
    }
}








// MARK:- // Network Connectivity

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}



// MARK:- // Border View

class borderView: UIView {
    
    override var bounds: CGRect {
        didSet {
            setupBorderview(color: UIColor.gray)
        }
    }
    
    public func setupBorderview(color: UIColor, borderWidth: CGFloat = 0.5) {
        
        self.layer.cornerRadius = 5
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 0.5
        
    }
    
}


// MARK:- // Class POI Item

class POIItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var price: String!
    var currency: String!
    var tag: Int!
    var icon: UIImage!
    
    init(position: CLLocationCoordinate2D, price: String, currency: String, tag: Int, icon: UIImage) {
        self.position = position
        self.price = price
        self.currency = currency
        self.tag = tag
        self.icon = icon
    }
}



// MARK:- // Custom Marker

class CustomMarkerView: UIView {
    var img: UIImage!
    var borderColor: UIColor!
    
    init(frame: CGRect, image: UIImage, borderColor: UIColor, tag: Int , text: String) {
        super.init(frame: frame)
        self.img = image
        self.borderColor = borderColor
        self.tag = tag
        setupViews(showText: text, color: .black)
    }
    
    func setupViews(showText : String , color : UIColor) {
        let imgView = UIImageView(image: img)
        imgView.frame=CGRect(x: 0, y: 0, width: 80, height: 35)
        //imgView.layer.cornerRadius = 10
//        imgView.layer.borderColor=borderColor?.cgColor
        //imgView.layer.borderWidth=4
        imgView.clipsToBounds = true
        let lbl=UILabel(frame: CGRect(x: 3, y: -1, width: 70, height: 30))
        //lbl.center = imgView.center
        //lbl.frame.origin.y = lbl.frame.origin.y - 5
        lbl.text = showText
        lbl.font = UIFont(name: "Verdana", size: 10)
        lbl.textColor = color
        lbl.textAlignment = .center

        self.addSubview(imgView)
        self.addSubview(lbl)
    }
    
    func imageWith(name: String?) -> UIImage? {
        let frame = CGRect(x: 0, y: 0, width: 65, height: 30)
        let nameLabel = UILabel(frame: frame)
        nameLabel.textAlignment = .center
        nameLabel.backgroundColor = .yellow
        nameLabel.textColor = .black
        nameLabel.font = UIFont.boldSystemFont(ofSize: 40)
        nameLabel.text = name
        UIGraphicsBeginImageContext(frame.size)
        if let currentContext = UIGraphicsGetCurrentContext() {
            nameLabel.layer.render(in: currentContext)
            let nameImage = UIGraphicsGetImageFromCurrentImageContext()
            return nameImage
        }
        return nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class dynamicStackview: UIStackView {
    
    override var bounds: CGRect {
        didSet {
            self.setupView()
        }
    }
    
    func setupView() {
        
        self.axis = .vertical
        self.alignment = .fill // .leading .firstBaseline .center .trailing .lastBaseline
        self.distribution = .fill // .fillEqually .fillProportionally .equalSpacing .equalCentering
        self.spacing = 10
    }
    
}

// MARK:- // Myplace Structure

struct MyPlace {
    var name: String
    var lat: Double
    var long: Double
}




