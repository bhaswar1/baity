//
//  ResetPasswordVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 08/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView
import FirebaseUI

class ResetPasswordVC: GlobalViewController {
    
    @IBOutlet weak var resetPasswordTitleLBL: UILabel!
    @IBOutlet weak var resetPasswordSubtitleLBL: UILabel!
    @IBOutlet weak var successMessageView: UIView!
    @IBOutlet weak var successMessage: UILabel!
    @IBOutlet weak var otpCodeView: UIView!
    @IBOutlet weak var otpCodeTXT: UITextField!
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var newPasswordTXT: UITextField!
    @IBOutlet weak var confirmNewPasswordView: UIView!
    @IBOutlet weak var confirmNewPasswordTXT: UITextField!
    
    @IBOutlet weak var resendView: UIView!
    @IBOutlet weak var resendStaticLbl: UILabel!
    @IBOutlet weak var resendCounterLbl: UILabel!
    
    
    var phoneNumber : String! = ""
    var phoneCode : String! = ""
    
    var verificationID : String!
    
    var resetPasswordResponseDictionary : NSDictionary!
    
    var OTPVerified : Bool! = false
    
    // MARK:- // View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.SetFont()
        
        self.localizeStrings()
        
        self.otpCodeView.layer.cornerRadius = self.otpCodeView.frame.size.height / 2
        self.newPasswordView.layer.cornerRadius = self.newPasswordView.frame.size.height / 2
        self.confirmNewPasswordView.layer.cornerRadius = self.confirmNewPasswordView.frame.size.height / 2
        self.submitButtonOutlet.layer.cornerRadius = self.submitButtonOutlet.frame.size.height / 2
        self.resendButton.layer.cornerRadius = self.resendButton.frame.size.height / 2
        
        self.PhoneVerify()

        self.resendButton.isUserInteractionEnabled = false
        self.resendView.layer.cornerRadius = self.resendView.frame.size.height / 2
        self.resendStaticLbl.isHidden = true
        
        self.startTimer()
    }
    
    
    
    
    // MARK:- // Buttons
    
    
    var counter = 150
    var resendTimer : Timer?
    
    func startTimer() {
        if resendTimer == nil {
            resendTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
            
        }
        
    }
    
    func stopTimer() {
        if resendTimer != nil {
            self.counter = 150
            resendTimer!.invalidate()
            resendTimer = nil
        }
    }
    
    // MARK:- // Resend Button
    
    @IBOutlet weak var resendButton: UIButton!
    @IBAction func resend(_ sender: UIButton) {
        
        self.resendButton.isUserInteractionEnabled = false
        
        self.PhoneVerify()
        
        self.startTimer()
        
    }
    
    // MARK:- // Update Counter
    
    @objc func updateCounter() {
        
        //example functionality
        if counter > 0 {
            self.resendStaticLbl.isHidden = false
            self.resendButton.titleLabel!.font = UIFont(name: self.resendButton.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
            self.resendCounterLbl.text = "\(counter)"
            self.resendButton.setTitleColor(.white, for: .normal)
            self.resendView.backgroundColor = .lightGray
            print("\(counter) seconds to the end of the world")
            counter -= 1
        }
        else if counter == 0 {
            self.resendButton.titleLabel!.font = UIFont(name: self.resendButton.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
            self.resendCounterLbl.text = "Resend"
            self.resendButton.setTitleColor(.white, for: .normal)
            self.resendView.backgroundColor = .orange
            self.resendButton.isUserInteractionEnabled = true
            self.resendStaticLbl.isHidden = true
            self.stopTimer()
            
            
        }
    }
    
    // MARK:- // Back Button
    
    @IBOutlet weak var backButtonOutlet: UIButton!
    @IBAction func backButton(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    
    // MARK:- // Submit Button
    
    @IBOutlet weak var submitButtonOutlet: UIButton!
    @IBAction func submitButton(_ sender: UIButton) {
        
        if self.OTPVerified {
            
            if (self.newPasswordTXT.text?.elementsEqual(""))! {
                self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "resetpass_new_pass_placeholder"))
            }
            else if globalFunctions.shared.validatePassword(enteredPassword: self.newPasswordTXT.text!) == false {
                self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "plese_enter_atlst"))
            }
            else if (self.confirmNewPasswordTXT.text?.elementsEqual(""))! {
                self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "resetpass_confirm_pass_placeholder"))
            }
                //        else if globalFunctions.shared.validatePassword(enteredPassword: self.confirmNewPasswordTXT.text!) == false {
                //            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "plese_enter_atlst"))
                //        }
            else {
                if self.confirmNewPasswordTXT.text!.elementsEqual(self.newPasswordTXT.text!) == false {
                    self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "android_nwpswrd_sm_cnfrm"))
                }
                else {
                    
                    self.resetPassword()
                    
                    
                }
                
            }
            
            
        }
        else {
            if (self.otpCodeTXT.text?.elementsEqual(""))! {
                self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "resetpass_otp_placeholder"))
            }
            else {
                
                
                let credential = PhoneAuthProvider.provider().credential(
                    withVerificationID: verificationID, verificationCode: "\(self.otpCodeTXT.text ?? "")")
                
                Auth.auth().signIn(with: credential) { (authResult, error) in
                    if let error = error {
                        self.ShowAlertMessage(title: "Alert", message: "Something went wrong! Try again later")
                        // ...
                        return
                    }
                    // User is signed in
                    // ...
                    self.OTPVerified = true
                    
                    self.successMessageView.isHidden = true
                    self.otpCodeView.isHidden = true
                    self.newPasswordView.isHidden = false
                    self.confirmNewPasswordView.isHidden = false
                    self.resendButton.isHidden = true
                    self.resendView.isHidden = true
                    
                }
                
            }
        }
        
        
        
        
        
        
    }
    
    
    
    
    // MARK:- // JSON Post Method to Reset Password
    
    func resetPassword() {
        
        var parameters : String = ""
        
        let userPhoneNumber = self.phoneCode + self.phoneNumber
        
        parameters = "number=\(userPhoneNumber)&password=\(newPasswordTXT.text ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        //&otp_code=\(otpCodeTXT.text ?? "")
        
        self.CallAPI(urlString: "resetpassword", param: parameters) {
            
            self.resetPasswordResponseDictionary = self.globalJson
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
                self.customAlert(title: "", message: "\(self.globalJson["message"] ?? "")", doesCancel: false, completion: {
                    
                    let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                    
                    self.navigationController?.pushViewController(navigate, animated: true)
                    
                })
                
            }
            
        }
        
        
    }
    
    
    
    // MARK:- // Verify Phone Number
    
    func PhoneVerify()
    {
        print("number",("+\(self.phoneCode ?? "")" + "\(self.phoneNumber ?? "")"))
        PhoneAuthProvider.provider().verifyPhoneNumber(("+\(self.phoneCode ?? "")" + "\(self.phoneNumber ?? "")"), uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print(error.localizedDescription)
                //self.showMessagePrompt(error.localizedDescription)
                return
            }
            // Sign in using the verificationID and the code sent to the user
            // ...
            self.verificationID = verificationID
            print("veri",verificationID)
        }
        
    }
    
    
    
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.resetPasswordTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "resetpass_text1")
        self.resetPasswordSubtitleLBL.text = globalFunctions.shared.getLanguageString(variable: "resetpass_text2")
        self.successMessage.text = globalFunctions.shared.getLanguageString(variable: "otp_sent_msg")
        self.otpCodeTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "resetpass_otp_placeholder")
        self.newPasswordTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "resetpass_new_pass_placeholder")
        self.confirmNewPasswordTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "resetpass_confirm_pass_placeholder")
        self.submitButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "forgot_password_submit"), for: .normal)
        //self.resendButton.setTitle("RESEND", for: .normal)
        
        self.resendCounterLbl.text = "Resend"
        self.resendStaticLbl.text = "\(globalFunctions.shared.getLanguageString(variable: "send_in_again")) :"
        
    }
    
    
    // MARK:- // Set Font
    
    func SetFont() {
        
        self.resendStaticLbl.font = UIFont(name: self.resendStaticLbl.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        self.resendCounterLbl.font = UIFont(name: self.resendCounterLbl.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        
        
        self.resetPasswordTitleLBL.font = UIFont(name: self.resetPasswordTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        self.resetPasswordSubtitleLBL.font = UIFont(name: self.resetPasswordSubtitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.successMessage.font = UIFont(name: self.successMessage.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.otpCodeTXT.font = UIFont(name: self.otpCodeTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.newPasswordTXT.font = UIFont(name: self.newPasswordTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.confirmNewPasswordTXT.font = UIFont(name: self.confirmNewPasswordTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.submitButtonOutlet.titleLabel!.font = UIFont(name: self.submitButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.resendButton.titleLabel!.font = UIFont(name: self.resendButton.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        
    }
    

}
