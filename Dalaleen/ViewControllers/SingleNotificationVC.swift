//
//  SingleNotificationVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 13/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView

class SingleNotificationVC: GlobalViewController {

    var Message : singleMessageDataFormat!
    
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var messageIconImageview: UIImageView!
    @IBOutlet weak var youReceivedMessageStaticLBL: UILabel!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var messageDate: UILabel!
    @IBOutlet weak var messageTime: UILabel!
    @IBOutlet weak var withFollowingDetailsStaticLBL: UILabel!
    @IBOutlet weak var aboutRealEstateStaticLBL: UILabel!
    @IBOutlet weak var realEstateNumber: UILabel!
    @IBOutlet weak var messageSender: UILabel!
    @IBOutlet weak var messageBodyTItleLBL: UILabel!
    @IBOutlet weak var messageBody: UILabel!
    @IBOutlet weak var sendToRecycleBinView: UIView!
    @IBOutlet weak var recycleBinImageview: UIImageView!
    @IBOutlet weak var sendToRecyclebinLBL: UILabel!
    @IBOutlet weak var separatorView: UIView!
    
    var fromRecycleBin : Bool! = false
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.fromRecycleBin == false {
            self.sendToRecycleBinView.isHidden = false
        }
        else {
            self.sendToRecycleBinView.isHidden = true
        }
        

        self.localizeStrings()
        
        self.SetFont()
        
        if "\(self.Message.readStatus ?? "")".elementsEqual("0") {
            self.makeMessageRead()
        }
        
        self.populateFieldsWithData()
        
    }
    
    
    
    
    // MARK:- // Buttons
    
    // MARK:- // Send To Recycle Bin
    
    @IBOutlet weak var sendToRecycleBinButtonOutlet: UIButton!
    @IBAction func sendToRecycleBinButton(_ sender: UIButton) {
        
        self.customAlert(title: globalFunctions.shared.getLanguageString(variable: "ios_are_you_sure"), message: globalFunctions.shared.getLanguageString(variable: "app_send_recycle") + " ?", doesCancel: true) {
            
            self.sendToRecycleBin()
            
            self.globalDispatchgroup.notify(queue: .main) {
                
                self.navigationController?.popViewController(animated: true)
                
                
            }
            
        }
        
    }
    
    
    
    
    // MARK:- // Populate Fields with Data
    
    func populateFieldsWithData() {
        
        self.senderName.text = "<<\(self.Message.name ?? "")>>"
        self.messageDate.text = "\(self.Message.date ?? "")"
        self.messageTime.text = "\(self.Message.time ?? "")"
        //self.realEstateNumber.text = "#\(self.Message.realEstateNumber ?? "")"
        self.messageSender.halfTextColorChange(fullText: "\(globalFunctions.shared.getLanguageString(variable: "app_msg_sender")) : \(self.Message.name ?? "")", changeText: "\(self.Message.name ?? "")", color: UIColor(red:38/255, green:39/255, blue:204/255, alpha: 1))
        print(self.Message.message ?? "")
        //self.messageBody.text = "\(self.Message.message ?? "")"
        self.messageBody.attributedText = "\(self.Message.message ?? "")".htmlToAttributedString
        let myString = self.messageBody.text
        let myAttribute = [NSAttributedString.Key.font: UIFont(name: self.messageBody.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))!]
        let myAttrString = NSAttributedString(string: myString!, attributes: myAttribute)
        self.messageBody.attributedText = myAttrString
        
    }
    
    
    
    // MARK:- // JSON Post Method to Make Message Read
    
    func makeMessageRead() {
        
        var parameters : String!
        
        parameters = "dalaleen_user_id=\(UserDefaults.standard.string(forKey: "userId") ?? "")&message_id=\(self.Message.id ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "message_read", param: parameters) {
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    
    
    // MARK:- // JSON Post Method to Send To Recycle Bin
    
    func sendToRecycleBin() {
        
        var parameters : String!
        
        parameters = "dalaleen_user_id=\(userInformation.shared.userID ?? "")&message_id=\(self.Message.id ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "message_delete", param: parameters) {
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.youReceivedMessageStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "app_receive_msg")
        self.withFollowingDetailsStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "ios_app_receive_msg_following_details")
        self.aboutRealEstateStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "app_noti") + " \(globalFunctions.shared.getLanguageString(variable: "with_the_following_details")) :" //globalFunctions.shared.getLanguageString(variable: "app_msg_real_estate")
        self.messageBodyTItleLBL.text = globalFunctions.shared.getLanguageString(variable: "app_msg_body") + "  :"
        self.sendToRecyclebinLBL.text = globalFunctions.shared.getLanguageString(variable: "app_send_recycle")
        
    }
    
    
    // MARK:- // Set Font
    
    func SetFont() {
        
        self.youReceivedMessageStaticLBL.font = UIFont(name: self.youReceivedMessageStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.senderName.font = UIFont(name: self.senderName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.messageDate.font = UIFont(name: self.messageDate.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 11)))
        self.messageTime.font = UIFont(name: self.messageTime.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 11)))
        self.withFollowingDetailsStaticLBL.font = UIFont(name: self.withFollowingDetailsStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.aboutRealEstateStaticLBL.font = UIFont(name: self.aboutRealEstateStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.realEstateNumber.font = UIFont(name: self.realEstateNumber.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.messageSender.font = UIFont(name: self.messageSender.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.messageBodyTItleLBL.font = UIFont(name: self.messageBodyTItleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.messageBody.font = UIFont(name: self.messageBody.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.sendToRecyclebinLBL.font = UIFont(name: self.sendToRecyclebinLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
    }
    

}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
