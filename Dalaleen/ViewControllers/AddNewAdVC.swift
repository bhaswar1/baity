//
//  AddNewAdVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 05/08/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import NVActivityIndicatorView
import GoogleMaps
import GooglePlaces
import StoreKit
import YouTubePlayer_Swift

class AddNewAdVC: GlobalViewController {
    
    var imageArray = [[UIImage]]()
    var cellIndexClicked : Int!

    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var mainStackview: UIStackView!
    @IBOutlet weak var realEstateTypeView: UIView!
    @IBOutlet weak var chooseCategoryTitleLBL: UILabel!
    @IBOutlet weak var realEstateTypeTitleLBL: UILabel!
    @IBOutlet weak var realEstateType: UILabel!
    @IBOutlet weak var subCategoryTitleLBL: UILabel!
    @IBOutlet weak var subCategory: UILabel!
    @IBOutlet weak var offerForView: UIView!
    @IBOutlet weak var offerForTitleLBL: UILabel!
    @IBOutlet weak var rentImageview: UIImageView!
    @IBOutlet weak var rentTitleLBL: UILabel!
    @IBOutlet weak var saleImageview: UIImageView!
    @IBOutlet weak var saleTitleLBL: UILabel!
    @IBOutlet weak var periodView: borderView!
    @IBOutlet weak var rentPeriod: UILabel!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var locationTitleLBL: UILabel!
    @IBOutlet weak var countryView: borderView!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var provinceView: borderView!
    @IBOutlet weak var province: UILabel!
    @IBOutlet weak var cityView: borderView!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var districtView: borderView!
    @IBOutlet weak var district: UILabel!
    @IBOutlet weak var nearestPointView: borderView!
    @IBOutlet weak var nearestPointTXT: UITextField!
    @IBOutlet weak var locationMapView: GMSMapView!
    @IBOutlet weak var fullScreenMapView: GMSMapView!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceTitleLBL: UILabel!
    @IBOutlet weak var priceTXT: UITextField!
    @IBOutlet weak var currency: UILabel!
    @IBOutlet weak var negotiablePriceView: UIView!
    @IBOutlet weak var negotiablePriceTitleLBL: UILabel!
    @IBOutlet weak var negotiablePriceYesImageview: UIImageView!
    @IBOutlet weak var negotiablePriceYesLBL: UILabel!
    @IBOutlet weak var negotiablepriceNoImageview: UIImageView!
    @IBOutlet weak var negotiablePriceNoLBL: UILabel!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var titleTXT: UITextField!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var descriptionTitleLBL: UILabel!
    @IBOutlet weak var propertyDescription: UITextView!
    @IBOutlet weak var uploadImagesView: UIView!
    @IBOutlet weak var uploadImagesTitleLBL: UILabel!
    @IBOutlet weak var uploadImagesCollectionview: UICollectionView!
    @IBOutlet weak var deleteSelectedView: UIView!
    @IBOutlet weak var deleteSelectedTitleLBL: UILabel!
    @IBOutlet weak var youtubeLinkView: borderView!
    @IBOutlet weak var youtubeLinkTXT: UITextField!
    
    @IBOutlet weak var viewOfPickerview: UIView!
    @IBOutlet weak var realEstateTypePickerview: UIPickerView!
    @IBOutlet weak var realEstateSubcategoryPickerview: UIPickerView!
    @IBOutlet weak var countryPickerview: UIPickerView!
    @IBOutlet weak var provincePickerview: UIPickerView!
    @IBOutlet weak var cityPickerview: UIPickerView!
    @IBOutlet weak var districtPickerview: UIPickerView!
    @IBOutlet weak var currencyPickerview: UIPickerView!
    @IBOutlet weak var planDurationPickerview: UIPickerView!
    @IBOutlet weak var periodPickerview: UIPickerView!
    @IBOutlet weak var viewOfCollectionviewHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var adsCostView: UIView!
    @IBOutlet weak var adsCostTitleLBL: UILabel!
    @IBOutlet weak var adsCostTableview: UITableView!
    
    @IBOutlet weak var addRealEStateButtonView: UIView!
    @IBOutlet weak var addRealEstateImageview: UIImageView!
    @IBOutlet weak var addRealEstateTitleLBL: UILabel!
    
    @IBOutlet weak var localityView: borderView!
    @IBOutlet weak var localityTXT: UITextField!
    @IBOutlet weak var businessVideo: YouTubePlayerView!
    
    
    
    var userRealEstateTypeArray = [filterDataFormat]()
    var subCategoryArray = [filterCategoriesDataFormat]()
    
    var offerFor : String! = ""
    
    var countryArray = [countryAndPhoneCodes]()
    
    var provinceArray = [NSDictionary]()
    
    var cityArray = [NSDictionary]()
    
    var districtArray = [NSDictionary]()
    
    var countryID : String! = ""
    var provinceID : String! = ""
    var cityID  : String! = ""
    var districtID : String! = ""
    
    var planIdArray = ["com.esolz.Dalaleen.gold30","com.esolz.Dalaleen.gold180","com.esolz.Dalaleen.gold365","com.esolz.Dalaleen.urgent30","com.esolz.Dalaleen.urgent180","com.esolz.Dalaleen.urgent365"]
    
    
    var periodArray = [["key" : "W" , "value" : "\(globalFunctions.shared.getLanguageString(variable: "home_week"))"] , ["key" : "M" , "value" : "\(globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_months"))"] , ["key" : "Y" , "value" : "\(globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_years"))"]]
    var periodID : String! = ""
    
    var currencyArray = [["key" : "1" , "value" : "IQD"] , ["key" : "2" , "value" : "USD"]]
    var currencyID : String! = ""
    
    var negotiablePrice : String! = ""
    
    
    var webImagesArray = [NSDictionary]()
    var localImagesArray = [UIImage]()
    
    var deletedImageString : String! = ""
    var localImagesToDeleteArray = [Int]()
    var webImagesToDeleteArray = [Int]()
    
    var longPressGesture = UILongPressGestureRecognizer()
    
    var dynamicInfoDictionary : NSDictionary!
    
    var dynamicInfoFeaturesDataArray = [addPropertyDynamicDataFormat]()
    
    var dynamicInfoStatusDataArray = [addPropertyDynamicDataFormat]()
    
    let dynamicStackView : dynamicStackview = {
       let dynamicstackview = dynamicStackview()
        dynamicstackview.translatesAutoresizingMaskIntoConstraints = false
        return dynamicstackview
    }()
    
    
    var pickerviewArray = [UIPickerView]()
    var pickerLBLArray = [UILabel]()
    
    var textfieldsArray = [UITextField]()
    
    
    
    var categoryNameID : String!
    var categoryName : String!
    var categoryTypeID : String!
    var CategoryType : String!
    
    var isEditingProperty : Bool! = false
    
    var propertyID : String! = ""
    
    var propertyEditDataDictionary : NSDictionary!
    
    var planDataArray = [planTypeDataFormat]()
    
    var planDurationArray = [planDurationDataForamt]()
    
    var planPriceID : String! = ""
    
    var subPlanId : String! = "999"
    
    
    var validated : Bool! = false
    
    var locationManager = CLLocationManager()
    
    var chosenPlace: MyPlace?
    
    var userLat : String!
    var userLong : String!
    
    
    var featureHeaderArray = [UILabel]()
    var statusHeaderArray = [UILabel]()
    
    
    
    
    
    
    var selectedPickerview : UIPickerView!
    var selectedPickerIndex : Int!
    var doneClicked : Bool!
    
    
    
    // MARK:- // Create Picker Toolbar
    
    func createToolbar() {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.tintColor = UIColor.white//"Done" button colour
        toolBar.barTintColor = UIColor.black// bar background colour
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "\(globalFunctions.shared.getLanguageString(variable: "ios_ok"))", style: UIBarButtonItem.Style.done, target: self, action: #selector(donePicker))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "\(globalFunctions.shared.getLanguageString(variable: "modal_button_text1"))", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelPicker))
        
        toolBar.setItems([cancelButton, spacer, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.viewOfPickerview.addSubview(toolBar)
        toolBar.anchor(top: nil, leading: nil, bottom: self.provincePickerview.topAnchor, trailing: nil, size: .init(width: UIScreen.main.bounds.size.width, height: 40))
        
    }
    
    
    // MARK:- // Done Picker Action Method
    
    @objc func donePicker() {
        
        print("Done Clicked")
        
        self.doneClicked = true
        
        self.pickerView(self.selectedPickerview, didSelectRow: self.selectedPickerIndex, inComponent: 0)
        
        self.view.sendSubviewToBack(self.viewOfPickerview)
        self.viewOfPickerview.isHidden = true
        self.selectedPickerview.isHidden = true
        
    }
    
    // MARK:- // Cancel Picker Action Method
    
    @objc func cancelPicker() {
        print("Cancel Clicked")
        
        self.view.sendSubviewToBack(self.viewOfPickerview)
        self.viewOfPickerview.isHidden = true
        self.selectedPickerview.isHidden = true
    }
    
    
    //MARK:- In app-purchase variabel
    var subscriptionContent : SubscriptionDataGroup!
    
    var products: [SKProduct] = []
    
    var purchaseIndex : Int!
    
    var currencyDefaultTxt: String!
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for transactionPending in SKPaymentQueue.default().transactions {
            SKPaymentQueue.default().finishTransaction(transactionPending)
        }
        
        if self.propertyID.elementsEqual("") {
            
            
            self.creaeteSubscriptionFunc()
            
            self.newProperty()
            
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "left_side_add_real_estate")
        }
        else {
            //self.editProperty()
            globalFunctions.shared.loadPlaceholder(onView: self.view)
            self.editPropertyGetValue()
            print(periodArray)
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "app_edit_property")
        }
        
        self.localizeStrings()
        
        self.setFont()
        
        locationManager.requestWhenInUseAuthorization()
        
        self.localityTXT.delegate = self
        
        self.propertyDescription.delegate = self
        
        self.priceTXT.delegate = self
        
        self.locationMapView.delegate = self
        
        
        self.titleTXT.delegate = self
        
        self.youtubeLinkTXT.delegate = self
        
        self.propertyDescription.delegate = self
        self.youtubeLinkTXT.isUserInteractionEnabled = true
        
        
        
        
        self.longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(sender:)))
        self.uploadImagesCollectionview.addGestureRecognizer(self.longPressGesture)
        

        //Creat Toolbar
        self.createToolbar()
        
        
        
    }
    
    
    
    // MARK:- // New Property
    
    func newProperty() {
        
        self.loadRealEstateTypes()
        
        //self.getCountryList()
        self.countryID = "102"
        self.getProvinceList(countryID: "\(self.countryID ?? "")")
        
        self.currencyPickerview.delegate = self
        self.currencyPickerview.dataSource = self
        self.currencyPickerview.reloadAllComponents()
        
        self.periodPickerview.delegate = self
        self.periodPickerview.dataSource = self
        self.periodPickerview.reloadAllComponents()
        
        self.uploadImagesCollectionview.delegate = self
        self.uploadImagesCollectionview.dataSource = self
        self.uploadImagesCollectionview.reloadData()
        
        self.mainStackview.addArrangedSubview(self.dynamicStackView)
        
        self.getPlanData()
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            self.planDurationArray = self.planDataArray[0].planPriceList
            
            self.planDataArray[0].isSelected = true
            
            self.planDataArray[0].planDuration = "\(self.planDurationArray[0].planDuration ?? "")"  + " \(self.planDataArray[0].planValidUnit ?? "")"
            self.planDataArray[0].planCost = "=\(self.planDurationArray[0].planCost ?? "")$"
            self.planDataArray[0].planPriceID = "\(self.planDurationArray[0].planPriceID ?? "")"
            
            self.adsCostTableview.delegate = self
            self.adsCostTableview.dataSource = self
            self.adsCostTableview.reloadData()
            
            //self.planDataArray[0].isSelected = true
            
        }
        
      
        
    }
    
    
    
    // MARK:- // Edit Property
    
    func editProperty() {
        
        //globalFunctions.shared.loadPlaceholder(onView: self.view)
        
        //self.editPropertyGetValue()
        
 //       self.globalDispatchgroup.notify(queue: .main) {
            
            
            self.realEstateType.text = "\(self.categoryName ?? "")"
            self.subCategory.text = "\(self.CategoryType ?? "")"
            
            self.realEstateTypeButtonOutlet.isUserInteractionEnabled = false
            self.subCategoryButtonOutlet.isUserInteractionEnabled = false
            
            
            
            // OFFER FOR
            if "\(self.propertyEditDataDictionary["category_sub"] ?? "")".elementsEqual("2") {
                self.rentImageview.image = UIImage(named: "checkbox2")
                self.saleImageview.image = UIImage(named: "Rectangle 22")
                
                self.offerFor = "2"
                
                self.periodView.isHidden = false
                
                for i in 0..<self.periodArray.count {
                    if "\(self.propertyEditDataDictionary["period"] ?? "")".elementsEqual("\((self.periodArray[i])["key"] ?? "")") {
                        self.rentPeriod.text = "\((self.periodArray[i])["value"] ?? "")"
                        self.periodID = "\((self.periodArray[i])["key"] ?? "")"
                    }
                }
            }
            else {
                self.rentImageview.image = UIImage(named: "Rectangle 22")
                self.saleImageview.image = UIImage(named: "checkbox2")
                
                self.offerFor = "1"
                
                self.periodView.isHidden = true
            }
            
            self.periodPickerview.delegate = self
            self.periodPickerview.dataSource = self
            self.periodPickerview.reloadAllComponents()
            
            
            
            // ADDRESS
            
            self.localityTXT.text = "\(self.propertyEditDataDictionary["address"] ?? "")"
            self.nearestPointTXT.text = "\(self.propertyEditDataDictionary["nearby_location"] ?? "")"
            
            self.userLat = "\(self.propertyEditDataDictionary["latitude"] ?? "00.00")"
            self.userLong = "\(self.propertyEditDataDictionary["longitude"] ?? "00.00")"
            
            self.showLocation(addressLat: self.userLat, addressLong: self.userLong)
            
            
            self.getCountryList()
            
            self.globalDispatchgroup.notify(queue: .main) {
                
                for i in 0..<self.countryArray.count {
                    
                    if "\(self.propertyEditDataDictionary["country_id"] ?? "")".elementsEqual("\(self.countryArray[i].id ?? "")") {
                        self.country.text = "\(self.countryArray[i].countryName ?? "")"
                        self.countryID = "\(self.countryArray[i].id ?? "")"
                        break
                    }
                    
                }
                
                self.getProvinceList(countryID: self.countryID!)
                
                self.globalDispatchgroup.notify(queue: .main, execute: {
                    
                    for i in 0..<self.provinceArray.count {
                        
                        if "\(self.propertyEditDataDictionary["province_id"] ?? "")".elementsEqual("\((self.provinceArray[i])["id"] ?? "")") {
                            self.province.text = "\((self.provinceArray[i])["title_eng"] ?? "")"
                            self.provinceID = "\((self.provinceArray[i])["id"] ?? "")"
                            break
                        }
                    }
                    
                    self.getCityList(provinceID: self.provinceID!)
                    
                    self.globalDispatchgroup.notify(queue: .main, execute: {
                        
                        for i in 0..<self.cityArray.count {
                            
                            if "\(self.propertyEditDataDictionary["city_id"] ?? "")".elementsEqual("\((self.cityArray[i])["id"] ?? "")") {
                                self.city.text = "\((self.cityArray[i])["cityname"] ?? "")"
                                self.cityID = "\((self.cityArray[i])["id"] ?? "")"
                                break
                            }
                        }
                        
                        self.getDistrictList(cityID: self.cityID)
                        
                        self.globalDispatchgroup.notify(queue: .main, execute: {
                            
                            for i in 0..<self.districtArray.count {
                                
                                if "\(self.propertyEditDataDictionary["district_id"] ?? "")".elementsEqual("\((self.districtArray[i])["id"] ?? "")") {
                                    
                                    self.district.text = "\((self.districtArray[i])["districtname"] ?? "")"
                                    self.districtID = "\((self.districtArray[i])["id"] ?? "")"
                                    break
                                }
                            }
                            
                            
                        })
                        
                    })
                    
                })
                
                
                
                // PRICE
                self.priceTXT.text = "\(self.propertyEditDataDictionary["other_price"] ?? "")"
                for i in 0..<self.currencyArray.count {
                    
                    if "\(self.propertyEditDataDictionary["currency"] ?? "")".elementsEqual("\((self.currencyArray[i])["key"] ?? "")") {
                        self.currency.text = "\((self.currencyArray[i])["value"] ?? "")"
                        self.currencyID = "\((self.currencyArray[i])["key"] ?? "")"
                        break
                    }
                }
                
                self.currencyPickerview.delegate = self
                self.currencyPickerview.dataSource = self
                self.currencyPickerview.reloadAllComponents()
                
                
                // NEGOTIABLE PRICE
                if "\(self.propertyEditDataDictionary["negotiable_price"] ?? "")".elementsEqual("Y") {
                    self.negotiablePriceYesImageview.image = UIImage(named: "checkbox2")
                    self.negotiablepriceNoImageview.image = UIImage(named: "Rectangle 22")
                    
                    self.negotiablePrice = "Y"
                }
                else {
                    self.negotiablePriceYesImageview.image = UIImage(named: "Rectangle 22")
                    self.negotiablepriceNoImageview.image = UIImage(named: "checkbox2")
                    
                    self.negotiablePrice = "N"
                }
                
                
                // TITLE
                self.titleTXT.text = "\(self.propertyEditDataDictionary["title"] ?? "")"
                
                // DESCRIPTION
                self.propertyDescription.text = "\(self.propertyEditDataDictionary["description"] ?? "")"
                self.propertyDescription.textColor = .black
                
                // UPLOAD IMAGES
                self.webImagesArray = self.propertyEditDataDictionary["property_image"] as! [NSDictionary]
                self.uploadImagesCollectionview.delegate = self
                self.uploadImagesCollectionview.dataSource = self
                self.uploadImagesCollectionview.reloadData()
                
                // Youtube Video
                self.youtubeLinkTXT.text = "\(self.propertyEditDataDictionary["youtube_url"] ?? "")"
                if let myVideoURL = NSURL(string: self.youtubeLinkTXT.text ?? ""){
                    self.businessVideo.loadVideoURL(myVideoURL as URL)
                }
            
                
                // DYNAMIC SECTION
                self.mainStackview.addArrangedSubview(self.dynamicStackView)
                
                self.getDynamicFieldsData(propertyType: "\(self.categoryTypeID ?? "")")
                
                self.globalDispatchgroup.notify(queue: .main) {
                    
                    self.createDataDictionaries()
                    
                    self.editDataDictionary()
                    
                    self.createFeaturesStackview()
                    
                    self.createStatusStackview()
                    
                }
            }
            
            globalFunctions.shared.removePlaceholder(fromView: self.view)
            
            
            self.adsCostView.isHidden = true
            
            self.addRealEstateImageview.isHidden = true
            
            self.addRealEstateTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_save")
            self.addRealEstateTitleLBL.textAlignment = .center
      //  }
    }
    
    // MARK:- // Edit Data Dictionary
    
    func editDataDictionary() {
        
        for i in 0..<self.dynamicInfoFeaturesDataArray.count {
            
            //print("FIELD ID ------------\(self.dynamicInfoFeaturesDataArray[i].fieldID ?? "")")
            //print("FIELD TYPE ID ------------\(self.dynamicInfoFeaturesDataArray[i].fieldTypeID ?? "")")
            
            self.dynamicInfoFeaturesDataArray[i].value = "\(((self.propertyEditDataDictionary["features"] as! NSArray)[i] as! NSDictionary)["value"] ?? "")"
            
            let tempArray = ("\(((self.propertyEditDataDictionary["features"] as! NSArray)[i] as! NSDictionary)["value"] ?? "")").components(separatedBy: ",")

            for j in 0..<self.dynamicInfoFeaturesDataArray[i].fieldOptionArray.count {
                
                for k in 0..<tempArray.count {
                    if "\(self.dynamicInfoFeaturesDataArray[i].fieldOptionArray[j].key ?? "")".elementsEqual("\(tempArray[k])") {
                        self.dynamicInfoFeaturesDataArray[i].fieldOptionArray[j].isSelected = true
                        
                    }
                }
                
                
                // for selectbox
                if (self.dynamicInfoFeaturesDataArray[i].fieldTypeID?.elementsEqual("3"))! {
                    
                    for t in 0..<self.dynamicInfoFeaturesDataArray[i].fieldOptionArray
                        .count {
                            
                            if (self.dynamicInfoFeaturesDataArray[i].value?.elementsEqual("\(self.dynamicInfoFeaturesDataArray[i].fieldOptionArray[t].key ?? "")"))! {
                                
                                self.dynamicInfoFeaturesDataArray[i].value = "\(self.dynamicInfoFeaturesDataArray[i].fieldOptionArray[t].value ?? "")"
                            }
                    }
                }
            }



        }
        
        for i in 0..<self.dynamicInfoStatusDataArray.count {
            
            self.dynamicInfoStatusDataArray[i].value = "\(((self.propertyEditDataDictionary["status"] as! NSArray)[i] as! NSDictionary)["value"] ?? "")"
            
            let tempArray = ("\(((self.propertyEditDataDictionary["status"] as! NSArray)[i] as! NSDictionary)["value"] ?? "")").components(separatedBy: ",")

            for j in 0..<self.dynamicInfoStatusDataArray[i].fieldOptionArray.count {
                
                for k in 0..<tempArray.count {
                    if "\(self.dynamicInfoStatusDataArray[i].fieldOptionArray[j].key ?? "")".elementsEqual("\(tempArray[k])") {
                        self.dynamicInfoStatusDataArray[i].fieldOptionArray[j].isSelected = true
                        
                    }
                }
                
                // for selectbox
                if (self.dynamicInfoStatusDataArray[i].fieldTypeID?.elementsEqual("3"))! {
                    
                    for t in 0..<self.dynamicInfoStatusDataArray[i].fieldOptionArray
                        .count {
                            
                            if (self.dynamicInfoStatusDataArray[i].value?.elementsEqual("\(self.dynamicInfoStatusDataArray[i].fieldOptionArray[t].key ?? "")"))! {
                                
                                self.dynamicInfoStatusDataArray[i].value = "\(self.dynamicInfoStatusDataArray[i].fieldOptionArray[t].value ?? "")"
                            }
                    }
                }
            }

        }
    
    }
    
    
    
    
    // MARK:- // Buttons
    
    
    // MARK:- // Add Real Estate Button
    
    @IBOutlet weak var addRealEstateButtonOutlet: UIButton!
    @IBAction func addRealEstateButton(_ sender: UIButton) {
        
//        self.purchaseItemIndex(index: 5) {
//
//            print("HI")
//
//        }
        
        self.saveRealEstate()
        
    }
    
    //MARK:- //Create subscription function
    func creaeteSubscriptionFunc(){
        // Creating Subscription Data
        //self.createSubscriptionData()
        
        // Rest of the work
        SubscriptionProducts.store.requestProducts { [weak self] success, products in
            guard let self = self else { return }
            guard success else {
                let alertController = UIAlertController(title: "Failed to load list of products",
                                                        message: "Check logs for details",
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .default))
                self.present(alertController, animated: true, completion: nil)
                return
            }
            self.products = products!
        }
        
        if SubscriptionProducts.store.isProductPurchased(SubscriptionProducts.oneMonthSubUrgent) {
            self.displaySubscriptionDetails(index: 0)
        }
        else if SubscriptionProducts.store.isProductPurchased(SubscriptionProducts.sixMonthsSubUrgent) {
            self.displaySubscriptionDetails(index: 1)
        }
        else if SubscriptionProducts.store.isProductPurchased(SubscriptionProducts.oneYearSubUrgent) {
            self.displaySubscriptionDetails(index: 2)
        }
        else if SubscriptionProducts.store.isProductPurchased(SubscriptionProducts.oneMonthSubGold) {
                self.displaySubscriptionDetails(index: 3)
        }
        else if SubscriptionProducts.store.isProductPurchased(SubscriptionProducts.sixMonthsSubGold) {
            self.displaySubscriptionDetails(index: 4)
        }
        else if SubscriptionProducts.store.isProductPurchased(SubscriptionProducts.oneYearSubGold) {
            self.displaySubscriptionDetails(index: 5)
        }
        else {
            self.displaySubscriptionAlert()
        }
    }
    
    // MARK:- // Create Subscription Data
    
    func createSubscriptionData() {
        
        self.subscriptionContent = SubscriptionDataGroup(subscriptionStrings: ["You have subscribed for 1 month.","You have subscribed for 1 year.","You have subscribed for 6 months."])
        
    }
    
    
    // MARK:- // Display Subscription Alert.
    
    func displaySubscriptionAlert() {
        
        //self.mainLBL.text = "You have not purchased any subscription yet."
        
    }
    
    // MARK:- // Display Subscription Details
    
    func displaySubscriptionDetails(index : Int) {
        
        //self.mainLBL.text = "\(self.subscriptionContent.subscriptionStrings[index])"
        
    }
    
    
    // MARK:- // Purchase Item Index
    
    func purchaseItemIndex(index: Int , completion : @escaping ()->()) {
        startAnimating(CGSize(width: ((30.0/320.0)*FullWidth), height: ((30.0/320.0)*FullWidth)), message: "", messageFont: UIFont(name: "Lato-Bold", size: 14), type: .ballClipRotateMultiple, color: UIColor(displayP3Red: 0/255, green: 135/255, blue: 228/255, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.4), textColor: .black, fadeInAnimation: nil)
        
        //print(index)
        print(products[index].productIdentifier)
        //print(indx)
        
        DispatchQueue.main.async {
            SubscriptionProducts.store.buyProduct(self.products[index]) { [weak self] success, productId in
                guard let self = self else { return }
                guard success else {
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    let alertController = UIAlertController(title: "Failed to purchase product",
                                                            message: "Check logs for details",
                                                            preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(alertController, animated: true, completion: nil)
                    return
                }
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                completion()
                
            }
        }
        
        
    }
    
    
    
    // MARK:- // Real Estate Type Button
    
    @IBOutlet weak var realEstateTypeButtonOutlet: UIButton!
    @IBAction func realEstateTypeButton(_ sender: Any) {
        
        if self.userRealEstateTypeArray.count == 0 {
            
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "please_select_type"))
        }
        else {
            
            self.view.bringSubviewToFront(self.viewOfPickerview)
            self.viewOfPickerview.isHidden = false
            self.realEstateTypePickerview.isHidden = false
            
            self.realEstateTypePickerview.selectRow(0, inComponent: 0, animated: false)
            
            self.selectedPickerview = self.realEstateTypePickerview
            self.selectedPickerIndex = 0
            self.doneClicked = false
        }
        
        self.closeAllTextfields(mainview: self.view)
        
    }
    
    
    // MARK:- // Real Estate Sub Category Button
    
    @IBOutlet weak var subCategoryButtonOutlet: UIButton!
    @IBAction func subCategoryButton(_ sender: UIButton) {
        
        if self.subCategoryArray.count == 0 {
            
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "please_select_subcategory_type"))
        }
        else {
            
            self.view.bringSubviewToFront(self.viewOfPickerview)
            self.viewOfPickerview.isHidden = false
            self.realEstateSubcategoryPickerview.isHidden = false
            
            self.realEstateSubcategoryPickerview.selectRow(0, inComponent: 0, animated: false)
            
            self.selectedPickerview = self.realEstateSubcategoryPickerview
            self.selectedPickerIndex = 0
            self.doneClicked = false
        }
        
        self.closeAllTextfields(mainview: self.view)
        
    }
    
    
    // MARK:- // Offer For Buttons
    
    @IBOutlet weak var rentButtonOutlet: UIButton!
    @IBOutlet weak var saleButtonOutlet: UIButton!
    @IBAction func offerForButtonActions(_ sender: UIButton) {
        
        if sender.tag == 2 {
            self.rentImageview.image = UIImage(named: "checkbox2")
            self.saleImageview.image = UIImage(named: "Rectangle 22")
            
            self.offerFor = "\(sender.tag)"
            
            self.periodView.isHidden = false
            
            self.rentPeriod.text = "\((self.periodArray[1])["value"] ?? "")"
            self.periodID = "\((self.periodArray[1])["key"] ?? "")"
        }
        else {
            self.rentImageview.image = UIImage(named: "Rectangle 22")
            self.saleImageview.image = UIImage(named: "checkbox2")
            
            self.offerFor = "\(sender.tag)"
            
            self.periodView.isHidden = true
            
            self.rentPeriod.text = "\((self.periodArray[1])["value"] ?? "")"
            self.periodID = ""
        }
        
    }
    
    
    
    
    // MARK:- // Period Button
    
    @IBOutlet weak var periodButtonOutlet: UIButton!
    @IBAction func periodButton(_ sender: UIButton) {
        
        if self.periodArray.count == 0 {
            
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "select_period"))
        }
        else {
            
            self.view.bringSubviewToFront(self.viewOfPickerview)
            self.viewOfPickerview.isHidden = false
            self.periodPickerview.isHidden = false
            
            self.periodPickerview.selectRow(0, inComponent: 0, animated: false)
            
            self.selectedPickerview = self.periodPickerview
            self.selectedPickerIndex = 0
            self.doneClicked = false
        }
        
        self.closeAllTextfields(mainview: self.view)
        
    }
    
    
    
    
    
    // MARK:- // Country Button
    
    @IBOutlet weak var countryButtonOutlet: UIButton!
    @IBAction func countryButton(_ sender: UIButton) {
        
        if self.countryArray.count == 0 {
            
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: "No Countries to be found.")
        }
        else {
            self.view.bringSubviewToFront(self.viewOfPickerview)
            self.viewOfPickerview.isHidden = false
            self.countryPickerview.isHidden = false
            
            self.countryPickerview.selectRow(0, inComponent: 0, animated: false)
            
            self.selectedPickerview = self.countryPickerview
            self.selectedPickerIndex = 0
            self.doneClicked = false
        }
        
        self.closeAllTextfields(mainview: self.view)
    }
    
    
    // MARK:- // Province Button
    
    @IBOutlet weak var provinceButtonOutlet: UIButton!
    @IBAction func provinceButton(_ sender: UIButton) {
        
        if self.provinceArray.count == 0 {
            
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "please_select_country"))
        }
        else {
            self.view.bringSubviewToFront(self.viewOfPickerview)
            self.viewOfPickerview.isHidden = false
            self.provincePickerview.isHidden = false
            
            self.provincePickerview.selectRow(0, inComponent: 0, animated: false)
            
            self.selectedPickerview = self.provincePickerview
            self.selectedPickerIndex = 0
            self.doneClicked = false
        }
        
        self.closeAllTextfields(mainview: self.view)
    }
    
    
    // MARK:- // City Button
    
    @IBOutlet weak var cityButtonOutlet: UIButton!
    @IBAction func cityButton(_ sender: UIButton) {
        
        if self.cityArray.count == 0 {
            
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "please_select_province"))
        }
        else {
            self.view.bringSubviewToFront(self.viewOfPickerview)
            self.viewOfPickerview.isHidden = false
            self.cityPickerview.isHidden = false
            
            self.cityPickerview.selectRow(0, inComponent: 0, animated: false)
            
            self.selectedPickerview = self.cityPickerview
            self.selectedPickerIndex = 0
            self.doneClicked = false
        }
        
        self.closeAllTextfields(mainview: self.view)
    }
    
    
    
    // MARK:- // District Button
    
    @IBOutlet weak var districtButtonOutlet: UIButton!
    @IBAction func districtButton(_ sender: UIButton) {
        
        if self.districtArray.count == 0 {
            
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "home_select_city"))
        }
        else {
            self.view.bringSubviewToFront(self.viewOfPickerview)
            self.viewOfPickerview.isHidden = false
            self.districtPickerview.isHidden = false
            
            self.districtPickerview.selectRow(0, inComponent: 0, animated: false)
            
            self.selectedPickerview = self.districtPickerview
            self.selectedPickerIndex = 0
            self.doneClicked = false
        }
        
        self.closeAllTextfields(mainview: self.view)
    }
    
    
    
    // MARK:- // Currency Button
    
    @IBOutlet weak var currencyButtonOutlet: UIButton!
    @IBAction func currencyButton(_ sender: UIButton) {
        
        if self.currencyArray.count == 0 {
            
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: "No Currency to be found.")
        }
        else {
            self.view.bringSubviewToFront(self.viewOfPickerview)
            self.viewOfPickerview.isHidden = false
            self.currencyPickerview.isHidden = false
            
            self.currencyPickerview.selectRow(0, inComponent: 0, animated: false)
            
            self.selectedPickerview = self.currencyPickerview
            self.selectedPickerIndex = 0
            self.doneClicked = false
        }
        
        self.closeAllTextfields(mainview: self.view)
        
    }
    
    
    // MARK:- // Negotiable Price Buttons
    
    @IBOutlet weak var negotiablePriceYesButtonOutlet: UIButton!
    @IBOutlet weak var negotiablePriceNoButtonOutlet: UIButton!
    @IBAction func negotiablePriceButtonActions(_ sender: UIButton) {
        
        if sender.tag == 0 {
            self.negotiablePriceYesImageview.image = UIImage(named: "checkbox2")
            self.negotiablepriceNoImageview.image = UIImage(named: "Rectangle 22")
            
            self.negotiablePrice = "Y"
        }
        else {
            self.negotiablePriceYesImageview.image = UIImage(named: "Rectangle 22")
            self.negotiablepriceNoImageview.image = UIImage(named: "checkbox2")
            
            self.negotiablePrice = "N"
        }
        
    }
    
    
    
    // MARK:- // Delete Images Button
    
    @IBOutlet weak var deleteImagesButtonOutlet: UIButton!
    @IBAction func deleteImagesButton(_ sender: UIButton) {
        
        self.localImagesArray.remove(at: self.localImagesToDeleteArray)
        
        // MAKING Images to Delete String
        var deletedStringArray = [String]()
        for i in 0..<self.webImagesToDeleteArray.count {
            deletedStringArray.append("\((self.webImagesArray[self.webImagesToDeleteArray[i]])["image_id"] ?? "")")
        }
        
        self.deletedImageString = deletedStringArray.joined(separator: ",")
        
        self.webImagesArray.remove(at: self.webImagesToDeleteArray)
        
        self.webImagesToDeleteArray.removeAll()
        
        self.localImagesToDeleteArray.removeAll()
        
        self.uploadImagesCollectionview.reloadData()
        
        self.deleteSelectedView.isHidden = true
        
    }
    
    
    // MARK:- // Close Pickerview Button
    
    @IBOutlet weak var closePickerviewButtonOutlet: UIButton!
    @IBAction func closePickerviewButton(_ sender: UIButton) {
        
        self.view.sendSubviewToBack(self.viewOfPickerview)
        self.viewOfPickerview.isHidden = true
        
        self.realEstateTypePickerview.isHidden = true
        self.realEstateSubcategoryPickerview.isHidden = true
        self.countryPickerview.isHidden = true
        self.provincePickerview.isHidden = true
        self.cityPickerview.isHidden = true
        self.districtPickerview.isHidden = true
        self.currencyPickerview.isHidden = true
        self.planDurationPickerview.isHidden = true
        self.periodPickerview.isHidden = true
        
        for i in 0..<self.pickerviewArray.count {
            self.pickerviewArray[i].isHidden = true
        }
    }
    
    
    
    
    // MARK:- // JSON Post Method to Load Real Estate Types
    
    func loadRealEstateTypes() {
        
        let parameters = "language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "parent_categories_list", param: parameters) {
            
            var tempFilterCategoriesArray = [filterCategoriesDataFormat]()
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                
                for j in 0..<(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["sub_category"] as! NSArray).count {
                    
                    let tempDict = (((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["sub_category"] as! NSArray)[j] as! NSDictionary
                    
                    tempFilterCategoriesArray.append(filterCategoriesDataFormat(id: "\(tempDict["sub_category_id"] ?? "")", categoryName: "\(tempDict["sub_category_name"] ?? "")", isSelected: false))
                    
                }
                
                self.userRealEstateTypeArray.append(filterDataFormat(parentID: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["parent_category_id"] ?? "")", parentName: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["parent_category_name"] ?? "")", childArray: tempFilterCategoriesArray, isselected: false))
                
                tempFilterCategoriesArray.removeAll()
                
            }
            
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.realEstateTypePickerview.delegate = self
                self.realEstateTypePickerview.dataSource = self
                self.realEstateTypePickerview.reloadAllComponents()
                
                self.realEstateSubcategoryPickerview.delegate = self
                self.realEstateSubcategoryPickerview.dataSource = self
                self.realEstateSubcategoryPickerview.reloadAllComponents()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    
    
    // MARK:- // JSON Post Method to fetch Country List
    
    func getCountryList() {
        
        let parameters = "language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "country", param: parameters) {
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                
                let tempDict = (self.globalJson["info"] as! NSArray)[i] as! NSDictionary
                
                self.countryArray.append(countryAndPhoneCodes(countryFlag: "\(tempDict["counrty_flag"] ?? "")", countryName: "\(tempDict["country_name"] ?? "")", countryCode: "\(tempDict["country_code"] ?? "")", id: "\(tempDict["id"] ?? "")", languageCode: "\(tempDict["language_code"] ?? "")", phoneCode: "\(tempDict["phone_code"] ?? "")", appSearchStatus: "\(tempDict["appsearch_status"] ?? "")"))
            }
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.countryPickerview.delegate = self
                self.countryPickerview.dataSource = self
                self.countryPickerview.reloadAllComponents()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
        }
    }
    
    
    
    // MARK:- // JSON Post Method to fetch Province List
    
    func getProvinceList(countryID : String) {
        
        let parameters = "country_id=\(countryID)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "fetch_province_by_country", param: parameters) {
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                
                let tempDict = (self.globalJson["info"] as! NSArray)[i] as! NSDictionary
                
                self.provinceArray.append(tempDict)
                
            }
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.provincePickerview.delegate = self
                self.provincePickerview.dataSource = self
                self.provincePickerview.reloadAllComponents()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
        }
    }
    
    
    
    // MARK:- // JSON Post Method to get City List
    
    func getCityList(provinceID : String) {
        
        let parameters = "province_id=\(provinceID)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "fetch_city_by_province", param: parameters) {
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                
                let tempDict = (self.globalJson["info"] as! NSArray)[i] as! NSDictionary
                
                self.cityArray.append(tempDict)
                
            }
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.cityPickerview.delegate = self
                self.cityPickerview.dataSource = self
                self.cityPickerview.reloadAllComponents()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
        }
        
    }
    
    
    // MARK:- // JSON Post Method to fetch District List
    
    func getDistrictList(cityID : String) {
        
        let parameters = "city_id=\(cityID)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "fetch_district_by_city", param: parameters) {
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                
                let tempDict = (self.globalJson["info"] as! NSArray)[i] as! NSDictionary
                
                if i > 0 {
                    self.districtArray.append(tempDict)
                }
                
                
            }
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.districtPickerview.delegate = self
                self.districtPickerview.dataSource = self
                self.districtPickerview.reloadAllComponents()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
        }
        
    }
    
    
    
    
    // MARK:- // JSOn Post Method to get Dynamic Fields Data
    
    func getDynamicFieldsData(propertyType : String) {
        
        var parameters : String!
        
        parameters = "property_type=\(propertyType)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "service_form_field", param: parameters) {
            
            self.dynamicInfoDictionary = self.globalJson
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    
    
    // MARK:- // Edit Property Get Value
    
    func editPropertyGetValue() {
        
        var parameters : String!
        
        parameters = "property_id=\(self.propertyID ?? "")&dalaleen_user_id=\(userInformation.shared.userID ?? "")"
        
        self.CallAPI(urlString: "propertEditValue", param: parameters) {
            
            self.propertyEditDataDictionary = self.globalJson["info"] as? NSDictionary
            
            DispatchQueue.main.async {
                self.editProperty()
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    
    
    // MARK:- // JSON Post Method to get Plan Data
    
    func getPlanData() {
        
        var parameters : String!
        
        parameters = "type=new&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "propertyPlan", param: parameters) {
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                
                var tempArray = [planDurationDataForamt]()
                
                for j in 0..<(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["plan_price_list"] as! NSArray).count {
                    
                    let tempDictionary = (((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["plan_price_list"] as! NSArray)[j] as! NSDictionary
                    
                    tempArray.append(planDurationDataForamt(planPriceID: "\(tempDictionary["plan_price_id"] ?? "")", planCost: "\(tempDictionary["cost"] ?? "")", planDuration: "\(tempDictionary["duration"] ?? "")"))
                }
                
                let tempDict = (self.globalJson["info"] as! NSArray)[i] as! NSDictionary
                
                self.planDataArray.append(planTypeDataFormat(planID: "\(tempDict["id"] ?? "")", planName: "\(tempDict["plan_name"] ?? "")", planValidUnit: "\(tempDict["plan_valid_unit"] ?? "")", planPriceList: tempArray, planDuration: "", isSelected: false , planCost: "" , planPriceID: ""))
                
            }
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    
    
    
    
    
    
    // MARK:- // Create Data Dictionaries
    
    func createDataDictionaries() {
        
        
        self.dynamicInfoFeaturesDataArray.removeAll()
        
        self.dynamicInfoStatusDataArray.removeAll()
        
        // CREATE FEATURES ARRAY
        for i in 0..<(((self.dynamicInfoDictionary["dynamic_info"] as! NSArray)[0] as! NSDictionary)["features"] as! NSArray).count {
            
            let tempdict = (((self.dynamicInfoDictionary["dynamic_info"] as! NSArray)[0] as! NSDictionary)["features"] as! NSArray)[i] as! NSDictionary
            
            var tempOptionsArray = [StaticDictDataFormat]()
            
            for j in 0..<(tempdict["field_option"] as! NSArray).count {
                
                tempOptionsArray.append(StaticDictDataFormat(key: "\(((tempdict["field_option"] as! NSArray)[j] as! NSDictionary)["field_value"] ?? "")", value: "\(((tempdict["field_option"] as! NSArray)[j] as! NSDictionary)["field_name"] ?? "")", isSelected: false))
                
            }
            self.dynamicInfoFeaturesDataArray.append(addPropertyDynamicDataFormat(fieldID: "\(tempdict["field_id"] ?? "")", fieldName: "\(tempdict["field_name"] ?? "")", fieldType: "\(tempdict["field_type"] ?? "")", fieldTypeID: "\(tempdict["field_type_id"] ?? "")", parameter: "\(tempdict["parameter"] ?? "")", fieldOptionArray: tempOptionsArray, value: "",fieldMadatoryStatus: "\(tempdict["field_mandatory_status"] ?? "")"))
            
        }
        
        
        // CREATE STATUS ARRAY
        for i in 0..<(((self.dynamicInfoDictionary["dynamic_info"] as! NSArray)[1] as! NSDictionary)["status"] as! NSArray).count {
            
            let tempdict = (((self.dynamicInfoDictionary["dynamic_info"] as! NSArray)[1] as! NSDictionary)["status"] as! NSArray)[i] as! NSDictionary
            
            var tempOptionsArray = [StaticDictDataFormat]()
            
            for j in 0..<(tempdict["field_option"] as! NSArray).count {
                
                tempOptionsArray.append(StaticDictDataFormat(key: "\(((tempdict["field_option"] as! NSArray)[j] as! NSDictionary)["field_value"] ?? "")", value: "\(((tempdict["field_option"] as! NSArray)[j] as! NSDictionary)["field_name"] ?? "")", isSelected: false))
                
            }
            self.dynamicInfoStatusDataArray.append(addPropertyDynamicDataFormat(fieldID: "\(tempdict["field_id"] ?? "")", fieldName: "\(tempdict["field_name"] ?? "")", fieldType: "\(tempdict["field_type"] ?? "")", fieldTypeID: "\(tempdict["field_type_id"] ?? "")", parameter: "\(tempdict["parameter"] ?? "")", fieldOptionArray: tempOptionsArray, value: "",fieldMadatoryStatus: "\(tempdict["field_mandatory_status"] ?? "")"))
            
        }
        
        
        
        // CREATE PLAN ARRAY
        
        
    }
    
    
    
    
    
    // MARK:- // Handle Long Press Gesture
    
    @objc func handleLongPress(sender : UITapGestureRecognizer) {
        
        if (self.localImagesArray.count + self.webImagesArray.count) > 0 {
            
            if self.longPressGesture.state == UIGestureRecognizer.State.began {
                
                let point = longPressGesture.location(in: self.uploadImagesCollectionview)
                let indexPath = self.uploadImagesCollectionview?.indexPathForItem(at: point)
                
                if let index = indexPath {
                    //var cell = self.imageSlideshowCollectionview.cellForItem(at: index)
                    // do stuff with your cell, for example print the indexPath
                    print(index.item)
                    
                    if index.item < self.webImagesArray.count {
                        
                        if self.webImagesToDeleteArray.contains((indexPath?.item)!) {
                            
                            var tempInt : Int!
                            
                            for i in 0..<self.webImagesToDeleteArray.count {
                                if indexPath?.item == self.webImagesToDeleteArray[i] {
                                    tempInt = i
                                }
                            }
                            
                            self.webImagesToDeleteArray.remove(at: tempInt)
                        }
                        else {
                            self.webImagesToDeleteArray.append((indexPath?.item)!)
                        }
                        
                        print(webImagesToDeleteArray)
                    }
                    else {
                        if self.localImagesToDeleteArray.contains((indexPath?.item)! - self.webImagesArray.count) {
                            
                            var tempInt : Int!
                            
                            for i in 0..<self.localImagesToDeleteArray.count {
                                if index.item == self.localImagesToDeleteArray[i] {
                                    tempInt = i
                                }
                            }
                            
                            self.localImagesToDeleteArray.remove(at: tempInt)
                        }
                        else {
                            self.localImagesToDeleteArray.append(((indexPath?.item)! - self.webImagesArray.count))
                        }
                    }
                    
                    print("local-",self.localImagesToDeleteArray)
                    
                    self.uploadImagesCollectionview.reloadData()
                    
                    
                } else {
                    print("Could not find index path")
                }
                
                if (self.webImagesToDeleteArray.count + self.localImagesToDeleteArray.count) > 0 {
                    self.deleteSelectedView.isHidden = false
                }
                else {
                    self.deleteSelectedView.isHidden = true
                }
                
            }
        }
        
    }
    
    
    
    
    
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.chooseCategoryTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "app_msg4")
        self.realEstateTypeTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "home_type"))
        self.realEstateType.text = globalFunctions.shared.getLanguageString(variable: "home_select_type")
        self.subCategoryTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "app_msg5"))
        self.subCategory.text = globalFunctions.shared.getLanguageString(variable: "home_select")
        self.offerForTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "property_post_sub_category"))
        self.rentTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "app_msg6")
        self.saleTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "app_msg7")
        self.locationTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "home_location"))
        self.country.text = globalFunctions.shared.getLanguageString(variable: "my_profile_select_country")
        self.province.text = globalFunctions.shared.getLanguageString(variable: "property_post_province")
        self.city.text = globalFunctions.shared.getLanguageString(variable: "property_post_city")
        self.district.text = globalFunctions.shared.getLanguageString(variable: "property_post_district")
        self.nearestPointTXT.attributedPlaceholder = NSAttributedString(string: globalFunctions.shared.getLanguageString(variable: "ios_please_enter_nearest_point"),
                                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
       // globalFunctions.shared.getLanguageString(variable: "ios_please_enter_nearest_point")
        self.localityTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "android_pls_ntr_lclty")
        
        self.priceTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "property_details_price"))
        self.priceTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "property_post_price")
        self.currency.text = globalFunctions.shared.getLanguageString(variable: "home_select")
        self.currencyDefaultTxt = globalFunctions.shared.getLanguageString(variable: "home_select")
        self.negotiablePriceTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "property_post_negotiable_price"))
        self.negotiablePriceYesLBL.text = globalFunctions.shared.getLanguageString(variable: "property_post_yes")
        self.negotiablePriceNoLBL.text = globalFunctions.shared.getLanguageString(variable: "property_post_no")
        self.titleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "property_post_title"))
        self.titleTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "please_enter_title")
        self.descriptionTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "home_description"))
        
        self.propertyDescription.text = globalFunctions.shared.getLanguageString(variable: "please_enter_description")
        self.propertyDescription.textColor = .lightGray
        
        
        self.uploadImagesTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_upload_image"))
        self.deleteSelectedTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "ios_delete_selected")
        self.youtubeLinkTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "property_post_url_placeholder") //+ globalFunctions.shared.getLanguageString(variable: "property_post_video_format_text1")
        self.adsCostTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "app_msg8"))
        self.addRealEstateTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "left_side_add_real_estate")
        
        
    }
    
    
    
    // MARK:- // Set Font
    
    func setFont() {
        
        self.chooseCategoryTitleLBL.font = UIFont(name: self.chooseCategoryTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.realEstateTypeTitleLBL.font = UIFont(name: self.realEstateTypeTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.realEstateType.font = UIFont(name: self.realEstateType.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.subCategoryTitleLBL.font = UIFont(name: self.subCategoryTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.subCategory.font = UIFont(name: self.subCategory.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.offerForTitleLBL.font = UIFont(name: self.offerForTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.rentTitleLBL.font = UIFont(name: self.rentTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.saleTitleLBL.font = UIFont(name: self.saleTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.rentPeriod.font = UIFont(name: self.rentPeriod.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.locationTitleLBL.font = UIFont(name: self.locationTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.country.font = UIFont(name: self.country.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.province.font = UIFont(name: self.province.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.city.font = UIFont(name: self.city.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.district.font = UIFont(name: self.district.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.nearestPointTXT.font = UIFont(name: self.nearestPointTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.localityTXT.font = UIFont(name: self.localityTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.priceTitleLBL.font = UIFont(name: self.priceTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.priceTXT.font = UIFont(name: self.priceTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.currency.font = UIFont(name: self.currency.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.negotiablePriceTitleLBL.font = UIFont(name: self.negotiablePriceTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.negotiablePriceYesLBL.font = UIFont(name: self.negotiablePriceYesLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.negotiablePriceNoLBL.font = UIFont(name: self.negotiablePriceNoLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.titleLBL.font = UIFont(name: self.titleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.titleTXT.font = UIFont(name: self.titleTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.descriptionTitleLBL.font = UIFont(name: self.descriptionTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.propertyDescription.font = UIFont(name: self.propertyDescription.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.uploadImagesTitleLBL.font = UIFont(name: self.uploadImagesTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.deleteSelectedTitleLBL.font = UIFont(name: self.deleteSelectedTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.youtubeLinkTXT.font = UIFont(name: self.youtubeLinkTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
         self.adsCostTitleLBL.font = UIFont(name: self.adsCostTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
         self.addRealEstateTitleLBL.font = UIFont(name: self.addRealEstateTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
        
    }
    
    
    
    // MARK:- // Open Image Picker
    
    func openImagepicker() {
        
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a Source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    
    
    
    // MARK:- // Create Features Stackview
    
    func createFeaturesStackview() {
        
        let featuresStackview : dynamicStackview = {
            let featuresstackview = dynamicStackview()
            featuresstackview.translatesAutoresizingMaskIntoConstraints = false
            return featuresstackview
        }()
        
        self.dynamicStackView.addArrangedSubview(featuresStackview)
        
        for i in 0..<self.dynamicInfoFeaturesDataArray.count {
           
            
            // CHECKBOXES
            if "\(self.dynamicInfoFeaturesDataArray[i].fieldTypeID ?? "")".elementsEqual("4") {
                self.createViewWithCollectionview(addInView: featuresStackview, tag: i, restorationIdentifier: "features", dataDictionary: self.dynamicInfoFeaturesDataArray[i], accessibilityIdentifier: "checkbox")
                
            }
                
            // TEXTFIELDS
            else if "\(self.dynamicInfoFeaturesDataArray[i].fieldTypeID ?? "")".elementsEqual("1") {
                
                self.createViewWithTextfields(addInView: featuresStackview, tag: i, restorationIdentifier: "features", dataDictionary: self.dynamicInfoFeaturesDataArray[i])
                
            }
            
            // RADIO BUTTONS
            else if "\(self.dynamicInfoFeaturesDataArray[i].fieldTypeID ?? "")".elementsEqual("5") {
                
                self.createViewWithCollectionview(addInView: featuresStackview, tag: i, restorationIdentifier: "features", dataDictionary: self.dynamicInfoFeaturesDataArray[i], accessibilityIdentifier: "radio")
                
            }
            
            // Select Fields
            else if "\(self.dynamicInfoFeaturesDataArray[i].fieldTypeID ?? "")".elementsEqual("3") {
                
                self.createViewWithPicker(addInView: featuresStackview, tag: i, restorationIdentifier: "features", dataDictionary: self.dynamicInfoFeaturesDataArray[i])
                
            }
            
        }
        
    }
    
    
    // MARK:- // Create Status Stackview
    
    func createStatusStackview() {
        
        let statusStackview : dynamicStackview = {
            let statusstackview = dynamicStackview()
            statusstackview.translatesAutoresizingMaskIntoConstraints = false
            return statusstackview
        }()
        
        self.dynamicStackView.addArrangedSubview(statusStackview)
        
        for i in 0..<self.dynamicInfoStatusDataArray.count {
            
            // CHECKBOXES
            if "\(self.dynamicInfoStatusDataArray[i].fieldTypeID ?? "")".elementsEqual("4") {
                self.createViewWithCollectionview(addInView: statusStackview, tag: i, restorationIdentifier: "status", dataDictionary: self.dynamicInfoStatusDataArray[i], accessibilityIdentifier: "checkbox")
                
            }
            
            // TEXT FIELDS
            else if "\(self.dynamicInfoStatusDataArray[i].fieldTypeID ?? "")".elementsEqual("1") {
                
                self.createViewWithTextfields(addInView: statusStackview, tag: i, restorationIdentifier: "status", dataDictionary: self.dynamicInfoStatusDataArray[i])
                
            }
            
            // RADIO BUTTONS
            else if "\(self.dynamicInfoStatusDataArray[i].fieldTypeID ?? "")".elementsEqual("5") {
                
                self.createViewWithCollectionview(addInView: statusStackview, tag: i, restorationIdentifier: "status", dataDictionary: self.dynamicInfoStatusDataArray[i], accessibilityIdentifier: "radio")
                
            }
            
            // Select Fields
            else if "\(self.dynamicInfoStatusDataArray[i].fieldTypeID ?? "")".elementsEqual("3") {
                
                self.createViewWithPicker(addInView: statusStackview, tag: i, restorationIdentifier: "status", dataDictionary: self.dynamicInfoStatusDataArray[i])
                
            }
            
        }
        
    }
    
    
    
    // MARK:- // Create View with Checkboxes
    
    func createViewWithCollectionview(addInView : UIStackView , tag : Int , restorationIdentifier : String , dataDictionary : addPropertyDynamicDataFormat , accessibilityIdentifier : String) {
        
        // MAIN VIEW
        let checkboxView : UIView = {
           let checkboxview = UIView()
            checkboxview.translatesAutoresizingMaskIntoConstraints = false
            return checkboxview
        }()
        
        addInView.addArrangedSubview(checkboxView)
        
        // HEADER LABEL
        let headerLBL : UILabel = {
            let headerlbl = UILabel()
            headerlbl.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
            if dataDictionary.fieldMadatoryStatus == "1"{
                headerlbl.attributedText = globalFunctions.shared.asterixText(withText: "\(dataDictionary.fieldName ?? "")")
                headerlbl.accessibilityIdentifier = "1"
            }
            else{
                headerlbl.accessibilityIdentifier = "0"
                headerlbl.text = "\(dataDictionary.fieldName ?? "")"
            }
            headerlbl.translatesAutoresizingMaskIntoConstraints = false
            return headerlbl
        }()
        
        checkboxView.addSubview(headerLBL)
        
        headerLBL.anchor(top: checkboxView.topAnchor, leading: checkboxView.leadingAnchor, bottom: nil, trailing: checkboxView.trailingAnchor, padding: .init(top: 10, left: 10, bottom: 0, right: 10))
        
        
        if restorationIdentifier.elementsEqual("features") {
            self.featureHeaderArray.append(headerLBL)
        }
        else {
            self.statusHeaderArray.append(headerLBL)
        }
        
        
        
        // CHECKBOX COLLECTION VIEW
        let checkboxCollectionView : UICollectionView = {
            
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            
            let width = (UIScreen.main.bounds.size.width - 20) / 2
            layout.itemSize = CGSize(width: floor(width), height: 45)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            layout.scrollDirection = .vertical
            
            let listingcollectionview = UICollectionView(frame: .zero, collectionViewLayout: layout)
            
            listingcollectionview.backgroundColor = .white
            listingcollectionview.showsVerticalScrollIndicator = false
            listingcollectionview.showsHorizontalScrollIndicator = false
            listingcollectionview.translatesAutoresizingMaskIntoConstraints = false
            listingcollectionview.isScrollEnabled = false
            
            return listingcollectionview
            
        }()
        
        checkboxCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "checkboxCVC")
        
        checkboxCollectionView.dataSource = self
        checkboxCollectionView.delegate = self
        
        checkboxView.addSubview(checkboxCollectionView)
        checkboxCollectionView.tag = tag
        checkboxCollectionView.restorationIdentifier = restorationIdentifier
        checkboxCollectionView.accessibilityIdentifier = accessibilityIdentifier
        
        checkboxCollectionView.anchor(top: headerLBL.bottomAnchor, leading: checkboxView.leadingAnchor, bottom: nil, trailing: checkboxView.trailingAnchor, padding: .init(top: 5, left: 10, bottom: 0, right: 10))
        
        let tempcount = (dataDictionary.fieldOptionArray.count / 2) + (dataDictionary.fieldOptionArray.count % 2)
        checkboxCollectionView.heightAnchor.constraint(equalToConstant: CGFloat(tempcount) * 45).isActive = true
        
        
        // BORDER VIEW
        let borderView : UIView = {
           let borderview = UIView()
            borderview.backgroundColor = .lightGray
            borderview.translatesAutoresizingMaskIntoConstraints = false
            return borderview
        }()
        
        checkboxView.addSubview(borderView)
        
        borderView.anchor(top: checkboxCollectionView.bottomAnchor, leading: checkboxView.leadingAnchor, bottom: checkboxView.bottomAnchor, trailing: checkboxView.trailingAnchor, padding: .init(top: 10, left: 10, bottom: 0, right: 10), size: .init(width: 0, height: 1))
        
    }
    
    
    
    // MARK:- // Create View with Text Fields
    
    func createViewWithTextfields(addInView : UIStackView , tag : Int , restorationIdentifier : String , dataDictionary : addPropertyDynamicDataFormat) {
        
        // MAIN VIEW
        let textfieldView : UIView = {
            let textfieldview = UIView()
            textfieldview.translatesAutoresizingMaskIntoConstraints = false
            return textfieldview
        }()
        
        addInView.addArrangedSubview(textfieldView)
        
        
        // HEADER LABEL
        let headerLBL : UILabel = {
            let headerlbl = UILabel()
            headerlbl.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
            if dataDictionary.fieldMadatoryStatus == "1"{
                headerlbl.attributedText = globalFunctions.shared.asterixText(withText: "\(dataDictionary.fieldName ?? "")")
                headerlbl.accessibilityIdentifier = "1"
            }
            else{
                headerlbl.accessibilityIdentifier = "0"
                headerlbl.text = "\(dataDictionary.fieldName ?? "")"
            }
            headerlbl.translatesAutoresizingMaskIntoConstraints = false
            return headerlbl
        }()
        
        textfieldView.addSubview(headerLBL)
        
        headerLBL.anchor(top: textfieldView.topAnchor, leading: textfieldView.leadingAnchor, bottom: nil, trailing: textfieldView.trailingAnchor, padding: .init(top: 10, left: 10, bottom: 0, right: 10))
        
        if restorationIdentifier.elementsEqual("features") {
            self.featureHeaderArray.append(headerLBL)
        }
        else {
            self.statusHeaderArray.append(headerLBL)
        }
        
        
        
        // TEXTFIELD Section
        let viewOfTextfield : UIView = {
            let viewoftextfield = UIView()
            viewoftextfield.layer.cornerRadius = 5
            viewoftextfield.layer.borderColor = UIColor.gray.cgColor
            viewoftextfield.layer.borderWidth = 0.5
            viewoftextfield.translatesAutoresizingMaskIntoConstraints = false
            return viewoftextfield
        }()
        
        textfieldView.addSubview(viewOfTextfield)
        
        viewOfTextfield.anchor(top: headerLBL.bottomAnchor, leading: nil, bottom: nil, trailing: nil, padding: .init(top: 10, left: 0, bottom: 0, right: 0), size: .init(width: (260/320)*self.FullWidth, height: 40))
        
        viewOfTextfield.centerXAnchor.constraint(equalTo: textfieldView.centerXAnchor).isActive = true
        
        
        let textfieldTXT : UITextField = {
           let textfieldtxt = UITextField()
            textfieldtxt.keyboardType = .asciiCapableNumberPad
            textfieldtxt.textAlignment = .center
            textfieldtxt.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
            textfieldtxt.placeholder = "\(globalFunctions.shared.getLanguageString(variable: "app_enter")) " + "\(dataDictionary.fieldName ?? "")"
            textfieldtxt.translatesAutoresizingMaskIntoConstraints = false
            return textfieldtxt
        }()
        
        viewOfTextfield.addSubview(textfieldTXT)
        
        textfieldTXT.restorationIdentifier = restorationIdentifier
        self.textfieldsArray.append(textfieldTXT)
        textfieldTXT.tag = tag
        
        if dataDictionary.value?.elementsEqual("") == false {
            
            textfieldTXT.text = "\(dataDictionary.value ?? "")"
            
        }
        
        
        textfieldTXT.anchor(top: viewOfTextfield.topAnchor, leading: viewOfTextfield.leadingAnchor, bottom: viewOfTextfield.bottomAnchor, trailing: viewOfTextfield.trailingAnchor, padding: .init(top: 5, left: 5, bottom: 5, right: 5))
        
        
        // BORDER VIEW
        let borderView : UIView = {
            let borderview = UIView()
            borderview.backgroundColor = .lightGray
            borderview.translatesAutoresizingMaskIntoConstraints = false
            return borderview
        }()
        
        textfieldView.addSubview(borderView)
        
        borderView.anchor(top: viewOfTextfield.bottomAnchor, leading: textfieldView.leadingAnchor, bottom: textfieldView.bottomAnchor, trailing: textfieldView.trailingAnchor, padding: .init(top: 15, left: 10, bottom: 0, right: 10), size: .init(width: 0, height: 1))
        
        
    }
    
    
    // MARK:- // Create View with Picker
    
    func createViewWithPicker(addInView : UIStackView , tag : Int , restorationIdentifier : String , dataDictionary : addPropertyDynamicDataFormat) {
        
        // MAIN VIEW
        let selectView : UIView = {
            let selectview = UIView()
            selectview.translatesAutoresizingMaskIntoConstraints = false
            return selectview
        }()
        
        addInView.addArrangedSubview(selectView)
        
        
        // HEADER LABEL
        let headerLBL : UILabel = {
            let headerlbl = UILabel()
            headerlbl.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
            if dataDictionary.fieldMadatoryStatus == "1"{
                headerlbl.attributedText = globalFunctions.shared.asterixText(withText: "\(dataDictionary.fieldName ?? "")")
                headerlbl.accessibilityIdentifier = "1"
            }
            else{
                headerlbl.accessibilityIdentifier = "0"
                headerlbl.text = "\(dataDictionary.fieldName ?? "")"
            }
            headerlbl.translatesAutoresizingMaskIntoConstraints = false
            return headerlbl
        }()
        
        selectView.addSubview(headerLBL)
        
        headerLBL.anchor(top: selectView.topAnchor, leading: selectView.leadingAnchor, bottom: nil, trailing: selectView.trailingAnchor, padding: .init(top: 10, left: 10, bottom: 0, right: 10))
        
        if restorationIdentifier.elementsEqual("features") {
            self.featureHeaderArray.append(headerLBL)
        }
        else {
            self.statusHeaderArray.append(headerLBL)
        }
        
        
        
        // Picker Section
        let viewOfPicker : UIView = {
            let viewofpicker = UIView()
            viewofpicker.layer.cornerRadius = 5
            viewofpicker.layer.borderColor = UIColor.gray.cgColor
            viewofpicker.layer.borderWidth = 0.5
            viewofpicker.translatesAutoresizingMaskIntoConstraints = false
            return viewofpicker
        }()
        
        selectView.addSubview(viewOfPicker)
        
        viewOfPicker.anchor(top: headerLBL.bottomAnchor, leading: nil, bottom: nil, trailing: nil, padding: .init(top: 10, left: 0, bottom: 0, right: 0), size: .init(width: (260/320)*self.FullWidth, height: 40))
        
        viewOfPicker.centerXAnchor.constraint(equalTo: selectView.centerXAnchor).isActive = true
        
        
        let insideLBL : UILabel = {
            let insidelbl = UILabel()
            insidelbl.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
            insidelbl.text = "\(globalFunctions.shared.getLanguageString(variable: "app_enter")) " + "\(dataDictionary.fieldName ?? "")"
            insidelbl.textColor = .lightGray
            insidelbl.translatesAutoresizingMaskIntoConstraints = false
            return insidelbl
        }()
        
        viewOfPicker.addSubview(insideLBL)
        
        self.pickerLBLArray.append(insideLBL)
        insideLBL.tag = tag
        
        if dataDictionary.value?.elementsEqual("") == false {
            
            insideLBL.text = "\(dataDictionary.value ?? "")"
            insideLBL.textColor = .black
            
            
        }
        
        insideLBL.anchor(top: viewOfPicker.topAnchor, leading: viewOfPicker.leadingAnchor, bottom: viewOfPicker.bottomAnchor, trailing: nil, padding: .init(top: 5, left: 5, bottom: 5, right: 0))
        
        let downImageview : UIImageView = {
           let downimageview = UIImageView()
            downimageview.contentMode = .center
            downimageview.image = UIImage(named : "Triangle 1")
            downimageview.translatesAutoresizingMaskIntoConstraints = false
            return downimageview
        }()
        
        viewOfPicker.addSubview(downImageview)
        
        downImageview.anchor(top: viewOfPicker.topAnchor, leading: nil, bottom: viewOfPicker.bottomAnchor, trailing: viewOfPicker.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 10, right: 5), size: .init(width: 20, height: 20))
        
        downImageview.leadingAnchor.constraint(greaterThanOrEqualTo: insideLBL.trailingAnchor, constant: 10).isActive = true
        
        
        let openPickerButton : UIButton = {
            let openpickerbutton = UIButton()
            openpickerbutton.translatesAutoresizingMaskIntoConstraints = false
            return openpickerbutton
        }()
        
        viewOfPicker.addSubview(openPickerButton)
        
        openPickerButton.anchor(top: viewOfPicker.topAnchor, leading: viewOfPicker.leadingAnchor, bottom: viewOfPicker.bottomAnchor, trailing: viewOfPicker.trailingAnchor)
        
        openPickerButton.tag = tag
        openPickerButton.addTarget(self, action: #selector(self.openPicker(sender:)), for: .touchUpInside)
        
        
        
        // BORDER VIEW
        let borderView : UIView = {
            let borderview = UIView()
            borderview.backgroundColor = .lightGray
            borderview.translatesAutoresizingMaskIntoConstraints = false
            return borderview
        }()
        
        selectView.addSubview(borderView)
        
        borderView.anchor(top: viewOfPicker.bottomAnchor, leading: selectView.leadingAnchor, bottom: selectView.bottomAnchor, trailing: selectView.trailingAnchor, padding: .init(top: 15, left: 10, bottom: 0, right: 10), size: .init(width: 0, height: 1))
        
        
        
        
        // PICKER VIEW
        let optionPickerview : UIPickerView = {
           let optionpickerview = UIPickerView()
            optionpickerview.backgroundColor = .lightGray
            optionpickerview.translatesAutoresizingMaskIntoConstraints = false
            return optionpickerview
        }()
        
        self.viewOfPickerview.addSubview(optionPickerview)
        
        optionPickerview.isHidden = true
        self.pickerviewArray.append(optionPickerview)
        optionPickerview.tag = tag
        optionPickerview.restorationIdentifier = restorationIdentifier
        
        optionPickerview.dataSource = self
        optionPickerview.delegate = self
        
        optionPickerview.anchor(top: nil, leading: self.viewOfPickerview.leadingAnchor, bottom: self.viewOfPickerview.bottomAnchor, trailing: self.viewOfPickerview.trailingAnchor, size: .init(width: 0, height: self.cityPickerview.frame.size.height))
        
    }
    
    
    // MARK:- // Open Picker Action Method
    
    @objc func openPicker(sender : UIButton) {
        
        self.view.bringSubviewToFront(self.viewOfPickerview)
        self.viewOfPickerview.isHidden = false
        
        for i in 0..<self.pickerviewArray.count {
            
            if pickerviewArray[i].tag == sender.tag {
                
                self.pickerviewArray[i].isHidden = false
                
                self.pickerviewArray[i].selectRow(0, inComponent: 0, animated: false)
                
                self.selectedPickerview = self.pickerviewArray[i]
                self.selectedPickerIndex = 0
                self.doneClicked = false
                
                break
                
            }
            
        }
        
        self.closeAllTextfields(mainview: self.view)
        
    }
    
    
    // MARK:- // Plan Type Button Action
    
    @objc func planTypeButtonAction(sender : UIButton) {
        
        for i in 0..<self.planDataArray.count {
            self.planDataArray[i].isSelected = false
        }
        self.planDataArray[sender.tag].isSelected = true
        
        for i in 0..<self.planDataArray.count {
            if i != sender.tag {
                self.planDataArray[i].planDuration = ""
                self.planDataArray[i].planCost = ""
                self.planDataArray[i].planPriceID = ""
            }
        }
        
        
        
        
        if "\(self.planDataArray[sender.tag].planID ?? "")".elementsEqual("1") {
            
            self.planDurationArray = self.planDataArray[sender.tag].planPriceList
            
            self.planDataArray[sender.tag].planDuration = "\(self.planDurationArray[0].planDuration ?? "")"  + " \(self.planDataArray[sender.tag].planValidUnit ?? "")"
            self.planDataArray[sender.tag].planCost = "=\(self.planDurationArray[0].planCost ?? "")$"
            self.planDataArray[sender.tag].planPriceID = "\(self.planDurationArray[0].planPriceID ?? "")"
            
            
        }
        
        
        
        self.adsCostTableview.reloadData()
        
    }
    
    // MARK:- // Plan Duration Button Action
    
    @objc func planDurationButtonAction(sender : UIButton) {
        
        self.view.bringSubviewToFront(self.viewOfPickerview)
        self.viewOfPickerview.isHidden = false
        self.planDurationPickerview.isHidden = false
        
        self.planDurationPickerview.tag = sender.tag
        
        self.planDurationArray = self.planDataArray[sender.tag].planPriceList
        
        self.planDurationPickerview.delegate = self
        self.planDurationPickerview.dataSource = self
        self.planDurationPickerview.reloadAllComponents()
        
        self.closeAllTextfields(mainview: self.view)
        
        self.planDurationPickerview.selectRow(0, inComponent: 0, animated: false)
        
        self.selectedPickerview = self.planDurationPickerview
        self.selectedPickerIndex = 0
        self.doneClicked = false
        
    }
    
    
    
    
    // MARK:- // Save Real Estate
    
    func saveRealEstate() {
        
        //PLANS
        for i in 0..<self.planDataArray.count {
            if self.planDataArray[i].planPriceID?.elementsEqual("") == false {
                planPriceID = self.planDataArray[i].planPriceID!
                
                if i > 0 {
                    
                    for j in 0..<self.planDataArray[i].planPriceList.count {
                        
                        if self.planPriceID.elementsEqual("\(self.planDataArray[i].planPriceList[j].planPriceID ?? "")") {
                            
                            if i == 1 {
                                self.subPlanId = planIdArray[j]
                            }
                            else {
                                self.subPlanId = planIdArray[j + 3]
                            }
                            print(self.subPlanId!)
                        }
                        
                    }
                    
                }
                
                break
            }
        }
        
        
        // TEXTFIELDS
        for i in 0..<self.textfieldsArray.count {
            if (textfieldsArray[i].restorationIdentifier?.elementsEqual("features"))! {
                for j in 0..<self.dynamicInfoFeaturesDataArray.count {
                    if self.textfieldsArray[i].tag == j {
                        self.dynamicInfoFeaturesDataArray[j].value = "\(self.textfieldsArray[i].text ?? "")"
                    }
                }
            }
            else if (textfieldsArray[i].restorationIdentifier?.elementsEqual("status"))! {
                for j in 0..<self.dynamicInfoStatusDataArray.count {
                    if self.textfieldsArray[i].tag == j {
                        self.dynamicInfoStatusDataArray[j].value = "\(self.textfieldsArray[i].text ?? "")"
                    }
                }
            }
        }
        
        
        // SELECT VIEW
        for i in 0..<self.dynamicInfoFeaturesDataArray.count {
            if (self.dynamicInfoFeaturesDataArray[i].fieldTypeID?.elementsEqual("3"))! {
                for j in 0..<self.dynamicInfoFeaturesDataArray[i].fieldOptionArray.count {
                    if (self.dynamicInfoFeaturesDataArray[i].value?.elementsEqual("\(self.dynamicInfoFeaturesDataArray[i].fieldOptionArray[j].value!)"))! {
                        self.dynamicInfoFeaturesDataArray[i].value = "\(self.dynamicInfoFeaturesDataArray[i].fieldOptionArray[j].key ?? "")"
                    }
                }
            }
        }
        
        for i in 0..<self.dynamicInfoStatusDataArray.count {
            if (self.dynamicInfoStatusDataArray[i].fieldTypeID?.elementsEqual("3"))! {
                for j in 0..<self.dynamicInfoStatusDataArray[i].fieldOptionArray.count {
                    if (self.dynamicInfoStatusDataArray[i].value?.elementsEqual("\(self.dynamicInfoStatusDataArray[i].fieldOptionArray[j].value!)"))! {
                        self.dynamicInfoStatusDataArray[i].value = "\(self.dynamicInfoStatusDataArray[i].fieldOptionArray[j].key ?? "")"
                    }
                }
            }
        }
        
        
        // CHECKBOXES
        var tempArray = [String]()
        
        for i in 0..<self.dynamicInfoFeaturesDataArray.count {
            if (self.dynamicInfoFeaturesDataArray[i].fieldTypeID?.elementsEqual("4"))! {
                for j in 0..<self.dynamicInfoFeaturesDataArray[i].fieldOptionArray.count {
                    if self.dynamicInfoFeaturesDataArray[i].fieldOptionArray[j].isSelected == true {
                        tempArray.append("\(self.dynamicInfoFeaturesDataArray[i].fieldOptionArray[j].key ?? "")")
                    }
                }
                self.dynamicInfoFeaturesDataArray[i].value = tempArray.joined(separator: ",")
            }
            tempArray.removeAll()
        }
        
        tempArray.removeAll()
        
        for i in 0..<self.dynamicInfoStatusDataArray.count {
            if (self.dynamicInfoStatusDataArray[i].fieldTypeID?.elementsEqual("4"))! {
                for j in 0..<self.dynamicInfoStatusDataArray[i].fieldOptionArray.count {
                    if self.dynamicInfoStatusDataArray[i].fieldOptionArray[j].isSelected == true {
                        tempArray.append("\(self.dynamicInfoStatusDataArray[i].fieldOptionArray[j].key ?? "")")
                    }
                }
                self.dynamicInfoStatusDataArray[i].value = tempArray.joined(separator: ",")
            }
            tempArray.removeAll()
        }
        
        
        
        //RADIO BUTTONS
        for i in 0..<self.dynamicInfoFeaturesDataArray.count {
            if (self.dynamicInfoFeaturesDataArray[i].fieldTypeID?.elementsEqual("5"))! {
                for j in 0..<self.dynamicInfoFeaturesDataArray[i].fieldOptionArray.count {
                    if self.dynamicInfoFeaturesDataArray[i].fieldOptionArray[j].isSelected == true {
                        self.dynamicInfoFeaturesDataArray[i].value = "\(self.dynamicInfoFeaturesDataArray[i].fieldOptionArray[j].key ?? "")"
                        break
                    }
                }
            }
        }
        
        for i in 0..<self.dynamicInfoStatusDataArray.count {
            if (self.dynamicInfoStatusDataArray[i].fieldTypeID?.elementsEqual("5"))! {
                for j in 0..<self.dynamicInfoStatusDataArray[i].fieldOptionArray.count {
                    if self.dynamicInfoStatusDataArray[i].fieldOptionArray[j].isSelected == true {
                        self.dynamicInfoStatusDataArray[i].value = "\(self.dynamicInfoStatusDataArray[i].fieldOptionArray[j].key ?? "")"
                        break
                    }
                }
            }
        }
        
        
//        print("dalaleen_user_id=\(userInformation.shared.userID ?? "")")
//        print("name=\(self.titleTXT.text ?? "")")
//        print("description=\(self.propertyDescription.text ?? "")")
//        print("address_field=\(self.nearestPointTXT.text ?? "")")
//        print("youtube_url=\(self.youtubeLinkTXT.text ?? "")")
//        print("negotiablePrice=\(self.negotiablePrice ?? "")")
//        print("plan_price_id=\(planPriceID ?? "")")
//        print("category_sub=\(self.offerFor ?? "")")
//        print("property_type=\(self.categoryTypeID ?? "")")
//        print("country_id=\(self.countryID ?? "")")
//        print("province_id=\(self.provinceID ?? "")")
//        print("city_id=\(self.cityID ?? "")")
//        print("district_id=\(self.districtID ?? "")")
//        print("price=\(self.priceTXT.text ?? "")")
//        print("currency=\(self.currencyID ?? "")")
//
//        for i in 0..<self.dynamicInfoFeaturesDataArray.count {
//            print("\(self.dynamicInfoFeaturesDataArray[i].parameter ?? "")=\(self.dynamicInfoFeaturesDataArray[i].value ?? "")")
//        }
//
//        for i in 0..<self.dynamicInfoStatusDataArray.count {
//            print("\(self.dynamicInfoStatusDataArray[i].parameter ?? "")=\(self.dynamicInfoStatusDataArray[i].value ?? "")")
//        }
        
        
        
        
        
        if self.propertyID.elementsEqual("") {
            self.runValidations {
                if self.validated == true {
                    if self.subPlanId != "999" {
                        let index = self.getIndexFromProductId(productId: self.subPlanId)
                        self.purchaseItemIndex(index: index) {
                            
                            self.saveNewProperty()
                            
                        }
                        
                    }
                    else {
                        
                        self.saveNewProperty()
                        
                    }
                }
                
            }
            
            
        }
        else {
            self.saveEditedProperty()
        }
        
    }
    
    
    
    // MARK:- // Save New Property
    
    func saveNewProperty() {
        
        var parameters = [String : String]()
        
        parameters = [
            "dalaleen_user_id" : "\(userInformation.shared.userID ?? "")" ,
            "name" : "\(self.titleTXT.text ?? "")" ,
            "description" : "\(self.propertyDescription.text ?? "")" ,
            "address_field" : "\(self.localityTXT.text ?? "")" ,
            "nearby_location" : "\(self.nearestPointTXT.text ?? "")" ,
            "youtube_url" : "\(self.youtubeLinkTXT.text ?? "")" ,
            "negotiablePrice" : "\(self.negotiablePrice ?? "")" ,
            "plan_price_id" : "\(planPriceID ?? "")" ,
            "category_sub" : "\(self.offerFor ?? "")" ,
            "property_type" : "\(self.categoryTypeID ?? "")" ,
            "country_id" : "\(self.countryID ?? "")" ,
            "province_id" : "\(self.provinceID ?? "")" ,
            "city_id" : "\(self.cityID ?? "")" ,
            "district_id" : "\(self.districtID ?? "")" ,
            "price" : "\(self.priceTXT.text ?? "")".replacingOccurrences(of: ",", with: "") ,
            "currency" : "\(self.currencyID ?? "")" ,
            "verify_lat" : "\(self.userLat ?? "")" ,
            "verify_long" : "\(self.userLong ?? "")" ,
            "period" : "\(self.periodID ?? "")" ,
            "language_code" : "\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        ]
        
        for i in 0..<self.dynamicInfoFeaturesDataArray.count {
            parameters["\(self.dynamicInfoFeaturesDataArray[i].parameter ?? "")"] = "\(self.dynamicInfoFeaturesDataArray[i].value ?? "")"
        }
        
        for i in 0..<self.dynamicInfoStatusDataArray.count {
            parameters["\(self.dynamicInfoStatusDataArray[i].parameter ?? "")"] = "\(self.dynamicInfoStatusDataArray[i].value ?? "")"
        }
        
        print("Parameters are---",parameters)
        
        
        self.runValidations {
            
            if self.validated == true {
                // PLAN TYPE VALIDATION
                if "\(self.planPriceID ?? "")".elementsEqual("") {
                    self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "property_post_plan_validation"))
                    
                    self.validated = false
                }
                else {
                    self.validated = true
                }
            }
            
            if self.validated == true {
                
                print("Validated.")
                
                self.saveProperty(parameters: parameters, urlString: "propertySave_ios", completion: {
                    
                    let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    
                    self.navigationController?.pushViewController(navigate, animated: true)
                    
                })
                
            }
            else {
                print("Not Validated.")
            }
        }
    }
    
    
    // MARK:- // Save Edited Property
    
    func saveEditedProperty() {
        
        var parameters = [String : String]()
        
        parameters = [
            "dalaleen_user_id" : "\(userInformation.shared.userID ?? "")" ,
            "name" : "\(self.titleTXT.text ?? "")" ,
            "description" : "\(self.propertyDescription.text ?? "")" ,
            "address_field" : "\(self.localityTXT.text ?? "")" ,
            "nearby_location" : "\(self.nearestPointTXT.text ?? "")" ,
            "youtube_url" : "\(self.youtubeLinkTXT.text ?? "")" ,
            "negotiablePrice" : "\(self.negotiablePrice ?? "")" ,
            "plan_price_id" : "\(planPriceID ?? "")" ,
            "category_sub" : "\(self.offerFor ?? "")" ,
            "country_id" : "\(self.countryID ?? "")" ,
            "province_id" : "\(self.provinceID ?? "")" ,
            "city_id" : "\(self.cityID ?? "")" ,
            "district_id" : "\(self.districtID ?? "")" ,
            "price" : "\(self.priceTXT.text ?? "")".replacingOccurrences(of: ",", with: "") ,
            "currency" : "\(self.currencyID ?? "")" ,
            "verify_lat" : "\(self.userLat ?? "")" ,
            "verify_long" : "\(self.userLong ?? "")" ,
            "property_id" : "\(self.propertyID ?? "")" ,
            "period" : "\(self.periodID ?? "")" ,
            "deleted_image_id" : "\(self.deletedImageString ?? "")" ,
            "language_code" : "\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        ]
        
        for i in 0..<self.dynamicInfoFeaturesDataArray.count {
            parameters["\(self.dynamicInfoFeaturesDataArray[i].parameter ?? "")"] = "\(self.dynamicInfoFeaturesDataArray[i].value ?? "")"
        }
        
        for i in 0..<self.dynamicInfoStatusDataArray.count {
            parameters["\(self.dynamicInfoStatusDataArray[i].parameter ?? "")"] = "\(self.dynamicInfoStatusDataArray[i].value ?? "")"
        }
        
        print("Parameters are---",parameters)
        
        
        self.runValidations {
            
            if self.validated == true {
                
                print("Validated")
                
                self.saveProperty(parameters: parameters, urlString: "propertyEdit", completion: {
                    
                    self.navigationController?.popViewController(animated: true)
                    
                })
            }
            else {
                print("Not Validated.")
            }
        }
    }
    
    
    
    
    // MARK:- // JSON Post Method to Save Property
    
    func saveProperty(parameters : [String : String] , urlString : String , completion : @escaping () -> ()) {
        
        self.globalDispatchgroup.enter()
        
        startAnimating(CGSize(width: ((30.0/320.0)*FullWidth), height: ((30.0/320.0)*FullWidth)), message: "", messageFont: UIFont(name: "Lato-Bold", size: 14), type: .ballClipRotateMultiple, color: UIColor(displayP3Red: 0/255, green: 135/255, blue: 228/255, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.4), textColor: .black, fadeInAnimation: nil)
        
//        DispatchQueue.main.async {
//            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
//        }
        
        var imageDataArray = [Data]()
        for i in 0..<self.localImagesArray.count {
            imageDataArray.append(self.localImagesArray[i].jpegData(compressionQuality: 1)!)
        }
        
        
        
        Alamofire.upload(
            multipartFormData: { MultipartFormData in

                for (key, value) in parameters {
                    MultipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }

                if imageDataArray.count > 0 {
                    for i in 0..<imageDataArray.count {
                        MultipartFormData.append(imageDataArray[i], withName: "image[]", fileName: "sliderImage\(i).jpeg", mimeType: "image/jpeg")
                    }
                }
                

        }, to: (GLOBALAPI + urlString))
        { (result) in
            switch result {
            case .success(let upload, _, _):

                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })

                upload.responseJSON { response in
                    if response.response!.statusCode == 200{
                        print("original URL request---",response.request!)  // original URL request
                        print("URL response---",response.response!) // URL response
                        print("server data---",response.data!)     // server data
                        print(response.response!.statusCode)
                        print("result of response serialization---",response.result)   // result of response serialization

                        if let JSON = response.result.value {

                            print("Save Property Response-----: \(JSON)")

                            DispatchQueue.main.async {

                                self.globalDispatchgroup.leave()

                                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                                //SVProgressHUD.dismiss()
                                
                                completion()
                                
                            }
                        }
                        else {
                            DispatchQueue.main.async {

                                self.globalDispatchgroup.leave()

                                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                                //SVProgressHUD.dismiss()
                                
                            }
                        }
                    }
                    else{
                        self.customAlert(title: "Something wrong!", message: "Please try again later", doesCancel: false, completion: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                }

            case .failure(let encodingError):

                print(encodingError)

                DispatchQueue.main.async {

                    self.globalDispatchgroup.leave()

                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    //SVProgressHUD.dismiss()
                    
                }
            }

        }
        
    }
    
    
    
    
    // MARK:- // Run Validations
    
    func runValidations(completion : @escaping ()->()) {
        
       self.realEstateTypeTitleLBL.textColor = .black
        self.realEstateTypeTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "home_type"))
       self.subCategoryTitleLBL.textColor = .black
        self.subCategoryTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "app_msg5"))
       self.offerForTitleLBL.textColor = .black
        self.offerForTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "property_post_sub_category"))
       self.locationTitleLBL.textColor = .black
        self.locationTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "home_location"))
       self.priceTitleLBL.textColor = .black
        self.priceTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "property_details_price"))
       self.negotiablePriceTitleLBL.textColor = .black
        self.negotiablePriceTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "property_post_negotiable_price"))
       self.titleLBL.textColor = .black
        self.titleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "property_post_title"))
       self.descriptionTitleLBL.textColor = .black
        self.descriptionTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "home_description"))
       self.uploadImagesTitleLBL.textColor = .black
        self.uploadImagesTitleLBL.attributedText = globalFunctions.shared.asterixText(withText: globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_upload_image"))
        
        
        if "\(self.categoryNameID ?? "")".elementsEqual("") {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "please_select_type"))
            self.realEstateTypeTitleLBL.textColor = .red
        }
        else if "\(self.categoryTypeID ?? "")".elementsEqual("") {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "please_select_subcategory_type"))
            self.subCategoryTitleLBL.textColor = .red
        }
        else if "\(self.offerFor ?? "")".elementsEqual("") {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "home_select_offer"))
            self.offerForTitleLBL.textColor = .red
        }
        else if "\(self.countryID ?? "")".elementsEqual("") {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "my_profile_select_country"))
            self.locationTitleLBL.textColor = .red
        }
        else if "\(self.provinceID ?? "")".elementsEqual("") {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "property_post_province"))
            self.locationTitleLBL.textColor = .red
        }
        else if "\(self.cityID ?? "")".elementsEqual("") {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "property_post_city"))
            self.locationTitleLBL.textColor = .red
        }
        else if "\(self.districtID ?? "")".elementsEqual("") {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "property_post_district"))
            self.locationTitleLBL.textColor = .red
        }
//        else if "\(self.nearestPointTXT.text ?? "")".elementsEqual("") {
//            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "ios_please_enter_nearest_point"))
//        }
        else if "\(self.localityTXT.text ?? "")".elementsEqual("") {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "android_pls_ntr_lclty"))
            self.locationTitleLBL.textColor = .red
        }
        else if "\(self.priceTXT.text ?? "")".elementsEqual(""){
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "property_post_price_validation"))
            self.priceTitleLBL.textColor = .red
        }
        else if "\(self.currencyID ?? "")".elementsEqual("") {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "property_post_price_validation"))
            self.priceTitleLBL.textColor = .red
        }
        else if "\(self.negotiablePrice ?? "")".elementsEqual("") {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "please_check_negotiable_price"))
            self.negotiablePriceTitleLBL.textColor = .red
        }
        else if "\(self.titleTXT.text ?? "")".elementsEqual("") {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "please_enter_title"))
            self.titleLBL.textColor = .red
        }
        else if "\(self.titleTXT.text ?? "")".count > 60{
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_title_placeholder"))
        }
        else if "\(self.propertyDescription.text ?? "")".elementsEqual("\(globalFunctions.shared.getLanguageString(variable: "please_enter_description"))") {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "please_enter_description"))
            self.descriptionTitleLBL.textColor = .red
        }
//        else if "\(self.propertyDescription.text ?? "")".count > 500{
//            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_description_placeholder"))
//        }
        else if (self.webImagesArray.count + self.localImagesArray.count) == 0 {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_upload_image"))
            self.uploadImagesTitleLBL.textColor = .red
        }
        else if self.currency.text == currencyDefaultTxt {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "property_post_price_validation"))
            self.priceTitleLBL.textColor = .red
        }
        else {
            
            for i in 0..<self.featureHeaderArray.count {
                if self.featureHeaderArray[i].accessibilityIdentifier! == "1"{
                    self.featureHeaderArray[i].textColor = .black
                    //print("Accresability Identifier",self.featureHeaderArray[i].accessibilityIdentifier!)
                    self.featureHeaderArray[i].attributedText = globalFunctions.shared.asterixText(withText: "\(self.dynamicInfoFeaturesDataArray[i].fieldName ?? "")")
                }
                else{
                    self.featureHeaderArray[i].textColor = .black
                    self.featureHeaderArray[i].text = "\(self.dynamicInfoFeaturesDataArray[i].fieldName ?? "")"
                }
                
            }
            
            for i in 0..<self.statusHeaderArray.count {
                if self.statusHeaderArray[i].accessibilityIdentifier! == "1"{
                    self.statusHeaderArray[i].textColor = .black
                    //print("Accresability Identifier",self.statusHeaderArray[i].accessibilityIdentifier!)
                    self.statusHeaderArray[i].attributedText = globalFunctions.shared.asterixText(withText: "\(self.dynamicInfoStatusDataArray[i].fieldName ?? "")")
                }
                else{
                    self.statusHeaderArray[i].textColor = .black
                    self.statusHeaderArray[i].text = "\(self.dynamicInfoStatusDataArray[i].fieldName ?? "")"
                }
            }
            
            
            if self.dynamicInfoFeaturesDataArray.count > 0 {
                for i in 0..<self.dynamicInfoFeaturesDataArray.count {
                    if "\(self.dynamicInfoFeaturesDataArray[i].value ?? "")".elementsEqual("") && self.dynamicInfoFeaturesDataArray[i].fieldMadatoryStatus == "1" {
                        //print(self.dynamicInfoStatusDataArray[i].fieldMadatoryStatus!)
                        self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: "\(globalFunctions.shared.getLanguageString(variable: "app_enter")) \(self.dynamicInfoFeaturesDataArray[i].fieldName ?? "")")
                        self.featureHeaderArray[i].textColor = .red
                        self.validated = false
                        break
                    }
                    else {
                        self.validated = true
                    }
                }
            }
            else {
                self.validated = true
            }
            
            
            
            if self.validated {
                
                for i in 0..<self.dynamicInfoStatusDataArray.count {
                    
                    print("\(self.dynamicInfoStatusDataArray[i].fieldName ?? "") --- \(self.dynamicInfoStatusDataArray[i].value ?? "")")
                    
                    if "\(self.dynamicInfoStatusDataArray[i].value ?? "")".elementsEqual("") && self.dynamicInfoStatusDataArray[i].fieldMadatoryStatus == "1"{
                        //print(self.dynamicInfoStatusDataArray[i].fieldMadatoryStatus!)
                        self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: "\(globalFunctions.shared.getLanguageString(variable: "app_enter")) \(self.dynamicInfoStatusDataArray[i].fieldName ?? "")")
                        self.statusHeaderArray[i].textColor = .red
                        self.validated = false
                        break
                    }
                    else {
                        self.validated = true
                    }
                }
            }
            
            completion()
        }
        
    }
    
    
    // MARK:- // Functions to show location
    
    
    func showLocation(addressLat : String , addressLong : String)
    {
        
        self.locationMapView.clear()
        
        print("lat---\(addressLat)   and   long---\(addressLong)")
        
        let camera = GMSCameraPosition.camera(withLatitude: Double(addressLat)!, longitude: Double(addressLong)!, zoom: 12.0)
        
        let marker = GMSMarker()
        
        marker.position = CLLocationCoordinate2D(latitude: Double(addressLat)!, longitude: Double(addressLong)!)
        marker.map = self.locationMapView
        marker.isDraggable = true
        
        self.locationMapView.animate(to: camera)
        
    }
    
    
    
    
    
    // MARK:- // Reverse Geocoding to get address from Entered Lat Long
    
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        
        var addressString : String = ""
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if let pm = placemarks{
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print("Country---\(pm.country ?? "")")
                        print("Locality---\(pm.locality ?? "")")
                        print("Sublocality---\(pm.subLocality ?? "")")
                        print("Throughfate---\(pm.thoroughfare ?? "")")
                        print("PostalCode---\(pm.postalCode ?? "")")
                        print("SubThroughfare---\(pm.subThoroughfare ?? "")")
                        print("AdministrativeArea---\(pm.administrativeArea ?? "")")
                        
                        
                        if pm.subThoroughfare != nil {    // Street No
                            addressString = addressString + pm.subThoroughfare! + ", "
                        }
                        if pm.thoroughfare != nil {    //Street Name
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.subLocality != nil {   //Area
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        
                        if pm.locality != nil {     //City
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.administrativeArea != nil {   //State
                            addressString = addressString + pm.administrativeArea! + ", "
                        }
                        if pm.country != nil {    //Country
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {   //ZipCode
                            addressString = addressString + pm.postalCode! + " "
                        }
                        
                        self.localityTXT.text = addressString
                        
                    }
                }
                

        })
        
    }
    
    
    
    
    // MARK:- // Close all Textfields
    
    func closeAllTextfields(mainview: UIView)
    {
        for view in mainview.subviews
        {
            if view.isKind(of: UIView.self)
            {
                self.closeAllTextfields(mainview: view)
            }
            
            if view.isKind(of: UITextField.self)
            {
                (view as! UITextField).resignFirstResponder()
            }
        }
    }
    
    
    //MARK:- Get Index from product Id
    func getIndexFromProductId(productId: String) -> Int{
        var index: Int?
        for i in 0..<products.count{
            if self.subPlanId == products[i].productIdentifier{
                index = i
                break
            }
        }
        return index!
    }

}





// MARK:- // Pickerview Delegate Methods

extension AddNewAdVC: UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == self.realEstateTypePickerview {
            return self.userRealEstateTypeArray.count
        }
        else if pickerView == self.realEstateSubcategoryPickerview {
            return self.subCategoryArray.count
        }
        else if pickerView == self.countryPickerview {
            return self.countryArray.count
        }
        else if pickerView == self.provincePickerview {
            return self.provinceArray.count
        }
        else if pickerView == self.cityPickerview {
            return self.cityArray.count
        }
        else if pickerView == self.districtPickerview {
            return self.districtArray.count
        }
        else if pickerView == self.currencyPickerview{
            return self.currencyArray.count
        }
        else if pickerView == self.planDurationPickerview {
            return self.planDurationArray.count
        }
        else if pickerView == self.periodPickerview {
            return self.periodArray.count
        }
        else {
            if (pickerView.restorationIdentifier?.elementsEqual("features"))! {
                return self.dynamicInfoFeaturesDataArray[pickerView.tag].fieldOptionArray.count
            }
            else {
                return self.dynamicInfoStatusDataArray[pickerView.tag].fieldOptionArray.count
            }
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == self.realEstateTypePickerview {
            return "\(self.userRealEstateTypeArray[row].parentName ?? "")"
        }
        else if pickerView == self.realEstateSubcategoryPickerview {
            return "\(self.subCategoryArray[row].categoryName ?? "")"
        }
        else if pickerView == self.countryPickerview {
            return "\(self.countryArray[row].countryName ?? "")"
        }
        else if pickerView == self.provincePickerview {
            return "\((self.provinceArray[row])["title_eng"] ?? "")"
        }
        else if pickerView == self.cityPickerview {
            return "\((self.cityArray[row])["cityname"] ?? "")"
        }
        else if pickerView == self.districtPickerview {
            return "\((self.districtArray[row])["districtname"] ?? "")"
        }
        else if pickerView == self.currencyPickerview {
            return "\((self.currencyArray[row])["value"] ?? "")"
        }
        else if pickerView == self.planDurationPickerview {
            return "\(self.planDurationArray[row].planDuration ?? "")"
        }
        else if pickerView == self.periodPickerview {
            return "\((self.periodArray[row])["value"] ?? "")"
        }
        else {
            if (pickerView.restorationIdentifier?.elementsEqual("features"))! {
                return "\(self.dynamicInfoFeaturesDataArray[pickerView.tag].fieldOptionArray[row].value ?? "")"
            }
            else {
                return "\(self.dynamicInfoStatusDataArray[pickerView.tag].fieldOptionArray[row].value ?? "")"
            }
        }
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.realEstateTypePickerview {
            
            if self.doneClicked {
                
                self.realEstateType.text = "\(self.userRealEstateTypeArray[row].parentName ?? "")"
                
                self.categoryNameID = "\(self.userRealEstateTypeArray[row].parentID ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPickerview)
                self.viewOfPickerview.isHidden = true
                self.realEstateTypePickerview.isHidden = true
                
                self.subCategoryArray = self.userRealEstateTypeArray[row].childArray
                self.realEstateSubcategoryPickerview.reloadAllComponents()
                
                self.subCategory.text = globalFunctions.shared.getLanguageString(variable: "home_select")
                
            }
            else {
                self.selectedPickerIndex = row
            }
            
        }
        else if pickerView == self.realEstateSubcategoryPickerview {
            
            if self.doneClicked {
                
                self.subCategory.text = "\(self.subCategoryArray[row].categoryName ?? "")"
                
                self.categoryTypeID = "\(self.subCategoryArray[row].id ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPickerview)
                self.viewOfPickerview.isHidden = true
                self.realEstateSubcategoryPickerview.isHidden = true
                
                self.featureHeaderArray.removeAll()
                self.statusHeaderArray.removeAll()
                
                self.getDynamicFieldsData(propertyType: "\(self.subCategoryArray[row].id ?? "")")
                
                self.globalDispatchgroup.notify(queue: .main) {
                    
                    self.createDataDictionaries()
                    
                    for view in self.dynamicStackView.subviews {
                        view.removeFromSuperview()
                    }
                    
                    for i in 0..<self.pickerviewArray.count {
                        self.pickerviewArray[i].removeFromSuperview()
                    }
                    
                    self.pickerviewArray.removeAll()
                    self.pickerLBLArray.removeAll()
                    
                    self.createFeaturesStackview()
                    
                    self.createStatusStackview()
                    
                }
                
            }
            else {
                self.selectedPickerIndex = row
            }
            
        }
        else if pickerView == self.countryPickerview {
            
            if self.doneClicked {
                
                self.country.text = "\(self.countryArray[row].countryName ?? "")"
                
                self.countryID = "\(self.countryArray[row].id ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPickerview)
                self.viewOfPickerview.isHidden = true
                self.countryPickerview.isHidden = true
                
                self.provinceArray.removeAll()
                self.cityArray.removeAll()
                self.districtArray.removeAll()
                
                self.province.text = globalFunctions.shared.getLanguageString(variable: "home_select_province")
                self.city.text = globalFunctions.shared.getLanguageString(variable: "home_select_city")
                self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
                
                self.getProvinceList(countryID: "\(self.countryArray[row].id ?? "")")
                
            }
            else {
                self.selectedPickerIndex = row
            }
            
            
        }
        else if pickerView == self.provincePickerview {
            
            if self.doneClicked {
                
                self.province.text = "\((self.provinceArray[row])["title_eng"] ?? "")"
                
                self.provinceID = "\((self.provinceArray[row])["id"] ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPickerview)
                self.viewOfPickerview.isHidden = true
                self.provincePickerview.isHidden = true
                
                self.cityArray.removeAll()
                self.districtArray.removeAll()
                
                self.city.text = globalFunctions.shared.getLanguageString(variable: "home_select_city")
                self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
                
                self.getCityList(provinceID: "\((self.provinceArray[row])["id"] ?? "")")
                
            }
            else {
                self.selectedPickerIndex = row
            }
            
            
        }
        else if pickerView == self.cityPickerview {
            
            if self.doneClicked {
                
                self.city.text = "\((self.cityArray[row])["cityname"] ?? "")"
                
                self.cityID = "\((self.cityArray[row])["id"] ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPickerview)
                self.viewOfPickerview.isHidden = true
                self.cityPickerview.isHidden = true
                
                self.districtArray.removeAll()
                
                self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
                
                self.getDistrictList(cityID: "\((self.cityArray[row])["id"] ?? "")")
                
            }
            else {
                self.selectedPickerIndex = row
            }
            
        }
        else if pickerView == self.districtPickerview {
            
            if self.doneClicked {
                
                self.district.text = "\((self.districtArray[row])["districtname"] ?? "")"
                
                self.districtID = "\((self.districtArray[row])["id"] ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPickerview)
                self.viewOfPickerview.isHidden = true
                self.districtPickerview.isHidden = true
                
            }
            else {
                self.selectedPickerIndex = row
            }
            
            
        }
        else if pickerView == self.currencyPickerview{
            
            if self.doneClicked {
                
                self.currency.text = "\((self.currencyArray[row])["value"] ?? "")"
                self.currencyID = "\((self.currencyArray[row])["key"] ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPickerview)
                self.viewOfPickerview.isHidden = true
                self.currencyPickerview.isHidden = true
                
            }
            else {
                self.selectedPickerIndex = row
            }
            
            
        }
        else if pickerView == self.planDurationPickerview {
            
            if self.doneClicked {
                
                self.planDataArray[pickerView.tag].planDuration = "\(self.planDurationArray[row].planDuration ?? "")"  + " \(self.planDataArray[pickerView.tag].planValidUnit ?? "")"
                self.planDataArray[pickerView.tag].planCost = "=\(self.planDurationArray[row].planCost ?? "")$"
                self.planDataArray[pickerView.tag].planPriceID = "\(self.planDurationArray[row].planPriceID ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPickerview)
                self.viewOfPickerview.isHidden = true
                self.planDurationPickerview.isHidden = true
                
                self.adsCostTableview.reloadData()
                
            }
            else {
                self.selectedPickerIndex = row
            }
            
            
            
        }
        else if pickerView == self.periodPickerview {
            
            if self.doneClicked {
                
                self.rentPeriod.text = "\((self.periodArray[row])["value"] ?? "")"
                
                self.periodID = "\((self.periodArray[row])["key"] ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPickerview)
                self.viewOfPickerview.isHidden = true
                self.periodPickerview.isHidden = true
                
            }
            else {
                self.selectedPickerIndex = row
            }
            
            
        }
        else {
            
            if self.doneClicked {
                
                if (pickerView.restorationIdentifier?.elementsEqual("features"))! {
                    for i in 0..<self.pickerLBLArray.count {
                        if self.pickerLBLArray[i].tag == pickerView.tag {
                            self.pickerLBLArray[i].textColor = .black
                            self.pickerLBLArray[i].text = "\(self.dynamicInfoFeaturesDataArray[pickerView.tag].fieldOptionArray[row].value ?? "")"
                            
                            for j in 0..<self.dynamicInfoFeaturesDataArray.count {
                                if self.pickerLBLArray[i].tag == j {
                                    self.dynamicInfoFeaturesDataArray[j].value = "\(self.dynamicInfoFeaturesDataArray[pickerView.tag].fieldOptionArray[row].key ?? "")"
                                    
                                }
                            }
                            
                            self.pickerviewArray[i].isHidden = true
                            break
                        }
                    }
                    
                }
                else {
                    for i in 0..<self.pickerLBLArray.count {
                        if self.pickerLBLArray[i].tag == pickerView.tag {
                            self.pickerLBLArray[i].textColor = .black
                            self.pickerLBLArray[i].text = "\(self.dynamicInfoStatusDataArray[pickerView.tag].fieldOptionArray[row].value ?? "")"
                            
                            for j in 0..<self.dynamicInfoStatusDataArray.count {
                                if self.pickerLBLArray[i].tag == j {
                                    self.dynamicInfoStatusDataArray[j].value = "\(self.dynamicInfoStatusDataArray[pickerView.tag].fieldOptionArray[row].key ?? "")"
                                    
                                }
                            }
                            
                            self.pickerviewArray[i].isHidden = true
                            break
                        }
                    }
                }
                
                self.view.sendSubviewToBack(self.viewOfPickerview)
                self.viewOfPickerview.isHidden = true
                
            }
            else {
                self.selectedPickerIndex = row
            }
            
        }
        
        
        
    }
    
}






// MARK:- // Collectionview Delegates

extension AddNewAdVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.uploadImagesCollectionview {
            return (self.webImagesArray.count + self.localImagesArray.count + 1)
        }
        else {
            
            
            if (collectionView.restorationIdentifier?.elementsEqual("features"))! {
                return self.dynamicInfoFeaturesDataArray[collectionView.tag].fieldOptionArray.count
            }
            else {
                return self.dynamicInfoStatusDataArray[collectionView.tag].fieldOptionArray.count
            }
            
            
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.uploadImagesCollectionview {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageSlideCVC", for: indexPath) as! imageSlideCVC
            
            cell.cellImage.layer.borderWidth = 0.5
            cell.cellImage.layer.borderColor = UIColor.lightGray.cgColor
            cell.cellImage.contentMode = .scaleAspectFit
            
            let tempcount = self.webImagesArray.count + self.localImagesArray.count + 1
            
            if indexPath.item == (tempcount - 1) {
                cell.cellImage.image = UIImage(named: "add image")
                
                cell.deleteCheckImage.isHidden = true
            }
            else {
                if indexPath.item < self.webImagesArray.count {
                    cell.cellImage.sd_setImage(with: URL(string: "\((self.webImagesArray[indexPath.row])["image_name"] ?? "")"))
                    
                    
                    cell.deleteCheckImage.isHidden = true
                    for i in 0..<self.webImagesToDeleteArray.count {
                        if self.webImagesToDeleteArray[i] == indexPath.row {
                            cell.deleteCheckImage.isHidden = false
                        }
                    }
                    
                }
                else {
                    cell.cellImage.image = self.localImagesArray[indexPath.item - self.webImagesArray.count]
                    
                    cell.deleteCheckImage.isHidden = true
                    for i in 0..<self.localImagesToDeleteArray.count {
                        if self.localImagesToDeleteArray[i] == (indexPath.row - self.webImagesArray.count) {
                            cell.deleteCheckImage.isHidden = false
                        }
                    }
                }
            }
            
            return cell
        }
        else {
            
            var dataArray : [addPropertyDynamicDataFormat]!
            
            if (collectionView.restorationIdentifier?.elementsEqual("features"))! {
                dataArray = self.dynamicInfoFeaturesDataArray
            }
            else {
                dataArray = self.dynamicInfoStatusDataArray
            }
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "checkboxCVC", for: indexPath)
            
            for view in cell.subviews {
                view.removeFromSuperview()
            }
            
            let checkboxImage : UIImageView = {
               let checkboximage = UIImageView()
                checkboximage.contentMode = .scaleAspectFit
                checkboximage.translatesAutoresizingMaskIntoConstraints = false
                return checkboximage
            }()
            
            cell.addSubview(checkboxImage)
            
            checkboxImage.anchor(top: cell.topAnchor, leading: cell.leadingAnchor, bottom: cell.bottomAnchor, trailing: nil, padding: .init(top: 10, left: 5, bottom: 10, right: 0), size: .init(width: 25, height: 25))
            
            
            let cellLBL : UILabel = {
                let celllbl = UILabel()
                celllbl.numberOfLines = 0
                celllbl.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 11)))
                celllbl.translatesAutoresizingMaskIntoConstraints = false
                return celllbl
            }()
            
            cell.addSubview(cellLBL)
            
            cellLBL.anchor(top: cell.topAnchor, leading: checkboxImage.trailingAnchor, bottom: cell.bottomAnchor, trailing: cell.trailingAnchor, padding: .init(top: 0, left: 5, bottom: 0, right: 10))
            
            
            
            if (collectionView.accessibilityIdentifier?.elementsEqual("checkbox"))! {
                
                if dataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected == true {
                    checkboxImage.image = UIImage(named: "checkbox2")
                }
                else {
                    checkboxImage.image = UIImage(named: "Rectangle 22")
                }
            }
            else {
                
                if dataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected == true {
                    checkboxImage.image = UIImage(named: "select black")
                }
                else {
                    checkboxImage.image = UIImage(named: "non select")
                }
            }

            cellLBL.text = "\(dataArray[collectionView.tag].fieldOptionArray[indexPath.item].value ?? "")"
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.uploadImagesCollectionview {
            let cellWidth = ((self.FullWidth - 20) - 30) / 4
            
            return CGSize(width: cellWidth, height: cellWidth)
        }
        else {
            return CGSize(width: (self.FullWidth - 20) / 2, height: 45)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == uploadImagesCollectionview {
            return 10
        }
        else {
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == uploadImagesCollectionview {
            return 10
        }
        else {
            return 0
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == uploadImagesCollectionview {
            let tempcount = self.webImagesArray.count + self.localImagesArray.count + 1
            
            if indexPath.item == (tempcount - 1) {
                self.openImagepicker()
            }
        }
        else {
            
            if (collectionView.accessibilityIdentifier?.elementsEqual("checkbox"))! {
                
                if (collectionView.restorationIdentifier?.elementsEqual("features"))! {
                     self.dynamicInfoFeaturesDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected = !self.dynamicInfoFeaturesDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected
                    print(self.dynamicInfoFeaturesDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected!)
                }
                else {
                    self.dynamicInfoStatusDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected = !self.dynamicInfoStatusDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected
                }
                
            }
            else {
                
                if (collectionView.restorationIdentifier?.elementsEqual("features"))! {
                    
                    for i in 0..<self.dynamicInfoFeaturesDataArray[collectionView.tag].fieldOptionArray.count {
                        self.dynamicInfoFeaturesDataArray[collectionView.tag].fieldOptionArray[i].isSelected = false
                    }
                    
                    self.dynamicInfoFeaturesDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected = !self.dynamicInfoFeaturesDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected
                }
                else {
                    
                    for i in 0..<self.dynamicInfoStatusDataArray[collectionView.tag].fieldOptionArray.count {
                        self.dynamicInfoStatusDataArray[collectionView.tag].fieldOptionArray[i].isSelected = false
                    }
                    
                    self.dynamicInfoStatusDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected = !self.dynamicInfoStatusDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected
                }
                
            }
            
            collectionView.reloadData()
        }
        
    }
    
}





// MARK:- // Imagepicker Delegate Methods

extension AddNewAdVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        
        self.localImagesArray.append(image)
        
        self.uploadImagesCollectionview.reloadData()
        
        picker.dismiss(animated: true, completion: nil)
        
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
}



// MARK:- // Tableview Delegate Methods

extension AddNewAdVC: UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.planDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "addRealEstatePlanTypeTVC") as! addRealEstatePlanTypeTVC
        
        cell.planDaysView.layer.cornerRadius = 5
        cell.planDaysView.layer.borderColor = UIColor.lightGray.cgColor
        cell.planDaysView.layer.borderWidth = 0.5
        
        if self.planDataArray[indexPath.row].isSelected == true {
            cell.checkBoxButton.setImage(UIImage(named: "checkbox2"), for: .normal)
            cell.planDurationButton.isUserInteractionEnabled = true
        }
        else {
            cell.checkBoxButton.setImage(UIImage(named: "Rectangle 22"), for: .normal)
            cell.planDurationButton.isUserInteractionEnabled = false
        }
        
        if (self.planDataArray[indexPath.row].planID?.elementsEqual("1"))! {
            
            cell.planName.text = "\(self.planDataArray[indexPath.row].planName ?? "")" + " (" + "\(self.planDataArray[indexPath.row].planPriceList[0].planDuration ?? "") \(self.planDataArray[indexPath.row].planValidUnit ?? "")" + ")"
            
            cell.planDurationButton.isHidden = true
            cell.planDaysView.isHidden = true
            cell.planCost.isHidden = true
            
            cell.planNameWidthCOnstraint.constant = 100
            
        }
        else {
            
            cell.planName.text = "\(self.planDataArray[indexPath.row].planName ?? "")"
            
            cell.planDays.text = "\(self.planDataArray[indexPath.row].planDuration ?? "")"
            
            cell.planCost.text = "\(self.planDataArray[indexPath.row].planCost ?? "")"
            
            cell.planDurationButton.isHidden = false
            cell.planDaysView.isHidden = false
            cell.planCost.isHidden = false
            
            cell.planNameWidthCOnstraint.constant = 65
            
        }
        
        cell.checkBoxButton.addTarget(self, action: #selector(self.planTypeButtonAction(sender:)), for: .touchUpInside)
        cell.checkBoxButton.tag = indexPath.row
        
        cell.planDurationButton.addTarget(self, action: #selector(self.planDurationButtonAction(sender:)), for: .touchUpInside)
        cell.planDurationButton.tag = indexPath.row
        
        
        //
        cell.selectionStyle = .none
        
        //SET FONT
        cell.planName.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        cell.planDays.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        cell.planCost.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
        
    }
    
}




// MARK:- // Textfield Delegate Functions

extension AddNewAdVC: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.localityTXT {
            let autoCompleteController = GMSAutocompleteViewController()
            autoCompleteController.delegate = self
            
            let filter = GMSAutocompleteFilter()
            autoCompleteController.autocompleteFilter = filter
            
            self.locationManager.startUpdatingLocation()
            self.present(autoCompleteController, animated: true, completion: nil)
            
            return false
            
        }
        else if textField == self.priceTXT {
            return true
        }
        else if textField == self.titleTXT {
            return true
        }
        else if textField == self.youtubeLinkTXT{
            return true
        }
        else {
            return false
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.youtubeLinkTXT{
            if let myVideoURL = NSURL(string: self.youtubeLinkTXT.text ?? ""){
                self.businessVideo.loadVideoURL(myVideoURL as URL)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.priceTXT {

//            let formatter = NumberFormatter()
//            formatter.numberStyle = .decimal
//            formatter.maximumFractionDigits = 8
//            // Combine the new text with the old; then remove any
//            // commas from the textField before formatting
//            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
//            let numberWithOutCommas = newString.replacingOccurrences(of: ",", with: "")
//            let number = formatter.number(from: numberWithOutCommas)
//            if number != nil {
//                var formattedString = formatter.string(from: number!)
//                // If the last entry was a decimal or a zero after a decimal,
//                // re-add it here because the formatter will naturally remove
//                // it.
//                if string == "." && range.location == textField.text?.count {
//                    formattedString = formattedString?.appending(".")
//                }
//                textField.text = formattedString
//            } else {
//                textField.text = nil
//            }
//
//            return false
            
            
            
//            let tempprice = (textField.text! as NSString).replacingCharacters(in: range, with: string)
//            let price = String(tempprice.reversed())
//            var pattern : String! = ""
//
//            for i in 0..<price.count {
//
//                if i == 0 {
//                    pattern = pattern + "#"
//                }
//                else {
//                    if i % 3 == 0 {
//                        pattern = pattern + ",#"
//                    }
//                    else {
//                        pattern = pattern + "#"
//                    }
//                }
//
//            }
//
//            var result = "\(String(price.applyPatternOnNumbers(pattern: pattern).reversed()))"
//
//
//            if result != nil {
//
//                if result.prefix(1).elementsEqual(",") {
//
//                    result.remove(at: result.startIndex)
//
//                    textField.text = result
//
//                }
//                else {
//                    textField.text = result
//                }
//
//            }
//            else {
//                textField.text = nil
//            }
//
//            return false
            
            let result = (textField.text! as NSString).replacingCharacters(in: range, with: string).creatingPriceStringLikeDecimal()
            
            textField.text = result
            
//            if result != nil {
//                textField.text = result
//            }
//            else {
//                textField.text = nil
//            }
            
            return false

        }
        
        
        else if textField == self.localityTXT {
            return false
        }
        else if textField == self.titleTXT {
            
            let newText = (self.titleTXT.text! as NSString).replacingCharacters(in: range, with: string)
            let numberOfChars = newText.count
            
            return numberOfChars < 600
            
            //return true
        }
        else if textField == self.youtubeLinkTXT{
            return true
        }
        else {
            return false
        }
        
    }
    
    
    
    
    
    
    
    
}




// MARK:- // Textview Delegates

extension AddNewAdVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView == self.propertyDescription {
            
            let temptxt = globalFunctions.shared.getLanguageString(variable: "please_enter_description")
            
            if textView.text!.elementsEqual(temptxt) {
                textView.text = ""
                textView.textColor = .black
            }
            
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView == self.propertyDescription {
            
            if textView.text!.elementsEqual("") {
                textView.text = globalFunctions.shared.getLanguageString(variable: "please_enter_description")
                textView.textColor = .lightGray
            }
            
        }
        
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView == self.propertyDescription {
            
            let newText = (self.propertyDescription.text! as NSString).replacingCharacters(in: range, with: text)
            let numberOfChars = newText.count
            if numberOfChars == 500{
                self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_description_placeholder"))
            }
            return numberOfChars < 501
            
        }
        else {
            return false
        }
//        else {
//
//            let newText = (self.propertyDescription.text! as NSString).replacingCharacters(in: range, with: text)
//            let numberOfChars = newText.count
//
//            return numberOfChars < 1000
//
//        }
        
    }
    
}
    
    
    



// MARK:- // GMS Autocomplete Delegate Functions

extension AddNewAdVC: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        let lat = place.coordinate.latitude
        let long = place.coordinate.longitude
        
        self.userLat = "\(lat)"
        self.userLong = "\(long)"
        
        self.showLocation(addressLat: self.userLat, addressLong: self.userLong)
        self.localityTXT.text=place.formattedAddress
        
//        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 10.0)
//        self.locationMapView.camera = camera
//        self.nearestPointTXT.text=place.formattedAddress
//        chosenPlace = MyPlace(name: place.formattedAddress!, lat: lat, long: long)
//
//        self.userLat = "\(chosenPlace?.lat ?? 00.00)"
//        self.userLong = "\(chosenPlace?.long ?? 00.00)"
//
//        //let marker=GMSMarker()
//        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
//        //        marker.title = "\(place.name)"
//        //        marker.snippet = "\(place.formattedAddress!)"
//        marker.map = self.locationMapView
        
        self.dismiss(animated: true, completion: nil) // dismiss after place selected
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("ERROR AUTO COMPLETE \(error)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}


// MARK:- //

extension AddNewAdVC: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        
        self.getAddressFromLatLon(pdblLatitude: "\(marker.position.latitude)", withLongitude: "\(marker.position.longitude)")
        
        self.userLat = "\(marker.position.latitude)"
        self.userLong = "\(marker.position.longitude)"
        
        //self.showLocation(addressLat: self.userLat, addressLong: self.userLong)
    }
    
}











// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}


struct DotNum {
    private var fraction:String = ""
    private var intval:String = ""
    init() {}
    mutating func enter(_ s:String) {
        if fraction.count < 2 {
            fraction = s + fraction
        } else {
            intval = s + intval
        }
    }
    private var sFract:String {
        if fraction.count == 0 { return "00" }
        if fraction.count == 1 { return "0\(fraction)" }
        return fraction
    }
    var stringVal:String {
        //if intval == ""  { return "0.\(sFract)" }
        //return "\(intval).\(sFract)"
        if intval == ""  { return "\(sFract)" }
        return "\(sFract)"
    }
}
