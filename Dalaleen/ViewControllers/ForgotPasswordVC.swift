//
//  ForgotPasswordVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 08/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView

class ForgotPasswordVC: GlobalViewController {
    
    @IBOutlet weak var forgotPasswordTitleLBL: UILabel!
    @IBOutlet weak var forgotPasswordSubtitleLBL: UILabel!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var countryFlag: UIImageView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var mobileNoView: UIView!
    @IBOutlet weak var mobileNoTXT: UITextField!
    
    @IBOutlet weak var countryListView: UIView!
    @IBOutlet weak var chooseCountryTitleLBL: UILabel!
    @IBOutlet weak var countryListTable: UITableView!
    
    var CountryCodeArray = countryPhoneCodes.shared.countryPhnCodes
    
    var phoneCode : String! = ""
    
    var forgotPasswordResponseDictionary : NSDictionary!
    
    
    // MARK:- // View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()

        self.SetFont()
        
        self.localizeStrings()
        
        self.countryView.layer.cornerRadius = self.countryView.frame.size.height / 2
        self.mobileNoView.layer.cornerRadius = self.mobileNoView.frame.size.height / 2
        self.submitButtonOutlet.layer.cornerRadius = self.submitButtonOutlet.frame.size.height / 2
        
        self.countryListTable.delegate = self
        self.countryListTable.dataSource = self
        self.countryListTable.reloadData()
    }
    
    
    // MARK:- // Buttons
    
    // MARK:- // Back Button
    
    @IBOutlet weak var backButtonOutlet: UIButton!
    @IBAction func backButton(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    // MARK:- // Open Country Dropdown Button
    
    @IBOutlet weak var openCountryDropdownButtonOutlet: UIButton!
    @IBAction func openCountryDropdown(_ sender: UIButton) {
        
        if self.countryListView.isHidden == true {
            self.view.bringSubviewToFront(self.countryListView)
            self.countryListView.isHidden = false
        }
        else {
            self.view.sendSubviewToBack(self.countryListView)
            self.countryListView.isHidden = true
        }
        
    }
    
    
    
    
    // MARK:- // Submit Button
    
    @IBOutlet weak var submitButtonOutlet: UIButton!
    @IBAction func submitButton(_ sender: UIButton) {
        
        if (self.countryName.text?.elementsEqual("\(globalFunctions.shared.getLanguageString(variable: "my_profile_select_country"))"))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "my_profile_select_country"))
        }
        else if (self.mobileNoTXT.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: "Warning!", message: globalFunctions.shared.getLanguageString(variable: "registration_enter_mobile_number"))
        }
        else {
            
            self.forgotPassword()
            
        }
    }
    
    
    
    
    // MARK:- // Close Country List VIew Button
    
    @IBOutlet weak var closeCountryListViewButtonOutlet: UIButton!
    @IBAction func closeCountryListViewButton(_ sender: UIButton) {
        
        self.view.sendSubviewToBack(self.countryListView)
        self.countryListView.isHidden = true
        
    }
    
    
    
    // MARK:- // JSON Post Method to Send Forgot Password Request
    
    func forgotPassword() {
        
        var parameters : String = ""
        
        let userPhoneNumber = self.countryCode.text! + self.mobileNoTXT.text!
        
        parameters = "number=\(userPhoneNumber)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "forgotpassword", param: parameters) {
            
            self.forgotPasswordResponseDictionary = self.globalJson
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
                self.customAlert(title: "", message: "\(self.globalJson["message"] ?? "")", doesCancel: false, completion: {
                    
                    let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
                    
                    navigate.phoneNumber = self.mobileNoTXT.text
                    navigate.phoneCode = self.phoneCode
                    
                    self.navigationController?.pushViewController(navigate, animated: true)
                    
                })
                
            }
            
        }
        
    }
    
    
    
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        self.forgotPasswordTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "forgotpass_text1")
        self.forgotPasswordSubtitleLBL.text = globalFunctions.shared.getLanguageString(variable: "forgotpass_text2")
        self.countryName.text = globalFunctions.shared.getLanguageString(variable: "my_profile_select_country")
        self.countryName.textColor = .gray
        self.mobileNoTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "registration_enter_mobile_number")
        self.submitButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "resetpass_submit_msg"), for: .normal)
    }
    
    
    
    // MARK:- // Set Font
    
    func SetFont() {
        self.forgotPasswordTitleLBL.font = UIFont(name: self.forgotPasswordTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        self.forgotPasswordSubtitleLBL.font = UIFont(name: self.forgotPasswordSubtitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        self.countryName.font = UIFont(name: self.countryName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.countryCode.font = UIFont(name: self.countryCode.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.mobileNoTXT.font = UIFont(name: self.mobileNoTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.submitButtonOutlet.titleLabel!.font = UIFont(name: self.submitButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
    }
    
    
    
}


// MARK:- // Tableview Delegate Functions

extension ForgotPasswordVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return CountryCodeArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "countryAndPhoneCodeTVC") as! countryAndPhoneCodeTVC
        
        let mydict = CountryCodeArray[indexPath.row]
        
        cell.countryName.text = "\(mydict.countryName ?? "")"
        cell.countryPhoneCode.text = "+ " + "\(mydict.phoneCode ?? "")"
        
        cell.countryImage.sd_setImage(with: URL(string: "\(mydict.countryFlag ?? "")"))
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let mydict = CountryCodeArray[indexPath.row]
        
        self.countryName.text = "\(mydict.countryName ?? "")"
        self.countryCode.text = "+ " + "\(mydict.phoneCode ?? "")"
        self.countryFlag.sd_setImage(with: URL(string: "\(mydict.countryFlag ?? "")"))
        
        self.phoneCode = "\(mydict.phoneCode ?? "")"
        
        self.view.sendSubviewToBack(self.countryListView)
        self.countryListView.isHidden = true
        
    }
    
}
