//
//  SelectCountryAndLanguageVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 09/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SelectCountryAndLanguageVC: GlobalViewController {

    @IBOutlet weak var headerLBL: UILabel!
    @IBOutlet weak var whereToSearchTitleLBL: UILabel!
    @IBOutlet weak var selectCountryView: UIView!
    @IBOutlet weak var selectCountryTitleLBL: UILabel!
    @IBOutlet weak var countryImageview: UIImageView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var selectLanguageView: UIView!
    @IBOutlet weak var selectLanguageTitleLBL: UILabel!
    @IBOutlet weak var languageName: UILabel!
    
    
    @IBOutlet weak var viewOfTable: UIView!
    @IBOutlet weak var choiceTitleLBL: UILabel!
    @IBOutlet weak var listTable: UITableView!
    
    //var langDataArray = [languageDataForm]()
    
    var langDictionary = NSDictionary()
    
    var tableType : String! = "C"
    
    var CountryCodeArray = [countryAndPhoneCodes]()//countryPhoneCodes.shared.countryPhnCodes
    
    var LanguageListArray = [languageListDataFormat]()//languageList.shared.languageListArray
    
    var langName : String! = "\(UserDefaults.standard.string(forKey: "searchLanguageName") ?? "")"
    
    var langCode : String! = "\(UserDefaults.standard.string(forKey: "searchLanguageCode") ?? "")"
    
    var countryCode : String! = "\(UserDefaults.standard.string(forKey: "searchCountryID") ?? "")"
    
    var countryNameString : String! = "\(UserDefaults.standard.string(forKey: "searchCountryName") ?? "")"
    
    var countryFlag : String! = "\(UserDefaults.standard.string(forKey: "searchCountryFlag") ?? "")"
    
    
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.selectCountryView.isHidden = true
        
        self.prefillData()
        
        self.headerView.contentVIew.backgroundColor = .clear

        self.localizeStrings()
        
        self.SetFont()
        
        self.loadLanguageData()
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            self.loadCountryCodes()
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                self.listTable.delegate = self
                self.listTable.dataSource = self
                
            })
            
        }
        
        if ((self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 2])?.isKind(of: SplashscreenVC.self))! {
            self.headerView.isHidden = true
        }
    }
    
    
    
    // MARK:- // Prefill Data
    
    func prefillData() {
       
        self.countryName.text = "\(UserDefaults.standard.string(forKey: "searchCountryName") ?? "")"
        
        self.countryImageview.sd_setImage(with: URL(string: "\(UserDefaults.standard.string(forKey: "searchCountryFlag") ?? "")"))
        
        self.languageName.text = "\(UserDefaults.standard.string(forKey: "searchLanguageName") ?? "")"
        
    }
    
    
    
    // MARK:- // Buttons
    
    // MARK:- // Select Country Button
    
    @IBOutlet weak var selectCountryButtonOutlet: UIButton!
    @IBAction func selectCountryButton(_ sender: UIButton) {
        
        if self.viewOfTable.isHidden == true {
            self.view.bringSubviewToFront(self.viewOfTable)
            self.viewOfTable.isHidden = false
        }
        else {
            self.view.sendSubviewToBack(self.viewOfTable)
            self.viewOfTable.isHidden = true
        }
        
        self.tableType = "C"
        
        self.listTable.reloadData()
        
        self.choiceTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_select_country")
        
    }
    
    
    // MARK:- // Select Language Button
    
    @IBOutlet weak var selectlanguageButtonOutlet: UIButton!
    @IBAction func selectLanguageButton(_ sender: UIButton) {
        
        if self.viewOfTable.isHidden == true {
            self.view.bringSubviewToFront(self.viewOfTable)
            self.viewOfTable.isHidden = false
        }
        else {
            self.view.sendSubviewToBack(self.viewOfTable)
            self.viewOfTable.isHidden = true
        }
        
        self.tableType = "L"
        
        self.listTable.reloadData()
        
        self.choiceTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "app_select_language")
        
    }
    
    
    // MARK:- // Enter Button
    
    @IBOutlet weak var enterButtonOutlet: UIButton!
    @IBAction func enterButton(_ sender: UIButton) {
        
        if (self.countryName.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "my_profile_select_country"))
        }
        else if (self.languageName.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "app_select_language"))
        }
        else {
            
            //UserDefaults.standard.set(false, forKey: "firstLaunch")
            
            
            // SEt Language
            UserDefaults.standard.set("\(self.langCode ?? "")", forKey: "searchLanguageCode")
            UserDefaults.standard.set("\(self.langName ?? "")", forKey: "searchLanguageName")
            
            userInformation.shared.setSearchLanguageCode(code: UserDefaults.standard.string(forKey: "searchLanguageCode")!)
            userInformation.shared.setSearchLanguageName(name: UserDefaults.standard.string(forKey: "searchLanguageName")!)
            
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "\(self.langCode ?? "")")
            
            
            // SEt COuntry
            UserDefaults.standard.set("\(self.countryCode ?? "")", forKey: "searchCountryID")
            UserDefaults.standard.set("\(self.countryNameString ?? "")", forKey: "searchCountryName")
            UserDefaults.standard.set("\(self.countryFlag ?? "")", forKey: "searchCountryFlag")
            
            userInformation.shared.setSearchCountryID(id: UserDefaults.standard.string(forKey: "searchCountryID")!)
            userInformation.shared.setSearchCountryName(name: UserDefaults.standard.string(forKey: "searchCountryName")!)
            userInformation.shared.setSearchCountryFlag(flag: UserDefaults.standard.string(forKey: "searchCountryFlag")!)
            
            
            
            
            
            self.getLanguageStrings(langCode: UserDefaults.standard.string(forKey: "searchLanguageCode")!)
            
            LocalizationSystem.sharedInstance.setLanguage(languageCode: UserDefaults.standard.string(forKey: "searchLanguageCode")!)
            
            self.globalDispatchgroup.notify(queue: .main) {
                
                self.loadCountryCodes()
                
                self.globalDispatchgroup.notify(queue: .main, execute: {
                    
                    self.loadLanguageData()
                    
                    self.globalDispatchgroup.notify(queue: .main, execute: {
                        
//                        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                        
                        //let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "AppStartSearchVC") as! AppStartSearchVC
                        
                        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        
                        self.navigationController?.pushViewController(navigate, animated: true)
                        
                    })
                    
                })
                
            }
            
        }
        
    }
    
    
    // MARK:- // Close View of Tableview
    
    @IBOutlet weak var closeViewOfTableButtonOutlet: UIButton!
    @IBAction func closeViewOfTable(_ sender: UIButton) {
        
        self.view.sendSubviewToBack(self.viewOfTable)
        self.viewOfTable.isHidden = true
        
    }
    
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.whereToSearchTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "app_where_to_search")
        self.selectCountryTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_select_country")
        //self.countryName.text = ""//globalFunctions.shared.getLanguageString(variable: "my_profile_select_country")
        self.selectLanguageTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "app_select_language")
        //self.languageName.text = ""//globalFunctions.shared.getLanguageString(variable: "app_select_language")
        self.enterButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "app_enter"), for: .normal)
        
    }
    
    
    // MARK:- // Set Font
    
    func SetFont() {
        
        self.headerLBL.font = UIFont(name: self.headerLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.whereToSearchTitleLBL.font = UIFont(name: self.whereToSearchTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 16)))
        self.selectCountryTitleLBL.font = UIFont(name: self.selectCountryTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.countryName.font = UIFont(name: self.countryName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.selectLanguageTitleLBL.font = UIFont(name: self.selectLanguageTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.languageName.font = UIFont(name: self.languageName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.choiceTitleLBL.font = UIFont(name: self.choiceTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.enterButtonOutlet.titleLabel!.font = UIFont(name: self.enterButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
    }
    
    
    
    // MARK:- // JSON Post Method to get language strings
    
    func getLanguageStrings(langCode : String) {
        
        //let langID : String? = LocalizationSystem.sharedInstance.getLanguage()
        
        //UserDefaults.standard.set(langID, forKey: "DeviceLanguageID")
        
        //print("Device Language Code : ",langID!)
        
        var parameters : String!
        
        parameters = "language_code=\(langCode)"
        
        self.CallAPI(urlString: "languageConvert_test", param: parameters) {
            
//            for i in 0..<(self.globalJson["info"] as! NSArray).count {
//                self.langDataArray.append(languageDataForm(variables: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["Variables"] ?? "")", defaultText: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["Default Text"] ?? "")", languageText: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["Language Text"] ?? "")"))
//            }
//
//            languageData.shared.storeLanguageData(langData: self.langDataArray)
            
            
            self.langDictionary = self.globalJson["info"] as! NSDictionary
            
            languageData.shared.storeLanguageData(langData: self.langDictionary)
            
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
            }
            
        }
        
    }
    
    
    
    // MARK: - // JSON Get Method to get PhoneCode and Country Data from Web
    
    
    func loadCountryCodes()
        
    {
        
        let parameters = "language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "country", param: parameters) {
            
            var countryCodeArray = [countryAndPhoneCodes]()
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                countryCodeArray.append(countryAndPhoneCodes(countryFlag: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["counrty_flag"] ?? "")", countryName: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["country_name"] ?? "")", countryCode: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["country_code"] ?? "")", id: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["id"] ?? "")", languageCode: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["language_code"] ?? "")", phoneCode: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["phone_code"] ?? "")", appSearchStatus: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["appsearch_status"] ?? "")"))
            }
            
            countryPhoneCodes.shared.storeCountryPhoneCodesData(countryPhoneCodeData: countryCodeArray)
            
            self.CountryCodeArray = countryPhoneCodes.shared.countryPhnCodes
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
            }
            
        }
    }
    
    
    
    // MARK: - // JSON Get Method to get Language Data from Web
    
    
    func loadLanguageData()
        
    {
        
        let parameters = "language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "language", param: parameters) {
            
            var langArray = [languageListDataFormat]()
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                langArray.append(languageListDataFormat(id: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["id"] ?? "")", langName: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["lang_name"] ?? "")", langCode: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["language_code"] ?? "")", langImage: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["lang_icon"] ?? "")", langStatus: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["status"] ?? "")"))
            }
            
            
            languageList.shared.storeCountryPhoneCodesData(langArray: langArray)
            
            self.LanguageListArray = languageList.shared.languageListArray
            
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
            }
            
        }
        
    }
    
    

}


// MARK:- // Tableview Delegate Functions

extension SelectCountryAndLanguageVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.tableType.elementsEqual("C") {
            return CountryCodeArray.count
        }
        else {
            return LanguageListArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "countryAndPhoneCodeTVC") as! countryAndPhoneCodeTVC
        
        if self.tableType.elementsEqual("C") {
            let mydict = CountryCodeArray[indexPath.row]
            
            cell.countryName.text = "\(mydict.countryName ?? "")"
            //cell.countryPhoneCode.text = "+ " + "\(mydict.phoneCode ?? "")"
            
            cell.countryImage.sd_setImage(with: URL(string: "\(mydict.countryFlag ?? "")"))
        }
        else {
            let mydict = LanguageListArray[indexPath.row]
            
            cell.countryName.text = "\(mydict.langName ?? "")"
            
            cell.countryImage.sd_setImage(with: URL(string: "\(mydict.langImage ?? "")"))
        }
        
        cell.countryPhoneCode.text = ""
        
        
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        // Set Font
        cell.countryName.font = UIFont(name: cell.countryName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        cell.countryPhoneCode.font = UIFont(name: cell.countryPhoneCode.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.tableType.elementsEqual("C") {
            let mydict = CountryCodeArray[indexPath.row]
            
            self.countryName.text = "\(mydict.countryName ?? "")"
            
            self.countryImageview.sd_setImage(with: URL(string: "\(mydict.countryFlag ?? "")"))
            
            self.countryNameString = "\(mydict.countryName ?? "")"
            
            self.countryCode = "\(mydict.id ?? "")"
            
            self.countryFlag = "\(mydict.countryFlag ?? "")"
            
        }
        else {
            let mydict = LanguageListArray[indexPath.row]
            
            self.languageName.text = "\(mydict.langName ?? "")"
            
            self.langName = "\(mydict.langName ?? "")"
            
            self.langCode = "\(mydict.langCode ?? "")"
            
        }
        
        
        self.view.sendSubviewToBack(self.viewOfTable)
        self.viewOfTable.isHidden = true
        
    }
    
}
