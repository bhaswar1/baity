//
//  SuperAgentDetailsVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 22/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import YouTubePlayer_Swift
import NVActivityIndicatorView
import UPCarouselFlowLayout

class SuperAgentDetailsVC: GlobalViewController {

    
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var mainStackview: UIStackView!
    @IBOutlet weak var agentDetailsView: UIView!
    
    
    @IBOutlet weak var addressTitleLBL: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var cityCountryTitleLBL: UILabel!
    @IBOutlet weak var cityCountry: UILabel!
    @IBOutlet weak var phoneNumberTitleLBL: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var emailTitleLBL: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var agentName: UILabel!
    @IBOutlet weak var agentImageview: UIImageView!
    
    @IBOutlet weak var callMessageView: UIView!
    
    @IBOutlet weak var businessDetailsView: UIView!
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var businessDescription: UILabel!
    
    @IBOutlet weak var agentVideoView: UIView!
    @IBOutlet weak var sendEmailStaticLbl: UILabel!
    @IBOutlet weak var videoTitleStaticLBL: UILabel!
    @IBOutlet weak var businessVideo: YouTubePlayerView!
    
    @IBOutlet weak var imageSlideshowView: UIView!
    @IBOutlet weak var imageSlideshowTitleLBL: UILabel!
    @IBOutlet weak var imageSlideshowCollectionview: UICollectionView!
    
    @IBOutlet weak var realEstateListStaticLBL: UILabel!
    
    @IBOutlet weak var popUpView: PopUpViewTypeOne!
    
    @IBOutlet weak var callNowStaticLBL: UILabel!
    @IBOutlet weak var sendMessageStaticLBL: UILabel!
    
    
    
    
    
    var agentID : String!
    
    var agentInformationDictionary : NSDictionary!
    
    var agentDetailsDictionary : NSDictionary!
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()

        globalFunctions.shared.loadPlaceholder(onView: self.view)
        
        self.setFont()
        
        self.getAgentProfileInfo()
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            self.getSuperAgentProfileDetails()
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                self.populateFieldsWithData()
                
                self.localizeStrings()
                
                globalFunctions.shared.removePlaceholder(fromView: self.view)
                
            })
            
        }
        
        
        
        //
        let floawLayout = UPCarouselFlowLayout()
        floawLayout.itemSize = CGSize(width: UIScreen.main.bounds.size.width - 60.0, height: self.imageSlideshowCollectionview.frame.size.height)
        floawLayout.scrollDirection = .horizontal
        floawLayout.sideItemScale = 0.8
        floawLayout.sideItemAlpha = 1.0
        floawLayout.spacingMode = .fixed(spacing: 5.0)
        imageSlideshowCollectionview.collectionViewLayout = floawLayout
    }
    
    
    
    // MARK:- // Buttons
    
    // MARK:- // Call Now Button
    
    @IBOutlet weak var callNowButtonOutlet: UIButton!
    @IBAction func callNowButton(_ sender: UIButton) {
        callNow()
    }
    
    func callNow(){
        let phoneNumber = "\((self.agentInformationDictionary["info"] as! NSDictionary)["phone_code"] ?? "")\((self.agentInformationDictionary["info"] as! NSDictionary)["phone"] ?? "")"
        
        if phoneNumber.elementsEqual("") == false {
            
            if let phoneURL = NSURL(string: ("tel://" + phoneNumber)) {
                
                self.customAlert(title: globalFunctions.shared.getLanguageString(variable: "ios_are_you_sure"), message: globalFunctions.shared.getLanguageString(variable: "property_details_call") + " " + phoneNumber + " ?", doesCancel: true) {
                    
                    UIApplication.shared.open(phoneURL as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    
                }
            }
        }
    }
    
    @objc func callButtonAction(sender: UITapGestureRecognizer){
        callNow()
    }
    
    
    
    // MARK:- // Send Message Button
    
    @IBOutlet weak var sendMessageButtonOutlet: UIButton!
    @IBAction func sendMessageButton(_ sender: UIButton) {
        
        if UserDefaults.standard.bool(forKey: "loginStatus") == true {
            print((self.agentInformationDictionary["info"] as! NSDictionary)["phone_code"] ?? "")
            let urlWhats = "whatsapp://send?phone=+\((self.agentInformationDictionary["info"] as! NSDictionary)["phone_code"] ?? "")\((self.agentInformationDictionary["info"] as! NSDictionary)["phone"] ?? "")"

            var characterSet = CharacterSet.urlQueryAllowed
            characterSet.insert(charactersIn: "?&")

            if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: characterSet){

                if let whatsappURL = NSURL(string: urlString) {
                    if UIApplication.shared.canOpenURL(whatsappURL as URL){
                        //UIApplication.shared.openURL(whatsappURL as URL)
                        UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: nil)
                    }
                    else {
                        print("Install Whatsapp")

                        self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "whatsapp_not_installed"))

                    }
                }
            }
            
            
            
//            self.setupPopupView()
//            self.view.bringSubviewToFront(self.popUpView)
//            self.popUpView.contentTXTView.text = ""
//            self.popUpView.isHidden = false
        }
        else {
            
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "you_have_to_login_first"))
            
        }
        
        
        
    }
    
    
    
    // MARK:- // Agent Real Estate List Button
    
    @IBOutlet weak var agentRealEstateListButtonOutlet: UIButton!
    @IBAction func agentRealEstateListButton(_ sender: UIButton) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "AgentAllRealEstatesListingVC") as! AgentAllRealEstatesListingVC
        
        navigate.agentID = self.agentID
        navigate.isFromSuperAgentDtls = true
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    // MARK: - // JSON Post Method to get Agent Profile Details
    
    
    func getAgentProfileInfo() {
        
        var parameters : String!
        
        parameters = "dalaleen_user_id=\(self.agentID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "profile_details", param: parameters) {
            
            self.agentInformationDictionary = self.globalJson
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
            }
        }
    }
    
    
    // MARK:- // JSON Post Method to get Super Agent Profile Informations
    
    func getSuperAgentProfileDetails() {
        
        var parameters : String!
        
        parameters = "dalaleen_user_id=\(self.agentID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "upgrade_to_superagent", param: parameters) {
            
            self.agentDetailsDictionary = self.globalJson["info"] as? NSDictionary
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.imageSlideshowCollectionview.delegate = self
                self.imageSlideshowCollectionview.dataSource = self
                self.imageSlideshowCollectionview.reloadData()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    
    // MARK:- // JSON Post Method to Send Message
    
    func sendMessage(message : String) {
        
        var parameters : String = ""
        
        parameters = "from_user_id=\(userInformation.shared.userID ?? "")&to_user_id=\(self.agentID ?? "")&subject=\(globalFunctions.shared.getLanguageString(variable: "ios_property_information") )&message=\(message)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "send_message_save", param: parameters) {
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
                self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "success_msg"), message: "\(self.globalJson["message"] ?? "")")
                
            }
            
        }
        
    }
    
    //MARK:- Adress btn Click
    @objc func tapOnAddress(sender: UITapGestureRecognizer){
        UserDefaults.standard.set("\((self.agentInformationDictionary["info"] as! NSDictionary)["latitude"] ?? "")", forKey: "lat")
        UserDefaults.standard.set("\((self.agentInformationDictionary["info"] as! NSDictionary)["longitude"] ?? "")", forKey: "long")
        let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "mapViewVC") as! mapViewVC
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
//MARK:- Open mail Btn Click
    @objc func tapOpenMail(sender: UITapGestureRecognizer){
        self.sendMail()
    }
    
    func sendMail(){
        let email = "\((self.agentInformationDictionary["info"] as! NSDictionary)["email"] ?? "")"
        if let url = URL(string: "mailto:\(email)") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
    }
    
    //MARK:- Send email Button Action
    @IBAction func sendEmailButtonClick(_ sender: Any) {
        self.sendMail()
    }
    
    
    // MARK:- // Populate Fields with Data
    
    func populateFieldsWithData() {
        
        self.address.text = "\((self.agentInformationDictionary["info"] as! NSDictionary)["strt_address"] ?? "")"
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnAddress(sender:)))
        self.address.isUserInteractionEnabled = true
        self.address.addGestureRecognizer(tap)
        
        self.cityCountry.text = "\((self.agentInformationDictionary["info"] as! NSDictionary)["city"] ?? "")/" + "\((self.agentInformationDictionary["info"] as! NSDictionary)["country"] ?? "")"
        
        self.phoneNumber.text = "\((self.agentInformationDictionary["info"] as! NSDictionary)["phone"] ?? "")"
        let tapNo = UITapGestureRecognizer(target: self, action: #selector(callButtonAction(sender:)))
        self.phoneNumber.isUserInteractionEnabled = true
        self.phoneNumber.addGestureRecognizer(tapNo)
        
        self.email.text = "\((self.agentInformationDictionary["info"] as! NSDictionary)["email"] ?? "")"
        let emailTap = UITapGestureRecognizer(target: self, action: #selector(tapOpenMail(sender:)))
        self.email.isUserInteractionEnabled = true
        self.email.addGestureRecognizer(emailTap)
        
        self.agentName.text = "\((self.agentInformationDictionary["info"] as! NSDictionary)["name"] ?? "")"
        self.agentImageview.sd_setImage(with: URL(string: "\((self.agentInformationDictionary["info"] as! NSDictionary)["photo"] ?? "")"))
        
        self.businessName.text = "\(self.agentDetailsDictionary["business_title"] ?? "")"
        self.businessDescription.text = "\(self.agentDetailsDictionary["business_description"] ?? "")"
        
        let myVideoURL = NSURL(string: "https://www.youtube.com/watch?v=" + "\(self.agentDetailsDictionary["video"] ?? "")")
        
        self.businessVideo.loadVideoURL(myVideoURL! as URL)
        
        
        if userInformation.shared.userID.elementsEqual("\((self.agentInformationDictionary["info"] as! NSDictionary)["id"] ?? "")") {
            self.callMessageView.isHidden = true
        }
        else {
            self.callMessageView.isHidden = false
        }
        
        if "\(self.agentDetailsDictionary["video"] ?? "")".elementsEqual("") {
            self.agentVideoView.isHidden = true
        }
        else {
            self.agentVideoView.isHidden = false
        }
        
        if (self.agentDetailsDictionary["gallery_image"] as! NSArray).count > 0 {
            self.imageSlideshowView.isHidden = false
        }
        else {
            self.imageSlideshowView.isHidden = true
        }
        
    }
    
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.sendEmailStaticLbl.text = globalFunctions.shared.getLanguageString(variable: "app_send_mail")
        self.addressTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_address")
        self.cityCountryTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "home_city") + "/" + globalFunctions.shared.getLanguageString(variable: "my_profile_country")
        self.phoneNumberTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "property_details_telephone")
        self.emailTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "super_agents_details_email")
        
        self.videoTitleStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "app_msg1")
        self.imageSlideshowTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "app_msg2")
        self.realEstateListStaticLBL.text = "\((self.agentInformationDictionary["info"] as! NSDictionary)["name"] ?? "")" + "'s " +  globalFunctions.shared.getLanguageString(variable: "super_agents_details_text")
        self.callNowStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "super_agents_details_call")
        self.sendMessageStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "send_message")
        
    }
    
    
    // MARK:- // Set Font
    
    func setFont() {
        self.sendEmailStaticLbl.font = UIFont(name: self.callNowStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.addressTitleLBL.font = UIFont(name: self.addressTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.address.font = UIFont(name: self.address.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.cityCountryTitleLBL.font = UIFont(name: self.cityCountryTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.cityCountry.font = UIFont(name: self.cityCountry.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.phoneNumberTitleLBL.font = UIFont(name: self.phoneNumberTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.phoneNumber.font = UIFont(name: self.phoneNumber.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.emailTitleLBL.font = UIFont(name: self.emailTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.email.font = UIFont(name: self.email.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.agentName.font = UIFont(name: self.agentName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        
        self.businessName.font = UIFont(name: self.businessName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.businessDescription.font = UIFont(name: self.businessDescription.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        
        self.videoTitleStaticLBL.font = UIFont(name: self.videoTitleStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.imageSlideshowTitleLBL.font = UIFont(name: self.imageSlideshowTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.realEstateListStaticLBL.font = UIFont(name: self.realEstateListStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.callNowStaticLBL.font = UIFont(name: self.callNowStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.sendMessageStaticLBL.font = UIFont(name: self.sendMessageStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
    }
    
    
    
    // MARK:- // Setup Popup View
    
    func setupPopupView() {
        
        self.popUpView.titleLBL.text = globalFunctions.shared.getLanguageString(variable: "send_message")
        
        self.popUpView.submitButton.addTarget(self, action: #selector(self.sendMessage(sender:)), for: .touchUpInside)
        
        self.popUpView.closeBackgroundButton.addTarget(self, action: #selector(self.closePopupView(sender:)), for: .touchUpInside)
        
        self.popUpView.crossButton.addTarget(self, action: #selector(self.closePopupView(sender:)), for: .touchUpInside)
        
    }
    
    // MARK:- // Close Popup VIew
    
    @objc func closePopupView(sender: UIButton) {
        
        self.view.sendSubviewToBack(self.popUpView)
        self.popUpView.isHidden = true
        
    }
    
    // MARK:- // Send Message
    
    @objc func sendMessage(sender: UIButton) {
        
        let trimmedString = (self.popUpView.contentTXTView.text ?? "").trimmingCharacters(in: .whitespaces)
        
        if trimmedString.elementsEqual("") == false {
            
            self.sendMessage(message: trimmedString)
            
            self.globalDispatchgroup.notify(queue: .main) {
                
                self.view.sendSubviewToBack(self.popUpView)
                self.popUpView.isHidden = true
                
            }
        }
        else {
            ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "please_type_message"))
        }
        
        
        
    }
    
    fileprivate var currentPage: Int = 0 {
        didSet {
            print("page at centre = \(currentPage)")
        }
    }
    

}



// MARK:- // Collectionview Delegate Methods

extension SuperAgentDetailsVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (self.agentDetailsDictionary["gallery_image"] as! NSArray).count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageSlideshowCVC", for: indexPath) as! imageSlideshowCVC
        
        cell.cellImage.sd_setImage(with: URL(string: "\(((self.agentDetailsDictionary["gallery_image"] as! NSArray)[indexPath.row] as! NSDictionary)["image"] ?? "")"))
        
        return cell
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        return CGSize(width: self.FullWidth - 60, height: 150)
//
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//
//        return 0
//
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        
//        return 0
//
//    }
    
    
    
    
    //
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let layout = self.imageSlideshowCollectionview.collectionViewLayout as! UPCarouselFlowLayout
        let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
        let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
        currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
    }
    
    
    
    fileprivate var pageSize: CGSize {
        let layout = self.imageSlideshowCollectionview.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        if layout.scrollDirection == .horizontal {
            pageSize.width += layout.minimumLineSpacing
        } else {
            pageSize.height += layout.minimumLineSpacing
        }
        return pageSize
    }
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
