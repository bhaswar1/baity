//
//  ChangePasswordVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 24/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView

class ChangePasswordVC: GlobalViewController {

    @IBOutlet weak var oldPasswordLBL: UILabel!
    @IBOutlet weak var viewOfOldPassText: UIView!
    @IBOutlet weak var oldPassTXT: UITextField!
    @IBOutlet weak var newPasswordLBL: UILabel!
    @IBOutlet weak var viewOfNewPassText: UIView!
    @IBOutlet weak var newPassTXT: UITextField!
    @IBOutlet weak var confirmNewPasswordLBL: UILabel!
    @IBOutlet weak var viewOfConfirmNewPassText: UIView!
    @IBOutlet weak var confirmNewPassTXT: UITextField!
    
    
    
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.localizeStrings()
        
        self.SetFont()
        
        self.viewOfOldPassText.layer.borderWidth = 0.5
        self.viewOfOldPassText.layer.borderColor = UIColor.lightGray.cgColor
        self.viewOfNewPassText.layer.borderWidth = 0.5
        self.viewOfNewPassText.layer.borderColor = UIColor.lightGray.cgColor
        self.viewOfConfirmNewPassText.layer.borderWidth = 0.5
        self.viewOfConfirmNewPassText.layer.borderColor = UIColor.lightGray.cgColor
        
        
        
        
    }
    
    
    // MARK:- // Buttons
    
    // MARK:- // Save Button
    
    @IBOutlet weak var saveButtonOutlet: UIButton!
    @IBAction func saveButton(_ sender: UIButton) {
        
        if (self.oldPassTXT.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "registration_enter_your_password"))
        }
        else if globalFunctions.shared.validatePassword(enteredPassword: self.oldPassTXT.text ?? "") == false {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "plese_enter_atlst"))
        }
        else if (self.newPassTXT.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "resetpass_new_pass_placeholder"))
        }
        else if globalFunctions.shared.validatePassword(enteredPassword: self.newPassTXT.text ?? "") == false {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "plese_enter_atlst"))
        }
        else if (self.confirmNewPassTXT.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "resetpass_confirm_pass_placeholder"))
        }
//        else if globalFunctions.shared.validatePassword(enteredPassword: self.confirmNewPassTXT.text ?? "") == false {
//            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "plese_enter_atlst"))
//        }
        else {
            
            if "\(self.confirmNewPassTXT.text!)".elementsEqual("\(self.newPassTXT.text!)") {
                
                self.changePassword()
                
            }
            else {
                self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "android_nwpswrd_sm_cnfrm"))
            }
        }
        
    }
    
    
    
    // MARK:- // JSON Post Method to Change Password
    
    func changePassword() {
        
        var parameters : String!
        
        parameters = "dalaleen_user_id=\(userInformation.shared.userID ?? "")&old_password=\(self.oldPassTXT.text ?? "")&new_password=\(self.newPassTXT.text ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "change_password", param: parameters) {
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
                let alert = UIAlertController(title: "Attention!", message: "\(self.globalJson["message"] ?? "")", preferredStyle: .alert)
                
                let ok = UIAlertAction(title: "OK", style: .default) {
                    UIAlertAction in
                    
                    UIView.animate(withDuration: 0.5)
                    {
                        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        self.navigationController?.pushViewController(navigate, animated: true)
                    }
                }
                
                alert.addAction(ok)
                
                self.present(alert, animated: true, completion: nil )
                
            }
            
        }
        
    }
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.oldPasswordLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_old_password")
        self.newPasswordLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_new_password")
        self.confirmNewPasswordLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_confirm_new_password")
        self.saveButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "my_profile_save"), for: .normal)
        
    }
    
    // MARK:- // Set Font
    
    func SetFont() {
        
        self.oldPasswordLBL.font = UIFont(name: self.oldPasswordLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.oldPassTXT.font = UIFont(name: self.oldPassTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.newPasswordLBL.font = UIFont(name: self.newPasswordLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.newPassTXT.font = UIFont(name: self.newPassTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.confirmNewPasswordLBL.font = UIFont(name: self.confirmNewPasswordLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.confirmNewPassTXT.font = UIFont(name: self.confirmNewPassTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.saveButtonOutlet.titleLabel!.font = UIFont(name: self.saveButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        
        
    }

    

}
