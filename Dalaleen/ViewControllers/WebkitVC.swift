//
//  WebkitVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 15/10/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import WebKit

class WebkitVC: GlobalViewController {

    
    @IBOutlet weak var staticWebview: UIWebView!
    
    var staticParam : String! = ""
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadHeader(param: self.staticParam!)
        
        self.loadURL(param: self.staticParam!)
        
    }
    
    
    // MARK:- // Load Header
    
    func loadHeader(param:String) {
        
        if param.elementsEqual("terms_condition") {
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "registration_check_text2")
        }
        else if param.elementsEqual("about_us") {
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "footer_about_us")
        }
        else {
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "footer_privacy_policy")
        }
        
    }
    
    
    
    // MARK:- // Load URL
    
    func loadURL(param:String) {
        
        let url = URL(string: GLOBALAPI + "static_page_info?page_name=" + param)
        let requestObj = URLRequest(url: url!)
        staticWebview.loadRequest(requestObj)
        
    }
    
}
