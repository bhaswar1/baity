//
//  MyFavouritesVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 13/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView

class MyFavouritesVC: GlobalViewController {

    @IBOutlet weak var favouriteRealEstatesStaticLBL: UILabel!
    @IBOutlet weak var favouriteRealEstateListingsTable: UITableView!
    
    @IBOutlet weak var NoListFoundView: UIView!
    @IBOutlet weak var noRecordFoundLbl: UILabel!
    
    var startValue = 0
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var recordsArray = NSMutableArray()
    
    var listingDataArray : NSArray!
    
    
    var agentID : String!
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        globalFunctions.shared.loadPlaceholder(onView: self.view)
        
        let identifier = "RealEstateListingTVC"
        let nibName = UINib(nibName: "RealEstateListingTVC", bundle: nil)
        self.favouriteRealEstateListingsTable.register(nibName, forCellReuseIdentifier: identifier)
        
        self.localizeStrings()
        
        self.setFont()
        
        self.getFavouriteListingData()
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            globalFunctions.shared.removePlaceholder(fromView: self.view)
            if self.recordsArray.count == 0{
                //app_no_result_found
                self.NoListFoundView.layer.cornerRadius = 10
                self.NoListFoundView.isHidden = false
                self.noRecordFoundLbl.font = UIFont(name: self.noRecordFoundLbl.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 16.0)))
                self.noRecordFoundLbl.text = globalFunctions.shared.getLanguageString(variable: "home_no_record_found")// app_no_result_found
            }
            
        }

    }
    
    
    
    
    // MARK:- // JSON Post Method to get Favourite Listing Data
    
    func getFavouriteListingData() {
        
        var parameters : String!
        
        parameters = "created_by=\(self.agentID ?? "")&start_value=\(self.startValue)&per_load=\(self.perLoad)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)&sort_by="
        
        self.CallAPI(urlString: "users_favourite_propertylist", param: parameters) {
            
            self.listingDataArray = self.globalJson["info"] as? NSArray
            
            for i in 0..<self.listingDataArray.count {
                
                let tempDict = self.listingDataArray[i] as! NSDictionary
                
                self.recordsArray.add(tempDict)
            }
            
            self.nextStart = "\(self.globalJson["next_start"]!)"
            
            self.startValue = self.startValue + self.perLoad
            
            print("Next Start Value : " , self.startValue)
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.favouriteRealEstateListingsTable.delegate = self
                self.favouriteRealEstateListingsTable.dataSource = self
                self.favouriteRealEstateListingsTable.reloadData()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
            }
            
        }
        
    }
    
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.favouriteRealEstatesStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "my_favorite_title")
        
    }
    
    
    
    // MARK:- // Set Font

    func setFont() {
        
        self.favouriteRealEstatesStaticLBL.font = UIFont(name: self.favouriteRealEstatesStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
    }

}




// MARK:- // Tableview Delegate Methods

extension MyFavouritesVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.recordsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RealEstateListingTVC") as! RealEstateListingTVC
        
        globalFunctions.shared.loadImage(inImageview: cell.propertyImage, url: "\((self.recordsArray[indexPath.row] as! NSDictionary)["property_image"] ?? "")", indicatorInView: cell.cellView)
        
        cell.imageCount.text = " \((self.recordsArray[indexPath.row] as! NSDictionary)["total_image"] ?? "") "
        
        cell.parentCategoryImage.sd_setImage(with: URL(string: "\((self.recordsArray[indexPath.row] as! NSDictionary)["parent_cat_image"] ?? "")"))
        
        
       cell.propertyType.text = "\((recordsArray[indexPath.row] as! NSDictionary)["property_type"] ?? "")" + " (\((recordsArray[indexPath.row] as! NSDictionary)["area"] ?? "") m) "  + "\(globalFunctions.shared.getLanguageString(variable: "property_details_for"))" + "\((recordsArray[indexPath.row] as! NSDictionary)["ribbon_value_lang"] ?? "")"
        
        
        //cell.address.text = "\((recordsArray[indexPath.row] as! NSDictionary)["province"] ?? "")"
        cell.city.text = "\((recordsArray[indexPath.row] as! NSDictionary)["distric"] ?? "")"
        cell.district.text = "\((recordsArray[indexPath.row] as! NSDictionary)["city"] ?? "")"
        
        cell.bedCount.text = "\((recordsArray[indexPath.row] as! NSDictionary)["bedroom"] ?? "")"
        cell.livingRoomCount.text = "\((recordsArray[indexPath.row] as! NSDictionary)["livingroom"] ?? "")"
        cell.bathroomCount.text = "\((recordsArray[indexPath.row] as! NSDictionary)["bathroom"] ?? "")"
        cell.diningCount.text = "\((recordsArray[indexPath.row] as! NSDictionary)["kichen"] ?? "")"
        
        cell.listingDate.text = "\((recordsArray[indexPath.row] as! NSDictionary)["created_on_date"] ?? "")".replacingOccurrences(of: "/", with: ".")
        
        cell.price.text = "\((recordsArray[indexPath.row] as! NSDictionary)["other_price"] ?? "")" + " " + "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"] ?? "")"
        
        if "\((recordsArray[indexPath.row] as! NSDictionary)["plan_id"] ?? "")".elementsEqual("2") {
            cell.urgentImageview.isHidden = false
            
            cell.urgentImageview.contentMode = .scaleAspectFit
            
            cell.urgentImageview.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSDictionary)["gold_logo"] ?? "")"))
        }
        else if "\((recordsArray[indexPath.row] as! NSDictionary)["plan_id"] ?? "")".elementsEqual("6") {
            cell.urgentImageview.isHidden = false
            
            cell.urgentImageview.contentMode = .scaleAspectFill
            cell.urgentImageview.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSDictionary)["urgent_logo"] ?? "")"))
        }
        else {
            cell.urgentImageview.isHidden = true
        }
        
        
        
        //
        cell.cellView.layer.cornerRadius = 8
        
        
        //
        
        cell.propertyType.font = UIFont(name: cell.propertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        cell.city.font = UIFont(name: cell.city.font.fontName, size: CGFloat(self.Get_fontSize(size: 7)))
        cell.address.font = UIFont(name: cell.address.font.fontName, size: CGFloat(self.Get_fontSize(size: 9)))
        cell.district.font = UIFont(name: cell.district.font.fontName, size: CGFloat(self.Get_fontSize(size: 7)))
        cell.price.font = UIFont(name: cell.price.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        cell.listingDate.font = UIFont(name: cell.listingDate.font.fontName, size: CGFloat(self.Get_fontSize(size: 9)))
        
        cell.bedCount.font = UIFont(name: cell.bedCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        cell.livingRoomCount.font = UIFont(name: cell.livingRoomCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        cell.bathroomCount.font = UIFont(name: cell.bathroomCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        cell.diningCount.font = UIFont(name: cell.diningCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let object : RealEstateListingTVC = cell as! RealEstateListingTVC
        
        if ("\((recordsArray[indexPath.row] as! NSDictionary)["plan_id"] ?? "")".elementsEqual("2"))
        {
            object.cellView.backgroundColor = UIColor(red:242/255, green:243/255, blue:193/255, alpha: 1)
        }
        else if ("\((recordsArray[indexPath.row] as! NSDictionary)["plan_id"] ?? "")".elementsEqual("6"))
        {
            object.cellView.backgroundColor = UIColor(red:242/255, green:243/255, blue:193/255, alpha: 1)
            //object.cellView.backgroundColor = UIColor(red:254/255, green:215/255, blue:218/255, alpha: 1)
        }
        else
        {
            object.cellView.backgroundColor = UIColor.white
        }
        
        
        if ("\((recordsArray[indexPath.row] as! NSDictionary)["ribbon_value"] ?? "")".elementsEqual("sale"))
        {
            object.cellSideView.backgroundColor = UIColor.green
        }
        else if ("\((recordsArray[indexPath.row] as! NSDictionary)["ribbon_value"] ?? "")".elementsEqual("rent"))
        {
            object.cellSideView.backgroundColor = UIColor.yellow
        }
        else if ("\((recordsArray[indexPath.row] as! NSDictionary)["ribbon_value"] ?? "")".elementsEqual("sold"))
        {
            object.cellSideView.backgroundColor = UIColor.red
        }
        else
        {
            object.cellSideView.backgroundColor = UIColor.white
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let object = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "PropertyDetailsVC") as! PropertyDetailsVC
        
        let mydict = recordsArray[indexPath.row] as! NSDictionary
        
        object.propertyID = "\(mydict["id"] ?? "")"
        
        self.navigationController?.pushViewController(object, animated: true)
        
    }
    
    
}



// MARK:- // Scrollview Delegate Methods

extension MyFavouritesVC: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {
            
        }
        else
        {
            print("next start : ",nextStart ?? "")
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    //SVProgressHUD.dismiss()
                }
            }
            else
            {
                self.getFavouriteListingData()
            }
        }
    }
}
