//
//  SuperAgentProfileVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 15/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import YouTubePlayer_Swift
import Alamofire
import NVActivityIndicatorView
import StoreKit

//struct subscriptionIdStruct {
//    var agentPlanPriceID: String?
//    var productId: String?
//
//    init(agentPlanPriceID: String?,productId: String?){
//        self.agentPlanPriceID = agentPlanPriceID
//        self.productId = productId
//    }
//}

class MySuperAgentProfileVC: GlobalViewController {

    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var mainStackview: UIStackView!
    @IBOutlet weak var membershipSubscriptionView: UIView!
    @IBOutlet weak var membershipDatesView: UIView!
    @IBOutlet weak var membershipSubscriptionTitleLBL: UILabel!
    @IBOutlet weak var membershipDateStaticLBL: UILabel!
    @IBOutlet weak var membershipDate: UILabel!
    @IBOutlet weak var expiryDateStaticLBL: UILabel!
    @IBOutlet weak var expiryDate: UILabel!
    @IBOutlet weak var extendMembershipStaticLBL: UILabel!
    @IBOutlet weak var superAgentPlansCollectionview: UICollectionView!
    @IBOutlet weak var advertisementTitleView: UIView!
    @IBOutlet weak var advertisementTitleTitleLBL: UILabel!
    @IBOutlet weak var editTitleStaticLBL: UILabel!
    @IBOutlet weak var titleTXT: UITextView!
    @IBOutlet weak var advertisementDescriptionView: UIView!
    @IBOutlet weak var advertisementDescriptionTitleLBL: UILabel!
    @IBOutlet weak var editDescriptionStaticLBL: UILabel!
    @IBOutlet weak var descriptionTXT: UITextView!
    @IBOutlet weak var advertisementBannerView: UIView!
    @IBOutlet weak var advertisementBannerTitleLBL: UILabel!
    @IBOutlet weak var advertisementBannerImageview: UIImageView!
    @IBOutlet weak var videoOfYourBusinessView: UIView!
    @IBOutlet weak var videoOfYourBusinessTitleLBL: UILabel!
    @IBOutlet weak var borderViewOfVideoText: UIView!
    @IBOutlet weak var videoText: UITextField!
    @IBOutlet weak var businessVideo: YouTubePlayerView!
    @IBOutlet weak var imageSlideshowView: UIView!
    @IBOutlet weak var yourImageSlideshowStaticLBL: UILabel!
    @IBOutlet weak var maximumImageLBL: UILabel!
    @IBOutlet weak var imageSlideshowCollectionview: UICollectionView!
    @IBOutlet weak var enterButtonView: UIView!
    @IBOutlet weak var subscriptionTableView: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    
    
    @IBOutlet weak var titleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sliderCollectionviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var agentPlansCollectionviewHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var deleteImagesView: UIView!
    @IBOutlet weak var deleteSelectedStaticLBL: UILabel!
    
    var subSelectionIndx: IndexPath?
    
    var agentID : String!
    
    var agentInformationDictionary : NSDictionary!
    
    var membershipButtonsArray = [UIButton]()
    
    var superAgentPlansArray : NSArray!
    
    var pickerType : Int!
    
    
    var webImagesArray = [NSDictionary]()
    var localImagesArray = [UIImage]()
    
    var deletedImageString : String! = ""
    var localImagesToDeleteArray = [Int]()
    var webImagesToDeleteArray = [Int]()
    
    var longPressGesture = UILongPressGestureRecognizer()
    
    var agentPlanPriceID : String! = ""
    
    var bannerUploaded : Bool! = false
    
    //var SubDetailArray = [subscriptionIdStruct]()
    let planId0 = ["com.esolz.Dalaleen.10dPrem","com.esolz.Dalaleen.30dPrem","com.esolz.Dalaleen.60dPrem"]
    let planId1 = ["com.esolz.Dalaleen.Seasonal30Days","com.esolz.Dalaleen.Seasonal180Days"]
    let planId2 = ["com.esolz.Dalaleen.Annual1Year"]
    
    //MARK:- In app-purchase variabel
    var subscriptionContent : SubscriptionDataGroup!
    
    var products: [SKProduct] = []
    
    var selectedIndex : Int!
    var prePlanPriceId: String!
    var selectedPlanId: String?
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        globalFunctions.shared.loadPlaceholder(onView: self.view)
        
        for transactionPending in SKPaymentQueue.default().transactions {
            SKPaymentQueue.default().finishTransaction(transactionPending)
        }

        self.localizeStrings()
        
        self.SetFont()
        
        self.creaeteSubscriptionFunc()
        
        self.titleTXT.delegate = self
        self.descriptionTXT.delegate = self
        
        
        self.imageSlideshowCollectionview.delegate = self
        self.imageSlideshowCollectionview.dataSource = self
        self.imageSlideshowCollectionview.reloadData()
        
        
        self.borderViewOfVideoText.layer.borderWidth = 0.5
        self.borderViewOfVideoText.layer.borderColor = UIColor.lightGray.cgColor
        
        self.getSuperAgentPlans()
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            if userInformation.shared.superAgent.elementsEqual("Y")
            {
                self.membershipDatesView.isHidden = false
                
                self.getSuperAgentProfileInformations()
                
                self.globalDispatchgroup.notify(queue: .main) {
                    
                    self.populateFieldsWithData()
                    
                    self.imageSlideshowCollectionview.reloadData()
                    
                    if self.webImagesArray.count == 8 {
                        self.uploadInImageSlideshowButtonOutlet.isHidden = true
                    }
                    else {
                        self.uploadInImageSlideshowButtonOutlet.isHidden = false
                    }
                    
                    
                    globalFunctions.shared.removePlaceholder(fromView: self.view)
                    
                }
            }
            else {
                self.membershipDatesView.isHidden = true
            }
            
        }
        
        self.longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(sender:)))
        self.imageSlideshowCollectionview.addGestureRecognizer(self.longPressGesture)
        
    }
    
    
    
    
    // MARK:- // Handle Long Press Gesture
    
    @objc func handleLongPress(sender : UITapGestureRecognizer) {
        
        if self.longPressGesture.state == UIGestureRecognizer.State.began {
            
            let point = longPressGesture.location(in: self.imageSlideshowCollectionview)
            let indexPath = self.imageSlideshowCollectionview?.indexPathForItem(at: point)
            
            
            if let index = indexPath {
                //var cell = self.imageSlideshowCollectionview.cellForItem(at: index)
                // do stuff with your cell, for example print the indexPath
                print(index.item)
                
                if index.item < self.webImagesArray.count {
                    
                    if self.webImagesToDeleteArray.contains((indexPath?.item)!) {
                        
                        var tempInt : Int!
                        
                        for i in 0..<self.webImagesToDeleteArray.count {
                            if indexPath?.item == self.webImagesToDeleteArray[i] {
                                tempInt = i
                            }
                        }
                        
                        self.webImagesToDeleteArray.remove(at: tempInt)
                    }
                    else {
                        self.webImagesToDeleteArray.append((indexPath?.item)!)
                    }
                    
                    print(webImagesToDeleteArray)
                }
                else {
                    if self.localImagesToDeleteArray.contains((indexPath?.item)! - self.webImagesArray.count) {
                        
                        var tempInt : Int!
                        
                        for i in 0..<self.localImagesToDeleteArray.count {
                            if index.item == self.localImagesToDeleteArray[i] {
                                tempInt = i
                            }
                        }
                        
                        self.localImagesToDeleteArray.remove(at: tempInt)
                    }
                    else {
                        self.localImagesToDeleteArray.append(((indexPath?.item)! - self.webImagesArray.count))
                    }
                }
                
                print("local-",self.localImagesToDeleteArray)
                
                self.imageSlideshowCollectionview.reloadData()
                
            } else {
                print("Could not find index path")
            }
            
            if (self.webImagesToDeleteArray.count + self.localImagesToDeleteArray.count) > 0 {
                self.deleteImagesView.isHidden = false
            }
            else {
                self.deleteImagesView.isHidden = true
            }
            
        }
        
        
        
        
        
    }
   
    
    
    
    
    // MARK:- // Buttons
    
    
    // MARK:- // Delete Images Button
    
    
    @IBOutlet weak var deleteImagesButtonOutlet: UIButton!
    @IBAction func deleteImagesButton(_ sender: UIButton) {
        print(self.localImagesArray.count,".....",self.localImagesToDeleteArray.count)
        print(self.webImagesArray.count)
        print(self.webImagesToDeleteArray.count)
        if self.localImagesArray.count == self.localImagesToDeleteArray.count && self.webImagesArray.count == self.webImagesToDeleteArray.count{
            customAlert(title: "Warning!", message: "You can,t delete all Image", doesCancel: false, completion: {
                
            })
        }
        else{
            self.localImagesArray.remove(at: self.localImagesToDeleteArray)
            
            //        for i in 0..<self.localImagesToDeleteArray.count {
            //            for j in 0..<self.localImagesArray.count {
            //                if (self.localImagesToDeleteArray[i] - self.webImagesArray.count) == j {
            //                    self.localImagesArray.remove(at: j)
            //                }
            //            }
            //        }
            
            
            // MAKING Images to Delete String
            var deletedStringArray = [String]()
            for i in 0..<self.webImagesToDeleteArray.count {
                deletedStringArray.append("\((self.webImagesArray[self.webImagesToDeleteArray[i]])["id"] ?? "")")
            }
            self.deletedImageString = deletedStringArray.joined(separator: ",")
            
            self.webImagesArray.remove(at: self.webImagesToDeleteArray)
            
            self.webImagesToDeleteArray.removeAll()
            
            self.localImagesToDeleteArray.removeAll()
            
            self.imageSlideshowCollectionview.reloadData()
            
            self.deleteImagesView.isHidden = true
            
            //        if self.webImagesToDeleteArray.count > 0 {
            //
            //            self.uploadSuperAgentDetails()
            //
            //            self.globalDispatchgroup.notify(queue: .main) {
            //
            //                self.webImagesToDeleteArray.removeAll()
            //                self.localImagesToDeleteArray.removeAll()
            //
            //                self.getSuperAgentProfileInformations()
            //
            //            }
            //
            //        }
            //        else {
            //            self.webImagesToDeleteArray.removeAll()
            //            self.localImagesToDeleteArray.removeAll()
            //
            //            self.getSuperAgentProfileInformations()
            //        }
            
            
            
            if (self.localImagesArray.count + self.webImagesArray.count) == 8 {
                self.uploadInImageSlideshowButtonOutlet.isHidden = true
            }
            else {
                self.uploadInImageSlideshowButtonOutlet.isHidden = false
            }
        }

    }
    
    
    
    
    
    // MARK:- // Enter Button
    
    @IBOutlet weak var enterButtonOutlet: UIButton!
    @IBAction func enterButton(_ sender: UIButton) {
        
        self.validation {
            if self.agentPlanPriceID != nil && self.prePlanPriceId != self.agentPlanPriceID {
                let index = self.getIndexFromProductId(productId: self.selectedPlanId!)
                self.purchaseItemIndex(index: index, completion: {
                    self.uploadSuperAgentDetails()
                    print("Successfully Purchases...")
                })
            }
            else{
                self.uploadSuperAgentDetails()
            }
            
        }
        
    }
    
    
    //MARK:- //Create subscription function
    func creaeteSubscriptionFunc(){
        // Creating Subscription Data
        //self.createSubscriptionData()
        
        // Rest of the work
        SubscriptionProducts.store.requestProducts { [weak self] success, products in
            guard let self = self else { return }
            guard success else {
                let alertController = UIAlertController(title: "Failed to load list of products",
                                                        message: "Check logs for details",
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .default))
                self.present(alertController, animated: true, completion: nil)
                return
            }
            
            self.products = products!
        }
//        if SubscriptionProducts.store.isProductPurchased(SubscriptionProducts.oneMonthSuperAgent) {
//            self.displaySubscriptionDetails(index: 6)
//        }
//        else if SubscriptionProducts.store.isProductPurchased(SubscriptionProducts.sixMonthSuperAgent) {
//            self.displaySubscriptionDetails(index: 7)
//        }
//        else if SubscriptionProducts.store.isProductPurchased(SubscriptionProducts.oneYearSuperAgent) {
//            self.displaySubscriptionDetails(index: 8)
//        }
//        else {
//            self.displaySubscriptionAlert()
//        }
    }
    
    // MARK:- // Create Subscription Data
    
    func createSubscriptionData() {
        
        self.subscriptionContent = SubscriptionDataGroup(subscriptionStrings: ["You have subscribed for 1 month.","You have subscribed for 1 year.","You have subscribed for 6 months."])
        
    }
    
    
    // MARK:- // Display Subscription Alert.
    
    func displaySubscriptionAlert() {
        
        //self.mainLBL.text = "You have not purchased any subscription yet."
        
    }
    
    // MARK:- // Display Subscription Details
    
    func displaySubscriptionDetails(index : Int) {
        //print("\(self.subscriptionContent.subscriptionStrings[index] )")
        //self.mainLBL.text = "\(self.subscriptionContent.subscriptionStrings[index])"
        
    }
    
    
    // MARK:- // Purchase Item Index
    
    func purchaseItemIndex(index: Int , completion : @escaping ()->()) {
        
        startAnimating(CGSize(width: 30.0, height: 30.0), message: "", messageFont: UIFont(name: "Lato-Bold", size: 14), type: .ballClipRotateMultiple, color: UIColor(displayP3Red: 0/255, green: 135/255, blue: 228/255, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.4), textColor: .black, fadeInAnimation: nil)
        
        print(products[index].productIdentifier)
        
        SubscriptionProducts.store.buyProduct(products[index]) { [weak self] success, productId in
            guard let self = self else { return }
            guard success else {
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                let alertController = UIAlertController(title: "Failed to purchase product",
                                                        message: "Check logs for details",
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .default))
                self.present(alertController, animated: true, completion: nil)
                return
            }
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            completion()
            
        }
        
        
    }
    
    
    
    // MARK:- // Validation
    
    func validation(completion : @escaping ()->()) {
        
        if self.agentPlanPriceID.elementsEqual("") {
            
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "select_any_plan"))
            
        }
        else if self.titleTXT.text.elementsEqual("") {
            
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "please_enter_title"))
            
        }
        else if self.descriptionTXT.text.elementsEqual("") {
            
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "please_enter_description"))
            
        }
        else if self.advertisementBannerImageview.image == nil {
            
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "select_image"))
            
        }
        else if (self.videoText.text?.elementsEqual(""))! {
            
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_video_id"))
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_video_id"))
            
        }
        else if (self.webImagesArray.count + self.localImagesArray.count) == 0 {
            
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "property_post_add_image"))
            
        }
        else {
            completion()
        }
        
    }
    
    
    
    
    
    // MARK:- // Upload in Image Slideshow Button
    
    @IBOutlet weak var uploadInImageSlideshowButtonOutlet: UIButton!
    @IBAction func uploadInImageSlideshowButton(_ sender: UIButton) {
        
        self.pickerType = 2
        
        self.openImagepicker()
        
    }
    
    
    
    // MARK:- // Upload Video
    
    @IBOutlet weak var uploadVideoButtonOutlet: UIButton!
    @IBAction func uploadVideoButton(_ sender: UIButton) {
        self.businessVideo.isHidden = false
        
        let myVideoURL = NSURL(string: "https://www.youtube.com/watch?v=" + self.videoText.text!)
        
        self.businessVideo.loadVideoURL(myVideoURL! as URL)
        
    }
    
    
    
    
    // MARK:- // Upload Banner
    
    @IBOutlet weak var uploadBannerButtonOutlet: UIButton!
    @IBAction func uploadBannerButton(_ sender: UIButton) {
        
        self.pickerType = 1
        
        self.openImagepicker()
        
    }
    
    
    
    
    
    // MARK:- // Edit Description Button
    
    @IBOutlet weak var editDescriptionButtonOutlet: UIButton!
    @IBAction func editDescriptionButton(_ sender: UIButton) {
        
        self.descriptionTXT.isUserInteractionEnabled = true
        
        self.descriptionTXT.becomeFirstResponder()
        
    }
    
    
    
    // MARK:- // Edit Title Button
    
    @IBOutlet weak var editTitleButtonOutlet: UIButton!
    @IBAction func editTitleButton(_ sender: UIButton) {
        
        self.titleTXT.isUserInteractionEnabled = true
        
        self.titleTXT.becomeFirstResponder()
        
    }
    
    
    
    
    // MARK:- // Membership Buttons action method
    
//    @objc func membershipButtons(sender : UIButton) {
//
//        for i in 0..<self.membershipButtonsArray.count {
//
//            if i == sender.tag {
//                self.membershipButtonsArray[i].isSelected = !self.membershipButtonsArray[i].isSelected
//                self.selectedIndex = i + 6
//            }
//            else {
//                self.membershipButtonsArray[i].isSelected = false
//            }
//        }
//
//       // self.buttonTransformIdentity(button: self.membershipButtonsArray[sender.tag])
//
//
//        //self.membershipButtonsArray[sender.tag].isSelected = !self.membershipButtonsArray[sender.tag].isSelected
//
//        if self.membershipButtonsArray[sender.tag].isSelected == false {
//            self.agentPlanPriceID = ""
//        }
//        else {
//            self.agentPlanPriceID = "\((self.superAgentPlansArray[sender.tag] as! NSDictionary)["plan_price_id"] ?? "")"
//        }
//
//    }
    
    
    
    
    // MARK:- // JSON Post Method to get Super Agent Profile Informations
    
    func getSuperAgentProfileInformations() {
        
        var parameters : String!
        
        parameters = "dalaleen_user_id=\(self.agentID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "upgrade_to_superagent", param: parameters) {
            
            self.agentInformationDictionary = self.globalJson["info"] as? NSDictionary
            
            self.webImagesArray.removeAll()
            for i in 0..<(self.agentInformationDictionary["gallery_image"] as! NSArray).count {
                self.webImagesArray.append((self.agentInformationDictionary["gallery_image"] as! NSArray)[i] as! NSDictionary)
            }
            self.agentPlanPriceID = "\(self.agentInformationDictionary["plan_price_id"] ?? "")"
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    
    // MARK:- // JSON Post Method to get Super Agent Plans
    
    func getSuperAgentPlans() {
        
        let parameters = "language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "upgradeSuperagentPlan", param: parameters) {
            
            self.superAgentPlansArray = self.globalJson["info"] as? NSArray
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                //                self.superAgentPlansCollectionview.delegate = self
                //                self.superAgentPlansCollectionview.dataSource = self
                //                self.superAgentPlansCollectionview.reloadData()
                self.subscriptionTableView.delegate = self
                self.subscriptionTableView.dataSource = self
                self.subscriptionTableView.reloadData()
                self.tableHeight.constant = 50.0 * 6.0 + 40.0 * 3.0
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                globalFunctions.shared.removePlaceholder(fromView: self.view)
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    
    
    
    
    // MARK:- // Alamofire Method to Upload Super Agent Details

    func uploadSuperAgentDetails() {
        
        var bannerImageData : Data!
        if self.bannerUploaded == true {
            bannerImageData = self.advertisementBannerImageview.image!.jpegData(compressionQuality: 1)!
        }
        
        
        var imageDataArray = [Data]()
        for i in 0..<self.localImagesArray.count {
            imageDataArray.append(self.localImagesArray[i].jpegData(compressionQuality: 1)!)
        }
        
        
        
        self.globalDispatchgroup.enter()
        
        startAnimating(CGSize(width: 30.0, height: 30.0), message: "", messageFont: UIFont(name: "Lato-Bold", size: 14), type: .ballClipRotateMultiple, color: UIColor(displayP3Red: 0/255, green: 135/255, blue: 228/255, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.4), textColor: .black, fadeInAnimation: nil)
        
//        DispatchQueue.main.async {
//            
//            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
//        }
        let parameters = [
            "dalaleen_user_id" : "\(userInformation.shared.userID ?? "")" , "plan_price_id" : "\(self.agentPlanPriceID ?? "")" , "business_title" : "\(self.titleTXT.text ?? "")" , "business_description" : "\(self.descriptionTXT.text ?? "")" , "video_id" : "\(videoText.text ?? "")" , "country" : "\(userInformation.shared.userCountryID ?? "")" , "province" : "\(483)" , "image_count" : "\(self.localImagesArray.count)" , "deleted_image_id" : self.deletedImageString! , "language_code" : "\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"]
        
        print("Parameters are---",parameters)
        
        Alamofire.upload(
            multipartFormData: { MultipartFormData in
                
                for (key, value) in parameters {
                    MultipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
                
                if self.bannerUploaded == true {
                    MultipartFormData.append(bannerImageData, withName: "profile_image", fileName: "profileImage.jpeg", mimeType: "image/jpeg")
                }
                
                for i in 0..<imageDataArray.count {
                    MultipartFormData.append(imageDataArray[i], withName: "image[]", fileName: "sliderImage\(i).jpeg", mimeType: "image/jpeg")
                }
                
                
                
        }, to: (GLOBALAPI + "superagentDetailsSave"))
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    //
                    print(response.request!)  // original URL request
                    print(response.response!) // URL response
                    print(response.data!)     // server data
                    print(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value {
                        
                        print("Image Search Response-----: \(JSON)")
                        
                        DispatchQueue.main.async {
                            
                            self.globalDispatchgroup.leave()
                            
                            if "\((response.result.value as! NSDictionary)["response"] ?? "")".elementsEqual("FALSE") {
                                
                                
                                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                                //SVProgressHUD.dismiss()
                                
                                self.ShowAlertMessage(title: "", message: "\((response.result.value as! NSDictionary)["message"] ?? "")")
                            }
                            else {
                                
                                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                                //SVProgressHUD.dismiss()
                                
                                self.customAlert(title: "", message: "\((response.result.value as! NSDictionary)["message"] ?? "")", doesCancel: false, completion: {
                                    
                                    let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                                    
                                    userInformation.shared.setSuperAgent(superAgent: "Y")
                                    
                                    self.navigationController?.pushViewController(navigate, animated: true)
                                    
                                })
                            }
                            
                            
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            
                            self.globalDispatchgroup.leave()
                            
                            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                            //SVProgressHUD.dismiss()
                        }
                    }
                    
                    
                }
                
            case .failure(let encodingError):
                
                print(encodingError)
                
                DispatchQueue.main.async {
                    
                    self.globalDispatchgroup.leave()
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    //SVProgressHUD.dismiss()
                }
            }
            
        }
        
        
    }
    
    
    
    
    
    
    
    
    // MARK:- // Populate Fields with Data
    
    func populateFieldsWithData() {
        
        
        for i in 0..<(self.superAgentPlansArray.count) {
            let temp = ((self.superAgentPlansArray[i] as! NSDictionary)["plan_price_data"] as! NSArray)
            for j in 0..<temp.count{
                if "\(self.agentInformationDictionary["plan_price_id"] ?? "")".elementsEqual("\((temp[j] as! NSDictionary)["plan_price_id"] ?? "")") {
                    
                    //self.membershipButtonsArray[i].isSelected = tru
                    
                    self.agentPlanPriceID = "\((temp[j] as! NSDictionary)["plan_price_id"] ?? "")"
                    //print(self.agentPlanPriceID)
                    self.prePlanPriceId = self.agentPlanPriceID
                }
                
            }
            
        }
        
        self.membershipDate.text = "\(self.agentInformationDictionary["membership_date"] ?? "")"
        self.expiryDate.text = "\(self.agentInformationDictionary["membership_expiry"] ?? "")"
        self.titleTXT.text = "\(self.agentInformationDictionary["business_title"] ?? "")"
        self.descriptionTXT.text = "\(self.agentInformationDictionary["business_description"] ?? "")"
        self.advertisementBannerImageview.sd_setImage(with: URL(string: "\(self.agentInformationDictionary["ads_card_img"] ?? "")"))
        self.videoText.text = "\(self.agentInformationDictionary["video"] ?? "")"
        
        let myVideoURL = NSURL(string: "https://www.youtube.com/watch?v=" + "\(self.agentInformationDictionary["video"] ?? "")")
        
        self.businessVideo.loadVideoURL(myVideoURL! as URL)
        
        self.titleHeightConstraint.constant = "\(self.agentInformationDictionary["business_title"] ?? "")".heightWithConstrainedWidth(width: self.FullWidth - 40, font: UIFont(name: (self.titleTXT.font?.fontName)!, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))!) + 10
        
        self.descriptionHeightConstraint.constant = "\(self.agentInformationDictionary["business_description"] ?? "")".heightWithConstrainedWidth(width: self.FullWidth - 40, font: UIFont(name: (self.descriptionTXT.font?.fontName)!, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))!) + 10
        
        
        //let tempHeight = (self.superAgentPlansArray.count / 2)*35 + (self.superAgentPlansArray.count % 2)*35
        //self.agentPlansCollectionviewHeightConstraint.constant = CGFloat(tempHeight)
        
        let cellWidth = ((self.FullWidth - 10) - 30) / 4
        self.sliderCollectionviewHeightConstraint.constant = (cellWidth * 2)+10
        
        self.subscriptionTableView.reloadData()
        
//        DispatchQueue.global(qos: .background).async {
//            
//        }
        
    }
    
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.membershipSubscriptionTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "app_member_subscription")
        self.membershipDateStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_membership_date")
        self.expiryDateStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_expiery_date")
        self.extendMembershipStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "ios_extend_membership_text")
        self.advertisementTitleTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "app_add_title")
        self.editTitleStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "my_real_estate_edit")
        self.advertisementDescriptionTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_text1")
        self.editDescriptionStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "my_real_estate_edit")
        self.advertisementBannerTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "app_add_banner")
        self.uploadBannerButtonOutlet.titleLabel?.text = globalFunctions.shared.getLanguageString(variable: "my_profile_pic_upload")
        self.videoOfYourBusinessTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "app_your_business")
        self.uploadVideoButtonOutlet.setTitle("   \(globalFunctions.shared.getLanguageString(variable: "my_profile_pic_upload"))   ", for: .normal)
        self.yourImageSlideshowStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "app_image_slideshow")
        self.maximumImageLBL.text = globalFunctions.shared.getLanguageString(variable: "app_length")
        self.uploadInImageSlideshowButtonOutlet.setTitle("   \(globalFunctions.shared.getLanguageString(variable: "my_profile_pic_upload"))   ", for: .normal)
        self.enterButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "app_enter"), for: .normal) 
        
        
    }
    
    
    
    // MARK:- // Set Font
    
    func SetFont() {
        
        self.membershipSubscriptionTitleLBL.font = UIFont(name: self.membershipSubscriptionTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.membershipDateStaticLBL.font = UIFont(name: self.membershipDateStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.expiryDateStaticLBL.font = UIFont(name: self.expiryDateStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.expiryDate.font = UIFont(name: self.expiryDate.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.membershipDate.font = UIFont(name: self.membershipDate.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.extendMembershipStaticLBL.font = UIFont(name: self.extendMembershipStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 11)))
        self.advertisementTitleTitleLBL.font = UIFont(name: self.advertisementTitleTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.editTitleStaticLBL.font = UIFont(name: self.editTitleStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.titleTXT.font = UIFont(name: self.titleTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.advertisementDescriptionTitleLBL.font = UIFont(name: self.advertisementDescriptionTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.editDescriptionStaticLBL.font = UIFont(name: self.editDescriptionStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.descriptionTXT.font = UIFont(name: self.descriptionTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.advertisementBannerTitleLBL.font = UIFont(name: self.advertisementBannerTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.uploadBannerButtonOutlet.titleLabel!.font = UIFont(name: self.uploadBannerButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.videoOfYourBusinessTitleLBL.font = UIFont(name: self.videoOfYourBusinessTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.videoText.font = UIFont(name: self.videoText.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.uploadVideoButtonOutlet.titleLabel!.font = UIFont(name: self.uploadVideoButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.yourImageSlideshowStaticLBL.font = UIFont(name: self.yourImageSlideshowStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.maximumImageLBL.font = UIFont(name: self.maximumImageLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.uploadInImageSlideshowButtonOutlet.titleLabel!.font = UIFont(name: self.uploadInImageSlideshowButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.enterButtonOutlet.titleLabel!.font = UIFont(name: self.enterButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
    }
    
    
    
    // MARK:- // Open Image Picker
    
    func openImagepicker() {
        
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a Source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    
    //MARK:- Get Index from product Id
    func getIndexFromProductId(productId: String) -> Int{
        var index: Int?
        for i in 0..<products.count{
            if self.selectedPlanId == products[i].productIdentifier{
                index = i
                break
            }
        }
        //print(index!)
        return index!
    }
    
    

}


//MARK:- // Table view Delegate
extension MySuperAgentProfileVC: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.superAgentPlansArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ((self.superAgentPlansArray[section] as! NSDictionary)["plan_price_data"] as! NSArray).count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let contentView : UIView = {
            let contentview = UIView()
            contentview.backgroundColor = .green
            return contentview
        }()
        let mainContainView: UIView = {
           let mainContainView = UIView()
            mainContainView.backgroundColor = .white
            return mainContainView
        }()
        
        let headerLBL : UILabel = {
            let headerlbl = UILabel()
            headerlbl.text = "\((self.superAgentPlansArray[section] as! NSDictionary)["plan_name"] ?? "")"
            headerlbl.textColor = UIColor.black
            headerlbl.font = UIFont(name: self.membershipSubscriptionTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
            headerlbl.translatesAutoresizingMaskIntoConstraints = false
            return headerlbl
        }()
        contentView.addSubview(mainContainView)
        mainContainView.addSubview(headerLBL)
        
        mainContainView.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor, padding: .init(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0))
        headerLBL.anchor(top: mainContainView.topAnchor, leading: mainContainView.leadingAnchor, bottom: mainContainView.bottomAnchor, trailing: mainContainView.trailingAnchor, padding: .init(top: 0, left: 5, bottom: 0, right: 0)) //, size: .init(width: 0, height: 50)
        headerLBL.centerYAnchor.constraint(equalTo: mainContainView.centerYAnchor).isActive = true
        return contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subscriptionTVC", for: indexPath) as! subscriptionTVC
        let temp = ((self.superAgentPlansArray[indexPath.section] as! NSDictionary)["plan_price_data"] as! NSArray)
        cell.planLbl.text = "\((temp[indexPath.row] as! NSDictionary)["duration"] ?? "") \((temp[indexPath.row] as! NSDictionary)["plan_valid_unit"] ?? "") \((self.superAgentPlansArray[indexPath.section] as! NSDictionary)["plan_name"] ?? "") Cost $\(((temp[indexPath.row] as! NSDictionary)["cost"] ?? ""))"
        if self.subSelectionIndx == indexPath || self.agentPlanPriceID == "\((temp[indexPath.row] as! NSDictionary)["plan_price_id"] ?? "")"{
            cell.checkBtnOut.setImage(UIImage(named:"checkbox2"), for: .normal)
        }
        else{
            cell.checkBtnOut.setImage(UIImage(named:"Rectangle 22"), for: .normal)
        }
        cell.planLbl.font =  UIFont(name: cell.planLbl.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.subSelectionIndx = NSIndexPath(row: indexPath.row, section: indexPath.section) as IndexPath
        self.agentPlanPriceID = "\((((self.superAgentPlansArray[indexPath.section] as! NSDictionary)["plan_price_data"] as! NSArray)[indexPath.row] as! NSDictionary)["plan_price_id"] ?? "")"
        if indexPath.section == 0{
            self.selectedPlanId = planId0[indexPath.row]
        }
        else if indexPath.section == 1{
            self.selectedPlanId = planId1[indexPath.row]
        }
        else{
            self.selectedPlanId = planId2[indexPath.row]
        }
        
        subscriptionTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    
}




// MARK:- // Collectionview Delegates

extension MySuperAgentProfileVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == imageSlideshowCollectionview {
            return (self.webImagesArray.count + self.localImagesArray.count)
        }
        else {
            return self.superAgentPlansArray.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == imageSlideshowCollectionview {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageSlideCVC", for: indexPath) as! imageSlideCVC
            
            if indexPath.item < self.webImagesArray.count {
                //cell.cellImage.sd_setImage(with: URL(string: "\((self.webImagesArray[indexPath.row])["image"] ?? "")"))
                globalFunctions.shared.loadImage(inImageview: cell.cellImage, url: "\((self.webImagesArray[indexPath.row])["image"] ?? "")", indicatorInView: cell.contentView)
                
                cell.deleteCheckImage.isHidden = true
                for i in 0..<self.webImagesToDeleteArray.count {
                    if self.webImagesToDeleteArray[i] == indexPath.row {
                        cell.deleteCheckImage.isHidden = false
                    }
                }
                
            }
            else {
                cell.cellImage.image = self.localImagesArray[indexPath.item - self.webImagesArray.count]
                
                cell.deleteCheckImage.isHidden = true
                for i in 0..<self.localImagesToDeleteArray.count {
                    if self.localImagesToDeleteArray[i] == (indexPath.row - self.webImagesArray.count) {
                        cell.deleteCheckImage.isHidden = false
                        
                        
                    }
                }
            }
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SuperAgentPlansCVC", for: indexPath) as! SuperAgentPlansCVC
            
            let tempDict = self.superAgentPlansArray[indexPath.row] as! NSDictionary
            //print(tempDict)
            cell.cellLBL.text = "\(tempDict["duration"] ?? "")" + "  \(tempDict["plan_valid_unit"] ?? "")" + "  \(tempDict["cost"] ?? "")" + "$"
            
            self.membershipButtonsArray.append(cell.cellButton)
            cell.cellButton.tag = indexPath.row
            //cell.cellButton.addTarget(self, action: #selector(self.membershipButtons(sender:)), for: .touchUpInside)
            
            
            //Set Font
            cell.cellLBL.font = UIFont(name: cell.cellLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
            
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == imageSlideshowCollectionview {
            let cellWidth = ((self.FullWidth - 10) - 30) / 4
            
            return CGSize(width: cellWidth, height: cellWidth)
        }
        else {
            return CGSize(width: (self.FullWidth - 40) / 2, height: 35)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == imageSlideshowCollectionview {
            return 10
        }
        else {
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == imageSlideshowCollectionview {
            return 10
        }
        else {
            return 0
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("GO")
        
    }
    
}



// MARK:- // Imagepicker Delegate Methods

extension MySuperAgentProfileVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        
        if self.pickerType == 1 {
            self.advertisementBannerImageview.isHidden = false
            self.advertisementBannerImageview.image = image
            
            self.bannerUploaded = true
        }
        else {
            self.localImagesArray.append(image)
            
            self.imageSlideshowCollectionview.reloadData()
        }
        
        if (self.localImagesArray.count + self.webImagesArray.count) == 8 {
            self.uploadInImageSlideshowButtonOutlet.isHidden = true
        }
        else {
            self.uploadInImageSlideshowButtonOutlet.isHidden = false
        }
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
}





// MARK:- // Textfield Delegate

extension MySuperAgentProfileVC: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView == self.titleTXT {
            
            let newText = (titleTXT.text! as NSString).replacingCharacters(in: range, with: text)
            let numberOfChars = newText.count
            
            return numberOfChars < 100
            
        }
        else {
            
            let newText = (descriptionTXT.text! as NSString).replacingCharacters(in: range, with: text)
            let numberOfChars = newText.count
            
            return numberOfChars < 1000
            
        }
        
        // Character Limit of 160
        
        
        
        //self.characterLimitLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "youCanAddOnly", comment: "") + " \(160-numberOfChars) " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "characters", comment: "")
        
        
        
    }
    
}


// MARK:- // Sorting array of indexes from high to low
extension Array {
    mutating func remove(at indexes: [Int]) {
        for index in indexes.sorted(by: >) {
            remove(at: index)
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
