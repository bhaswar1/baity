//
//  HomeVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 02/08/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView


class HomeVC: GlobalViewController,GMUClusterManagerDelegate {
    
    @IBOutlet weak var addImage: UIImageView!
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var mainStackview: UIStackView!
    @IBOutlet weak var segmentView: UIView!
    @IBOutlet weak var listBarView: UIView!
    @IBOutlet weak var mapBarView: UIView!
    @IBOutlet weak var superAgentView: UIView!
    @IBOutlet weak var superAgentCardImage: UIImageView!
    @IBOutlet weak var superAgentDescription: UILabel!
    @IBOutlet weak var superAgentImage: UIImageView!
    @IBOutlet weak var superAgentName: UILabel!
    @IBOutlet weak var listSection: UIView!
    @IBOutlet weak var homeListingTableview: UITableView!
    
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var sortTableView: UITableView!
    @IBOutlet weak var sortTableviewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var mapSection: UIView!
    @IBOutlet weak var mapVIew: GMSMapView!
    @IBOutlet weak var mapCollectionviewView: UIView!
    @IBOutlet weak var homeListingCollectionview: UICollectionView!
    @IBOutlet weak var nextPreviousView: UIView!
    @IBOutlet weak var toStatusLBL: UILabel!
    @IBOutlet weak var fromStatusLBL: UILabel!
    
    @IBOutlet weak var containerOfSuperAgentviewwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerOfSuperAgent: UIView!
    
    @IBOutlet weak var noResultsView: UIView!
    @IBOutlet weak var toogleMapTrailingConstant: NSLayoutConstraint!
    @IBOutlet weak var NoResultFoundStaticLbl: UILabel!
    @IBOutlet weak var homeLbl: UILabel!
    @IBOutlet weak var editFilterLbl: UILabel!
    @IBOutlet weak var NoResultMainView: UIView!
    
    
    var clickedMapMarkerArray = [Int]()
    
    var markerArray = [GMSMarker]()
    
    var startValue = 0
    var perLoad = 20
    
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var recordsArray = NSMutableArray()
    
    var mapDataArray = NSMutableArray()
    
    var listingArray : NSArray!
    var superAgentDictionary : NSDictionary!
    
    var countryID : String! = ""
    var cityID : String! = ""
    var districtID : String! = ""
    
    var agentID : String!
    
    var sortListArray = [sortTypeDataFormat]()
    
    var sortBy : String! = ""
    
    var testint : Int! = 0
    
    var totalCount : Int!
    
    var fromFilter : Bool! = false
    
    var KeywordVal : String! = ""
    
    var lat : String! = ""
    
    var lng : String! = ""
    
    var currency : String! = ""
    
    var formMap : String! = ""
    
    var markerViewArray = [UIView]()
    
    var paginationComplete : Bool! = true
    
    var advertisement: NSDictionary?
    
    
    @IBOutlet weak var listTitleView: UIView!
    @IBOutlet weak var listTitleLBL: UILabel!
    @IBOutlet weak var ListImageView: UIImageView!
    @IBOutlet weak var mapTitleView: UIView!
    @IBOutlet weak var mapTitleLBL: UILabel!
    @IBOutlet weak var MapImageView: UIImageView!
    
    private var clusterManager: GMUClusterManager!
    
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        globalFunctions.shared.loadPlaceholder(onView: self.view)
        
        self.SetFont()
        
        self.localizeStrings()
        
        //        var tempView : UIView = {
        //           let tempview = UIView()
        //            tempview.translatesAutoresizingMaskIntoConstraints = false
        //            tempview.backgroundColor = .red
        //            return tempview
        //        }()
        //
        //        self.view.addSubview(self.noNetworkView)
        //
        //        self.noNetworkView.anchor(top: self.view.topAnchor, leading: self.view.leadingAnchor, bottom: self.view.bottomAnchor, trailing: self.view.trailingAnchor)
        //
        //        self.view.bringSubviewToFront(self.noNetworkView)
        
        //Listing Screen Advertisements
        self.homeListingTableview.tableHeaderView = self.containerOfSuperAgent
//        self.containerOfSuperAgentviewwidthConstraint.constant = self.FullWidth
//        self.containerOfSuperAgent.bringSubviewToFront(self.superAgentView)
//        self.superAgentView.anchor(top: self.containerOfSuperAgent.topAnchor, leading: self.containerOfSuperAgent.leadingAnchor, bottom: self.containerOfSuperAgent.bottomAnchor, trailing: self.containerOfSuperAgent.trailingAnchor, padding: .init(top: 10, left: 5, bottom: 15, right: 5))
        
        let identifier = "RealEstateListingTVC"
        let nibName = UINib(nibName: "RealEstateListingTVC", bundle: nil)
        self.homeListingTableview.register(nibName, forCellReuseIdentifier: identifier)
        
        self.headerView.headerIconPositionOne.addTarget(self, action: #selector(self.sortClick), for: .touchUpInside)
        
        self.headerView.headerIconPositionTwo.addTarget(self, action: #selector(self.filterClick), for: .touchUpInside)
        
        if (userInformation.shared.searchLanguageCode.elementsEqual("ar")) {
            self.toogleMapTrailingConstant.constant = (FullWidth - 16.0 - self.toogleMapBtnOut.frame.size.width)
            self.nextButtonOutlet.transform = CGAffineTransform(rotationAngle: .pi)
            self.previousButtonOutlet.transform = CGAffineTransform(rotationAngle: .pi)
        }
        
        
        
                self.startValue = 0
                //self.testint = 0
                
                self.recordsArray.removeAllObjects()
                self.mapDataArray.removeAllObjects()
    
                
                self.getListingDetails(mapLoad: true)
                
                self.globalDispatchgroup.notify(queue: .main) {
                    
                    
                     //Show or hide No Results View
                    if self.listingEmpty {
        //                self.homeListingTableview.isHidden = true
        //                self.noResultsView.isHidden = false
        //                self.view.bringSubviewToFront(self.noResultsView)

                        self.headerView.searchResultsLBL.text = "( 0 ) \(globalFunctions.shared.getLanguageString(variable: "app_result_found"))"
                        self.superAgentDictionary = superAgentDtls.shared.superAgentInfo
                        self.NoResultMainView.isHidden = false
                        self.toogleMapBtnOut.isHidden = true
                        self.loadSuperAgent()
                        //self.containerOfSuperAgent.isHidden = true
                    }
                    else {
        //                self.homeListingTableview.isHidden = false
        //                self.noResultsView.isHidden = true
        //                self.view.sendSubviewToBack(self.noResultsView)
                    }
                    
                    if self.recordsArray.count > 0 {
                        self.loadSuperAgent()
                        
                    }
                    
                    print("\(self.testint! + 1)     ")
                    
                    self.toStatusLBL.text = "\(self.testint! + 1)     " + "\(globalFunctions.shared.getLanguageString(variable: "home_to"))     " + "\(self.mapDataArray.count)"
                    globalFunctions.shared.removePlaceholder(fromView: self.view)
                }
        
        
    }
    
    
    // MARK:- // View Will Appear
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        if self.listSection.isHidden {
//
//        }
//        else {
//            self.ListImageView.image = UIImage(named: "white list")
//            self.MapImageView.image = UIImage(named: "black map")
//
//            self.listButtonOutlet.titleLabel?.textColor = .white
//        }
        
        
        self.sideMenuLeadingConstraint.constant = -self.FullWidth
        self.sideMenuTrailingConstraint.constant = -self.FullWidth
        
//        self.startValue = 0
//        self.testint = 0
//
//        self.recordsArray.removeAllObjects()
//        self.mapDataArray.removeAllObjects()
////        self.markerViewArray.removeAll()
////        self.markerArray.removeAll()
////        self.clickedMapMarkerArray.removeAll()
//
//        self.getListingDetails(mapLoad: true)
//
//        self.globalDispatchgroup.notify(queue: .main) {
//
//
//             //Show or hide No Results View
//            if self.listingEmpty {
////                self.homeListingTableview.isHidden = true
////                self.noResultsView.isHidden = false
////                self.view.bringSubviewToFront(self.noResultsView)
//
//                self.headerView.searchResultsLBL.text = "( 0 ) results"
//
//                self.containerOfSuperAgent.isHidden = true
//            }
//            else {
////                self.homeListingTableview.isHidden = false
////                self.noResultsView.isHidden = true
////                self.view.sendSubviewToBack(self.noResultsView)
//            }
//
//            if self.recordsArray.count > 0 {
//                self.loadSuperAgent()
//
//            }
//
//            print("\(self.testint! + 1)     ")
//
//            self.toStatusLBL.text = "\(self.testint! + 1)     " + "\(globalFunctions.shared.getLanguageString(variable: "home_to"))     " + "\(self.mapDataArray.count)"
//            globalFunctions.shared.removePlaceholder(fromView: self.view)
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.headerView.contentVIew.backgroundColor = UIColor(red: 0.117, green: 0.282, blue: 0.454, alpha: 1.0)
        self.headerView.headerTitle.backgroundColor = UIColor(red: 0.117, green: 0.282, blue: 0.454, alpha: 1.0)
    }
    
    
    
    // MARK:- // Buttons
    
    // MARK:- // Home Button
    
    @IBOutlet weak var homeButtonOutlet: UIButton!
    @IBAction func homeButton(_ sender: UIButton) {
    }
    //MARK:- Click on Add button
    @IBAction func clickOnAddBtn(_ sender: UIButton) {
        guard let url = URL(string: "\(self.advertisement!["url"] ?? "")") else { return }
        UIApplication.shared.open(url)
    }
    
    
    // MARK:- // Reset Filter
    
    @IBOutlet weak var resetFilterOutlet: UIButton!
    @IBAction func resetFilter(_ sender: UIButton) {
        
        let filterDict = filterDictionary(parentCategory: "", propertyType: "", categorySub: "", period: "", country: userInformation.shared.searchCountryID, province: "", city: "", district: "", minPrice: "", maxPrice: "", minPriceID: "", maxPriceID: "", minArea: "", maxArea: "", viewDays: "", addedBy: "", negotiablePrice: "")
        
        userInformation.shared.setUserFilterDictionary(dict: filterDict)
        
        self.fromFilter = true
        
        
        
        self.getListingDetails(mapLoad: true)
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            if self.listingEmpty {
                self.homeListingTableview.isHidden = true
                self.addView.isHidden = true
                self.noResultsView.isHidden = false
                self.view.bringSubviewToFront(self.noResultsView)
                
                self.headerView.searchResultsLBL.text = "( 0 ) \(globalFunctions.shared.getLanguageString(variable: "app_result_found"))"
                self.superAgentDictionary = superAgentDtls.shared.superAgentInfo
                self.NoResultMainView.isHidden = false
                self.toogleMapBtnOut.isHidden = true
                self.loadSuperAgent()
                //self.containerOfSuperAgent.isHidden = true
            }
            else {
                self.NoResultMainView.isHidden = true
                self.homeListingTableview.isHidden = false
                self.addView.isHidden = false
                self.noResultsView.isHidden = true
                self.view.sendSubviewToBack(self.noResultsView)
                
                self.containerOfSuperAgent.isHidden = false
                
                self.loadSuperAgent()
            }
            
        }
        
    }
    
    
    
    // MARK:- // Filter Icon
    
    @IBOutlet weak var filterIconOutlet: UIButton!
    @IBAction func filterIcon(_ sender: UIButton) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    // MARK:- // List Button
    
    @IBOutlet weak var listButtonOutlet: UIButton!
    @IBAction func listButton(_ sender: UIButton) {
        
        self.listSection.isHidden = false
        self.mapSection.isHidden = true
        
        self.ListImageView.image = UIImage(named: "black list")
        self.MapImageView.image = UIImage(named: "white map")
        
        self.mapTitleView.backgroundColor = UIColor(red:0/255, green:135/255, blue:228/255, alpha: 1)
        self.mapBarView.backgroundColor = .clear
        self.mapTitleLBL.textColor = .white
        
        self.listTitleView.backgroundColor = .white
        self.listBarView.backgroundColor = UIColor(red:255/255, green:255/255, blue:0/255, alpha: 1)
        self.listTitleLBL.textColor = .black
        
        self.headerView.headerIconPositionOne.isHidden = false
        self.headerView.iconImageOne.isHidden = false
        
        // Show or hide No Results View
        if self.listingEmpty {
//            self.homeListingTableview.isHidden = true
//            self.noResultsView.isHidden = false
//            self.view.bringSubviewToFront(self.noResultsView)

            self.headerView.searchResultsLBL.text = "( 0 ) \(globalFunctions.shared.getLanguageString(variable: "app_result_found"))"
            self.superAgentDictionary = superAgentDtls.shared.superAgentInfo
            self.NoResultMainView.isHidden = false
            self.toogleMapBtnOut.isHidden = true
            self.loadSuperAgent()
            //self.containerOfSuperAgent.isHidden = true
        }
        else {
//            self.homeListingTableview.isHidden = false
//            self.noResultsView.isHidden = true
//            self.view.sendSubviewToBack(self.noResultsView)

            self.loadSuperAgent()
        }
        
//        self.startValue = 0
//
//        self.recordsArray.removeAllObjects()
//
//        self.getListingDetails()
        
        
        //
        //        self.globalDispatchgroup.notify(queue: .main) {
        //
        //            self.toStatusLBL.text = "\(self.testint! + 1)" + "     To     " + "\(self.mapDataArray.count)"
        //
        //        }
        
    }
    
    
    // MARK:- // Map Button
    
    @IBOutlet weak var mapButtonOutlet: UIButton!
    @IBAction func mapButton(_ sender: UIButton) {
        
        self.listSection.isHidden = true
        self.mapVIew.layoutIfNeeded()
        self.mapSection.isHidden = false
        self.NoResultMainView.isHidden = true
        
        self.ListImageView.image = UIImage(named: "white list")
        self.MapImageView.image = UIImage(named: "black map")
        
        self.listTitleView.backgroundColor = UIColor(red:0/255, green:135/255, blue:228/255, alpha: 1)
        self.listBarView.backgroundColor = .clear
        self.listTitleLBL.textColor = .white
        
        self.mapTitleView.backgroundColor = .white
        self.mapBarView.backgroundColor = UIColor(red:255/255, green:255/255, blue:0/255, alpha: 1)
        self.mapTitleLBL.textColor = .black
        
        self.headerView.headerIconPositionOne.isHidden = true
        self.headerView.iconImageOne.isHidden = true
        
        
        if self.recordsArray.count > 0 {
            self.mapCollectionviewView.isHidden = false
            self.nextPreviousView.isHidden = false
        }
        else {
            self.mapCollectionviewView.isHidden = true
            self.nextPreviousView.isHidden = true
            
            self.noResultsView.isHidden = true
            self.view.sendSubviewToBack(self.noResultsView)
        }
//
//        self.startValue = 0
//
//        self.recordsArray.removeAllObjects()
//
//        self.getListingDetails()
//
        
        //
        //        self.globalDispatchgroup.notify(queue: .main) {
        //
        //            self.toStatusLBL.text = "\(self.testint! + 1)" + "     To     " + "\(self.mapDataArray.count)"
        //
        //        }
        //
        
    }
    
    // MARK:- // Super Agent Button
    
    @IBOutlet weak var superAgentButtonOutlet: UIButton!
    @IBAction func superAgentButton(_ sender: UIButton) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "SuperAgentDetailsVC") as! SuperAgentDetailsVC
        
        navigate.agentID = self.agentID
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    // MARK:- // Close Sortview
    
    
    @IBOutlet weak var closeSortviewButtonOutlet: UIButton!
    @IBAction func closeSortviewButton(_ sender: UIButton) {
        
        self.sortView.isHidden = true
        self.view.sendSubviewToBack(self.sortView)
        self.sortTableviewHeightConstraint.constant = 0
        
    }
    
    // MARK:- // Next Button
    
    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBAction func nextButton(_ sender: UIButton) {
        
        if self.mapDataArray.count > 0 {
            self.moveToNext()
        }
    }
    
    // MARK:- // Previous Button
    
    @IBOutlet weak var previousButtonOutlet: UIButton!
    @IBAction func previousButton(_ sender: UIButton) {
        
        if self.mapDataArray.count > 0 {
            self.movetoPrevious()
        }
        
    }
    
    // MARK:- // Sort CLick
    
    @objc func sortClick() {
        
        view.bringSubviewToFront(self.sortView)
        
        UIView.animate(withDuration: 0.3, animations: {
            
            if self.sortView.isHidden == true
            {
                
                self.getSortingTypes()
                
                self.globalDispatchgroup.notify(queue: .main, execute: {
                    
                    self.sortView.isHidden = false
                    globalFunctions.shared.SetTableheight(table: self.sortTableView, heightConstraint: self.sortTableviewHeightConstraint)
                    
                })
                
                
            }
            else
            {
                self.sortView.isHidden = true
                self.view.sendSubviewToBack(self.sortView)
                self.sortTableviewHeightConstraint.constant = 0
            }
        }, completion: nil)
    }
    
    // MARK:- // Filter Click
    
    @objc func filterClick() {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    //MARK:- //Filter Button Click
    @IBAction func btnFilter(_ sender: UIButton) {
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    //MARK:- Button Home Click
    @IBAction func btnHome(_ sender: UIButton) {
        
        let filterDict = filterDictionary(parentCategory: "", propertyType: "", categorySub: "", period: "", country: userInformation.shared.searchCountryID, province: "", city: "", district: "", minPrice: "", maxPrice: "", minPriceID: "", maxPriceID: "", minArea: "", maxArea: "", viewDays: "", addedBy: "", negotiablePrice: "")
        
        userInformation.shared.setUserFilterDictionary(dict: filterDict)
        
        self.fromFilter = true
        
        
        
        self.getListingDetails(mapLoad: true)
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            if self.listingEmpty {
                self.homeListingTableview.isHidden = true
                self.addView.isHidden = true
                self.noResultsView.isHidden = false
                self.view.bringSubviewToFront(self.noResultsView)
                
                self.headerView.searchResultsLBL.text = "( 0 ) \(globalFunctions.shared.getLanguageString(variable: "app_result_found"))"
                self.superAgentDictionary = superAgentDtls.shared.superAgentInfo
                self.NoResultMainView.isHidden = false
                 self.toogleMapBtnOut.isHidden = true
                self.loadSuperAgent()
                //self.containerOfSuperAgent.isHidden = true
            }
            else {
                self.NoResultMainView.isHidden = true
                self.homeListingTableview.isHidden = false
                self.addView.isHidden = false
                self.noResultsView.isHidden = true
                self.view.sendSubviewToBack(self.noResultsView)
                
                self.containerOfSuperAgent.isHidden = false
                
                self.loadSuperAgent()
            }
            
        }
        
    }
    
    
    // MARK: - // Load Super Agent Section
    
    func loadSuperAgent()
    {
        //print("\(superAgentDictionary)")
        if superAgentDictionary != nil{
            self.superAgentCardImage.sd_setImage(with: URL(string: "\(superAgentDictionary["card_image"] ?? "")"))
            
            self.superAgentImage.sd_setImage(with: URL(string: "\(superAgentDictionary["super_agent_profile_image"] ?? "")"))
            
            
            self.superAgentName.text = "\(superAgentDictionary["title"]!)"
            //print(superAgentDictionary["title"]!)
            self.superAgentDescription.text = "\(superAgentDictionary["description"]!)"
            
            self.agentID = "\(superAgentDictionary["id"] ?? "")"
        }
        else{
            
        }
        
        
    }
    
    
    
    
    
    
    // MARK: - // JSON Get Method to get ALL Data from Web
    
    
    func getListingDetails(mapLoad : Bool) {
        
        var parameters : String!
        
        self.countryID = userInformation.shared.searchCountryID ?? ""
        
        if self.fromFilter {
            
            self.cityID = userInformation.shared.userFilterDictionary.city ?? ""
            self.districtID = userInformation.shared.userFilterDictionary.district ?? ""
        }
        else {
            self.cityID = userInformation.shared.searchCityID ?? ""
            self.districtID = userInformation.shared.searchDistrictID ?? ""
        }
//        print(self.cityID, self.districtID)
        if self.fromFilter && self.cityID != "" && self.districtID == "" {
            self.zoomStatus = "50km"
        }
        else if  self.cityID != "" && self.districtID == ""{
            self.zoomStatus = "100km"
        }
        else if self.cityID != "" && self.districtID != ""{
            self.zoomStatus = "10km"
        }
        else{
            self.zoomStatus = "50km"
        }
//        print(self.zoomStatus)
//        self.countryID = userInformation.shared.searchCountryID ?? ""
//        self.cityID = userInformation.shared.searchCityID ?? ""
//        self.districtID = userInformation.shared.searchDistrictID ?? ""
        
        //&city=\(self.cityID ?? "")&district=\(self.districtID ?? "")
        
        //print("CITY ID ----\(userInformation.shared.userFilterDictionary.city ?? "")")
        
        parameters = "start_value=\(startValue)&per_load=\(perLoad)&sort_by=\(sortBy ?? "")&country_id=\(self.countryID ?? "")&city=\(self.cityID ?? "")&district=\(self.districtID ?? "")&parent_category=\(userInformation.shared.userFilterDictionary.parentCategory ?? "")&property_type=\(userInformation.shared.userFilterDictionary.propertyType ?? "")&category_sub=\(userInformation.shared.userFilterDictionary.categorySub ?? "")&period=\(userInformation.shared.userFilterDictionary.period ?? "")&province_val=\(userInformation.shared.userFilterDictionary.province ?? "")&min_price=\(userInformation.shared.userFilterDictionary.minPrice ?? "")&max_price=\(userInformation.shared.userFilterDictionary.maxPrice ?? "")&min_price_id=\(userInformation.shared.userFilterDictionary.minPriceID ?? "")&max_price_id=\(userInformation.shared.userFilterDictionary.maxPriceID ?? "")&min_area=\(userInformation.shared.userFilterDictionary.minArea ?? "")&max_area=\(userInformation.shared.userFilterDictionary.maxArea ?? "")&view_days=\(userInformation.shared.userFilterDictionary.viewDays ?? "")&added_by=\(userInformation.shared.userFilterDictionary.addedBy ?? "")&negotiablePrice=\(userInformation.shared.userFilterDictionary.negotiablePrice ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)&keyword_val=\(KeywordVal ?? "")&lat=\(lat ?? "")&lng=\(lng ?? "")&currency=\(currency ?? "")&form_map=\(formMap ?? "")" + "\(userInformation.shared.userDynamicFilterString ?? "")"
        
        self.CallAPI(urlString: "home_all_property_listing", param: parameters) {
            
            if let addInfo = self.globalJson["advertisement"] as? NSDictionary {
                
                self.advertisement = addInfo
                self.addImage.sd_setImage(with: URL(string: "\(self.advertisement!["image"] ?? "")"))
            }
            else {
                //Not Dictionary
                self.addView.isHidden = true
            }
            
            self.superAgentDictionary = self.globalJson["super_agent_info"] as? NSDictionary
            superAgentDtls.shared.storeAgentInfo(superAgentinfo: self.superAgentDictionary)
            
            self.listingArray = self.globalJson["info"] as? NSArray
            
            if mapLoad == true {
                self.mapDataArray.removeAllObjects()
                self.markerArray.removeAll()
                self.clickedMapMarkerArray.removeAll()
            }
            
            
            for i in 0..<self.listingArray.count
                
            {
                
                let tempDict = self.listingArray[i] as! NSDictionary
                
                print(tempDict)
                
                self.recordsArray.add(tempDict)
                if mapLoad == true {
                    self.mapDataArray.add(tempDict)
                }
                
            }
            
            self.totalCount = Int("\(self.globalJson["total_count"]!)")
            
            self.nextStart = "\(self.globalJson["next_start"]!)"
            
            self.startValue = self.startValue + self.listingArray.count
            
            print("Next Start Value : " , self.startValue)
            
            DispatchQueue.main.async {
                self.toogleMapBtnOut.isHidden = false
                self.NoResultMainView.isHidden = true
                self.globalDispatchgroup.leave()
                
                self.fromStatusLBL.text = "\(globalFunctions.shared.getLanguageString(variable: "from")) \(self.totalCount!)"
                
//                if "\(UserDefaults.standard.string(forKey: "searchLanguageCode") ?? "")".elementsEqual("ar") {
//
//                    self.fromStatusLBL.text = "\(self.totalCount!) \(globalFunctions.shared.getLanguageString(variable: "from"))"
//
//                }
//                else {
//                    self.fromStatusLBL.text = "\(globalFunctions.shared.getLanguageString(variable: "from")) \(self.totalCount!)"
//                }
                
                
                
                self.homeListingTableview.delegate = self
                self.homeListingTableview.dataSource = self
                self.homeListingTableview.reloadData()
                
                self.homeListingCollectionview.delegate = self
                self.homeListingCollectionview.dataSource = self
                self.homeListingCollectionview.reloadData()
                
                self.generateMapSection()
                
                self.headerView.searchResultsLBL.text = "( \(self.globalJson["total_count"] ?? "")" + " ) \(globalFunctions.shared.getLanguageString(variable: "app_result_found"))"
                
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
            }
        }
    }
    
    
    
    
    // MARK: - // JSON Post Method to get Sorting Types
    
    
    func getSortingTypes() {
        
        let parameters = "language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "filter_by_values", param: parameters) {
            
            self.sortListArray.removeAll()
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                
                let tempDict = (self.globalJson["info"] as! NSArray)[i] as! NSDictionary
                
                self.sortListArray.append(sortTypeDataFormat(sortID: "\(tempDict["filter_value"] ?? "")", sortName: "\(tempDict["filter_name"] ?? "")", isSelected: false))
                
            }
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.sortTableView.delegate = self
                self.sortTableView.dataSource = self
                self.sortTableView.reloadData()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
            }
            
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.listTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "home_list")
        self.mapTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "home_map")
        self.NoResultFoundStaticLbl.text = globalFunctions.shared.getLanguageString(variable: "app_no_result_found")
        self.homeLbl.text = globalFunctions.shared.getLanguageString(variable: "app_home")
        self.editFilterLbl.text = globalFunctions.shared.getLanguageString(variable: "app_edit_filter")
        
    }
    
    
    // MARK:- // Set Font
    
    func SetFont() {
        
//        self.listButtonOutlet.titleLabel?.font = UIFont(name: (self.listButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 16)))
//        self.mapButtonOutlet.titleLabel?.font = UIFont(name: (self.mapButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 16)))
        self.listTitleLBL.font = UIFont(name: "Verdana-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.mapTitleLBL.font = UIFont(name: "Verdana-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.NoResultFoundStaticLbl.font = UIFont(name: "Verdana-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.homeLbl.font = UIFont(name: "Verdana-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.editFilterLbl.font = UIFont(name: "Verdana-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
    }
    
    
    //MARK:- Toogle map button Action
    @IBOutlet weak var toogleMapBtnOut: UIButton!
    @IBAction func toogleMapView(_ sender: UIButton) {
        if mapVIew.mapType == .normal{
            mapVIew.mapType = .hybrid
            mapVIew.isMyLocationEnabled = true
            mapVIew.settings.myLocationButton = true
            toogleMapBtnOut.setImage(UIImage(named: "maps"), for: .normal)
        }
        else{
            toogleMapBtnOut.setImage(UIImage(named: "Satelite"), for: .normal)
            mapVIew.mapType = .normal
            mapVIew.isMyLocationEnabled = true
            mapVIew.settings.myLocationButton = true
        }
    }
    
    
    // MARK:- // MAP SECTION
    
    // MARK:- // Generate Map Section
    
    func generateMapSection()
    {
        toogleMapBtnOut.layer.cornerRadius = 22.5
        toogleMapBtnOut.layer.borderWidth = 0.25
        toogleMapBtnOut.layer.borderColor = UIColor.lightGray.cgColor
        //        self.nextPreviousStatusLBL.text = "1   to   " + "\(recordsArray.count)"
        //
        let tempDict:NSDictionary = self.mapDataArray[0] as! NSDictionary
        
        mapVIew.camera = GMSCameraPosition.camera(withLatitude: Double("\(tempDict["latitude"]!)")!, longitude: Double("\(tempDict["longitude"]!)")!, zoom: 9.65)
        //mapVIew.mapType = .satellite
        mapVIew.mapType = .normal
        mapVIew.isMyLocationEnabled = true
        mapVIew.settings.myLocationButton = true
        mapVIew.padding = UIEdgeInsets(top: 0, left: 0, bottom: 90, right: 0)
        mapVIew.delegate = self
        
        
        for i in 0..<self.mapDataArray.count {
            
            //let markerView = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: 65, height: 30), image: UIImage(named: "map")!, borderColor: .red, tag: i, text: "\((self.mapDataArray[i] as! NSDictionary)["map_price"] ?? "")" + " \((self.mapDataArray[i] as! NSDictionary)["currency_symbol"] ?? "")")
            
            var markerImage = UIImage()
            
            if ("\((mapDataArray[i] as! NSDictionary)["ribbon_value"] ?? "")".elementsEqual("sale"))
            {
                markerImage = UIImage(named: "map3x-(g)")!
            }
            else
            {
                markerImage = UIImage(named: "map3x-(y)")!
            }
            
            let markerView = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: 80, height: 40), image: markerImage, borderColor: .clear, tag: i, text: "\((self.mapDataArray[i] as! NSDictionary)["map_price"] ?? "")" + " \((self.mapDataArray[i] as! NSDictionary)["currency_symbol"] ?? "")")
            
//            markerView.layer.cornerRadius = 20
//            markerView.clipsToBounds = true
            
            self.markerViewArray.append(markerView)
            
            if ("\((mapDataArray[i] as! NSDictionary)["ribbon_value"] ?? "")".elementsEqual("sale"))
            {
                markerView.setupViews(showText: "\((self.mapDataArray[i] as! NSDictionary)["map_price"] ?? "")" + " \((self.mapDataArray[i] as! NSDictionary)["currency_symbol"] ?? "")", color: .white)
            }
            else
            {
                markerView.setupViews(showText: "\((self.mapDataArray[i] as! NSDictionary)["map_price"] ?? "")" + " \((self.mapDataArray[i] as! NSDictionary)["currency_symbol"] ?? "")", color: .black)
            }
            
            
            let markerIcon : UIImage = markerView.asImage()
            
            let marker = GMSMarker()
            
            marker.icon = markerIcon
            //marker.iconView = markerView
            marker.userData = ["tag" : i , "markerImage" : markerImage , "text" : "\((self.mapDataArray[i] as! NSDictionary)["map_price"] ?? "")" + " \((self.mapDataArray[i] as! NSDictionary)["currency_symbol"] ?? "")"]
            
            marker.position = CLLocationCoordinate2D(latitude: Double("\((self.mapDataArray[i] as! NSDictionary)["latitude"]!)")!, longitude: Double("\((self.mapDataArray[i] as! NSDictionary)["longitude"]!)")!)
            
            marker.map = self.mapVIew
            
            self.markerArray.append(marker)
            
        }
        
        let markerView = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: 100, height: 40), image: UIImage(named: "map3x-(w)")!, borderColor: .clear, tag: 0, text: (self.markerArray[0].userData as! [String : Any])["text"] as! String)
        

        
        let markerIcon : UIImage = markerView.asImage()
        
        self.markerArray[0].icon = markerIcon
        
        if self.clickedMapMarkerArray.contains(0) == false {
            
            self.clickedMapMarkerArray.append(0)
            
        }
        self.setZposition(ind: 0)
        //self.updateZoomLebel()
    }
    
    
    
    
    // MARK:- // Move to Next
    
    func moveToNext() {
        
        if self.totalCount > (self.testint + 20) {
            
            self.mapVIew.clear()
            
            
            self.testint = self.testint + 20
            
            self.startValue = self.testint
            
            self.getListingDetails(mapLoad: true)
            
            self.globalDispatchgroup.notify(queue: .main) {
                
                self.toStatusLBL.text = "\(self.testint! + 1)     " + "\(globalFunctions.shared.getLanguageString(variable: "home_to"))     " + "\(self.testint + self.mapDataArray.count)"
                
//                self.toStatusLBL.text = "\(self.testint + 1)" + "     To     " + "\(self.testint + self.mapDataArray.count)"
                
                self.moveCamera(dataIndex: 0)
                
                
                self.homeListingTableview.reloadData()
                self.homeListingCollectionview.reloadData()
                
                // Scrolling CollectionView According to Selected Property
                let nextItem: IndexPath = IndexPath(item: 0, section: 0)
                // Scroll happens here
                self.homeListingCollectionview.scrollToItem(at: nextItem, at: .left, animated: true)
            }
            
        }
        
        
        /*
         if (testint + 1) == (recordsArray.count) { // tapping next on last item
         
         self.getListingDetails()
         
         self.globalDispatchgroup.notify(queue: .main) {
         
         if ((self.testint + 1) == (self.recordsArray.count)) == false {
         
         self.testint = self.testint + 1
         
         self.moveCamera(dataIndex: self.testint)
         
         self.nextPreviousStatusLBL.text = "\(self.testint!)" + "   to   " + "\(self.recordsArray.count)"
         
         // Scrolling CollectionView According to Selected Property
         let nextItem: IndexPath = IndexPath(item: self.testint, section: 0)
         // Scroll happens here
         self.homeListingCollectionview.scrollToItem(at: nextItem, at: .left, animated: true)
         
         }
         }
         
         }
         else { // tapping next normally
         
         self.testint = self.testint + 1
         
         self.moveCamera(dataIndex: self.testint)
         
         self.nextPreviousStatusLBL.text = "\(self.testint! + 1)" + "   to   " + "\(self.recordsArray.count)"
         
         // Scrolling CollectionView According to Selected Property
         let nextItem: IndexPath = IndexPath(item: self.testint, section: 0)
         // Scroll happens here
         self.homeListingCollectionview.scrollToItem(at: nextItem, at: .left, animated: true)
         
         }
         */
    }
    
    
    // MARK:- // Move to Previous
    
    func movetoPrevious() {
        
        print("\(self.testint!)")
        
        if (self.testint - 20) >= 0 {
            
            
            self.mapVIew.clear()
            
            self.testint = self.testint - 20
            
            self.startValue = self.testint
            
            self.getListingDetails(mapLoad: true)
            
            self.globalDispatchgroup.notify(queue: .main) {
                
                
                
        
                self.toStatusLBL.text = "\(1)     " + "\(globalFunctions.shared.getLanguageString(variable: "home_to"))     " + "\(self.testint! + self.mapDataArray.count)"
                
                //                self.toStatusLBL.text = "\(self.testint + 1)" + "     To     " + "\(self.testint + self.mapDataArray.count)"
                
                self.moveCamera(dataIndex: 0)
                
                
                self.homeListingTableview.reloadData()
                self.homeListingCollectionview.reloadData()
                
                // Scrolling CollectionView According to Selected Property
                let nextItem: IndexPath = IndexPath(item: 0, section: 0)
                // Scroll happens here
                self.homeListingCollectionview.scrollToItem(at: nextItem, at: .left, animated: true)
            }
            
            
            
            
            
            
//            self.toStatusLBL.text = "\(self.testint - 19)     " + "\(globalFunctions.shared.getLanguageString(variable: "home_to"))     " + "\(self.testint!)"
//
//            //self.toStatusLBL.text = "\(self.testint - 19)" + "     To     " + "\(self.testint!)"
//
//            self.moveCamera(dataIndex: 0)
//
//
//            self.testint = self.testint - 20
//
//            self.startValue = self.testint
//
//            self.mapDataArray.removeAllObjects()
//
//            for i in 0..<20 {
//
//                let tempcount = testint + i
//
//                self.mapDataArray.add(self.recordsArray[tempcount])
//
//            }
//
//            self.homeListingCollectionview.reloadData()
//
//            // Scrolling CollectionView According to Selected Property
//            let nextItem: IndexPath = IndexPath(item: 0, section: 0)
//            // Scroll happens here
//            self.homeListingCollectionview.scrollToItem(at: nextItem, at: .left, animated: true)
        }
        
        
        /*
         if ((testint - 1) < 0) == false {
         
         testint = testint - 1
         
         self.moveCamera(dataIndex: testint)
         
         nextPreviousStatusLBL.text = "\(testint! + 1)" + "   to   " + "\(recordsArray.count)"
         
         // Scrolling CollectionView According to Selected Property
         let nextItem: IndexPath = IndexPath(item: testint, section: 0)
         // Scroll happens here
         self.homeListingCollectionview.scrollToItem(at: nextItem, at: .left, animated: true)
         
         }
         */
        
    }
    
    
    
    // MARK:- // Move Camera
    
    func moveCamera(dataIndex : Int) {
        
        let tempDict:NSDictionary = self.mapDataArray[dataIndex] as! NSDictionary
        
        let lat  = "\(tempDict["latitude"]!)"
        let long = "\(tempDict["longitude"]!)"
        
        let coordinates = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!)
        
        // Keep Rotation Short
        CATransaction.begin()
        CATransaction.setAnimationDuration(2)
        
        CATransaction.commit()
        
        // Movement
        CATransaction.begin()
        CATransaction.setAnimationDuration(2)
        
        // Center Map View
        let camera = GMSCameraUpdate.setTarget(coordinates)
        mapVIew.animate(with: camera)
        
        CATransaction.commit()
        
    }
    
    
    
    
    
    // MARK:- // Highlight Map Marker
    
    func highlightMapMarker(markerToChange : GMSMarker) {
        
        let tempInt : Int = (markerToChange.userData as! [String : Any])["tag"] as! Int
        print(tempInt)
        
        if self.clickedMapMarkerArray.contains(tempInt) == false {
            
            self.clickedMapMarkerArray.append(tempInt)
            
        }
        
        for i in 0..<self.markerArray.count {
            
            for j in 0..<self.clickedMapMarkerArray.count {
                
                if ((self.markerArray[i].userData as! [String : Any])["tag"] as! Int) == (self.clickedMapMarkerArray[j]) {
                    
                    let markerView = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: 100, height: 40), image: UIImage(named: "map3x-(g)-1")!, borderColor: .clear, tag: ((self.markerArray[i].userData as! [String : Any])["tag"] as! Int), text: (self.markerArray[i].userData as! [String : Any])["text"] as! String)

                    
                    let markerIcon : UIImage = markerView.asImage()
                    
                    markerArray[i].icon = markerIcon
                    
                }
                
            }
            
        }
        
        
        let markerView = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: 100, height: 40), image: UIImage(named: "map3x-(w)")!, borderColor: .clear, tag: tempInt, text: (markerToChange.userData as! [String : Any])["text"] as! String)
        
        
        let markerIcon : UIImage = markerView.asImage()
        
        markerToChange.icon = markerIcon
     
        self.setZposition(ind: tempInt)
    }
    
    //MARK:- Func for Set Zposition
    func setZposition(ind: Int){
        for i in 0..<markerArray.count{
            if i == ind{
                markerArray[i].zIndex = 101
            }
            else{
                markerArray[i].zIndex = 100
            }
        }
    }
    
    func updateZoomLebel(){
        var val: Float!
        if self.zoomStatus == "100km"{
            val = 8.7
        }
        else if self.zoomStatus == "50km"{
            val = 9.65
        }
        else if self.zoomStatus == "3km"{
            val = 15.0
        }
        else{
            val = 12.0
        }
        let zoomCamera = GMSCameraUpdate.zoom(to: val)
        mapVIew.animate(with: zoomCamera)
    }
    
}//End of View Controller












// MARK:- // Tableview Delegate Methods

extension HomeVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == homeListingTableview
        {
            return self.recordsArray.count
        }
        else
        {
            return self.sortListArray.count
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == homeListingTableview
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "RealEstateListingTVC") as! RealEstateListingTVC
            
            globalFunctions.shared.loadImage(inImageview: cell.propertyImage, url: "\((self.recordsArray[indexPath.row] as! NSDictionary)["property_image"] ?? "")", indicatorInView: cell.cellView)
            
            cell.imageCount.text = " \((self.recordsArray[indexPath.row] as! NSDictionary)["total_image"] ?? "") "
            
            cell.parentCategoryImage.sd_setImage(with: URL(string: "\((self.recordsArray[indexPath.row] as! NSDictionary)["parent_cat_image"] ?? "")"))
            
//            if ((UserDefaults.standard.string(forKey: "searchLanguageCode"))!.elementsEqual("ar")){
//                cell.propertyType.text = "\((recordsArray[indexPath.row] as! NSDictionary)["property_type"] ?? "")" + "(\((recordsArray[indexPath.row] as! NSDictionary)["area"] ?? "") \(globalFunctions.shared.getLanguageString(variable: "app_home_m2"))2)"//  + "\((recordsArray[indexPath.row] as! NSDictionary)["ribbon_value_lang"] ?? "")"
//                let font:UIFont? = UIFont(name: cell.propertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
//                let fontSuper:UIFont? = UIFont(name: cell.propertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 5)))
//                let attString:NSMutableAttributedString = NSMutableAttributedString(string: cell.propertyType.text!, attributes: [.font:font!])
//                attString.setAttributes([.font:fontSuper!,.baselineOffset:5], range: NSRange(location:(cell.propertyType.text!.count - 2),length:1))
//                cell.propertyType.attributedText = attString
//
//            }
          //  else{
            if (recordsArray[indexPath.row] as! NSDictionary)["area"] as! String == ""{
                cell.propertyType.text = "\((recordsArray[indexPath.row] as! NSDictionary)["property_type"] ?? "")"
                cell.propertyType.font = UIFont(name: cell.propertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            }
            else{
                cell.propertyType.text = "\((recordsArray[indexPath.row] as! NSDictionary)["property_type"] ?? "")" + "(\((recordsArray[indexPath.row] as! NSDictionary)["area"] ?? "") \(globalFunctions.shared.getLanguageString(variable: "app_home_m2"))2)"//  + "\((recordsArray[indexPath.row] as! NSDictionary)["ribbon_value_lang"] ?? "")"
                let font:UIFont? = UIFont(name: cell.propertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
                let fontSuper:UIFont? = UIFont(name: cell.propertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 6)))
                let attString:NSMutableAttributedString = NSMutableAttributedString(string: cell.propertyType.text!, attributes: [.font:font!])
                attString.setAttributes([.font:fontSuper!,.baselineOffset:5], range: NSRange(location:(cell.propertyType.text!.count - 2),length:1))
                cell.propertyType.attributedText = attString
            }

         //   }
            
            
            
            //cell.address.text = "\((recordsArray[indexPath.row] as! NSDictionary)["province"] ?? "")"
            
            
            
            cell.city.text = "\((recordsArray[indexPath.row] as! NSDictionary)["distric"] ?? "")"
            cell.district.text = "\((recordsArray[indexPath.row] as! NSDictionary)["city"] ?? "")"
            
            cell.bedCount.text = ((recordsArray[indexPath.row] as! NSDictionary)["bedroom"] as! String) == "" ? "0" : "\((recordsArray[indexPath.row] as! NSDictionary)["bedroom"] ?? "0")"
            cell.livingRoomCount.text = ((recordsArray[indexPath.row] as! NSDictionary)["livingroom"] as! String) == "" ? "0" : "\((recordsArray[indexPath.row] as! NSDictionary)["livingroom"] ?? "0")"//"\((recordsArray[indexPath.row] as! NSDictionary)["livingroom"] ?? "0")"
            cell.bathroomCount.text = ((recordsArray[indexPath.row] as! NSDictionary)["bathroom"] as! String) == "" ? "0" : "\((recordsArray[indexPath.row] as! NSDictionary)["bathroom"] ?? "0")"//"\((recordsArray[indexPath.row] as! NSDictionary)["bathroom"] ?? "0")"
            cell.diningCount.text = ((recordsArray[indexPath.row] as! NSDictionary)["kichen"] as! String) == "" ? "0" : "\((recordsArray[indexPath.row] as! NSDictionary)["kichen"] ?? "0")"//"\((recordsArray[indexPath.row] as! NSDictionary)["kichen"] ?? "0")"
            
            cell.listingDate.text = "\((recordsArray[indexPath.row] as! NSDictionary)["created_on_date"] ?? "")".replacingOccurrences(of: "/", with: ".")
            
            cell.price.text = "\((recordsArray[indexPath.row] as! NSDictionary)["other_price"] ?? "")" + " " + "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"] ?? "")"
            
            if "\((recordsArray[indexPath.row] as! NSDictionary)["plan_id"] ?? "")".elementsEqual("2") {
                cell.urgentImageview.isHidden = false
                
                cell.urgentImageview.contentMode = .scaleAspectFit
                
                cell.urgentImageview.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSDictionary)["gold_logo"] ?? "")"))
            }
            else if "\((recordsArray[indexPath.row] as! NSDictionary)["plan_id"] ?? "")".elementsEqual("6") {
                cell.urgentImageview.isHidden = false
                
                cell.urgentImageview.contentMode = .scaleAspectFill
                cell.urgentImageview.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSDictionary)["urgent_logo"] ?? "")"))
            }
            else {
                cell.urgentImageview.isHidden = true
            }
            
            
            //
            cell.cellView.layer.cornerRadius = 8
            
            
            //
            //cell.price.textColor = UIColor(red: 0.094, green: 0.4, blue: 0.725, alpha: 1)
            //
            
            cell.imageCount.font = UIFont(name: cell.imageCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 12)))
            //cell.propertyType.font = UIFont(name: cell.propertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            cell.city.font = UIFont(name: cell.city.font.fontName, size: CGFloat(self.Get_fontSize(size: 7)))
            cell.address.font = UIFont(name: cell.address.font.fontName, size: CGFloat(self.Get_fontSize(size: 9)))
            cell.district.font = UIFont(name: cell.district.font.fontName, size: CGFloat(self.Get_fontSize(size: 7)))
            cell.price.font = UIFont(name: cell.price.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            cell.listingDate.font = UIFont(name: cell.listingDate.font.fontName, size: CGFloat(self.Get_fontSize(size: 9)))

            cell.bedCount.font = UIFont(name: cell.bedCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            cell.livingRoomCount.font = UIFont(name: cell.livingRoomCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            cell.bathroomCount.font = UIFont(name: cell.bathroomCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            cell.diningCount.font = UIFont(name: cell.diningCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
            
        }
        else
        {
            let object = tableView.dequeueReusableCell(withIdentifier: "sort")
            
            //let languageString = "\((sortListArray[indexPath.row] as! NSDictionary)["filter_name"] ?? "")"
            
            //object?.textLabel?.text = globalFunctions.shared.getLanguageString(variable: "\(languageString)")
            
            if self.sortListArray[indexPath.row].sortID == "\(self.sortBy ?? "")" {
                object?.textLabel?.textColor = UIColor(red:46/255, green:138/255, blue:87/255, alpha: 1)
            }
            else {
                object?.textLabel?.textColor = .white
            }
            
            object?.textLabel?.text =  "\(self.sortListArray[indexPath.row].sortName ?? "")" //"\((sortListArray[indexPath.row] as! NSDictionary)["filter_name"] ?? "")"
            
            object?.textLabel?.font = UIFont(name: (object?.textLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 13)))
            
            //
            object!.selectionStyle = UITableViewCell.SelectionStyle.none
            return object!
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        if tableView == homeListingTableview
        {
            
            let object : RealEstateListingTVC = cell as! RealEstateListingTVC
            
            if ("\((recordsArray[indexPath.row] as! NSDictionary)["plan_id"] ?? "")".elementsEqual("2"))
            {
                object.cellView.backgroundColor = UIColor(red:242/255, green:243/255, blue:193/255, alpha: 1)
            }
            else if ("\((recordsArray[indexPath.row] as! NSDictionary)["plan_id"] ?? "")".elementsEqual("6"))
            {
                object.cellView.backgroundColor = UIColor(red:242/255, green:243/255, blue:193/255, alpha: 1)
                //object.cellView.backgroundColor = UIColor(red:254/255, green:215/255, blue:218/255, alpha: 1)
            }
            else
            {
                object.cellView.backgroundColor = UIColor.white
            }
            
            
            if ("\((recordsArray[indexPath.row] as! NSDictionary)["ribbon_value"] ?? "")".elementsEqual("sale"))
            {
                object.cellSideView.backgroundColor = UIColor(red:72/255, green:149/255, blue:84/255, alpha: 1)
            }
            else if ("\((recordsArray[indexPath.row] as! NSDictionary)["ribbon_value"] ?? "")".elementsEqual("rent"))
            {
                object.cellSideView.backgroundColor = UIColor.yellow
            }
            else if ("\((recordsArray[indexPath.row] as! NSDictionary)["ribbon_value"] ?? "")".elementsEqual("sold"))
            {
                object.cellSideView.backgroundColor = UIColor.red
            }
            else
            {
                object.cellSideView.backgroundColor = UIColor.white
            }
            
        }
        else
        {
            
            //print("nothing to do here")
        }
        
        
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == homeListingTableview
        {
            
            let object = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "PropertyDetailsVC") as! PropertyDetailsVC
            
            let mydict = recordsArray[indexPath.row] as! NSDictionary
            
            object.propertyID = "\(mydict["id"] ?? "")"
            
            self.navigationController?.pushViewController(object, animated: true)
        }
        else
        {
            self.sortView.isHidden = true
            self.sortTableviewHeightConstraint.constant = 0
            
            sortBy = "\(self.sortListArray[indexPath.row].sortID ?? "")"//"\((sortListArray[indexPath.row] as! NSDictionary)["filter_value"] ?? "")"
            
            for i in 0..<self.sortListArray.count {
                self.sortListArray[i].isSelected = false
            }
            self.sortListArray[indexPath.row].isSelected = true
            
            
            self.recordsArray.removeAllObjects()
            
            startValue = 0
            
            self.getListingDetails(mapLoad: false)
            
            self.globalDispatchgroup.notify(queue: .main) {
                
                if self.listingEmpty {
//                    self.homeListingTableview.isHidden = true
//                    self.noResultsView.isHidden = false
//                    self.view.bringSubviewToFront(self.noResultsView)

                    self.headerView.searchResultsLBL.text = "( 0 ) \(globalFunctions.shared.getLanguageString(variable: "app_result_found"))"
                    self.superAgentDictionary = superAgentDtls.shared.superAgentInfo
                    self.NoResultMainView.isHidden = false
                    self.toogleMapBtnOut.isHidden = true
                    self.loadSuperAgent()
                   // self.containerOfSuperAgent.isHidden = true
                }
                else {
//                    self.homeListingTableview.isHidden = false
//                    self.noResultsView.isHidden = true
//                    self.view.sendSubviewToBack(self.noResultsView)

                    self.loadSuperAgent()
                }
                
            }
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
}





// MARK:- // Scrollview Delegate Methods

extension HomeVC: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        if scrollView == self.homeListingTableview {
            
//            UIView.animate(withDuration: 0.3, delay: 0.2, options: .curveEaseOut, animations: {
//                self.segmentView.alpha = 0.0
//                self.segmentView.isHidden = true
//                self.loadSuperAgent()
//                self.view.layoutIfNeeded()
//
//            })
//
//            UIView.animate(withDuration: 1.5, delay: 0.2, options: .curveEaseOut, animations: {
//                self.segmentView.isHidden = true
//            })
//
            scrollBegin = scrollView.contentOffset.y
        }
        else {
            
            scrollBegin = scrollView.contentOffset.x
        }
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == self.homeListingTableview {
            
//            UIView.animate(withDuration: 0.3, delay: 0.2, options: .curveEaseOut, animations: {
//                self.segmentView.alpha = 1.0
//                self.segmentView.isHidden = false
//                self.view.layoutIfNeeded()
//
//            })
            
//            UIView.animate(withDuration: 0)
//            {
//                self.segmentView.isHidden = false
//            }
            
            scrollEnd = scrollView.contentOffset.y
        }
        else {
            
            scrollEnd = scrollView.contentOffset.x
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollBegin > scrollEnd
        {
            if scrollView == self.homeListingCollectionview {
                
                if "\(UserDefaults.standard.string(forKey: "searchLanguageCode") ?? "")".elementsEqual("ar") {
                    
                    let tempIndex = self.mapDataArray.count - Int(self.scrollBegin / self.FullWidth)
                    
                    if self.scrollEnd > 0 {
                        
                        self.moveCamera(dataIndex: tempIndex)
                        
                        //self.toStatusLBL.text = "\(self.testint + tempIndex + 1)" + "     To     " + "\(self.testint + self.mapDataArray.count)"
                        
                        self.highlightMapMarker(markerToChange: self.markerArray[tempIndex])
                        
                    }
                    
                }
                else {
                    let tempIndex = Int(self.scrollEnd / self.FullWidth)
                    
                    if self.scrollEnd > 0 {
                        
                        print("\(tempIndex)")
                        
                        self.moveCamera(dataIndex: tempIndex)
                        
                        //self.toStatusLBL.text = "\(self.testint + tempIndex + 1)" + "     To     " + "\(self.testint + self.mapDataArray.count)"
                        
                        self.highlightMapMarker(markerToChange: self.markerArray[tempIndex])
                        
                    }
                }
                
                
                
            }
        }
        else
        {
            
            if scrollView == self.homeListingTableview {
                
                print("next start : ",nextStart ?? "")
                
                if (nextStart).isEmpty
                {
                    DispatchQueue.main.async {
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        //SVProgressHUD.dismiss()
                        
                    }
                }
                else
                {
                    if self.paginationComplete {
                        
                        self.paginationComplete = false
                        
                        self.getListingDetails(mapLoad: false)
                        
                        self.globalDispatchgroup.notify(queue: .main) {
                            
                            self.paginationComplete = true
                            
                        }
                        
                    }
                }
            }
            else {
                
                if "\(UserDefaults.standard.string(forKey: "searchLanguageCode") ?? "")".elementsEqual("ar") {
                    
                    let tempIndex = self.mapDataArray.count - (Int(self.scrollBegin / self.FullWidth) + 1)
                    
                    if tempIndex > 0 {
                        
                        self.moveCamera(dataIndex: tempIndex + 1)
                        
                        //self.toStatusLBL.text = "\(self.testint + tempIndex + 2)" + "     To     " + "\(self.testint + self.mapDataArray.count)"
                        
                        self.highlightMapMarker(markerToChange: self.markerArray[tempIndex + 1])
                        
                    }
                }
                else {
                    let tempIndex = Int(self.scrollEnd / self.FullWidth)
                    
                    if tempIndex + 1 < self.mapDataArray.count {
                        
                        self.moveCamera(dataIndex: tempIndex + 1)
                        
                        //self.toStatusLBL.text = "\(self.testint + tempIndex + 2)" + "     To     " + "\(self.testint + self.mapDataArray.count)"
                        
                        self.highlightMapMarker(markerToChange: self.markerArray[tempIndex + 1])
                        
                    }
                }
                
                
                
                
                
                //                testint = Int(self.scrollEnd / self.FullWidth)
                //
                //                if (testint + 1) == (recordsArray.count) {
                //
                //                    self.getListingDetails()
                //
                //                    self.globalDispatchgroup.notify(queue: .main) {
                //
                //                        if ((self.testint + 1) == (self.recordsArray.count)) == false {
                //
                //                            self.testint = self.testint + 1
                //
                //                            self.moveCamera(dataIndex: self.testint)
                //
                //                            self.nextPreviousStatusLBL.text = "\(self.testint!)" + "   to   " + "\(self.recordsArray.count)"
                //
                //                            // Scrolling CollectionView According to Selected Property
                //                            let nextItem: IndexPath = IndexPath(item: self.testint, section: 0)
                //                            // Scroll happens here
                //                            self.homeListingCollectionview.scrollToItem(at: nextItem, at: .left, animated: true)
                //
                //                        }
                //                    }
                //                }
                //                else {
                //                    self.testint = self.testint + 1
                //
                //                    self.moveCamera(dataIndex: self.testint)
                //
                //                    self.nextPreviousStatusLBL.text = "\(self.testint! + 1)" + "   to   " + "\(self.recordsArray.count)"
                //                }
                
            }
            
            
            
        }
    }
    
}







// MARK:- // Collectionview Delegate Methods

extension HomeVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.mapDataArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listColletionVIew", for: indexPath) as! NewHomeListingCVCCollectionViewCell
        
        cell.propertyImage.sd_setImage(with: URL(string: "\((self.mapDataArray[indexPath.row] as! NSDictionary)["property_image"] ?? "")"))
        cell.imageCount.text = " \((self.mapDataArray[indexPath.row] as! NSDictionary)["total_image"] ?? "") "
        
        cell.parentCategoryImage.sd_setImage(with: URL(string: "\((self.recordsArray[indexPath.row] as! NSDictionary)["parent_cat_image"] ?? "")"))
        
        
//        cell.propertyType.text = "\((mapDataArray[indexPath.row] as! NSDictionary)["property_type"] ?? "")" + " (\((mapDataArray[indexPath.row] as! NSDictionary)["area"] ?? "") \(globalFunctions.shared.getLanguageString(variable: "app_home_m2")))"//  + "for " + "\((mapDataArray[indexPath.row] as! NSDictionary)["ribbon_value"] ?? "")"
        if (recordsArray[indexPath.row] as! NSDictionary)["area"] as! String == ""{
            cell.propertyType.text = "\((recordsArray[indexPath.row] as! NSDictionary)["property_type"] ?? "")"
        }
        else{
            cell.propertyType.text = "\((recordsArray[indexPath.row] as! NSDictionary)["property_type"] ?? "")" + "(\((recordsArray[indexPath.row] as! NSDictionary)["area"] ?? "") \(globalFunctions.shared.getLanguageString(variable: "app_home_m2"))2)"//  + "\((recordsArray[indexPath.row] as! NSDictionary)["ribbon_value_lang"] ?? "")"
            let font:UIFont? = UIFont(name: cell.propertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            let fontSuper:UIFont? = UIFont(name: cell.propertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 5)))
            let attString:NSMutableAttributedString = NSMutableAttributedString(string: cell.propertyType.text!, attributes: [.font:font!])
            attString.setAttributes([.font:fontSuper!,.baselineOffset:5], range: NSRange(location:(cell.propertyType.text!.count - 2),length:1))
            cell.propertyType.attributedText = attString
        }
        
        cell.address.text = ""//"\((mapDataArray[indexPath.row] as! NSDictionary)["address"] ?? "")"
        
        cell.city.text = "\((recordsArray[indexPath.row] as! NSDictionary)["distric"] ?? "")"
        cell.district.text = "\((recordsArray[indexPath.row] as! NSDictionary)["city"] ?? "")"
        
        //cell.bedCount.text = "\((mapDataArray[indexPath.row] as! NSDictionary)["bedroom"] ?? "0")"
        //cell.livingRoomCount.text = "\((mapDataArray[indexPath.row] as! NSDictionary)["livingroom"] ?? "0")"
        //cell.bathroomCount.text = "\((mapDataArray[indexPath.row] as! NSDictionary)["bathroom"] ?? "0")"
        //cell.diningCount.text = "\((mapDataArray[indexPath.row] as! NSDictionary)["kichen"] ?? "0")"
        
        cell.bedCount.text = ((mapDataArray[indexPath.row] as! NSDictionary)["bedroom"] as! String) == "" ? "0" : "\((mapDataArray[indexPath.row] as! NSDictionary)["bedroom"] ?? "0")"
        cell.livingRoomCount.text = ((mapDataArray[indexPath.row] as! NSDictionary)["livingroom"] as! String) == "" ? "0" : "\((mapDataArray[indexPath.row] as! NSDictionary)["livingroom"] ?? "0")"//"\((recordsArray[indexPath.row] as! NSDictionary)["livingroom"] ?? "0")"
        cell.bathroomCount.text = ((mapDataArray[indexPath.row] as! NSDictionary)["bathroom"] as! String) == "" ? "0" : "\((mapDataArray[indexPath.row] as! NSDictionary)["bathroom"] ?? "0")"//"\((recordsArray[indexPath.row] as! NSDictionary)["bathroom"] ?? "0")"
        cell.diningCount.text = ((mapDataArray[indexPath.row] as! NSDictionary)["kichen"] as! String) == "" ? "0" : "\((mapDataArray[indexPath.row] as! NSDictionary)["kichen"] ?? "0")"//"\((recordsArray[indexPath.row] as! NSDictionary)["kichen"] ?? "0")"
        
        cell.listingDate.text = "\((mapDataArray[indexPath.row] as! NSDictionary)["created_on_date"] ?? "")".replacingOccurrences(of: "/", with: ".")
        
        cell.price.text = "\((mapDataArray[indexPath.row] as! NSDictionary)["other_price"] ?? "")" + " " + "\((mapDataArray[indexPath.row] as! NSDictionary)["currency_symbol"] ?? "")"
        
        if "\((mapDataArray[indexPath.row] as! NSDictionary)["plan_id"] ?? "")".elementsEqual("2") {
            cell.urgentImageview.isHidden = false
            
            cell.urgentImageview.contentMode = .scaleAspectFit
            
            cell.urgentImageview.sd_setImage(with: URL(string: "\((mapDataArray[indexPath.row] as! NSDictionary)["gold_logo"] ?? "")"))
        }
        else if "\((recordsArray[indexPath.row] as! NSDictionary)["plan_id"] ?? "")".elementsEqual("6") {
            cell.urgentImageview.isHidden = false
            
            cell.urgentImageview.contentMode = .scaleAspectFill
            cell.urgentImageview.sd_setImage(with: URL(string: "\((mapDataArray[indexPath.row] as! NSDictionary)["urgent_logo"] ?? "")"))
        }
        else {
            cell.urgentImageview.isHidden = true
        }
        
        
        if ("\((mapDataArray[indexPath.row] as! NSDictionary)["ribbon_value"] ?? "")".elementsEqual("sale"))
        {
            cell.cellSideView.backgroundColor = UIColor(red:72/255, green:149/255, blue:84/255, alpha: 1)
        }
        else if ("\((mapDataArray[indexPath.row] as! NSDictionary)["ribbon_value"] ?? "")".elementsEqual("rent"))
        {
            cell.cellSideView.backgroundColor = UIColor.yellow
        }
        else if ("\((mapDataArray[indexPath.row] as! NSDictionary)["ribbon_value"] ?? "")".elementsEqual("sold"))
        {
            cell.cellSideView.backgroundColor = UIColor.red
        }
        else
        {
            cell.cellSideView.backgroundColor = UIColor.white
        }
        
        
        
        if ("\((mapDataArray[indexPath.row] as! NSDictionary)["plan_id"] ?? "")".elementsEqual("2"))
        {
            cell.cellView.backgroundColor = UIColor(red:242/255, green:243/255, blue:193/255, alpha: 1)
        }
        else if ("\((mapDataArray[indexPath.row] as! NSDictionary)["plan_id"] ?? "")".elementsEqual("6"))
        {
            cell.cellView.backgroundColor = UIColor(red:242/255, green:243/255, blue:193/255, alpha: 1)
            //object.cellView.backgroundColor = UIColor(red:254/255, green:215/255, blue:218/255, alpha: 1)
        }
        else
        {
            cell.cellView.backgroundColor = UIColor.white
        }
        
        
        //
        cell.cellView.layer.cornerRadius = 8
        
        
        //
        cell.price.textColor = UIColor(red: 0.094, green: 0.4, blue: 0.725, alpha: 1)
        
        //
        
//        cell.propertyType.font = UIFont(name: cell.propertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
//        cell.city.font = UIFont(name: cell.address.font.fontName, size: CGFloat(self.Get_fontSize(size: 9)))
//        cell.address.font = UIFont(name: cell.address.font.fontName, size: CGFloat(self.Get_fontSize(size: 9)))
//        cell.district.font = UIFont(name: cell.address.font.fontName, size: CGFloat(self.Get_fontSize(size: 9)))
//        cell.price.font = UIFont(name: cell.price.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
//        cell.listingDate.font = UIFont(name: cell.listingDate.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
//
//        cell.bedCount.font = UIFont(name: cell.bedCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
//        cell.livingRoomCount.font = UIFont(name: cell.livingRoomCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
//        cell.bathroomCount.font = UIFont(name: cell.bathroomCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
//        cell.diningCount.font = UIFont(name: cell.diningCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
        
        
        //
        //cell.selectionStyle = UICollectionViewCellSelectionStyle.none
        
        return cell
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.FullWidth, height: 84.5)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let object = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "PropertyDetailsVC") as! PropertyDetailsVC
        
        let mydict = recordsArray[indexPath.row] as! NSDictionary
        
        object.propertyID = "\(mydict["id"] ?? "")"
        
        self.navigationController?.pushViewController(object, animated: true)
    }
    
}



// MARK:- // GSM Mapview Delegate Method

extension HomeVC: GMSMapViewDelegate {
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        print("Tap My Location Button")
        if self.zoomStatus != "3km"{
            self.zoomStatus = "3km"
            self.updateZoomLebel()
        }
        else{
            self.zoomStatus = "100km"
            self.updateZoomLebel()
        }
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("Idle at camera position")
//        if self.zoomStatus == "3km"{
//            let zoomCamera = GMSCameraUpdate.zoom(to: 15.0)
//            mapVIew.animate(with: zoomCamera)
//        }
    }
    
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        let tempInt : Int = (marker.userData as! [String : Any])["tag"] as! Int
        
        let nextItem: IndexPath = IndexPath(item: tempInt, section: 0)
        
        //self.toStatusLBL.text = "\(self.testint + tempInt + 1)" + "     To     " + "\(self.testint + self.mapDataArray.count)"
        
        
        
        self.homeListingCollectionview.scrollToItem(at: nextItem, at: .left, animated: true)
        
        
        self.highlightMapMarker(markerToChange: marker)
        
        
        
//        if self.clickedMapMarkerArray.contains(tempInt) == false {
//
//            self.clickedMapMarkerArray.append(tempInt)
//
//        }
//
//        for i in 0..<self.markerArray.count {
//
//            for j in 0..<self.clickedMapMarkerArray.count {
//
//                if ((self.markerArray[i].userData as! [String : Any])["tag"] as! Int) == (self.clickedMapMarkerArray[j]) {
//
//                    let markerView = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: 100, height: 40), image: UIImage(named: "map3x-(g)-1")!, borderColor: .clear, tag: ((self.markerArray[i].userData as! [String : Any])["tag"] as! Int), text: (self.markerArray[i].userData as! [String : Any])["text"] as! String)
//
//                    let markerIcon : UIImage = markerView.asImage()
//
//                    markerArray[i].icon = markerIcon
//
//                }
//
//            }
//
//        }
//
//
//        let markerView = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: 100, height: 40), image: UIImage(named: "map3x-(w)")!, borderColor: .clear, tag: tempInt, text: (marker.userData as! [String : Any])["text"] as! String)
//
//        let markerIcon : UIImage = markerView.asImage()
//
//        marker.icon = markerIcon
        
        
        
        
        //
        return true
        
    }
    
    
    
}







