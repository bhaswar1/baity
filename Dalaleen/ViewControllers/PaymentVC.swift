//
//  PaymentVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 21/08/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit

class PaymentVC: GlobalViewController {

    @IBOutlet weak var backgroundImageview: UIImageView!
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var paymentTitleLBL: UILabel!
    @IBOutlet weak var segmentView: UIView!
    @IBOutlet weak var byCreditCardLBL: UILabel!
    @IBOutlet weak var creditCardBottomview: UIView!
    @IBOutlet weak var byMobileBalanceLBL: UILabel!
    @IBOutlet weak var mobileBalanceBottomview: UIView!
    @IBOutlet weak var mainStackview: UIStackView!
    @IBOutlet weak var creditCardView: UIView!
    @IBOutlet weak var visaImageview: UIImageView!
    @IBOutlet weak var visaLBL: UILabel!
    @IBOutlet weak var creditCardLBL: UILabel!
    @IBOutlet weak var cardHoldersNameLBL: UILabel!
    @IBOutlet weak var cardHoldersNameTXT: UITextField!
    @IBOutlet weak var cardNumberLBL: UILabel!
    @IBOutlet weak var cardNumberTXT: UITextField!
    @IBOutlet weak var validityLBL: UILabel!
    @IBOutlet weak var validityTXT: UITextField!
    @IBOutlet weak var cvcLBL: UILabel!
    @IBOutlet weak var cvcTXT: UITextField!
    @IBOutlet weak var termsAndConditionsLBL: UILabel!
    @IBOutlet weak var saveCardInformationLBL: UILabel!
    
    
    @IBOutlet weak var mobileBalanceView: UIView!
    @IBOutlet weak var toPayThroughStaticLBL: UILabel!
    @IBOutlet weak var youWillTransferStaticLBL: UILabel!
    @IBOutlet weak var yourRegisteredPhoneNumberLBL: UILabel!
    @IBOutlet weak var youWIllPayStaticLBL: UILabel!
    @IBOutlet weak var balanceTermsAndConditionsLBL: UILabel!
    
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.SetFont()
        
        self.mobileBalanceView.isHidden = true
        
    }
    
    
    
    // MARK:- // Buttons
    
    // MARK:- // Back Button
    
    @IBOutlet weak var backButtonOutlet: UIButton!
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK:- // By Credit Card Button
    
    @IBOutlet weak var byCreditCardButtonOutlet: UIButton!
    @IBAction func byCreditCardButton(_ sender: UIButton) {
        
        self.byCreditCardLBL.textColor = UIColor(displayP3Red: 0/255, green: 135/255, blue: 228/255, alpha: 1)
        self.creditCardBottomview.backgroundColor = UIColor(displayP3Red: 0/255, green: 135/255, blue: 228/255, alpha: 1)
        self.byMobileBalanceLBL.textColor = .lightGray
        
        self.creditCardBottomview.isHidden = false
        self.mobileBalanceBottomview.isHidden = true
        
        self.creditCardView.isHidden = false
        self.mobileBalanceView.isHidden = true
        
    }
    
    
    // MARK:- // By Mobile Number Button
    
    @IBOutlet weak var byMobileNumberButtonOutlet: UIButton!
    @IBAction func byMobileNumberButton(_ sender: UIButton) {
        
        self.byCreditCardLBL.textColor = .lightGray
        self.creditCardBottomview.backgroundColor = .clear
        self.byMobileBalanceLBL.textColor = UIColor(displayP3Red: 0/255, green: 135/255, blue: 228/255, alpha: 1)
        
        self.creditCardBottomview.isHidden = true
        self.mobileBalanceBottomview.isHidden = false
        
        self.creditCardView.isHidden = true
        self.mobileBalanceView.isHidden = false
        
    }
    
    
    
    
    // MARK:- // Saved Visa Button
    
    @IBOutlet weak var savedVisaButtonOutlet: UIButton!
    @IBAction func savedVisaButton(_ sender: UIButton) {
        
        self.savedVisaButtonOutlet.setImage(UIImage(named: "select black"), for: .normal)
        self.newCreditCardButtonOutlet.setImage(UIImage(named: "non select"), for: .normal)
        
    }
    
    
    // MARK:- // New Credit Card Button
    
    @IBOutlet weak var newCreditCardButtonOutlet: UIButton!
    @IBAction func newCreditCardButton(_ sender: UIButton) {
        
        self.savedVisaButtonOutlet.setImage(UIImage(named: "non select"), for: .normal)
        self.newCreditCardButtonOutlet.setImage(UIImage(named: "select black"), for: .normal)
        
    }
    
    
    
    // MARK:- // Terms and Conditions Button
    
    @IBOutlet weak var termsAndConditionsButtonOutlet: UIButton!
    @IBAction func termsAndConditionsButton(_ sender: UIButton) {
        
        self.buttonTransformIdentity(button: self.termsAndConditionsButtonOutlet)
        
    }
    
    
    
    // MARK:- // Save Card Information Button
    
    @IBOutlet weak var saveCardInformationButtonOutlet: UIButton!
    @IBAction func saveCardInformationButton(_ sender: UIButton) {
        
        self.buttonTransformIdentity(button: self.saveCardInformationButtonOutlet)
        
    }
    
    
    
    // MARK:- // Pay Button
    
    @IBOutlet weak var payButtonOutlet: UIButton!
    @IBAction func payButton(_ sender: UIButton) {
    }
    
    
    // MARK:- // Mobile Balance Terms and Conditions Button
    
    @IBOutlet weak var mobileBalanceTermsAndConditionsButtonOutlet: UIButton!
    @IBAction func mobileBalanceTermsAndConditionsButton(_ sender: UIButton) {
        
        self.buttonTransformIdentity(button: self.mobileBalanceTermsAndConditionsButtonOutlet)
        
    }
    
    
    
    // MARK:- // PROCEDD TO PAYMENT Button
    
    @IBOutlet weak var proceedToPaymentButtonOutlet: UIButton!
    @IBAction func proceedToPaymentButton(_ sender: UIButton) {
    }
    
    
    
    
    
    
    // MARK:- // Set Font
    
    func SetFont() {
        
        self.paymentTitleLBL.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 32)))
        self.byCreditCardLBL.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 16)))
        self.byMobileBalanceLBL.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 16)))
        self.visaLBL.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.creditCardLBL.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.cardHoldersNameLBL.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.cardHoldersNameTXT.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.cardNumberLBL.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.cardNumberTXT.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.validityLBL.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.validityTXT.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.cvcLBL.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.cvcTXT.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.termsAndConditionsLBL.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.saveCardInformationLBL.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.payButtonOutlet.titleLabel!.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 16)))
        
        self.toPayThroughStaticLBL.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.youWillTransferStaticLBL.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.yourRegisteredPhoneNumberLBL.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.youWIllPayStaticLBL.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.balanceTermsAndConditionsLBL.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.proceedToPaymentButtonOutlet.titleLabel!.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 16)))
        
    }
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
    }
    

}
