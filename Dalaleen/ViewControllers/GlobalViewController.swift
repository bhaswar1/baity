//
//  GlobalViewController.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 21/06/18.
//  Copyright © 2018 esolz. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD
import NVActivityIndicatorView
import GoogleMaps
import GooglePlaces

class GlobalViewController: UIViewController, Side_menu_delegate, NVActivityIndicatorViewable {
    
    @IBOutlet weak var latoRegularLBL: UILabel!
    @IBOutlet weak var latoBoldLBL: UILabel!
    @IBOutlet weak var sideMenuView: SideMenuView!
    @IBOutlet weak var headerView: HeaderView!
    
    @IBOutlet weak var sideMenuLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var sideMenuTrailingConstraint: NSLayoutConstraint!
    
    var listingEmpty : Bool! = false
    
    
    var tapped : Bool = false
    
    var zoomStatus: String!
    
    static let Isiphone6 = (UIScreen.main.bounds.size.height==667) ? true : false
    static let Isiphone6Plus = (UIScreen.main.bounds.size.height==736) ? true : false
    static let IsiphoneX = (UIScreen.main.bounds.size.height>=812) ? true : false
    static let IsiphoneXSMAX = (UIScreen.main.bounds.size.height==896) ? true : false
    
    var FullHeight = (UIScreen.main.bounds.size.height)
    var FullWidth = (UIScreen.main.bounds.size.width)
    
    //let GLOBALAPI = "http://esolz.co.in/lab6/dalaleen/jsonapp_control/"
    
    //let GLOBALAPI = "http://www.jidar.com/jsonapp_control/"
    
    let GLOBALAPI = "https://www.ibaity.com/jsonapp_control/"
    
    var SignInActivation : Bool! = false
    
    var globalDispatchgroup = DispatchGroup()
    
    var globalJson : NSDictionary!
    
    var userInformationDict : NSDictionary!
    
    var noNetworkView : UIView = {
        let nonetworkview = UIView()
        nonetworkview.backgroundColor = .white
        nonetworkview.translatesAutoresizingMaskIntoConstraints = false
        return nonetworkview
    }()
    
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        
        
        if (userInformation.shared.searchLanguageCode.elementsEqual("ar"))
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            
            globalFunctions.shared.changeTextfieldAlignment(mainview: self.view, Alignment: .right)
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            globalFunctions.shared.changeTextfieldAlignment(mainview: self.view, Alignment: .left)
        }
        
        
//        if userInformation.shared.userCountryName.elementsEqual("Agent") {
//            print("It's happening again.")
//        }
//
//        if (UserDefaults.standard.string(forKey: "userCountryName")?.elementsEqual("Agent"))! {
//            print("It's happening again.")
//        }
        
        
        
        
        // MARK:- // LOAD ALL THE VIEWCONTROLLERS //
        
        // MARK: - // HomeVC View Did Load
        
        if (self.topViewController()?.isKind(of: HomeVC.self))! {
            
            self.sideMenuLeadingConstraint.constant = -self.FullWidth
            self.sideMenuTrailingConstraint.constant = -self.FullWidth
            self.headerView.sideMenuButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            self.headerView.headerViewOfImage.isHidden = false
            self.headerView.headerIconPositionOne.isHidden = false
            self.headerView.iconImageOne.isHidden = false
            self.headerView.headerIconPositionTwo.isHidden = false
            self.headerView.iconImageTwo.isHidden = false
            
            self.headerView.searchResultsLBL.isHidden = false
            
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "header_title")
            self.headerView.headerImage.sd_setImage(with: URL(string: "\(userInformation.shared.searchCountryFlag ?? "")"))
            
            //self.headerView.headerIconPositionOne.setImage(UIImage(named: "Combined Shape"), for: .normal)
            
            self.headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            
            self.sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
            
        }
        
        
        
        
        // MARK: - // SelectCountryAndLanguageVC View Did Load
        
        if (self.topViewController()?.isKind(of: SelectCountryAndLanguageVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
            
        }
        
        
        
        // MARK: - // PropertyDetailsVC View Did Load
        
        if (self.topViewController()?.isKind(of: PropertyDetailsVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            self.headerView.headerIconPositionOne.isHidden = false
            self.headerView.iconImageOne.isHidden = false
            
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "app_property_details")
            
            self.headerView.iconImageOne.image = UIImage(named: "share")
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        // MARK: - // AgentAllRealEstatesListingVC View Did Load
        
        if (self.topViewController()?.isKind(of: AgentAllRealEstatesListingVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        
        // MARK: - // InboxVC View Did Load
        
        if (self.topViewController()?.isKind(of: InboxVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "left_side_inbox")
            
//            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        // MARK: - // SingleNotificationVC View Did Load
        
        if (self.topViewController()?.isKind(of: SingleNotificationVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "app_noti")
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        
        
        // MARK: - // RecyclebinVC View Did Load
        
        if (self.topViewController()?.isKind(of: RecyclebinVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "left_side_recycle_bin")
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        
        
        // MARK: - // MyFavouritesVC View Did Load
        
        if (self.topViewController()?.isKind(of: MyFavouritesVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "app_my_fav")
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        
        // MARK: - // MySuperAgentProfileVC View Did Load
        
        if (self.topViewController()?.isKind(of: MySuperAgentProfileVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "app_super_agent")
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        
        // MARK: - // AllSuperAgentsListVC View Did Load
        
        if (self.topViewController()?.isKind(of: AllSuperAgentsListVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "app_super_agent")
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        
        // MARK: - // SuperAgentDetailsVC View Did Load
        
        if (self.topViewController()?.isKind(of: SuperAgentDetailsVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "app_super_agent") + " " + globalFunctions.shared.getLanguageString(variable: "app_details")
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        
        
        // MARK: - // ContactUsVC View Did Load
        
        if (self.topViewController()?.isKind(of: ContactUsVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "footer_contact_us")
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        
        // MARK: - // ChangePasswordVC View Did Load
        
        if (self.topViewController()?.isKind(of: ChangePasswordVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "my_profile_change_password")
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        
        
        // MARK: - // MyProfileVC View Did Load
        
        if (self.topViewController()?.isKind(of: MyProfileVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "left_side_my_profile")
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        
        
        // MARK: - // FilterVC View Did Load
        
        if (self.topViewController()?.isKind(of: FilterVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            self.headerView.headerViewOfImage.isHidden = false
            
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "header_title")
            self.headerView.headerImage.sd_setImage(with: URL(string: "\(userInformation.shared.searchCountryFlag ?? "")"))
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        
        // MARK: - // AddNewAdVC View Did Load
        
        if (self.topViewController()?.isKind(of: AddNewAdVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        // MARK: - // ExtendPlanVC View Did Load
        
        if (self.topViewController()?.isKind(of: ExtendPlanVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "property_post_property_cost")
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        
        // MARK: - // WebKit View Did Load
        
        if (self.topViewController()?.isKind(of: WebkitVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        // MARK: - // HelpVC View Did Load
        
        if (self.topViewController()?.isKind(of: HelpVC.self))! {
            
            self.headerView.headerBackButton.isHidden = false
            self.headerView.headerTitle.isHidden = false
            
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "footer_help_text")
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        
        // MARK: - // AppStartSearchVC View Did Load
        
        if (self.topViewController()?.isKind(of: AppStartSearchVC.self))! {
            
            self.headerView.backgroundColor = .clear
            self.headerView.contentVIew.backgroundColor = .clear
            
            self.headerView.headerBackButton.addTarget(self, action: #selector(GlobalViewController.headerBackButton), for: .touchUpInside)
            
        }
        
        
    }
    
    
    
    // MARK:- // View Will Appear
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.topViewController()?.isKind(of: GlobalViewController.self) == true  {
            ConnectivityManager.shared().addListener(listener: self.topViewController() as! NetworkConnectionStatusListener)
        }
        
//        if self.topViewController()?.isKind(of: GMSAutocompleteViewController.self) == false {
//
//        }
        
    }
    
    
    // MARK:- // View Will Disappear
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.topViewController()?.isKind(of: GlobalViewController.self) == true {
            
            ConnectivityManager.shared().removeListener(listener: self.topViewController() as! NetworkConnectionStatusListener)
            
        }
        
//        if self.topViewController()?.isKind(of: GMSAutocompleteViewController.self) == false {
//
//
//        }
        
        
    }

    @objc func methodOfReceivedNotification(notification: Notification) {
        print(notification)
//        guard let rootVC = UIStoryboard.init(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "InboxVC") as? InboxVC else {
//            return
//        }
//        let navigationController = UINavigationController(rootViewController: rootVC)
//        self.navigationController?.isNavigationBarHidden = true
//        UIApplication.shared.windows.first?.rootViewController = navigationController
//        UIApplication.shared.windows.first?.makeKeyAndVisible()
        if !(self.topViewController()?.isKind(of: InboxVC.self))!{
            let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "InboxVC") as! InboxVC
            self.navigationController?.pushViewController(nav, animated: true)
        }
        else{
            NotificationCenter.default.post(name: Notification.Name("ReloadData"), object: nil)
        }
    }
    
    
     // MARK: - // Defined Functions
    
     // MARK: - // function TopViewController
    
    
    func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    
    
    
    
   
    
    
    // MARK: - // Function for DidSelect in SideMenuView for User
    
    
    func action_method(sender: NSInteger?)
    {
        
//        var tempSender : NSInteger!
//
//        if "\(userInformation.shared.userType ?? "")".elementsEqual("U") {
//            if sender! > 7 {
//                tempSender = sender! + 1
//            }
//            else {
//                tempSender = sender
//            }
//        }
//        else {
//            tempSender = sender
//        }
        
        
        if (sender==0)    // Home
        {
            if (self.topViewController()?.isKind(of: HomeVC.self))!
            {
                UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations:
                    {
                        self.sideMenuLeadingConstraint.constant = -self.FullWidth
                        self.sideMenuTrailingConstraint.constant = -self.FullWidth
                        self.view.layoutIfNeeded()
                }, completion: { (finished: Bool) in
                })
            }
            else
            {
                let homeView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                
                
                self.sideMenuLeadingConstraint.constant = -self.FullWidth
                self.sideMenuTrailingConstraint.constant = -self.FullWidth
                
                self.navigationController?.pushViewController(homeView, animated: true)
            }
        }
        
        
//        else if (sender == 1) {  // Select Country
//            let homeView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "SelectCountryAndLanguageVC") as! SelectCountryAndLanguageVC
//
//            self.sideMenuLeadingConstraint.constant = -self.FullWidth
//            self.sideMenuTrailingConstraint.constant = -self.FullWidth
//
//            self.navigationController?.pushViewController(homeView, animated: true)
//        }
        
        else if (sender == 1) {   // Select language
            let homeView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "SelectCountryAndLanguageVC") as! SelectCountryAndLanguageVC
            
            self.sideMenuLeadingConstraint.constant = -self.FullWidth
            self.sideMenuTrailingConstraint.constant = -self.FullWidth
            
            self.navigationController?.pushViewController(homeView, animated: true)
        }
        
        else if (sender == 2) {   // Inbox
            let homeView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "InboxVC") as! InboxVC
            
            self.sideMenuLeadingConstraint.constant = -self.FullWidth
            self.sideMenuTrailingConstraint.constant = -self.FullWidth
            
            self.navigationController?.pushViewController(homeView, animated: true)
        }
        
        else if (sender == 3) {   // Reclycle Bin
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "RecyclebinVC") as! RecyclebinVC
            
            self.sideMenuLeadingConstraint.constant = -self.FullWidth
            self.sideMenuTrailingConstraint.constant = -self.FullWidth
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
            
        else if (sender == 4) { // My Ads
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "AgentAllRealEstatesListingVC") as! AgentAllRealEstatesListingVC
            
            self.sideMenuLeadingConstraint.constant = -self.FullWidth
            self.sideMenuTrailingConstraint.constant = -self.FullWidth
            
            navigate.agentID = "\(userInformation.shared.userID ?? "")"
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        
        else if (sender == 5) {  // My Favourites
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "MyFavouritesVC") as! MyFavouritesVC
            
            self.sideMenuLeadingConstraint.constant = -self.FullWidth
            self.sideMenuTrailingConstraint.constant = -self.FullWidth
            
            navigate.agentID = "\(userInformation.shared.userID ?? "")"
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender == 6) {  // Add New Real Estate
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "AddNewAdVC") as! AddNewAdVC
            
            self.sideMenuLeadingConstraint.constant = -self.FullWidth
            self.sideMenuTrailingConstraint.constant = -self.FullWidth
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
            
        else if (sender == 7) { // Upgrade to Super Agent
            
            if "\(userInformation.shared.userType ?? "")".elementsEqual("U") {
                
                self.customAlert(title: "", message: globalFunctions.shared.getLanguageString(variable: "modal_tex6"), doesCancel: true) {
                    
                    let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
                    self.navigationController?.pushViewController(navigate, animated: true)
                    
                }
                
            }
            else {
                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "MySuperAgentProfileVC") as! MySuperAgentProfileVC
                
                self.sideMenuLeadingConstraint.constant = -self.FullWidth
                self.sideMenuTrailingConstraint.constant = -self.FullWidth
                
                navigate.agentID = "\(userInformation.shared.userID ?? "")"
                
                self.navigationController?.pushViewController(navigate, animated: true)
            }
            
            


//            if "\(userInformation.shared.superAgent ?? "")".elementsEqual("Y") {
//
//            }
//            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "SuperAgentVC") as! NewSuperAgentProfileVC
//            navigate.agentID = "\(userInformation.shared.userID ?? "")"
//            self.navigationController?.pushViewController(navigate, animated: true)
            
        }
        else if (sender == 8) {   // Show all Super Agents
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "AllSuperAgentsListVC") as! AllSuperAgentsListVC
            
            self.sideMenuLeadingConstraint.constant = -self.FullWidth
            self.sideMenuTrailingConstraint.constant = -self.FullWidth
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender == 9) {  // Help
            
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HelpVC") as! HelpVC
            
            self.sideMenuLeadingConstraint.constant = -self.FullWidth
            self.sideMenuTrailingConstraint.constant = -self.FullWidth
            
            self.navigationController?.pushViewController(navigate, animated: true)
            
        }
        else if (sender == 10) {  // Change Password
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            
            self.sideMenuLeadingConstraint.constant = -self.FullWidth
            self.sideMenuTrailingConstraint.constant = -self.FullWidth
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender == 11) {  // Contact Us
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            
            self.sideMenuLeadingConstraint.constant = -self.FullWidth
            self.sideMenuTrailingConstraint.constant = -self.FullWidth
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender == 12) {  // Logout
            
//            self.customAlert(title: "", message: "\(globalFunctions.shared.getLanguageString(variable: "app_logout_msg"))?", doesCancel: true) {
                
                let alert = UIAlertController(title: "", message: "\(globalFunctions.shared.getLanguageString(variable: "app_logout_msg"))?", preferredStyle: .alert)
                
                let attributedString = NSAttributedString(string: "Logout", attributes: [
                    NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16), //your font here
                    NSAttributedString.Key.foregroundColor : UIColor.red
                    ])
                
                
                
                let logoutAction = UIAlertAction(title: "LOGOUT", style: .destructive, handler: { (UIAlertAction) in
                    self.callApiLogout()
                    //UserDefaults.standard.set(false, forKey: "loginStatus")
                    
//                    let logoutView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                    
//                    let filterDict = filterDictionary(parentCategory: "", propertyType: "", categorySub: "", period: "", country: userInformation.shared.searchCountryID, province: "\(UserDefaults.standard.string(forKey: "searchProvinceID") ?? "")", city: "\(UserDefaults.standard.string(forKey: "searchCityID") ?? "")", district: "\(UserDefaults.standard.string(forKey: "searchDistrictID") ?? "")", minPrice: "", maxPrice: "", minPriceID: "", maxPriceID: "", minArea: "", maxArea: "", viewDays: "", addedBy: "", negotiablePrice: "")
//
//                    userInformation.shared.setUserFilterDictionary(dict: filterDict)
                    
//                    self.navigationController?.pushViewController(logoutView, animated: true)
                    })
                
                //logoutAction.setValue(attributedString, forKey: "attributedTitle")
                
                let cancelAction = UIAlertAction(title: "CANCEL", style: .cancel, handler: nil)
                
                alert.addAction(logoutAction)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            
            
        }
        
    }
    
    func callApiLogout(){
        var parameters : String = ""
        
        parameters = "user_id=\(UserDefaults.standard.string(forKey: "userId") ?? "")&ios_status=1"
        
        self.CallAPI(urlString: "app_logout", param: parameters) {
            
            let temp = self.globalJson.copy() as? NSDictionary
            
            if "\(temp!["response"] ?? "")".elementsEqual("1") {
                
                
                DispatchQueue.main.async {
                    
                    self.globalDispatchgroup.leave()
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    let logoutView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                    UserDefaults.standard.set(false, forKey: "loginStatus")
                    let filterDict = filterDictionary(parentCategory: "", propertyType: "", categorySub: "", period: "", country: userInformation.shared.searchCountryID, province: "\(UserDefaults.standard.string(forKey: "searchProvinceID") ?? "")", city: "\(UserDefaults.standard.string(forKey: "searchCityID") ?? "")", district: "\(UserDefaults.standard.string(forKey: "searchDistrictID") ?? "")", minPrice: "", maxPrice: "", minPriceID: "", maxPriceID: "", minArea: "", maxArea: "", viewDays: "", addedBy: "", negotiablePrice: "")
                    
                    userInformation.shared.setUserFilterDictionary(dict: filterDict)
                    
                    self.navigationController?.pushViewController(logoutView, animated: true)
                    //SVProgressHUD.dismiss()
                    
                }
            }
            else {
                DispatchQueue.main.async {
                    
                    self.globalDispatchgroup.leave()
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    self.ShowAlertMessage(title: "", message: "\(temp!["message"] ?? "")")
                    //SVProgressHUD.dismiss()
                    
                }
            }
            
        }
        
    }
    
    
    // MARK: - // Function for DidSelect in SideMenuView for Guest
    
    
    
    func guest_action_method(sender: NSInteger?){
        
        
        if (sender==0)  // Home
        {
            if (self.topViewController()?.isKind(of: HomeVC.self))!
            {
                UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations:
                    {
                        self.sideMenuLeadingConstraint.constant = -self.FullWidth
                        self.sideMenuTrailingConstraint.constant = -self.FullWidth
                        self.view.layoutIfNeeded()
                }, completion: { (finished: Bool) in
                })
            }
            else
            {
                let homeView = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(homeView, animated: true)
            }
        }
            
//        else if (sender == 1) {  // Where to Search
//            let homeView = self.storyboard?.instantiateViewController(withIdentifier: "SelectCountryAndLanguageVC") as! SelectCountryAndLanguageVC
//            self.navigationController?.pushViewController(homeView, animated: true)
//        }
            
        else if (sender == 1)  // Select Language
        {
            let homeView = self.storyboard?.instantiateViewController(withIdentifier: "SelectCountryAndLanguageVC") as! SelectCountryAndLanguageVC
            self.navigationController?.pushViewController(homeView, animated: true)
        }
        else if (sender == 2)  // Add New Real Estate
        {
            
            self.customAlert(title: "\(globalFunctions.shared.getLanguageString(variable: "header_sign_in"))?", message: "", doesCancel: true) {
                
                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                
                self.navigationController?.pushViewController(navigate, animated: true)
                
            }
        }
        else if (sender == 3)  // Show all Super Agents
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "AllSuperAgentsListVC") as! AllSuperAgentsListVC
            
            self.sideMenuLeadingConstraint.constant = -self.FullWidth
            self.sideMenuTrailingConstraint.constant = -self.FullWidth
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender == 4)  // Help
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HelpVC") as! HelpVC
            
            self.sideMenuLeadingConstraint.constant = -self.FullWidth
            self.sideMenuTrailingConstraint.constant = -self.FullWidth
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender == 5)  // Contact US
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            
            self.sideMenuLeadingConstraint.constant = -self.FullWidth
            self.sideMenuTrailingConstraint.constant = -self.FullWidth
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        
    }
    
    
    
    // MARK: - // Function for LeftMenuClick
    
    @objc func LeftMenuClick() {
   
        UIView.animate(withDuration: 0.3, animations: {
            
            self.sideMenuView.Side_delegate=self;
            
            if userInformation.shared.userPhoneNumber.elementsEqual("") == false {
                self.sideMenuView.clickOnProfile.addTarget(self, action: #selector(GlobalViewController.ProfileClick), for: .touchUpInside)
                
                
                self.getUserInformation {
                    
                    userInformation.shared.setUserInboxCount(inboxCount: "\((self.userInformationDict["count_all_info"] as! NSDictionary)["inbox_count"] ?? "")")
                    userInformation.shared.setUserRecyclebinCount(recyclebinCount: "\((self.userInformationDict["count_all_info"] as! NSDictionary)["recyclebin_count"] ?? "")")
                    userInformation.shared.setUserAdsCount(adsCount: "\((self.userInformationDict["count_all_info"] as! NSDictionary)["my_real_estate"] ?? "")")
                    userInformation.shared.setUserFavouritesCount(favouritesCount: "\((self.userInformationDict["count_all_info"] as! NSDictionary)["my_fav_real_estate"] ?? "")")
                    
                    userInformation.shared.setUserImageString(imageString: "\((self.userInformationDict["info"] as! NSDictionary)["photo"] ?? "")")
                    userInformation.shared.setUserName(name: "\((self.userInformationDict["info"] as! NSDictionary)["name"] ?? "")")
                    userInformation.shared.setUserEmail(email: "\((self.userInformationDict["info"] as! NSDictionary)["email"] ?? "")")
                    
                    self.sideMenuView.sideMenuTable.reloadData()
                    
                    self.sideMenuView.userImage.sd_setImage(with: URL(string: "\(userInformation.shared.userImageString ?? "")"), placeholderImage: UIImage(named: "user-image-with-black-background"))
                    
                    self.sideMenuView.userPhoneNumber.text = "\(userInformation.shared.userName ?? "")"
                    self.sideMenuView.userEmail.text = "\(userInformation.shared.userEmail ?? "")"
                    
                }
                
                
            }
            else {
                self.sideMenuView.signInButton.addTarget(self, action: #selector(GlobalViewController.SignInClick), for: .touchUpInside)
                self.sideMenuView.signUpButton.addTarget(self, action: #selector(GlobalViewController.SignUpClick), for: .touchUpInside)
            }
            
            self.view.bringSubviewToFront(self.sideMenuView)
            self.sideMenuLeadingConstraint.constant = 0
            self.sideMenuTrailingConstraint.constant = 0
            self.view.layoutIfNeeded()
            
                    }, completion: nil)
        
    }
    
    
    // MARK: - // Function for LeftMenuClose
    
    @objc func LeftMenuClose() {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.sideMenuLeadingConstraint.constant = -self.FullWidth
            self.sideMenuTrailingConstraint.constant = -self.FullWidth
            self.view.layoutIfNeeded()
            
                    }, completion: nil)
        
    }
    
    
    // MARK: - // Click on Profile Icon
    
    @objc func ProfileClick() {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    // MARK:- // Header Back Button
    
    @objc func headerBackButton() {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    
    // MARK: - // Function for SignInClick
    
    @objc func SignInClick() {
        
        UIView.animate(withDuration: 0.3, animations: {
            
            let object = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
            
            let filterDict = filterDictionary(parentCategory: "", propertyType: "", categorySub: "\(UserDefaults.standard.string(forKey: "searchCategory") ?? "")", period: "", country: userInformation.shared.searchCountryID, province: "\(UserDefaults.standard.string(forKey: "searchProvinceID") ?? "")", city: "\(UserDefaults.standard.string(forKey: "searchCityID") ?? "")", district: "\(UserDefaults.standard.string(forKey: "searchDistrictID") ?? "")", minPrice: "", maxPrice: "", minPriceID: "", maxPriceID: "", minArea: "", maxArea: "", viewDays: "", addedBy: "", negotiablePrice: "")
            
            userInformation.shared.setUserFilterDictionary(dict: filterDict)
            
            self.navigationController?.pushViewController(object, animated: true)
            
        }, completion: nil)
        
    }
    
    
    
    // MARK: - // Function for SignUpClick
    
    @objc func SignUpClick() {
        
        UIView.animate(withDuration: 0.3, animations: {
            
            let object = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
            
            let filterDict = filterDictionary(parentCategory: "", propertyType: "", categorySub: "\(UserDefaults.standard.string(forKey: "searchCategory") ?? "")", period: "", country: userInformation.shared.searchCountryID, province: "\(UserDefaults.standard.string(forKey: "searchProvinceID") ?? "")", city: "\(UserDefaults.standard.string(forKey: "searchCityID") ?? "")", district: "\(UserDefaults.standard.string(forKey: "searchDistrictID") ?? "")", minPrice: "", maxPrice: "", minPriceID: "", maxPriceID: "", minArea: "", maxArea: "", viewDays: "", addedBy: "", negotiablePrice: "")
            
            userInformation.shared.setUserFilterDictionary(dict: filterDict)
            
            self.navigationController?.pushViewController(object, animated: true)
            
        }, completion: nil)
        
    }
    
    
    // MARK:- // Function Get_Fontsize
    
    
    func Get_fontSize(size: Float) -> Float
    {
        var size1 = size
        
        if(GlobalViewController.Isiphone6)
        {
            size1 += 1.0
        }
        else if(GlobalViewController.Isiphone6Plus)
        {
            size1 += 2.0
        }
        else if(GlobalViewController.IsiphoneX)
        {
            size1 += 3.0
        }
        
        return size1
    }
    
    
    //MARK:- //Show Alert Function
    func ShowAlertMessage(title: String,message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction.init(title: globalFunctions.shared.getLanguageString(variable: "ios_ok"), style: .cancel, handler: nil)
        
        alert.addAction(ok)
        
        self.present(alert, animated: true, completion: nil )
        
    }
    
    // MARK:- // Validation Custom Alert Function
    
    func customAlert(title: String,message: String, doesCancel : Bool , completion : @escaping () -> ())
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: globalFunctions.shared.getLanguageString(variable: "ios_ok"), style: .default) {
            UIAlertAction in
            
            UIView.animate(withDuration: 0.5)
            {
                completion()
            }
            
        }
        
        let cancel = UIAlertAction(title: "\(globalFunctions.shared.getLanguageString(variable: "property_details_cost_extend_cancel"))", style: .cancel, handler: nil)
        
        alert.addAction(ok)
        
        if doesCancel == true {
            alert.addAction(cancel)
        }
        
        self.present(alert, animated: true, completion: nil )
    }
    
    
    
    func CallAPI(urlString : String,param : String, completion : @escaping () -> ()){
        
        self.globalDispatchgroup.enter()
        
        startAnimating(CGSize(width: ((30.0/320.0)*FullWidth), height: ((30.0/320.0)*FullWidth)), message: "", messageFont: UIFont(name: "Lato-Bold", size: 14), type: .ballClipRotateMultiple, color: UIColor(displayP3Red: 0/255, green: 135/255, blue: 228/255, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.4), textColor: .black, fadeInAnimation: nil) //(displayP3Red: 0/255, green: 135/255, blue: 228/255, alpha: 1)
        if Connectivity.isConnectedToInternet() {
            var localTimeZoneName: String { return TimeZone.current.identifier }
            print(localTimeZoneName)
            let  urlstr = URL(string: GLOBALAPI + urlString)!   //change the url
            let parameters = param
            
            print("Parameters are : " , parameters)
            print("URL : ", "\(urlstr)" + "?" + parameters)
            let session = URLSession.shared
            var request = URLRequest(url: urlstr)
            request.httpMethod = "POST" //set http method as POST
            do {
                request.httpBody = param.data(using: String.Encoding.utf8)
                //request.addValue(localTimeZoneName, forHTTPHeaderField: "timezone")
            }
            print("URL : ", "\(urlstr)" + "?" + parameters)
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    
                    DispatchQueue.main.async {
                        
                        //self.globalDispatchgroup.leave()
                        
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        //SVProgressHUD.dismiss()
                        
                    }
                    
                    return
                    
                }
                if let response = response as? HTTPURLResponse{
                    if response.statusCode == 200{
                        DispatchQueue.main.async {
                            guard let data = data else {
                                DispatchQueue.main.async { NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                                    
                                }
                                return
                                
                            }
                            
                            do {
                                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                                    print("JSon Response: " , json)
                                    
                                    
                                    if "\(json["response"]!)".elementsEqual("TRUE")
                                    {
                                        self.globalJson = json.copy() as? NSDictionary
                                        
                                        if urlString.elementsEqual("home_all_property_listing") {
                                            self.listingEmpty = false
                                        }
                                        
                                        completion()
                                    }
                                    else
                                    {
                                        print("Unsuccessful Block")
                                        
                                        DispatchQueue.main.async {
                                            
                                            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                                            //SVProgressHUD.dismiss()
                                            
                                            if urlString.elementsEqual("app_logout"){
                                                self.globalJson = json.copy() as? NSDictionary
                                                
                                                completion()
                                            }
                                            else if urlString.elementsEqual("login") {
                                                
                                                self.globalJson = json.copy() as? NSDictionary
                                                
                                                completion()
                                                
                                            }
                                            else if urlString.elementsEqual("home_all_property_listing") {
                                                
                                                self.listingEmpty = true
                                                self.globalJson = json.copy() as? NSDictionary
                                                superAgentDtls.shared.storeAgentInfo(superAgentinfo: (self.globalJson["super_agent_info"] as? NSDictionary)!)
                                                self.globalDispatchgroup.leave()
                                                
                                                //                                    self.ShowAlertMessage(title: "", message: "\(json["message"] ?? "")")
                                                
                                                globalFunctions.shared.removePlaceholder(fromView: self.view)
                                                
                                                
                                                //                                    self.headerView.headerIconPositionOne.isHidden = true
                                                //                                    self.headerView.iconImageOne.isHidden = true
                                                //
                                                //                                    self.ShowAlertMessage(title: "", message: "\(json["message"] ?? "")")
                                                //
                                                //                                    globalFunctions.shared.removePlaceholder(fromView: self.view)
                                                
                                            }
                                                //                                else if urlString.elementsEqual("fetch_country_allcity") {
                                                //
                                                //                                    self.globalJson = json.copy() as? NSDictionary
                                                //
                                                //                                    completion()
                                                //
                                                //                                }
                                            else {
                                                self.ShowAlertMessage(title: "", message: "\(json["message"] ?? "")")
                                                
                                                globalFunctions.shared.removePlaceholder(fromView: self.view)
                                                
                                                self.view.isUserInteractionEnabled = true
                                            }
                                            //                                UIApplication.shared.endIgnoringInteractionEvents()
                                            
                                        }
                                    }
                                }
                                
                            } catch let error {
                                print(error.localizedDescription)
                                DispatchQueue.main.async {      //self.globalDispatchgroup.leave()     NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                                }
                            }
                        }
                    }
                }
            })
            task.resume()
        }
        else
        {
            DispatchQueue.main.async {
                
                //self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                //Create No network view
                globalFunctions.shared.createNoNetworkView(addInView: self.view, noNetView: self.noNetworkView)
                
            }
            
        }
        
    }
    
    
    //MARK:- // Global APi Call
    
//    func CallAPI(urlString : String,param : String, completion : @escaping () -> ())
//    {
//
//        self.globalDispatchgroup.enter()
//
//        startAnimating(CGSize(width: ((30.0/320.0)*FullWidth), height: ((30.0/320.0)*FullWidth)), message: "", messageFont: UIFont(name: "Lato-Bold", size: 14), type: .ballClipRotateMultiple, color: UIColor(displayP3Red: 0/255, green: 135/255, blue: 228/255, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.4), textColor: .black, fadeInAnimation: nil)
//
//        //SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
//
//        //        UIApplication.shared.beginIgnoringInteractionEvents()
//
//
//
//
//        if Connectivity.isConnectedToInternet()
//        {
//
//            let  urlstr = URL(string: GLOBALAPI + urlString)!   //change the url
//
//            let parameters = param //+ "&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
//
//            print("Parameters are : " , parameters)
//            print("URL : ", "\(urlstr)" + "?" + parameters)
//
//
//            let session = URLSession.shared
//
//            var request = URLRequest(url: urlstr)
//            request.httpMethod = "POST" //set http method as POST
//
//            do {
//                request.httpBody = param.data(using: String.Encoding.utf8)
//            }
//
//            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
//
//                guard error == nil else {
//
//                    DispatchQueue.main.async {
//
//                        self.globalDispatchgroup.leave()
//
//                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
//                        //SVProgressHUD.dismiss()
//
//                    }
//
//                    return
//                }
//
//                guard let data = data else {
//
//                    DispatchQueue.main.async {
//
//                        self.globalDispatchgroup.leave()
//
//                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
//                        //SVProgressHUD.dismiss()
//
//                    }
//
//                    return
//                }
//
//                do {
//
//                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
//                        print("JSon Response: " , json)
//
//
//                        if "\(json["response"]!)".elementsEqual("TRUE")
//                        {
//                            self.globalJson = json.copy() as? NSDictionary
//
//                            if urlString.elementsEqual("home_all_property_listing") {
//                                self.listingEmpty = false
//                            }
//
//                            completion()
//                        }
//                        else
//                        {
//                            print("Unsuccessful Block")
//
//                            DispatchQueue.main.async {
//
//                                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
//                                //SVProgressHUD.dismiss()
//
//
//                                if urlString.elementsEqual("login") {
//
//                                    self.globalJson = json.copy() as? NSDictionary
//
//                                    completion()
//
//                                }
//                                else if urlString.elementsEqual("home_all_property_listing") {
//
//                                    self.listingEmpty = true
//                                    self.globalJson = json.copy() as? NSDictionary
//                                    superAgentDtls.shared.storeAgentInfo(superAgentinfo: (self.globalJson["super_agent_info"] as? NSDictionary)!)
//                                    self.globalDispatchgroup.leave()
//
////                                    self.ShowAlertMessage(title: "", message: "\(json["message"] ?? "")")
//
//                                    globalFunctions.shared.removePlaceholder(fromView: self.view)
//
//
////                                    self.headerView.headerIconPositionOne.isHidden = true
////                                    self.headerView.iconImageOne.isHidden = true
////
////                                    self.ShowAlertMessage(title: "", message: "\(json["message"] ?? "")")
////
////                                    globalFunctions.shared.removePlaceholder(fromView: self.view)
//
//                                }
////                                else if urlString.elementsEqual("fetch_country_allcity") {
////
////                                    self.globalJson = json.copy() as? NSDictionary
////
////                                    completion()
////
////                                }
//                                else {
//                                    self.ShowAlertMessage(title: "", message: "\(json["message"] ?? "")")
//
//                                    globalFunctions.shared.removePlaceholder(fromView: self.view)
//
//                                    self.view.isUserInteractionEnabled = true
//                                }
//                                //                                UIApplication.shared.endIgnoringInteractionEvents()
//
//                            }
//                        }
//                    }
//
//                } catch let error {
//
//                    print(error.localizedDescription)
//
//                    DispatchQueue.main.async {
//
//                        self.globalDispatchgroup.leave()
//
//                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
//
//                        //SVProgressHUD.dismiss()
//
//                    }
//                }
//            })
//            task.resume()
//        }
//        else
//        {
//            DispatchQueue.main.async {
//
//                //self.globalDispatchgroup.leave()
//
//                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
//
//                //Create No network view
//                globalFunctions.shared.createNoNetworkView(addInView: self.view, noNetView: self.noNetworkView)
//
//            }
//
//        }
//
//    }
    
    
    
    
    
    // MARK:- // JSON Post method Get User Information
    
    func getUserInformation(completion : @escaping ()->()) {
        
        self.globalDispatchgroup.enter()
        
        if Connectivity.isConnectedToInternet()
        {
            DispatchQueue.main.async {
            }
            
            let  urlstr = URL(string: GLOBALAPI + "profile_details")!   //change the url
            
            let parameters = "dalaleen_user_id=\(userInformation.shared.userID ?? "")"
            
            print("Parameters are : " , parameters)
            print("URL : ", "\(urlstr)" + "?" + parameters)
            
            
            let session = URLSession.shared
            
            var request = URLRequest(url: urlstr)
            request.httpMethod = "POST" //set http method as POST
            
            do {
                request.httpBody = parameters.data(using: String.Encoding.utf8)
            }
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    
                    DispatchQueue.main.async {
                        
                        self.globalDispatchgroup.leave()
                        
                    }
                    
                    return
                }
                
                guard let data = data else {
                    
                    DispatchQueue.main.async {
                        
                        self.globalDispatchgroup.leave()
                        
                    }
                    
                    return
                }
                
                do {
                    
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                        print("JSon Response: " , json)
                        
                        if "\(json["response"]!)".elementsEqual("TRUE")
                        {
                            self.globalJson = json.copy() as? NSDictionary
                            
                            self.userInformationDict = self.globalJson
                            
                            DispatchQueue.main.async {
                                
                                self.globalDispatchgroup.leave()
                                
                                completion()
                                
                            }
                        }
                        else
                        {
                            print("Unsuccessful Block")
                            
                            DispatchQueue.main.async {
                                
                                self.globalDispatchgroup.leave()
                                
                            }
                        }
                    }
                    
                } catch let error {
                    
                    print(error.localizedDescription)
                    
                    DispatchQueue.main.async {
                        
                        self.globalDispatchgroup.leave()
                        
                    }
                }
            })
            task.resume()
        }
        else
        {
            DispatchQueue.main.async {
                
                //self.globalDispatchgroup.leave()
                
                //Create No network view
                globalFunctions.shared.createNoNetworkView(addInView: self.view, noNetView: self.noNetworkView)
                
            }
            
        }
        
    }
    
    
    // MARK:- // Button Transform Identity
    
    func buttonTransformIdentity(button: UIButton) {
        
        if button.isSelected == true {
            
            UIView.animate(withDuration: 0.2, delay: 0.1, options: .curveLinear, animations: {
                button.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                //self.rememberMeChecked = false
                
            })
            { (success) in
                button.isSelected = !button.isSelected
                UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveLinear, animations:
                    {
                        button.transform = .identity
                }, completion: nil)
            }
            
        }
        else {
            
            UIView.animate(withDuration: 0.2, delay: 0.1, options: .curveLinear, animations: {
                button.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                //self.rememberMeChecked = true
            })
            { (success) in
                button.isSelected = !button.isSelected
                UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveLinear, animations:
                    {
                        button.transform = .identity
                }, completion: nil)
            }
            
        }
        
    }
    

    
    
}


// MARK:- // Half Colored Text in a UILABEL
extension UILabel {
    
    func halfTextColorChange (fullText : String , changeText : String , color : UIColor ) {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: color , range: range)
        self.attributedText = attribute
    }
    
}


// MARK:- // Delete Prefix of a String

extension String {
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
}



// MARK:- // Get Height/Width of a given String

extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}




// MARK:- // Anchor Functions

extension UIView {
    
    func fillSuperview() {
        anchor(top: superview?.topAnchor, leading: superview?.leadingAnchor, bottom: superview?.bottomAnchor, trailing: superview?.trailingAnchor)
    }
    
    func anchorSize(to view: UIView) {
        widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    }
    
    func anchor(top: NSLayoutYAxisAnchor?, leading: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, trailing: NSLayoutXAxisAnchor?, padding: UIEdgeInsets = .zero, size: CGSize = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -padding.bottom).isActive = true
        }
        
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: -padding.right).isActive = true
        }
        
        if size.width != 0 {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
        
        if size.height != 0 {
            heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
    }
}




// MARK:- // Connectivity Extension

extension GlobalViewController: NetworkConnectionStatusListener {
    
    func networkStatusDidChange(status: NetworkConnectionStatus) {
        
        switch status {
        case .offline:
            globalFunctions.shared.createNoNetworkView(addInView: self.view, noNetView: self.noNetworkView)
        case .online:
            print("Online")
        }
        
    }
    
}



// MARK:- // Creating an UIImage from a UIView

extension UIView {
    
    // Using a function since `var image` might conflict with an existing variable
    // (like on `UIImageView`)
    func asImage() -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContext(self.frame.size)
            self.layer.render(in:UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
    }
}
