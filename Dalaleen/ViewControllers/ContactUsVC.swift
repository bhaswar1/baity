//
//  ContactUsVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 24/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView

class ContactUsVC: GlobalViewController {

    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var quickContactView: UIView!
    @IBOutlet weak var quickContactTitleLBL: UILabel!
    @IBOutlet weak var fullNameTXT: UITextField!
    @IBOutlet weak var emailTXT: UITextField!
    @IBOutlet weak var phoneNumberTXT: UITextField!
    @IBOutlet weak var companyNameTXT: UITextField!
    @IBOutlet weak var messageTXT: UITextView!
    
    
    @IBOutlet weak var contactInformationView: UIView!
    @IBOutlet weak var contactInformationTitleLBL: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var email: UILabel!
    
    var adminDetailsDictionary : NSDictionary!
    
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        globalFunctions.shared.loadPlaceholder(onView: self.view)

        self.localizeStrings()
        
        self.SetFont()
        
        self.messageTXT.delegate = self
        
        self.sendMessageButtonOutlet.layer.cornerRadius = self.sendMessageButtonOutlet.frame.size.height / 2
        
        self.getAdminDetails()
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            self.phoneNumber.text = "\(self.adminDetailsDictionary["contact_phone"] ?? "")"
            self.email.text = "\(self.adminDetailsDictionary["contact_email"] ?? "")"
            
            globalFunctions.shared.removePlaceholder(fromView: self.view)
            
        }
        
    }
    
    
    
    // MARK:- // Buttons
    
    // MARK:- // Send Message Button
    
    @IBOutlet weak var sendMessageButtonOutlet: UIButton!
    @IBAction func sendMessageButton(_ sender: UIButton) {
        
        let temptxt = globalFunctions.shared.getLanguageString(variable: "property_details_msg") + ":"
        
        if (self.fullNameTXT.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: "Please Enter Name.")
        }
        else if (self.emailTXT.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: "Please Enter Email.")
        }
        else if (self.phoneNumberTXT.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: "Please Enter Phone Number.")
        }
        else if (self.messageTXT.text?.elementsEqual(temptxt))! {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: "Please Enter Message.")
        }
        else {
            self.sendMessage()
        }
        
    }
    
    
    
    // MARK:- // JSON Post Method to Send Message
    
    func sendMessage() {
        
        var parameters : String = ""
        
        parameters = "user_name=\(self.fullNameTXT.text ?? "")&email_address=\(self.emailTXT.text ?? "")&phoneno=\(self.phoneNumberTXT.text ?? "")&msg=\(self.messageTXT.text ?? "")&com_name=\(self.companyNameTXT.text ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "contact_us", param: parameters) {
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
                let alert = UIAlertController(title: globalFunctions.shared.getLanguageString(variable: "success_msg"), message: "\(self.globalJson["message"] ?? "")", preferredStyle: .alert)
                
                let ok = UIAlertAction(title: "OK", style: .default) {
                    UIAlertAction in
                    
                    UIView.animate(withDuration: 0.5)
                    {
                        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        self.navigationController?.pushViewController(navigate, animated: true)
                    }
                }
                
                alert.addAction(ok)
                
                self.present(alert, animated: true, completion: nil )
            }
            
        }
        
    }
    
    
    
    // MARK:- // JSON Post Method to get Admin Details
    
    func getAdminDetails() {
        
        let parameters = "language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "contact_us_value_display", param: parameters) {
            
            self.adminDetailsDictionary = (self.globalJson["info"] as! NSArray)[0] as? NSDictionary
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
        }
        
    }
    
    
    
    
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.quickContactTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "quick_contact")
        self.fullNameTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "full_name")
        self.emailTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "email")
        self.phoneNumberTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "phone")
        self.companyNameTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "company_name")
        self.messageTXT.text = globalFunctions.shared.getLanguageString(variable: "property_details_msg") + ":"
        self.contactInformationTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "contact_info")
        self.sendMessageButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "property_details_send_msg"), for: .normal)
    }

    
    // MARK:- // Set Font
    
    func SetFont() {
        
        self.quickContactTitleLBL.font = UIFont(name: self.quickContactTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        self.fullNameTXT.font = UIFont(name: self.fullNameTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.emailTXT.font = UIFont(name: self.quickContactTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.phoneNumberTXT.font = UIFont(name: self.quickContactTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.companyNameTXT.font = UIFont(name: self.quickContactTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.messageTXT.font = UIFont(name: self.quickContactTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.sendMessageButtonOutlet.titleLabel!.font = UIFont(name: self.sendMessageButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.contactInformationTitleLBL.font = UIFont(name: self.contactInformationTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        self.phoneNumber.font = UIFont(name: self.phoneNumber.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.email.font = UIFont(name: self.email.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        
        
    }
    
    
    //MARK:- Open mail Btn Click
    @IBAction func tapOpenMail(sender: UIButton){
        let email = self.email.text
        if let url = URL(string: "mailto:\(email ?? "")") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
    }
    
    //MARK:- Tap On number
    @IBAction func callButtonAction(sender: UIButton){
        let phoneNumber = (self.phoneNumber.text)?.replacingOccurrences(of: " ", with: "")
            
            if let phoneURL = NSURL(string: ("tel://" + phoneNumber!)) {

                self.customAlert(title: globalFunctions.shared.getLanguageString(variable: "ios_are_you_sure"), message: globalFunctions.shared.getLanguageString(variable: "property_details_call") + " " + phoneNumber! + " ?", doesCancel: true) {

                    UIApplication.shared.open(phoneURL as URL, options: [:], completionHandler: nil)

                }
            }
        
    }
    

}



// MARK:- // Textview Delegate Functions

extension ContactUsVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        let temptxt = globalFunctions.shared.getLanguageString(variable: "property_details_msg") + ":"
        
        if textView.text!.elementsEqual(temptxt) {
            textView.text = ""
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text!.elementsEqual("") {
            textView.text = globalFunctions.shared.getLanguageString(variable: "property_details_msg") + ":"
        }
        
    }
    
}
