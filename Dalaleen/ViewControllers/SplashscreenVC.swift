//
//  SplashscreenVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 08/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView

class SplashscreenVC: GlobalViewController {
    
    
    //var langDataArray = [languageDataForm]()
    
    var langDictionary = NSDictionary()
    
    var locationManager: CLLocationManager! = CLLocationManager()
    
    var locationFetchDict : locationFetchDataFormat!
    
    var Latitude : Double!
    var Longitude : Double!
    
    var allCityArray = NSArray()
    
    
    var alreadyNavigated : Bool! = false
    
    var searchLangCode : String! = ""
    
    var langArray = [languageListDataFormat]()
    
    
    // MARK:- // Set Language
    
    func setLanguage(completion : @escaping ()->()) {
        
        if globalFunctions.shared.isKeyPresentInUserDefaults(key: "firstLaunch") == false {
            
            print("Device Language Code---\(LocalizationSystem.sharedInstance.getLanguage())")
            
            self.searchLangCode = LocalizationSystem.sharedInstance.getLanguage()
            
            
            for i in 0..<self.langArray.count {
                
                if "\(self.langArray[i].langCode ?? "")".elementsEqual("\(self.searchLangCode ?? "")") {
                    
                    self.searchLangCode = "\(self.langArray[i].langCode ?? "")"
                    
                    break
                    
                }
                else {
                    
                    self.searchLangCode = "en"
                    
                }
            }
            
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "\(searchLangCode ?? "")")
            
            UserDefaults.standard.set("\(searchLangCode ?? "")", forKey: "searchLanguageCode")
            
        }
        
        completion()
    }
    
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.customAlert(title: "push", message: "\(UserDefaults.standard.bool(forKey: "isClickOnPush"))", doesCancel: false, completion: {
//            UserDefaults.standard.set(false, forKey: "isClickOnPush")
//        })
        
        UserDefaults.standard.set(LocalizationSystem.sharedInstance.getLanguage(), forKey: "searchLanguageCode")
        
        print(LocalizationSystem.sharedInstance.getLanguage())
        
        self.searchLangCode = LocalizationSystem.sharedInstance.getLanguage()
        
        self.loadLanguageData()
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            self.setLanguage {
                
                self.getLanguageStrings()
                
                self.globalDispatchgroup.notify(queue: .main) {
                    if UserDefaults.standard.bool(forKey: "isClickOnPush"){
                        let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "InboxVC") as! InboxVC
                        UserDefaults.standard.set(false, forKey: "isClickOnPush")
                        self.navigationController?.pushViewController(nav, animated: true)
                    }
                    else{
                        self.loadCountryCodes()
                        
                        self.globalDispatchgroup.notify(queue: .main, execute: {
                            
                            //if globalFunctions.shared.isKeyPresentInUserDefaults(key: "firstLaunch") == false {
                            
                            if CLLocationManager.locationServicesEnabled() == true
                            {
                                self.locationManager.pausesLocationUpdatesAutomatically = true
                                self.locationManager.requestWhenInUseAuthorization()
                                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                                self.locationManager.startUpdatingLocation()
                                
                                self.locationManager.delegate = self
                                
                                
                            }
                            else
                            {
                                print("Do Nothing")
                            }
                            
                            //}
                            //                    else {
                            //                        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "AppStartSearchVC") as! AppStartSearchVC
                            //
                            //                        UserDefaults.standard.set(LocalizationSystem.sharedInstance.getLanguage(), forKey: "searchLanguageCode")
                            //                        userInformation.shared.setSearchLanguageCode(code: UserDefaults.standard.string(forKey: "searchLanguageCode")!)
                            //
                            //                        self.navigationController?.pushViewController(navigate, animated: true)
                            //                    }
                            
                        })
                    }

                }
                
            }
            
        }
        
    }
    
    
    // MARK:- // JSON Post Method to get language strings
    
    func getLanguageStrings() {
        
        let langID : String? = LocalizationSystem.sharedInstance.getLanguage()
        
        UserDefaults.standard.set(langID, forKey: "DeviceLanguageID")
        
        print("Device Language Code : ",langID!)
        
        var parameters : String!
        
        parameters = "language_code=\(langID ?? "")"
        
        self.CallAPI(urlString: "languageConvert_test", param: parameters) {
            
//            for i in 0..<(self.globalJson["info"] as! NSArray).count {
//                self.langDataArray.append(languageDataForm(variables: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["Variables"] ?? "")", defaultText: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["Default Text"] ?? "")", languageText: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["Language Text"] ?? "")"))
//            }
//
//            languageData.shared.storeLanguageData(langData: self.langDataArray)
            
            
            self.langDictionary = self.globalJson["info"] as! NSDictionary
            
            languageData.shared.storeLanguageData(langData: self.langDictionary)
            
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
            }
            
        }
        
    }
    
    
    
    
    
    // MARK: - // JSON Get Method to get PhoneCode and Country Data from Web
    
    
    func loadCountryCodes()
        
    {
        
        let parameters = "language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "country", param: parameters) {
            
            var countryCodeArray = [countryAndPhoneCodes]()
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                countryCodeArray.append(countryAndPhoneCodes(countryFlag: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["counrty_flag"] ?? "")", countryName: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["country_name"] ?? "")", countryCode: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["country_code"] ?? "")", id: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["id"] ?? "")", languageCode: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["language_code"] ?? "")", phoneCode: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["phone_code"] ?? "")", appSearchStatus: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["appsearch_status"] ?? "")"))
            }
            
            countryPhoneCodes.shared.storeCountryPhoneCodesData(countryPhoneCodeData: countryCodeArray)
            
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
            }
            
        }
    }
    
    
    
    // MARK: - // JSON Get Method to get Language Data from Web
    
    
    func loadLanguageData()
        
    {
        
        let parameters = "language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "language", param: parameters) {
            
            
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                self.langArray.append(languageListDataFormat(id: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["id"] ?? "")", langName: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["lang_name"] ?? "")", langCode: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["language_code"] ?? "")", langImage: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["lang_icon"] ?? "")", langStatus: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["status"] ?? "")"))
            }
            
            languageList.shared.storeCountryPhoneCodesData(langArray: self.langArray)
            
//            for i in 0..<langArray.count {
//
//                print("\(UserDefaults.standard.string(forKey: "searchLanguageCode") ?? "")")
//                
//                if "\(langArray[i].langCode ?? "")".elementsEqual("\(UserDefaults.standard.string(forKey: "searchLanguageCode") ?? "")") {
//
//                    LocalizationSystem.sharedInstance.setLanguage(languageCode: LocalizationSystem.sharedInstance.getLanguage())
//
//                    UserDefaults.standard.set(LocalizationSystem.sharedInstance.getLanguage(), forKey: "searchLanguageCode")
//
//                    break
//
//                }
//                else {
//
//                    LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
//
//                    UserDefaults.standard.set("en", forKey: "searchLanguageCode")
//
//                }
//
//
//            }
            
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
            }
            
        }
        
    }
    
    
    
    // MARK:- // Reverse Geocoding to get address from Entered Lat Long
    
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String ) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if let pm = placemarks{
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print("Country---\(pm.country ?? "")")
                        print("CountryCode---\(pm.isoCountryCode ?? "")")
                        print("Locality---\(pm.locality ?? "")")
                        print("Sublocality---\(pm.subLocality ?? "")")
                        print("Throughfate---\(pm.thoroughfare ?? "")")
                        print("PostalCode---\(pm.postalCode ?? "")")
                        print("SubThroughfare---\(pm.subThoroughfare ?? "")")
                        print("AdministrativeArea---\(pm.administrativeArea ?? "")")
                        
                        self.locationFetchDict = locationFetchDataFormat(countryName: "\(pm.country ?? "")", countryCode: "\(pm.isoCountryCode ?? "")", locality: "\(pm.locality ?? "")", sublocality: "\(pm.subLocality ?? "")", throughfate: "\(pm.thoroughfare ?? "")", postalCode: "\(pm.postalCode ?? "")", subthroughfare: "\(pm.subThoroughfare ?? "")", administrativeArea: "\(pm.administrativeArea ?? "")")
                        
                        if self.alreadyNavigated == false {
                            
                            self.alreadyNavigated = true
                            
                            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "AppStartSearchVC") as! AppStartSearchVC
                            
                            //                        UserDefaults.standard.set(LocalizationSystem.sharedInstance.getLanguage(), forKey: "searchLanguageCode")
                            //                        userInformation.shared.setSearchLanguageCode(code: UserDefaults.standard.string(forKey: "searchLanguageCode")!)
                            
                            //                    if globalFunctions.shared.isKeyPresentInUserDefaults(key: "searchLanguageCode") == false {
                            //                        UserDefaults.standard.set("en", forKey: "searchLanguageCode")
                            //                        UserDefaults.standard.set("English", forKey: "searchLanguageName")
                            //
                            //                        userInformation.shared.setSearchLanguageCode(code: "en")
                            //                        userInformation.shared.setSearchLanguageName(name: "English")
                            //                    }
                            //                    else {
                            //                        userInformation.shared.setSearchLanguageCode(code: "\(UserDefaults.standard.string(forKey: "searchLanguageCode") ?? "")")
                            //                        userInformation.shared.setSearchLanguageName(name: "\(UserDefaults.standard.string(forKey: "searchLanguageName") ?? "")")
                            //                    }
                            
                            navigate.locationFetchDict = self.locationFetchDict
                            
                            
                            self.navigationController?.pushViewController(navigate, animated: true)
                            
                        }
                        
                    }
                }
                

        })
        
    }
    
    
    
    
    
    
    
}


// MARK:- // Location Manager Delegate Method

extension SplashscreenVC: CLLocationManagerDelegate {
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations.last
        
        
        self.Latitude = userLocation!.coordinate.latitude
        self.Longitude = userLocation!.coordinate.longitude
        
        locationManager.stopUpdatingLocation()
        
        self.getAddressFromLatLon(pdblLatitude: "\(self.Latitude ?? 22.0)", withLongitude: "\(self.Longitude ?? 50.33)")
    }
    
}
