//
//  AppStartSearchVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 17/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

//        var x = NSArray()
//        var y = NSArray()
//
//        var a = NSArray()
//        var b = NSArray()
//
//
//        for i in 0..<a.count {
//
//            for j in 0..<b.count {
//
//                (x[i])[j].adkj = a[i][j]
//
//            }
//
//        }



import UIKit
import SVProgressHUD
import NVActivityIndicatorView

class AppStartSearchVC: GlobalViewController {

    
    @IBAction func appShareBtn(_ sender: UIButton) {
        let text = "This is some text that I want to share."
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBOutlet weak var appShareBtnOutlet: UIButton!
    @IBOutlet weak var realestateLogoImageview: UIImageView!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var districtView: UIView!
    @IBOutlet weak var district: UILabel!
    @IBOutlet weak var viewOfPicker: UIView!
    @IBOutlet weak var selectCityPickerview: UIPickerView!
    @IBOutlet weak var selectDistrictPickerview: UIPickerView!
    @IBOutlet weak var selectLanguagePickerview: UIPickerView!
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var languageStaticLBL: UILabel!
    @IBOutlet weak var language: UILabel!
    @IBOutlet weak var copyrightLine: UILabel!
    
    var selectedPickerview : UIPickerView!
    var selectedPickerIndex : Int!
    var doneClicked : Bool!
    
    
    var locationFetchDict : locationFetchDataFormat!
    
    //var langDataArray = [languageDataForm]()
    
    var langDictionary = NSDictionary()
    
    
    var locationManager: CLLocationManager! = CLLocationManager()
    var Latitude : Double!
    var Longitude : Double!
    
    var countryID : String! = ""
    
    
    var cityID : String! = ""
    
    var allCityArray = NSArray()
    
    var districtArray : NSArray!
    
    var languageArray : NSArray!
    
    var searchCategory : String! = ""
    
    
    
    // MARK:- // Create Picker Toolbar
    
    func createToolbar() {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.tintColor = UIColor.white//"Done" button colour
        toolBar.barTintColor = UIColor.black// bar background colour
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "\(globalFunctions.shared.getLanguageString(variable: "ios_ok"))", style: UIBarButtonItem.Style.done, target: self, action: #selector(donePicker))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "\(globalFunctions.shared.getLanguageString(variable: "modal_button_text1"))", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelPicker))
        
        toolBar.setItems([cancelButton, spacer, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.viewOfPicker.addSubview(toolBar)
        toolBar.anchor(top: nil, leading: nil, bottom: self.selectCityPickerview.topAnchor, trailing: nil, size: .init(width: UIScreen.main.bounds.size.width, height: 40))
        
    }
    
    
    // MARK:- // Done Picker Action Method
    
    @objc func donePicker() {
        
        print("Done Clicked")
        
        self.doneClicked = true
        
        self.pickerView(self.selectedPickerview, didSelectRow: self.selectedPickerIndex, inComponent: 0)
        
        self.view.sendSubviewToBack(self.viewOfPicker)
        self.viewOfPicker.isHidden = true
        self.selectedPickerview.isHidden = true
        
    }
    
    // MARK:- // Cancel Picker Action Method
    
    @objc func cancelPicker() {
        print("Cancel Clicked")
        
        self.view.sendSubviewToBack(self.viewOfPicker)
        self.viewOfPicker.isHidden = true
        self.selectedPickerview.isHidden = true
    }
    
    
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if ((self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 2])?.isKind(of: SelectCountryAndLanguageVC.self))! {
            self.headerView.headerBackButton.isHidden = false
        }
        
        self.pageLoadFunction()
        
        self.createToolbar()
        var arabic: Bool! = false
        if ((UserDefaults.standard.string(forKey: "searchLanguageCode"))!.elementsEqual("ar")){
            arabic = true
        }
        UIView.appearance().semanticContentAttribute = arabic ? .forceRightToLeft : .forceLeftToRight
        
    }
    
    
    
    // MARK:- // Page Load Function
    
    func pageLoadFunction() {
        
        self.localizeStrings()
        
        self.setFont()
//
//        UserDefaults.standard.set("102", forKey: "searchCountryID")
//        UserDefaults.standard.set("Iraq", forKey: "searchCountryName")
//
        //self.view.isUserInteractionEnabled = false
        
        //self.getAllCities(countryID: <#T##String#>, completion: <#T##() -> ()#>)
        
//        self.globalDispatchgroup.notify(queue: .main) {
//
//
//        }
        
        
        self.prefillData()
        
        
    }
    
    
    
    
    // MARK:- // Prefill Data
    
    func prefillData() {
        
        if globalFunctions.shared.isKeyPresentInUserDefaults(key: "firstLaunch") == false {
            
            
            if countryPhoneCodes.shared.countryPhnCodes.count > 0 {
                
                for i in 0..<countryPhoneCodes.shared.countryPhnCodes.count {
                    
                    if (countryPhoneCodes.shared.countryPhnCodes[i].countryCode ?? "").elementsEqual("\(self.locationFetchDict.countryCode ?? "")") {
                        
                        if (countryPhoneCodes.shared.countryPhnCodes[i].appSearchStatus ?? "").elementsEqual("Y") {
                            
                            self.countryID = "\(countryPhoneCodes.shared.countryPhnCodes[i].id ?? "")"
                            
                            UserDefaults.standard.set("\(countryPhoneCodes.shared.countryPhnCodes[i].id ?? "")", forKey: "searchCountryID")
                            UserDefaults.standard.set("\(countryPhoneCodes.shared.countryPhnCodes[i].countryName ?? "")", forKey: "searchCountryName")
                            
                            //                        self.countryID = "102" // Iraq
                            //                        UserDefaults.standard.set("102", forKey: "searchCountryID")
                            //                        UserDefaults.standard.set("Iraq", forKey: "searchCountryName")
                            
                            break
                            
                        }
                        
                    }
                    else {
                        self.countryID = "102" // Iraq
                        
                        UserDefaults.standard.set("102", forKey: "searchCountryID")
                        UserDefaults.standard.set("Iraq", forKey: "searchCountryName")
                    }
                    
                }
                
            }

            self.city.text = globalFunctions.shared.getLanguageString(variable: "home_select_city")
            self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
            
            self.getAllCities(countryID: "\(self.countryID ?? "")")
            
            self.globalDispatchgroup.notify(queue: .main) {
                
                self.prefillCityData(checkCity: "\(self.locationFetchDict.administrativeArea ?? "")")
                
            }

        }
        else {
            
            print("CountryID----\(UserDefaults.standard.string(forKey: "searchCountryID") ?? "")")
            
            self.getAllCities(countryID: "\(UserDefaults.standard.string(forKey: "searchCountryID") ?? "")")
            //self.getAllCities(countryID: "102")
            
            self.globalDispatchgroup.notify(queue: .main) {
                
                // Preselect City
                for i in 0..<self.allCityArray.count {
                    
                    if "\((self.allCityArray[i] as! NSDictionary)["id"] ?? "")".elementsEqual("\(UserDefaults.standard.string(forKey: "searchCityID") ?? "")") {
                        
                        self.city.text = "\((self.allCityArray[i] as! NSDictionary)["cityname"] ?? "")"
                        self.cityID = "\((self.allCityArray[i] as! NSDictionary)["id"] ?? "")"
                        
                        UserDefaults.standard.set("\((self.allCityArray[i] as! NSDictionary)["cityname"] ?? "")", forKey: "searchCityName")
                        UserDefaults.standard.set("\((self.allCityArray[i] as! NSDictionary)["id"] ?? "")", forKey: "searchCityID")
                        
                    }
                    
                }
                
//                self.city.text = UserDefaults.standard.string(forKey: "searchCityName")
//                self.cityID = UserDefaults.standard.string(forKey: "searchCityID")
                
                self.getDistrictBasedOnCity()
                
                self.globalDispatchgroup.notify(queue: .main, execute: {
                    
                    
//                    if ((self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 2])?.isKind(of: SelectCountryAndLanguageVC.self))! {
//
//                        self.city.text = globalFunctions.shared.getLanguageString(variable: "home_select_city")
//                        self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
//
//                    }
//                    else {
//                        
                        if globalFunctions.shared.isKeyPresentInUserDefaults(key: "searchDistrictName") == false {
                            self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
                        }
                        else {
                            if "\(UserDefaults.standard.string(forKey: "searchDistrictName") ?? "")".elementsEqual("") {
                                self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
                            }
                            else {
                                
                                // Preselect District
                                for i in 0..<self.districtArray.count {
                                    
                                    if "\((self.districtArray[i] as! NSDictionary)["id"] ?? "")".elementsEqual("\(UserDefaults.standard.string(forKey: "searchDistrictID") ?? "")") {
                                        
                                        self.district.text = "\((self.districtArray[i] as! NSDictionary)["districtname"] ?? "")"
                                        
                                        
                                        UserDefaults.standard.set("\((self.districtArray[i] as! NSDictionary)["districtname"] ?? "")", forKey: "searchDistrictName")
                                        UserDefaults.standard.set("\((self.districtArray[i] as! NSDictionary)["id"] ?? "")", forKey: "searchDistrictID")
                                        
                                    }
                                    
                                }
                                
                                //self.district.text = UserDefaults.standard.string(forKey: "searchDistrictName")
                            }
                            
                        }
                        
                        
//                    }
                    
                })
                
                
            }
            
 

            

//            if "\(UserDefaults.standard.string(forKey: "searchCategory") ?? "")".elementsEqual("1") {
//                self.saleButtonOutlet.backgroundColor = UIColor(red:255/255, green:231/255, blue:152/255, alpha: 1)
//            }
//            else {
//                self.rentButtonOutlet.backgroundColor = UIColor(red:255/255, green:231/255, blue:152/255, alpha: 1)
//            }

            

        }
        
        
//        if CLLocationManager.locationServicesEnabled() == true
//        {
//            locationManager.pausesLocationUpdatesAutomatically = false
//            locationManager.requestWhenInUseAuthorization()
//            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.startUpdatingLocation()
//
//            locationManager.delegate = self
//        }
//        else
//        {
//            print("Do Nothing")
//        }
        
        
//        if globalFunctions.shared.isKeyPresentInUserDefaults(key: "searchCategory") ==  false {
//            self.saleButtonOutlet.backgroundColor = .white
//            self.rentButtonOutlet.backgroundColor = .white
//        }
//        else {
//            if "\(UserDefaults.standard.string(forKey: "searchCategory") ?? "")".elementsEqual("1") {
//                self.saleButtonOutlet.backgroundColor = UIColor(red:255/255, green:231/255, blue:152/255, alpha: 1)
//            }
//            else {
//                self.rentButtonOutlet.backgroundColor = UIColor(red:255/255, green:231/255, blue:152/255, alpha: 1)
//            }
//        }
        
        
        
        self.getLanguageList()
        
        self.globalDispatchgroup.notify(queue: .main, execute: {
            
            self.view.isUserInteractionEnabled = true
            self.language.text = UserDefaults.standard.string(forKey: "searchLanguageName")
            
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "\(UserDefaults.standard.string(forKey: "searchLanguageCode") ?? "")")
            
            for i in 0..<self.languageArray.count {
                
                if "\((self.languageArray[i] as! NSDictionary)["language_code"] ?? "")".elementsEqual("\(UserDefaults.standard.string(forKey: "searchLanguageCode") ?? "")") {
                    
                    UserDefaults.standard.set("\((self.languageArray[i] as! NSDictionary)["lang_name"] ?? "")", forKey: "searchLanguageName")
                    
                }
                
            }
            
        })
        
    }
    
    
    // MARK:- // Set Userinformation
    
    func setUserInformation() {
        
        userInformation.shared.setSearchCountryID(id: UserDefaults.standard.string(forKey: "searchCountryID")!)
        userInformation.shared.setSearchCountryName(name: UserDefaults.standard.string(forKey: "searchCountryName")!)
        userInformation.shared.setSearchCountryFlag(flag: UserDefaults.standard.string(forKey: "searchCountryFlag")!)
        userInformation.shared.setSearchLanguageCode(code: UserDefaults.standard.string(forKey: "searchLanguageCode")!)
        userInformation.shared.setSearchLanguageName(name: "\(UserDefaults.standard.string(forKey: "searchLanguageName") ?? "")")
        
        userInformation.shared.setSearchCityID(id: UserDefaults.standard.string(forKey: "searchCityID")!)
        userInformation.shared.setSearchCityName(name: UserDefaults.standard.string(forKey: "searchCityName")!)
        
        userInformation.shared.setSearchDistrictID(id: "\(UserDefaults.standard.string(forKey: "searchDistrictID") ?? "")")
        userInformation.shared.setSearchDistrictName(name: "\(UserDefaults.standard.string(forKey: "searchDistrictName") ?? "")")
        
        userInformation.shared.setSearchCategory(category: "\(UserDefaults.standard.string(forKey: "searchCategory") ?? "")")
        
    }
    
    
    
    
    // MARK:- // Buttons
    
    // MARK:- // Select City Button
    
    @IBOutlet weak var selectCityButtonOutlet: UIButton!
    @IBAction func selectCityButton(_ sender: UIButton) {
        
        self.view.bringSubviewToFront(self.viewOfPicker)
        self.viewOfPicker.isHidden = false
        self.selectCityPickerview.isHidden = false
        
        self.selectCityPickerview.selectRow(0, inComponent: 0, animated: false)
        
        self.selectedPickerview = self.selectCityPickerview
        self.selectedPickerIndex = 0
        self.doneClicked = false
    }
    
    
    
    // MARK:- // Select District Button
    
    @IBOutlet weak var selectDistrictButtonOutlet: UIButton!
    @IBAction func selectDistrictButton(_ sender: UIButton) {
        
        if self.cityID.elementsEqual("") {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "home_select_city"))
        }
        else {
            self.view.bringSubviewToFront(self.viewOfPicker)
            self.viewOfPicker.isHidden = false
            self.selectDistrictPickerview.isHidden = false
            
            self.selectDistrictPickerview.selectRow(0, inComponent: 0, animated: false)

            self.selectedPickerview = self.selectDistrictPickerview
            self.selectedPickerIndex = 0
            self.doneClicked = false
        }
        
    }
    
    
    
    // MARK:- // Rent Button
    
    @IBOutlet weak var rentButtonOutlet: UIButton!
    @IBAction func rentButton(_ sender: UIButton) {
        
        UserDefaults.standard.set("2", forKey: "searchCategory")
        
        self.searchCategory = "2"
        
        UIView.animate(withDuration: 0.5) {
            
            self.saleButtonOutlet.backgroundColor = .white
            self.rentButtonOutlet.backgroundColor = UIColor(red:255/255, green:231/255, blue:152/255, alpha: 1)
            
        }
        
    }
    
    
    
    
    
    // MARK:- // Sale Button
    
    @IBOutlet weak var saleButtonOutlet: UIButton!
    @IBAction func saleButton(_ sender: UIButton) {
        
        UserDefaults.standard.set("1", forKey: "searchCategory")
        
        self.searchCategory = "1"
        
        UIView.animate(withDuration: 0.5) {
            
            self.rentButtonOutlet.backgroundColor = .white
            self.saleButtonOutlet.backgroundColor = UIColor(red:255/255, green:231/255, blue:152/255, alpha: 1)
            
        }
        
    }
    
    
    
    
    // MARK:- // Search Button
    
    @IBOutlet weak var searchButtonOutlet: UIButton!
    @IBAction func searchButton(_ sender: UIButton) {
        
        self.searchButtonOutlet.backgroundColor = UIColor(red:255/255, green:231/255, blue:152/255, alpha: 1)
        
        userInformation.shared.setUserFilterDictionary(dict: filterDictionary(parentCategory: "", propertyType: "", categorySub: "\(UserDefaults.standard.string(forKey: "searchCategory") ?? "")", period: "", country: "\(UserDefaults.standard.string(forKey: "searchCountryID") ?? "")", province: "\(UserDefaults.standard.string(forKey: "searchProvinceID") ?? "")", city: "\(UserDefaults.standard.string(forKey: "searchCityID") ?? "")", district: "\(UserDefaults.standard.string(forKey: "searchDistrictID") ?? "")", minPrice: "", maxPrice: "", minPriceID: "", maxPriceID: "", minArea: "", maxArea: "", viewDays: "", addedBy: "", negotiablePrice: ""))
        
        
        if self.cityID.elementsEqual("") {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "home_select_city"))
        }
        else if "\(self.searchCategory ?? "")".elementsEqual("") {
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "ios_select_search_category"))
        }
        else {
            
            self.getLanguageStrings(langCode: "\((UserDefaults.standard.string(forKey: "searchLanguageCode") ?? ""))")
            
            self.globalDispatchgroup.notify(queue: .main) {
                
                self.setUserInformation()
                
                if globalFunctions.shared.isKeyPresentInUserDefaults(key: "firstLaunch") == false {
                    
                    let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    
                    UserDefaults.standard.set(false, forKey: "firstLaunch")
                    
                    self.navigationController?.pushViewController(navigate, animated: true)
                }
                else {
                    
                    if globalFunctions.shared.isKeyPresentInUserDefaults(key: "loginStatus") == true {
                        
                        if UserDefaults.standard.bool(forKey: "loginStatus") == true {
                            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                            
                            self.navigationController?.pushViewController(navigate, animated: false)
                        }
                        else {
                            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                            
                            userInformation.shared.setValuesForGuest()
                            
                            self.navigationController?.pushViewController(navigate, animated: false)
                        }
                        
                        
                    }
                    else {
                        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        
                        self.navigationController?.pushViewController(navigate, animated: false)
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
    
    // MARK:- // Select Language Button
    
    @IBOutlet weak var selectLanguageButtonOutlet: UIButton!
    @IBAction func selectLanguageButton(_ sender: UIButton) {
        
        self.view.bringSubviewToFront(self.viewOfPicker)
        self.viewOfPicker.isHidden = false
        self.selectLanguagePickerview.isHidden = false
        
        self.selectLanguagePickerview.selectRow(0, inComponent: 0, animated: false)
        
        self.selectedPickerview = self.selectLanguagePickerview
        self.selectedPickerIndex = 0
        self.doneClicked = false
        
    }
    
    
    
    // MARK:- // Close Picker Button
    
    @IBOutlet weak var closePickerButtonOutlet: UIButton!
    @IBAction func closePickerButton(_ sender: UIButton) {
        
        self.view.sendSubviewToBack(self.viewOfPicker)
        self.viewOfPicker.isHidden = true
        self.selectCityPickerview.isHidden = true
        self.selectDistrictPickerview.isHidden = true
        self.selectLanguagePickerview.isHidden = true
        
    }
    
    
    
    

    
    
    
    // MARK:- // JSON Post Method to get All Cities based on Country
    // Currently only fetching for IRAQ by default
    
    func getAllCities(countryID : String) {
        
        let parameters = "country_id=\(countryID)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "fetch_country_allcity", param: parameters) {
            
            if "\(self.globalJson["response"] ?? "")".elementsEqual("TRUE") {
                
                self.allCityArray = (self.globalJson["info"] as? NSArray)!
                
                UserDefaults.standard.set("\(self.globalJson["country_flag"] ?? "")", forKey: "searchCountryFlag")
                
                DispatchQueue.main.async {
                    
                    self.globalDispatchgroup.leave()
                    
                    self.selectCityPickerview.delegate = self
                    self.selectCityPickerview.dataSource = self
                    self.selectCityPickerview.reloadAllComponents()
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    //SVProgressHUD.dismiss()
                    
                }
                
            }
            
        }
        
    }
    
    
    
    // MARK:- // JSON Post method to Get District based on City
    
    func getDistrictBasedOnCity() {
        
        var parameters : String!
        
        parameters = "city_id=\(self.cityID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "fetch_district_by_city", param: parameters) {
            
            self.districtArray = self.globalJson["info"] as? NSArray
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.selectDistrictPickerview.delegate = self
                self.selectDistrictPickerview.dataSource = self
                self.selectDistrictPickerview.reloadAllComponents()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    
    
    // MARK:- // JSON Post Method to get Language List
    
    func getLanguageList() {
        
        let parameters = "language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "language", param: parameters) {
            
            self.languageArray = self.globalJson["info"] as? NSArray
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.selectLanguagePickerview.delegate = self
                self.selectLanguagePickerview.dataSource = self
                self.selectLanguagePickerview.reloadAllComponents()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
        }
    }
    
    
    
    
    // MARK:- // Reverse Geocoding to get address from Entered Lat Long
    
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if let pm = placemarks{
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print("Country---\(pm.country ?? "")")
                        print("CountryCode---\(pm.isoCountryCode ?? "")")
                        print("Locality---\(pm.locality ?? "")")
                        print("Sublocality---\(pm.subLocality ?? "")")
                        print("Throughfate---\(pm.thoroughfare ?? "")")
                        print("PostalCode---\(pm.postalCode ?? "")")
                        print("SubThroughfare---\(pm.subThoroughfare ?? "")")
                        print("AdministrativeArea---\(pm.administrativeArea ?? "")")
                        
                        
                    }
                }
                

        })
        
    }
    
    
    // MARK:- // Frefill City Data
    
    func prefillCityData(checkCity : String) {
        
        var cityDataArray = NSArray()
        
        let parameters = "country_id=\(countryID ?? "")&language_code=en"
        
        self.CallAPI(urlString: "fetch_country_allcity", param: parameters) {
            
            cityDataArray = (self.globalJson["info"] as? NSArray)!
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                
                if cityDataArray.count > 0 {
                    
                    for i in 0..<cityDataArray.count {
                        if "\((cityDataArray[i] as! NSDictionary)["cityname"] ?? "")".elementsEqual(checkCity) {
                            
                            self.city.text = "\((self.allCityArray[i] as! NSDictionary)["cityname"] ?? "")"
                            self.cityID = "\((self.allCityArray[i] as! NSDictionary)["id"] ?? "")"
                            
                            UserDefaults.standard.set("\((self.allCityArray[i] as! NSDictionary)["id"] ?? "")", forKey: "searchCityID")
                            UserDefaults.standard.set("\((self.allCityArray[i] as! NSDictionary)["cityname"] ?? "")", forKey: "searchCityName")
                            
                            self.getDistrictBasedOnCity()
                            
                            self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
                            
                            UserDefaults.standard.set("", forKey: "searchDistrictID")
                            UserDefaults.standard.set("", forKey: "searchDistrictName")
                            
                            break
                            
                        }
                        else {
                            
                            self.city.text = globalFunctions.shared.getLanguageString(variable: "home_select_city")
                            self.cityID = ""
                            
                            UserDefaults.standard.set("", forKey: "searchCityID")
                            UserDefaults.standard.set("", forKey: "searchCityName")
                            
                            self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
                            
                            UserDefaults.standard.set("", forKey: "searchDistrictID")
                            UserDefaults.standard.set("", forKey: "searchDistrictName")
                            
                        }
                    }
                    
                }
                
            }
            
        }
        

        
        
        if self.allCityArray.count > 0 {
            
            for i in 0..<self.allCityArray.count {
                if "\((self.allCityArray[i] as! NSDictionary)["cityname"] ?? "")".elementsEqual(checkCity) {
                    
                    self.city.text = "\((self.allCityArray[i] as! NSDictionary)["cityname"] ?? "")"
                    self.cityID = "\((self.allCityArray[i] as! NSDictionary)["id"] ?? "")"
                    
                    UserDefaults.standard.set("\((self.allCityArray[i] as! NSDictionary)["id"] ?? "")", forKey: "searchCityID")
                    UserDefaults.standard.set("\((self.allCityArray[i] as! NSDictionary)["cityname"] ?? "")", forKey: "searchCityName")
                    
                    self.getDistrictBasedOnCity()
                    
                    self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
                    
                    UserDefaults.standard.set("", forKey: "searchDistrictID")
                    UserDefaults.standard.set("", forKey: "searchDistrictName")
                    
                    break
                    
                }
                else {
                    
                    self.city.text = globalFunctions.shared.getLanguageString(variable: "home_select_city")
                    self.cityID = ""
                    
                    UserDefaults.standard.set("", forKey: "searchCityID")
                    UserDefaults.standard.set("", forKey: "searchCityName")
                    
                    self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
                    
                    UserDefaults.standard.set("", forKey: "searchDistrictID")
                    UserDefaults.standard.set("", forKey: "searchDistrictName")
                    
                }
            }
            
        }
        
    }
    
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.rentButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "app_rent"), for: .normal)
        self.saleButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "home_sale"), for: .normal)
        self.searchButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "app_search"), for: .normal)
        self.languageStaticLBL.text = "\(globalFunctions.shared.getLanguageString(variable: "header_language"))" + ": "
        
        self.copyrightLine.text = "© 2020 \(globalFunctions.shared.getLanguageString(variable: "footer_text4"))"
    }
    
    
    // MARK:- // Set Font
    
//    func setFont() {
//
//        self.city.font = UIFont(name: self.city.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        self.district.font = UIFont(name: self.district.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        self.rentButtonOutlet.titleLabel!.font = UIFont(name: self.rentButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        self.saleButtonOutlet.titleLabel!.font = UIFont(name: self.saleButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        self.searchButtonOutlet.titleLabel!.font = UIFont(name: self.searchButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.languageStaticLBL.font = UIFont(name: self.languageStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        self.language.font = UIFont(name: self.language.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.copyrightLine.font = UIFont(name: self.language.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 11)))
//
//    }
    
    func setFont() {
        
        self.city.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.district.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.rentButtonOutlet.titleLabel!.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.saleButtonOutlet.titleLabel!.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.searchButtonOutlet.titleLabel!.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.languageStaticLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.language.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.copyrightLine.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 11)))
        
    }
    
    
    
    
    // MARK:- // JSON Post Method to get language strings
    
    func getLanguageStrings(langCode : String) {
        
        //let langID : String? = LocalizationSystem.sharedInstance.getLanguage()
        
        //UserDefaults.standard.set(langID, forKey: "DeviceLanguageID")
        
        //print("Device Language Code : ",langID!)
        
        var parameters : String!
        
        parameters = "language_code=\(langCode)"
        
        self.CallAPI(urlString: "languageConvert_test", param: parameters) {
            
//            for i in 0..<(self.globalJson["info"] as! NSArray).count {
//                self.langDataArray.append(languageDataForm(variables: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["Variables"] ?? "")", defaultText: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["Default Text"] ?? "")", languageText: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["Language Text"] ?? "")"))
//            }
            
            self.langDictionary = self.globalJson["info"] as! NSDictionary
            
            //languageData.shared.storeLanguageData(langData: self.langDataArray)
            languageData.shared.storeLanguageData(langData: self.langDictionary)
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
            }
            
        }
        
    }
    
    
    
    
    
    
    
    

}



// MARK:- // Pickerview Delegate Methods

extension AppStartSearchVC: UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == self.selectCityPickerview {
            return self.allCityArray.count
        }
        else if pickerView == self.selectLanguagePickerview {
            return languageArray.count
        }
        else {
            return self.districtArray.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == self.selectCityPickerview {
            return "\((self.allCityArray[row] as! NSDictionary)["cityname"] ?? "")"
        }
        else if pickerView == self.selectLanguagePickerview {
//            print((self.languageArray[row] as! NSDictionary))
            return "\((self.languageArray[row] as! NSDictionary)["lang_name"] ?? "")"
        }
        else {
            return "\((self.districtArray[row] as! NSDictionary)["districtname"] ?? "")"
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.selectCityPickerview {
            
            if self.doneClicked {

                self.city.text = "\((self.allCityArray[row] as! NSDictionary)["cityname"] ?? "")"
                self.cityID = "\((self.allCityArray[row] as! NSDictionary)["id"] ?? "")"
                
                self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
                
                UserDefaults.standard.set("\((self.allCityArray[row] as! NSDictionary)["id"] ?? "")", forKey: "searchCityID")
                UserDefaults.standard.set("\((self.allCityArray[row] as! NSDictionary)["cityname"] ?? "")", forKey: "searchCityName")
                
                UserDefaults.standard.set("\((self.allCityArray[row] as! NSDictionary)["province_name"] ?? "")", forKey: "searchProvinceName")
                
                UserDefaults.standard.set("\((self.allCityArray[row] as! NSDictionary)["province_id"] ?? "")", forKey: "searchProvinceID")
                
                
                UserDefaults.standard.set("", forKey: "searchDistrictID")
                UserDefaults.standard.set("", forKey: "searchDistrictName")
                
                
                self.getDistrictBasedOnCity()
                
                self.globalDispatchgroup.notify(queue: .main) {
                    
                    self.view.sendSubviewToBack(self.viewOfPicker)
                    self.viewOfPicker.isHidden = true
                    self.selectCityPickerview.isHidden = true
                    
                }
                
                self.doneClicked = false
                
            }
            else {
                self.selectedPickerIndex = row
            }
            
        }
        else if pickerView == self.selectLanguagePickerview {
            
            if self.doneClicked {
                
                self.language.text = "\((self.languageArray[row] as! NSDictionary)["lang_name"] ?? "")"
                
                UserDefaults.standard.set("\((self.languageArray[row] as! NSDictionary)["language_code"] ?? "")", forKey: "searchLanguageCode")
                UserDefaults.standard.set("\((self.languageArray[row] as! NSDictionary)["lang_name"] ?? "")", forKey: "searchLanguageName")
                
                self.view.sendSubviewToBack(self.viewOfPicker)
                self.viewOfPicker.isHidden = true
                self.selectLanguagePickerview.isHidden = true
                
                //print("\((self.languageArray[row] as! NSDictionary)["language_code"]!)".lowercased())
                
                LocalizationSystem.sharedInstance.setLanguage(languageCode: "\((self.languageArray[row] as! NSDictionary)["language_code"]!)")
                
                //            self.getLanguageStrings(langCode: "\((self.languageArray[row] as! NSDictionary)["language_code"]!)")
                
                
                
                //            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "SplashscreenVC") as! SplashscreenVC
                //
                //            self.navigationController?.pushViewController(navigate, animated: true)
                
            }
            else {
                self.selectedPickerIndex = row
            }
             
        }
        else {
            
            if self.doneClicked {
                
                self.district.text = "\((self.districtArray[row] as! NSDictionary)["districtname"] ?? "")"
                
                UserDefaults.standard.set("\((self.districtArray[row] as! NSDictionary)["id"] ?? "")", forKey: "searchDistrictID")
                UserDefaults.standard.set("\((self.districtArray[row] as! NSDictionary)["districtname"] ?? "")", forKey: "searchDistrictName")
                
                self.view.sendSubviewToBack(self.viewOfPicker)
                self.viewOfPicker.isHidden = true
                self.selectDistrictPickerview.isHidden = true
                
            }
            else {
                self.selectedPickerIndex = row
            }
            
        }
        
    }
    
}



// MARK:- // Location Manager Delegate Method

extension AppStartSearchVC: CLLocationManagerDelegate {
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations.last
        Latitude = userLocation!.coordinate.latitude
        Longitude = userLocation!.coordinate.longitude
        locationManager.stopUpdatingLocation()
        
        self.getAddressFromLatLon(pdblLatitude: "\(self.Latitude!)", withLongitude: "\(self.Longitude!)")
    }
    
}
