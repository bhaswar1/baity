//
//  InboxVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 12/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView

class InboxVC: GlobalViewController {
    
    
    @IBOutlet weak var inboxListingTable: UITableView!

 
    var startValue = 0
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var recordsArray = [singleMessageDataFormat]()
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(false, forKey: "isClickOnPush")
        self.headerView.headerBackButton.addTarget(self, action: #selector(headerBackButton), for: .touchUpInside)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadInboxData(notification:)), name: Notification.Name("ReloadData"), object: nil)
        self.view.isUserInteractionEnabled = false
        
    }
    
    // MARK:- // View Will Appear
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.inboxListingTable.isHidden = true
       
        self.inboxListingTable.estimatedRowHeight = 100
        self.inboxListingTable.rowHeight = UITableView.automaticDimension
        
        globalFunctions.shared.loadPlaceholder(onView: self.view)
        
        self.recordsArray.removeAll()
        
        self.startValue = 0
        
        self.getInboxListings()
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            globalFunctions.shared.removePlaceholder(fromView: self.view)
            
            self.view.isUserInteractionEnabled = true
            
            self.inboxListingTable.isHidden = false
            
        }
        
    }
    
    @objc func reloadInboxData(notification: Notification) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("ReloadData"), object: nil)
         self.inboxListingTable.isHidden = true
        
         self.inboxListingTable.estimatedRowHeight = 100
         self.inboxListingTable.rowHeight = UITableView.automaticDimension
         
         globalFunctions.shared.loadPlaceholder(onView: self.view)
         
         self.recordsArray.removeAll()
         
         self.startValue = 0
         
         self.getInboxListings()
         
         self.globalDispatchgroup.notify(queue: .main) {
             
             globalFunctions.shared.removePlaceholder(fromView: self.view)
             
             self.view.isUserInteractionEnabled = true
             
             self.inboxListingTable.isHidden = false
             
         }
    }
    
    @objc override func headerBackButton() {
        if self.navigationController?.viewControllers.count == 2{
            let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "AppStartSearchVC") as! AppStartSearchVC
            self.navigationController?.pushViewController(nav, animated: false)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    
    
    // MARK:- // JSON Post Method to get Inbox Listings
    
    func getInboxListings() {
        
        var parameters : String!
        
        parameters = "dalaleen_user_id=\(UserDefaults.standard.string(forKey: "userId") ?? "")&start_value=\(self.startValue)&per_load=\(self.perLoad)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "message_inbox", param: parameters) {
            
            let listingArray = self.globalJson["info"] as? NSArray
            
            for i in 0..<listingArray!.count {
                
                let tempDict = listingArray![i] as! NSDictionary
                
                self.recordsArray.append(singleMessageDataFormat(id: "\(tempDict["id"] ?? "")", name: "\(tempDict["name"] ?? "")", email: "\(tempDict["email"] ?? "")", date: "\(tempDict["date"] ?? "")", time: "\(tempDict["time"] ?? "")", readStatus: "\(tempDict["read_status"] ?? "")", subject: "\(tempDict["subject"] ?? "")", message: "\(tempDict["message"] ?? "")", realEstateNumber: "\(tempDict["realstate_number"] ?? "")"))
                
            }
            
            self.nextStart = "\(self.globalJson["next_start"]!)"
            
            self.startValue = self.startValue + listingArray!.count
            
            print("Next Start Value : " , self.startValue)
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.inboxListingTable.delegate = self
                self.inboxListingTable.dataSource = self
                self.inboxListingTable.reloadData()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
            }
            
        }
    }
    
    
    // MARK:- // View Message Button Action
    
    @objc func viewMessage(sender: UIButton) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "SingleNotificationVC") as! SingleNotificationVC
        
        navigate.Message = self.recordsArray[sender.tag]
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
}


// MARK:- // Scrollview Delegate Methods

extension InboxVC: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {
            
        }
        else
        {
            print("next start : ",nextStart ?? "")
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    //SVProgressHUD.dismiss()
                }
            }
            else
            {
                self.getInboxListings()
            }
        }
    }
}



// MARK:- // Tableview Delegates

extension InboxVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recordsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "inboxListingTVC") as! inboxListingTVC
        
        if "\(self.recordsArray[indexPath.row].readStatus ?? "")".elementsEqual("0") {
            cell.readUnreadImageview.image = UIImage(named: "close-envelope")
        }
        else {
            cell.readUnreadImageview.image = UIImage(named: "email")
        }
        
        cell.messageSubject.text = "\(self.recordsArray[indexPath.row].subject ?? "")"
        cell.messageDate.text = "\(self.recordsArray[indexPath.row].date ?? "")"
        cell.messageTime.text = "\(self.recordsArray[indexPath.row].time ?? "")"
        cell.senderName.text = globalFunctions.shared.getLanguageString(variable: "inbox_name") + ":  \(self.recordsArray[indexPath.row].name ?? "")"
        cell.senderEmail.text = globalFunctions.shared.getLanguageString(variable: "my_profile_email") + ":  \(self.recordsArray[indexPath.row].email ?? "")"
        //cell.message.text = globalFunctions.shared.getLanguageString(variable: "property_details_msg") + ":  \((self.recordsArray[indexPath.row].message ?? "")!.htmlToAttributedString!)"
        let temp = (self.recordsArray[indexPath.row].message ?? "").htmlToAttributedString
        let temp1 = temp?.string
        cell.message.text = globalFunctions.shared.getLanguageString(variable: "property_details_msg") + ":  \(temp1 ?? "")"
        
        cell.viewMessageButton.setTitle(globalFunctions.shared.getLanguageString(variable: "inbox_view_msg"), for: .normal)
        
        cell.viewMessageButton.tag = indexPath.row
        cell.viewMessageButton.addTarget(self, action: #selector(self.viewMessage(sender:)), for: .touchUpInside)
        
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        //Set FOnt
//        cell.messageSubject.font = UIFont(name: cell.messageSubject.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
//        cell.messageDate.font = UIFont(name: cell.messageDate.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        cell.messageTime.font = UIFont(name: cell.messageTime.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        cell.senderName.font = UIFont(name: cell.senderName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
//        cell.senderEmail.font = UIFont(name: cell.senderEmail.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
//        cell.message.font = UIFont(name: cell.message.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
//        cell.viewMessageButton.titleLabel!.font = UIFont(name: cell.viewMessageButton.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
        cell.messageSubject.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        cell.messageDate.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        cell.messageTime.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        cell.senderName.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        cell.senderEmail.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        cell.message.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        cell.viewMessageButton.titleLabel!.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
        
        return cell
        
    }
     
}



