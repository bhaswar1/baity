//
//  RecyclebinVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 24/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView

class RecyclebinVC: GlobalViewController {

    
    @IBOutlet weak var recyclebinTable: UITableView!
    @IBOutlet weak var bottomView: UIView!
    
    
    var startValue = 0
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var recordsArray = [singleMessageDataFormat]()
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.SetFont()
        
        self.localizeStrings()
        
        globalFunctions.shared.loadPlaceholder(onView: self.view)

        self.getRecyclebinMessages()
        
        self.recyclebinTable.estimatedRowHeight = 400
        self.recyclebinTable.rowHeight = UITableView.automaticDimension
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            globalFunctions.shared.removePlaceholder(fromView: self.view)
            
                self.bottomView.isHidden = false
            
        }
        
    }
    
    
    
    
    // MARK:- // Buttons
    
    // MARK:- // Recover All Button
    
    @IBOutlet weak var recoverAllButtonOutlet: UIButton!
    @IBAction func recoverAllButton(_ sender: UIButton) {

        
        self.customAlert(title: globalFunctions.shared.getLanguageString(variable: "ios_are_you_sure"), message: globalFunctions.shared.getLanguageString(variable: "recycle_modal_recover_all_msg"), doesCancel: true) {
            
            self.recoverAllMessages()
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                
                self.navigationController?.pushViewController(navigate, animated: true)
                
            })
            
        }
        
    }
    
    
    // MARK:- // Delete All Button
    
    @IBOutlet weak var deleteAllButtonOutlet: UIButton!
    @IBAction func deleteAllButton(_ sender: UIButton) {
        
        self.customAlert(title: globalFunctions.shared.getLanguageString(variable: "ios_are_you_sure"), message: globalFunctions.shared.getLanguageString(variable: "recycle_modal_delete_all_msg"), doesCancel: true) {
            
            self.deleteAllMessages()
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                
                self.navigationController?.pushViewController(navigate, animated: true)
                
            })
            
        }
        
    }
    
    
    // MARK:- // View Message Button Action
    
    @objc func viewMessage(sender: UIButton) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "SingleNotificationVC") as! SingleNotificationVC
        
        navigate.Message = self.recordsArray[sender.tag]
        navigate.fromRecycleBin = true
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
    
    
    // MARK:- // Delete Message Button Action
    
    @objc func deleteMessage(sender: UIButton) {
        
        self.customAlert(title: globalFunctions.shared.getLanguageString(variable: "ios_are_you_sure"), message: globalFunctions.shared.getLanguageString(variable: "inbox_modal_text") + " ?", doesCancel: true) {
            
            self.deleteFromRecyclebin(id: "\(self.recordsArray[sender.tag].id ?? "")", completion: {
                
                if self.recordsArray.count > 1 {
                    self.recordsArray.removeAll()
                    
                    self.startValue = 0
                    
                    self.getRecyclebinMessages()
                }
                else {
                    
                    self.navigationController?.popViewController(animated: true)
                }
                
            })
            
        }
        
    }
    
    // MARK:- // Restore Message Button Action
    
    @objc func restoreMessage(sender: UIButton) {
        
        self.customAlert(title: globalFunctions.shared.getLanguageString(variable: "ios_are_you_sure"), message: globalFunctions.shared.getLanguageString(variable: "ios_recyclebin_text1"), doesCancel: true) {
            
            self.restoreMessage(id: "\(self.recordsArray[sender.tag].id ?? "")", completion: {
                
                if self.recordsArray.count > 1 {
                    self.recordsArray.removeAll()
                    
                    self.startValue = 0
                    
                    self.getRecyclebinMessages()
                }
                else {
                    
                    self.navigationController?.popViewController(animated: true)
                }
                
            })
            
        }
        
    }
    
    
    
    
    
    // MARK:- // JSON Post Method to get all Recyclebin Messages
    
    func getRecyclebinMessages() {
       
        var parameters : String!
        
        parameters = "dalaleen_user_id=\(userInformation.shared.userID ?? "")&start_value=\(startValue)&per_load=\(perLoad)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "message_recyclebin", param: parameters) {
            
            let listingArray = self.globalJson["info"] as? NSArray
            
            for i in 0..<listingArray!.count {
                
                let tempDict = listingArray![i] as! NSDictionary
                
                self.recordsArray.append(singleMessageDataFormat(id: "\(tempDict["id"] ?? "")", name: "\(tempDict["name"] ?? "")", email: "\(tempDict["email"] ?? "")", date: "\(tempDict["date"] ?? "")", time: "\(tempDict["time"] ?? "")", readStatus: "\(tempDict["read_status"] ?? "")", subject: "\(tempDict["subject"] ?? "")", message: "\(tempDict["message"] ?? "")", realEstateNumber: "\(tempDict["realstate_number"] ?? "")"))
                
            }
            
            self.nextStart = "\(self.globalJson["next_start"]!)"
            
            self.startValue = self.startValue + listingArray!.count
            
            print("Next Start Value : " , self.startValue)
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.recyclebinTable.delegate = self
                self.recyclebinTable.dataSource = self
                self.recyclebinTable.reloadData()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
            }
            
        }
    }

    
    
    
    // MARK:- // JSON Post Method to Delete Single Messages from Recycle Bin
    
    func deleteFromRecyclebin(id : String! , completion : @escaping ()->()) {
        
        var parameters : String!
        
        parameters = "message_id=\(id ?? "")&dalaleen_user_id=\(userInformation.shared.userID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "delete_message_recyclebin", param: parameters) {
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                self.customAlert(title: "", message: "\(self.globalJson["message"] ?? "")", doesCancel: false, completion: {
                    
                    completion()
                    
                })
                
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    // MARK:- // JSON Post Method to Recover Single Messages from Recycle Bin
    
    func restoreMessage(id : String! , completion : @escaping ()->()) {
        
        var parameters : String!
        
        parameters = "message_id=\(id ?? "")&dalaleen_user_id=\(userInformation.shared.userID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "recover_message_recycle", param: parameters) {
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                self.customAlert(title: "", message: "\(self.globalJson["message"] ?? "")", doesCancel: false, completion: {
                    
                    completion()
                    
                })
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    
    
    // MARK:- // JSON Post Method to recover all Recycle Bin Messages
    
    func recoverAllMessages() {
        
        var parameters : String!
        
        parameters = "dalaleen_user_id=\(userInformation.shared.userID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "recover_all_message_recycle", param: parameters) {
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    
    
    // MARK:- // JSON Post Method to Delete all Recycle Bin Messages
    
    func deleteAllMessages() {
        
        var parameters : String!
        
        parameters = "dalaleen_user_id=\(userInformation.shared.userID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "delete_all_message_recycle", param: parameters) {
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
            }
            
        }
        
    }
    
    
    // MARK:- // Set Font
    
    func SetFont() {
        
        self.deleteAllButtonOutlet.titleLabel!.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.recoverAllButtonOutlet.titleLabel!.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
    }
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.deleteAllButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "recycle_bin_delete_all"), for: .normal)
        self.recoverAllButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "recycle_bin_recover_all"), for: .normal)
        
    }
    

}





// MARK:- // Tableview Delegates

extension RecyclebinVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recordsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecyclebinTVC") as! RecyclebinTVC
        
        cell.messageSubject.text = "\(self.recordsArray[indexPath.row].subject ?? "")"
        cell.messageDate.text = "\(self.recordsArray[indexPath.row].date ?? "")"
        cell.messageTime.text = "\(self.recordsArray[indexPath.row].time ?? "")"
        cell.senderName.text = globalFunctions.shared.getLanguageString(variable: "inbox_name") + ":  \(self.recordsArray[indexPath.row].name ?? "")"
        cell.senderEmail.text = globalFunctions.shared.getLanguageString(variable: "my_profile_email") + ":  \(self.recordsArray[indexPath.row].email ?? "")"
        let temp = (self.recordsArray[indexPath.row].message ?? "").htmlToAttributedString
        let temp1 = temp?.string
        cell.message.text = globalFunctions.shared.getLanguageString(variable: "property_details_msg") + ":  \(temp1 ?? "")"
        
        
        cell.deleteMessageButton.tag = indexPath.row
        cell.deleteMessageButton.addTarget(self, action: #selector(self.deleteMessage(sender:)), for: .touchUpInside)
        
        cell.restoreMessageButton.tag = indexPath.row
        cell.restoreMessageButton.addTarget(self, action: #selector(self.restoreMessage(sender:)), for: .touchUpInside)
        
        cell.viewMessageButton.tag = indexPath.row
        cell.viewMessageButton.addTarget(self, action: #selector(self.viewMessage(sender:)), for: .touchUpInside)
        
        cell.viewMessageButton.setTitle(globalFunctions.shared.getLanguageString(variable: "inbox_view_msg"), for: .normal)
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        //Set FOnt
        cell.messageSubject.font = UIFont(name: cell.messageSubject.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        cell.messageDate.font = UIFont(name: cell.messageDate.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        cell.messageTime.font = UIFont(name: cell.messageTime.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        cell.senderName.font = UIFont(name: cell.senderName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        cell.senderEmail.font = UIFont(name: cell.senderEmail.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        cell.message.font = UIFont(name: cell.message.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        cell.viewMessageButton.titleLabel!.font = UIFont(name: cell.viewMessageButton.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
        
        return cell
        
    }
    
}



// MARK:- // Scrollview Delegate Methods

extension RecyclebinVC: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {
            
        }
        else
        {
            print("next start : ",nextStart ?? "")
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    //SVProgressHUD.dismiss()
                }
            }
            else
            {
                self.getRecyclebinMessages()
            }
        }
    }
}
