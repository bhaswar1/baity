//
//  SignUpVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 05/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView
import FirebaseUI

class SignUpVC: GlobalViewController {
    
    
    @IBOutlet weak var screenBackgroundImageview: UIImageView!
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var baityLogoImageview: UIImageView!
    @IBOutlet weak var countryImageview: UIImageView!
    @IBOutlet weak var whiteBackgroundImageview: UIImageView!
    @IBOutlet weak var avatarLogo: UIImageView!
    @IBOutlet weak var infoContentView: UIView!
    @IBOutlet weak var signUpHeaderLBL: UILabel!
    @IBOutlet weak var userTypeView: UIView!
    @IBOutlet weak var customerLBL: UILabel!
    @IBOutlet weak var realEstateAgentLBL: UILabel!
    @IBOutlet weak var selectCountryTitleLBL: UILabel!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var selectedCountryImage: UIImageView!
    @IBOutlet weak var dropDownImageview: UIImageView!
    @IBOutlet weak var mobileNoView: UIView!
    @IBOutlet weak var countryCode: UITextField!
    @IBOutlet weak var mobileNoTXT: UITextField!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameTXT: UITextField!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTXT: UITextField!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordTXT: UITextField!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var confirmPasswordTXT: UITextField!
    @IBOutlet weak var showPasswordLBL: UILabel!
    @IBOutlet weak var termsAndConditionsLBLLBL: UILabel!
    @IBOutlet weak var alreadyHaveAnAccountLBL: UILabel!
    @IBOutlet weak var countryListView: UIView!
    @IBOutlet weak var chooseCountryTitleLBL: UILabel!
    @IBOutlet weak var countryListTable: UITableView!
    
    var CountryCodeArray = countryPhoneCodes.shared.countryPhnCodes
     var verificationID : String!
    var userType : String! = ""
    
    
    var signUpResponseDictionary : NSDictionary!
    
    var phoneCode : String! = ""
    
    var phoneNumber : String! = ""
    
    // MARK:- // View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.countryView.layer.borderWidth = 0.5
        self.countryView.layer.borderColor = UIColor.lightGray.cgColor
        self.countryView.layer.cornerRadius = 5
        
        self.SetFont()
        
        self.localizeStrings()

        
        self.countryListTable.delegate = self
        self.countryListTable.dataSource = self
        self.countryListTable.reloadData()
        
        self.mobileNoTXT.delegate = self
        
        
    }
    
    
    
    // MARK:- // Buttons
    
    
    // MARK:- // Terms and Conditions Button
    
    @IBOutlet weak var termsAndConditionsButton: UIButton!
    @IBAction func termsAndCondition(_ sender: UIButton) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "WebkitVC") as! WebkitVC
        
        navigate.staticParam = "terms_condition"
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
    
    // MARK:- // Country Dropdown Button
    
    @IBOutlet weak var countryDropdownButtonOutlet: UIButton!
    @IBAction func countryDropdownButton(_ sender: UIButton) {
        
        if self.countryListView.isHidden == true {
            self.view.bringSubviewToFront(self.countryListView)
            self.countryListView.isHidden = false
        }
        else {
            self.view.sendSubviewToBack(self.countryListView)
            self.countryListView.isHidden = true
        }
        
    }
    
    
    // MARK:- // User Type BUttons
    
    
    @IBOutlet weak var customerButtonOutlet: UIButton!
    @IBOutlet weak var realEstateButtonOutlet: UIButton!
    @IBAction func userTypeButtons(_ sender: UIButton) {
        
        if sender.tag == 0 {
            
            if sender.isSelected == false {
                self.realEstateButtonOutlet.isSelected = false
                self.buttonTransformIdentity(button: self.customerButtonOutlet)
                
                self.userType = "U"
                
                self.emailTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "registration_customer_email")
            }
            
        }
        else {
            
            if sender.isSelected == false {
                self.customerButtonOutlet.isSelected = false
                self.buttonTransformIdentity(button: self.realEstateButtonOutlet)
                
                self.userType = "A"
                
                self.emailTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "registration_enter_your_email")
            }
            
        }
        
    }
    
    
    
    
    
    // MARK:- // Show Password Button
    
    @IBOutlet weak var showPasswordButtonOutlet: UIButton!
    @IBAction func showPasswordButton(_ sender: UIButton) {
        
        self.buttonTransformIdentity(button: self.showPasswordButtonOutlet)
        
        if sender.isSelected == true {
            self.passwordTXT.isSecureTextEntry = true
        }
        else {
            self.passwordTXT.isSecureTextEntry = false
        }
    }
    
    
    
    // MARK:- // Terms and Conditions Button
    
    @IBOutlet weak var termsAndConditionsButtonOutlet: UIButton!
    @IBAction func termsAndConditionsButton(_ sender: UIButton) {
        
        self.buttonTransformIdentity(button: self.termsAndConditionsButtonOutlet)
        
    }
    
    // MARK:- // Submit Button
    
    @IBOutlet weak var submitButtonOutlet: UIButton!
    @IBAction func submitButton(_ sender: UIButton) {
        
        self.validation {
            
//            self.signUp {
//
//                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "AccountActivationVC") as! AccountActivationVC
//
//              navigate.phoneCode = self.phoneCode
//                navigate.phoneNumber = self.mobileNoTXT.text ?? ""
//
//                self.navigationController?.pushViewController(navigate, animated: true)
//
//                print("Do nothing")
//
//            }
            
            if (self.mobileNoTXT.text?.replacingOccurrences(of: " ", with: "").count)! > 10 {
                
                if (self.mobileNoTXT.text?.prefix(1).elementsEqual("0"))! {
                    
                    var tempString = self.mobileNoTXT.text!
                    
                    tempString.remove(at: tempString.startIndex)
                    
                    self.phoneNumber = tempString
                    
                }
                else {
                    self.phoneNumber = self.mobileNoTXT.text
                }
            }
            else {
                self.phoneNumber = self.mobileNoTXT.text
            }
            
            self.checkNumberExisis {
                
                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "AccountActivationVC") as! AccountActivationVC
                navigate.SignUpName = "\(self.nameTXT.text ?? "")"
                navigate.SignUpPhone = "\(self.phoneNumber ?? "")"
                navigate.SignUpEmail =  "\(self.emailTXT.text ?? "")"
                navigate.SignUpPassword = "\(self.passwordTXT.text ?? "")"
                navigate.SignUpPhoneCode = "\(self.countryCode.text ?? "")"
                navigate.SignUpType = "\(self.userType ?? "")"
                navigate.verificationID = "\(self.verificationID ?? "")"
                navigate.SignUpCountryName = "\(self.countryName.text ?? "")"
                self.navigationController?.pushViewController(navigate, animated: true)
                
            }
        }
    }
    
    // MARK:- // JSON Post Method to Send Sign Up Date
    
    func signUp(completion : @escaping ()->()) {
        
        var parameters : String = ""
        
        parameters = "phone=\(self.phoneNumber ?? "")&name=\(nameTXT.text ?? "")&email=\(emailTXT.text ?? "")&phone_code=\(countryCode.text ?? "")&password=\(passwordTXT.text ?? "")&type=\(self.userType ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "signup", param: parameters) {
            
            self.signUpResponseDictionary = self.globalJson
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                completion()
                
                self.customAlert(title: "", message: "\(self.globalJson["message"] ?? "")", doesCancel: false, completion: {
                    
                    let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                    
                    self.navigationController?.pushViewController(navigate, animated: true)
                    
                })
                
            }
            
        }
        
    }
    
    
    // MARK:- // JSON Post Method to Check if the No Exists
    
    func checkNumberExisis(completion : @escaping ()->()) {
        
        var parameters : String = ""
        
        parameters = "phone_code=\(countryCode.text ?? "")&phone=\(self.phoneNumber ?? "")&email=\(self.emailTXT.text ?? "")&user_type=\(self.userType ?? "")"
        
        self.CallAPI(urlString: "Phone_no_check", param: parameters) {
            
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                completion()
                
            }
            
        }
        
    }
    
    
    
    
    // MARK:- // Validation
    
    func validation(completion : () -> ()) {
        
        if self.userType.elementsEqual("") {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "app_invld_usrtyp"))
        }
        else if (self.countryName.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "please_select_country"))
        }
        else if (self.mobileNoTXT.text?.replacingOccurrences(of: " ", with: "").elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "registration_enter_mobile_number"))
        }
        else if (self.nameTXT.text?.replacingOccurrences(of: " ", with: "").elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "registration_enter_your_name"))
        }
        else if (self.passwordTXT.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "registration_enter_your_password"))
        }
        else if globalFunctions.shared.validatePassword(enteredPassword: self.passwordTXT.text!) == false {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "plese_enter_atlst"))
        }
        else if (self.confirmPasswordTXT.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "resetpass_confirm_pass_placeholder"))
        }
//        else if globalFunctions.shared.validatePassword(enteredPassword: self.confirmPasswordTXT.text!) == false {
//            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "plese_enter_atlst"))
//        }
        else if ("\(self.confirmPasswordTXT.text ?? "")".elementsEqual("\(self.passwordTXT.text ?? "")")) == false {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "android_nwpswrd_sm_cnfrm"))
        }
        else if self.termsAndConditionsButtonOutlet.isSelected == false {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "plse_select_terms_cndtion_to_cntnu"))
        }
        else {
            
            if self.userType.elementsEqual("U") {
                
                if "\(self.emailTXT.text ?? "")".elementsEqual("") == false {
                    
                    if globalFunctions.shared.validateEmail(enteredEmail: self.emailTXT.text!) == false {
                        self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "valid_email"))
                    }
                    else {
                        completion()
                    }
                    
                }
                else {
                    completion()
                }
            }
            else {
                
                if (self.emailTXT.text?.elementsEqual(""))! {
                    self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "registration_enter_your_email"))
                }
                else if globalFunctions.shared.validateEmail(enteredEmail: self.emailTXT.text!) == false {
                    self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "valid_email"))
                }
                else {
                    completion()
                }
            }
        }
    }
    
    
    
    
    // MARK:- // Already have an Account Button
    
    @IBOutlet weak var alreadyHaveAnAccountButtonOutlet: UIButton!
    @IBAction func alreadyHaveAnAccount(_ sender: UIButton) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK:- // Close Country List VIew Button
    
    @IBOutlet weak var closeCountryListViewButtonOutlet: UIButton!
    @IBAction func closeCountryListViewButton(_ sender: UIButton) {
        
        self.view.sendSubviewToBack(self.countryListView)
        self.countryListView.isHidden = true
        
    }
    
    
    
    
    
   
    
    
    
    
    
    
    // MARK:- // Set Font
    
//    func SetFont() {
//        self.signUpHeaderLBL.font = UIFont(name: self.signUpHeaderLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
//        self.selectCountryTitleLBL.font = UIFont(name: self.selectCountryTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
//        self.countryName.font = UIFont(name: self.countryName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.countryCode.font = UIFont(name: self.countryCode.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.mobileNoTXT.font = UIFont(name: self.mobileNoTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.nameTXT.font = UIFont(name: self.nameTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.emailTXT.font = UIFont(name: self.emailTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.passwordTXT.font = UIFont(name: self.passwordTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.confirmPasswordTXT.font = UIFont(name: self.confirmPasswordTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.showPasswordLBL.font = UIFont(name: self.showPasswordLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        self.termsAndConditionsLBLLBL.font = UIFont(name: self.termsAndConditionsLBLLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        self.submitButtonOutlet.titleLabel?.font = UIFont(name: self.submitButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        self.alreadyHaveAnAccountLBL.font = UIFont(name: self.alreadyHaveAnAccountLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//    }
    
    
    func SetFont() {
        self.signUpHeaderLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        self.selectCountryTitleLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.countryName.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.countryCode.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.mobileNoTXT.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.nameTXT.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.emailTXT.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.passwordTXT.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.confirmPasswordTXT.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.showPasswordLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.termsAndConditionsLBLLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.submitButtonOutlet.titleLabel?.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.alreadyHaveAnAccountLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
    }
    
    // MARK:- // Locatize Strings
    
    func localizeStrings() {
        
        self.signUpHeaderLBL.text = globalFunctions.shared.getLanguageString(variable: "header_sign_up")
        self.customerLBL.text = globalFunctions.shared.getLanguageString(variable: "registration_customer")
        self.realEstateAgentLBL.text = globalFunctions.shared.getLanguageString(variable: "registration_real_estate_agent")
        self.selectCountryTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_select_country")
        self.mobileNoTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "registration_enter_mobile_number")
        self.nameTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "registration_enter_your_name")
        self.emailTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "registration_enter_your_email")
        self.passwordTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "registration_enter_your_password")
        self.confirmPasswordTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "registration_confirm_password")
        self.showPasswordLBL.text = globalFunctions.shared.getLanguageString(variable: "registration_show_password")
        self.submitButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "resetpass_submit_msg"), for: .normal) 
        
        self.termsAndConditionsLBLLBL.halfTextColorChange(fullText: globalFunctions.shared.getLanguageString(variable: "registration_check_text1") + " " + globalFunctions.shared.getLanguageString(variable: "registration_check_text2"), changeText: globalFunctions.shared.getLanguageString(variable: "registration_check_text2"), color: .blue)
        
        self.alreadyHaveAnAccountLBL.halfTextColorChange(fullText: globalFunctions.shared.getLanguageString(variable: "registration_text3") + "? " + globalFunctions.shared.getLanguageString(variable: "header_sign_in") + "?", changeText: globalFunctions.shared.getLanguageString(variable: "header_sign_in") + "?", color: .black)
        
    }
    
}




// MARK:- // Tableview Delegate Functions

extension SignUpVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return CountryCodeArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "countryAndPhoneCodeTVC") as! countryAndPhoneCodeTVC
        
        let mydict = CountryCodeArray[indexPath.row]
        
        cell.countryName.text = "\(mydict.countryName ?? "")"
        cell.countryPhoneCode.text = "+ " + "\(mydict.phoneCode ?? "")"
        
        cell.countryImage.sd_setImage(with: URL(string: "\(mydict.countryFlag ?? "")"))
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        // Set Font
//        cell.countryName.font = UIFont(name: cell.countryName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        cell.countryPhoneCode.font = UIFont(name: cell.countryPhoneCode.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
        cell.countryName.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        cell.countryPhoneCode.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let mydict = CountryCodeArray[indexPath.row]
        
        self.countryName.text = "\(mydict.countryName ?? "")"
        self.countryCode.text = "+ " + "\(mydict.phoneCode ?? "")"
        self.selectedCountryImage.sd_setImage(with: URL(string: "\(mydict.countryFlag ?? "")"))
        UserDefaults.standard.set("\(mydict.countryFlag ?? "")", forKey: "userCountryFlag")
        
        self.phoneCode = "\(mydict.phoneCode ?? "")"
        
        self.view.sendSubviewToBack(self.countryListView)
        self.countryListView.isHidden = true
        
    }
    
}



// MARK:- // Textfield Delegate Methods

extension SignUpVC : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//        let authUI = FUIAuth.defaultAuthUI()!
//
//        // You need to adopt a FUIAuthDelegate protocol to receive callback
//        authUI.delegate = self
//
//        let providers: [FUIAuthProvider] = [
//            FUIPhoneAuth(authUI:FUIAuth.defaultAuthUI()!)
//        ]
//        authUI.providers = providers
//
//        let authViewController = authUI.authViewController()
//        //let phoneProvider = FUIAuth.defaultAuthUI()!.providers.first as! FUIPhoneAuth
//        present(authViewController, animated: true, completion: nil)
        
    }
    
}


// MARK:- // Firebase UI Delegate Methods

extension SignUpVC : FUIAuthDelegate {
    
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        
        // CHeck if there was an error
        guard error == nil else {
            // Login Error
            return
        }
        
        print("USERID ------\(authDataResult?.user.uid ?? "")")
        print("User Info -----\(authDataResult?.user.phoneNumber ?? "")")
        
        let text = "\(authDataResult?.user.phoneNumber ?? "")"
        let textone = text.dropFirst()
        let texttwo = textone.dropFirst()
        let textthree = texttwo.dropFirst()
        
        print("TextThree",textthree)
        
        self.mobileNoTXT.text = "\(textthree)"
        
       
        
    }
    
}
