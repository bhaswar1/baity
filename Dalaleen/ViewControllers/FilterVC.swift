//
//  FilterVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 27/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView

class FilterVC: GlobalViewController {
    
    @IBOutlet weak var realEstateTypeTable: UITableView!
    @IBOutlet weak var realEstateTypeTableHeightCOnstraint: NSLayoutConstraint!
    
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var offerForStackview: UIStackView!
    @IBOutlet weak var offerForTitleLBL: UILabel!
    @IBOutlet weak var offerForButtonsView: UIView!
    @IBOutlet weak var rentImageview: UIImageView!
    @IBOutlet weak var rentLBL: UILabel!
    @IBOutlet weak var saleImageview: UIImageView!
    @IBOutlet weak var saleLBL: UILabel!
    @IBOutlet weak var peroidView: borderView!
    @IBOutlet weak var period: UILabel!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var addressTitleLBL: UILabel!
    @IBOutlet weak var provinceView: borderView!
    @IBOutlet weak var province: UILabel!
    @IBOutlet weak var cityView: borderView!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var districtView: borderView!
    @IBOutlet weak var district: UILabel!
    
    
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceTitleLBL: UILabel!
    @IBOutlet weak var currencyLBL: UILabel!
    @IBOutlet weak var toStaticLBLCurrency: UILabel!
    @IBOutlet weak var fromCurrencyTXT: UITextField!
    @IBOutlet weak var toCurrencyTXT: UITextField!
    @IBOutlet weak var idLBL: UILabel!
    @IBOutlet weak var toStaticLBLID: UILabel!
    @IBOutlet weak var fromIDTXT: UITextField!
    @IBOutlet weak var toIDTXT: UITextField!
    @IBOutlet weak var areaTitleLBL: UILabel!
    @IBOutlet weak var meterSquareStaticLBL: UILabel!
    @IBOutlet weak var toStaticLBLMeter: UILabel!
    @IBOutlet weak var fromMeterSquareTXT: UITextField!
    @IBOutlet weak var toMeterSquareTXT: UITextField!
    
    
    @IBOutlet weak var generalInfoView: UIView!
    @IBOutlet weak var generalInfoTitleLBL: UILabel!
    @IBOutlet weak var addedDateTitleLBL: UILabel!
    @IBOutlet weak var addedDateCollectionview: UICollectionView!
    @IBOutlet weak var addedByTitleLBL: UILabel!
    @IBOutlet weak var addedByCollectionview: UICollectionView!
    @IBOutlet weak var negotiablePriceTitleLBL: UILabel!
    @IBOutlet weak var negotiablePriceColletionview: UICollectionView!
    
    
    @IBOutlet weak var addedDateCollectionviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var addedByCollectionviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var negotiablePriceCollectionviewHeightConstraint: NSLayoutConstraint!
    
    
    
    
    
    @IBOutlet weak var viewOfPickerview: UIView!
    @IBOutlet weak var periodPickerview: UIPickerView!
    @IBOutlet weak var provincePickerview: UIPickerView!
    @IBOutlet weak var cityPickerview: UIPickerView!
    @IBOutlet weak var districtPickerview: UIPickerView!
    
    @IBOutlet weak var dynamicStackView: UIStackView!
    @IBOutlet weak var dynamicInfoStackviewHeightConstraint: NSLayoutConstraint!
    
    
    var userRealEstateTypeArray = [filterDataFormat]()
    
    var filterDict : filterDictionary!
    
    var parentCategory : String! = ""
    
    var propertyType : String! = ""
    
    var periodArray = [["key" : "W" , "value" : "\(globalFunctions.shared.getLanguageString(variable: "home_week"))"] , ["key" : "M" , "value" : "\(globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_months"))"] , ["key" : "Y" , "value" : "\(globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_years"))"]]//[["key" : "W" , "value" : "Weeks"] , ["key" : "M" , "value" : "Months"] , ["key" : "Y" , "value" : "Years"]]
    
    var viewDaysArray = [StaticDictDataFormat(key: "", value: "\(globalFunctions.shared.getLanguageString(variable: "home_all"))", isSelected: false) , StaticDictDataFormat(key: "1", value: "\(globalFunctions.shared.getLanguageString(variable: "home_last_24_hours"))", isSelected: false) , StaticDictDataFormat(key: "3", value: "\(globalFunctions.shared.getLanguageString(variable: "home_In_the_last_3_days"))", isSelected: false) , StaticDictDataFormat(key: "7", value: "\(globalFunctions.shared.getLanguageString(variable: "home_In_the_last_7_days"))", isSelected: false) , StaticDictDataFormat(key: "15", value: "\(globalFunctions.shared.getLanguageString(variable: "home_In_the_last_15_days"))", isSelected: false) , StaticDictDataFormat(key: "30", value: "\(globalFunctions.shared.getLanguageString(variable: "home_In_the_last_30_days"))", isSelected: false)]
    
    var AddedByArray = [StaticDictDataFormat(key: "", value: "\(globalFunctions.shared.getLanguageString(variable: "home_all"))", isSelected: false) , StaticDictDataFormat(key: "U", value: "\(globalFunctions.shared.getLanguageString(variable: "home_owner"))", isSelected: false) , StaticDictDataFormat(key: "A", value: "\(globalFunctions.shared.getLanguageString(variable: "home_agent"))", isSelected: false)]
    
    
    var negotiablePriceArray = [StaticDictDataFormat(key: "", value: "\(globalFunctions.shared.getLanguageString(variable: "home_all"))", isSelected: false) , StaticDictDataFormat(key: "Y", value: "\(globalFunctions.shared.getLanguageString(variable: "home_yes"))", isSelected: false) , StaticDictDataFormat(key: "N", value: "\(globalFunctions.shared.getLanguageString(variable: "home_no"))", isSelected: false)]
    
    var searchCategory : String!
    
    var periodID : String!
    
    var provinceArray = [NSDictionary]()
    
    var cityArray = [NSDictionary]()
    
    var districtArray = [NSDictionary]()
    
    var provinceID : String! = ""
    var cityID  : String! = ""
    var districtID : String! = ""
    
    var minPrice : String! = ""
    var maxPrice : String! = ""
    var minPriceID : String! = ""
    var maxPriceID : String! = ""
    var minArea : String! = ""
    var maxArea : String! = ""
    var viewDays : String! = ""
    var addedBy : String! = ""
    var negotiablePrice : String! = ""
    
    var selectedPickerview : UIPickerView!
    var selectedPickerIndex : Int!
    var doneClicked : Bool!
    
    var dynamicInfoFeaturesDataArray = [dynamicFilterDictionary]()
    var dynamicInfoStatusDataArray = [dynamicFilterDictionary]()
    
    var minTXTArray = [UITextField]()
    var maxTXTArray = [UITextField]()
    
    
    
    // MARK:- // Create Picker Toolbar
    
    func createToolbar() {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.tintColor = UIColor.white//"Done" button colour
        toolBar.barTintColor = UIColor.black// bar background colour
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "\(globalFunctions.shared.getLanguageString(variable: "ios_ok"))", style: UIBarButtonItem.Style.done, target: self, action: #selector(donePicker))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "\(globalFunctions.shared.getLanguageString(variable: "modal_button_text1"))", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelPicker))
        
        toolBar.setItems([cancelButton, spacer, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.viewOfPickerview.addSubview(toolBar)
        toolBar.anchor(top: nil, leading: nil, bottom: self.provincePickerview.topAnchor, trailing: nil, size: .init(width: UIScreen.main.bounds.size.width, height: 40))
        
    }
    
    
    // MARK:- // Done Picker Action Method
    
    @objc func donePicker() {
        
        print("Done Clicked")
        
        self.doneClicked = true
        
        self.pickerView(self.selectedPickerview, didSelectRow: self.selectedPickerIndex, inComponent: 0)
        
        self.view.sendSubviewToBack(self.viewOfPickerview)
        self.viewOfPickerview.isHidden = true
        self.selectedPickerview.isHidden = true
        
    }
    
    // MARK:- // Cancel Picker Action Method
    
    @objc func cancelPicker() {
        print("Cancel Clicked")
        
        self.view.sendSubviewToBack(self.viewOfPickerview)
        self.viewOfPickerview.isHidden = true
        self.selectedPickerview.isHidden = true
    }
    
    
    
    
    
    
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if "\(UserDefaults.standard.string(forKey: "searchLanguageCode") ?? "")".elementsEqual("en") {
            
            self.searchButtonOutlet.imageEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
            
        }
        else {
            self.searchButtonOutlet.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        }
        
        globalFunctions.shared.loadPlaceholder(onView: self.view)
        
        self.localizeStrings()
        
        self.SetFont()
        
        self.periodPickerview.delegate = self
        self.periodPickerview.dataSource = self
        
        self.addedDateCollectionview.delegate = self
        self.addedDateCollectionview.dataSource = self
        self.addedByCollectionview.delegate = self
        self.addedByCollectionview.dataSource = self
        self.negotiablePriceColletionview.delegate = self
        self.negotiablePriceColletionview.dataSource = self
        
        self.setCollectionviewHeights()
        
        
        self.loadRealEstateTypes()
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            self.getProvinceList(countryID: "\(userInformation.shared.searchCountryID ?? "")")
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                var tempHeight : CGFloat! = 0
                
                for i in 0..<self.userRealEstateTypeArray.count {
                    tempHeight = tempHeight + CGFloat(50 * (self.userRealEstateTypeArray[i].childArray.count))
                }
                
                self.realEstateTypeTableHeightCOnstraint.constant = CGFloat(40 * self.userRealEstateTypeArray.count) + tempHeight
                
                self.preselectUserFilteredValues()
                
            })
            
            
            
            self.dynamicStackView.removeConstraint(self.dynamicInfoStackviewHeightConstraint)
            
            globalFunctions.shared.removePlaceholder(fromView: self.view)
            
        }
        
        self.meterSquareStaticLBL.text = "\(globalFunctions.shared.getLanguageString(variable: "app_home_m2"))2"//  + "\((recordsArray[indexPath.row] as! NSDictionary)["ribbon_value_lang"] ?? "")"
        let font:UIFont? = UIFont(name: self.meterSquareStaticLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 12)))
        let fontSuper:UIFont? = UIFont(name: self.meterSquareStaticLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 5)))
        let attString:NSMutableAttributedString = NSMutableAttributedString(string: self.meterSquareStaticLBL.text!, attributes: [.font:font!])
        attString.setAttributes([.font:fontSuper!,.baselineOffset:5], range: NSRange(location:(self.meterSquareStaticLBL.text!.count - 1),length:1))
        self.meterSquareStaticLBL.attributedText = attString
        // Create toolbar
        self.createToolbar()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.headerView.contentVIew.backgroundColor = UIColor(red: 0.117, green: 0.282, blue: 0.454, alpha: 1.0)
        self.headerView.headerTitle.backgroundColor = UIColor(red: 0.117, green: 0.282, blue: 0.454, alpha: 1.0)
        self.resetButtonOutlet.backgroundColor = UIColor(red: 0.117, green: 0.282, blue: 0.454, alpha: 1.0)
        self.searchButtonOutlet.backgroundColor = UIColor(red: 0.117, green: 0.282, blue: 0.454, alpha: 1.0)
        
    }
    
    // MARK:- // Preselect User Filtered Values
    
    func preselectUserFilteredValues() {
        
        for i in 0..<self.userRealEstateTypeArray.count {
            
            if (self.userRealEstateTypeArray[i].parentID?.elementsEqual("\(userInformation.shared.userFilterDictionary.parentCategory ?? "")"))! {
                self.userRealEstateTypeArray[i].isSelected = true
                
                for j in 0..<self.userRealEstateTypeArray[i].childArray.count {
                    
                    if (self.userRealEstateTypeArray[i].childArray[j].id?.elementsEqual("\(userInformation.shared.userFilterDictionary.propertyType ?? "")"))! {
                        
                        self.userRealEstateTypeArray[i].childArray[j].isSelected = true
                        
                        //
                        self.getDynamicFieldsData(propertyType: "\(userInformation.shared.userFilterDictionary.propertyType ?? "")") {
                            
                            //creating dynamic filter arrays
                            //self.createDynamicFilterArrays(array: self.globalJson["dynamic_info"] as! NSArray)
                            
                            self.prefillDynamicFilterValues()
                            
                        }
                        
                    }
                }
            }
            
        }
        
        self.realEstateTypeTable.reloadData()
        
        
        // Offer For
        
        self.searchCategory = userInformation.shared.userFilterDictionary.categorySub
        
        if self.searchCategory.elementsEqual("2") {
            self.rentImageview.image = UIImage(named: "radio-button")
            self.saleImageview.image = UIImage(named: "non select")
            
            self.searchCategory = "2"
            
            self.peroidView.isHidden = false
            
            for i in 0..<self.periodArray.count {
                if ((self.periodArray[i])["key"]?.elementsEqual("\(userInformation.shared.userFilterDictionary.period ?? "")"))! {
                    self.period.text = "\((self.periodArray[i])["value"] ?? "")"
                    self.periodID = "\((self.periodArray[i])["key"] ?? "")"
                }
            }
        }
        else if self.searchCategory.elementsEqual("1") {
            self.rentImageview.image = UIImage(named: "non select")
            self.saleImageview.image = UIImage(named: "radio-button")
            
            self.searchCategory = "1"
            
            self.peroidView.isHidden = true
        }
        else {
            self.rentImageview.image = UIImage(named: "non select")
            self.saleImageview.image = UIImage(named: "non select")
            
            self.peroidView.isHidden = true
        }
        
        
        
        // Address
        
        if userInformation.shared.userFilterDictionary.province.elementsEqual("") == false {
            
            for i in 0..<self.provinceArray.count {
                if "\((self.provinceArray[i])["id"] ?? "")".elementsEqual("\(userInformation.shared.userFilterDictionary.province ?? "")") {
                    
                    self.province.text = "\((self.provinceArray[i])["title_eng"] ?? "")"
                    
                    self.provinceID = "\((self.provinceArray[i])["id"] ?? "")"
                    
                }
            }
            
            self.getCityList(provinceID: self.provinceID)
            
            self.globalDispatchgroup.notify(queue: .main) {
                
                if userInformation.shared.userFilterDictionary.city.elementsEqual("") == false {
                    
                    for i in 0..<self.cityArray.count {
                        if "\((self.cityArray[i])["id"] ?? "")".elementsEqual("\(userInformation.shared.userFilterDictionary.city ?? "")") {
                            
                            self.city.text = "\((self.cityArray[i])["cityname"] ?? "")"
                            
                            self.cityID = "\((self.cityArray[i])["id"] ?? "")"
                            
                        }
                    }
                    
                    self.getDistrictList(cityID: self.cityID)
                    
                    self.globalDispatchgroup.notify(queue: .main, execute: {
                        
                        if userInformation.shared.userFilterDictionary.district.elementsEqual("") == false {
                            
                            for i in 0..<self.districtArray.count {
                                if "\((self.districtArray[i])["id"] ?? "")".elementsEqual("\(userInformation.shared.userFilterDictionary.district ?? "")") {
                                    
                                    self.district.text = "\((self.districtArray[i])["districtname"] ?? "")"
                                    
                                    self.districtID = "\((self.districtArray[i])["id"] ?? "")"
                                    
                                }
                            }
                            
                        }
                        
                    })
                    
                }
                
            }
            
        }
        
        
        
        
        // PRice
        self.fromCurrencyTXT.text = "\(userInformation.shared.userFilterDictionary.minPrice ?? "")"
        self.toCurrencyTXT.text = "\(userInformation.shared.userFilterDictionary.maxPrice ?? "")"
        self.fromIDTXT.text = "\(userInformation.shared.userFilterDictionary.minPriceID ?? "")"
        self.toIDTXT.text = "\(userInformation.shared.userFilterDictionary.maxPriceID ?? "")"
        self.fromMeterSquareTXT.text = "\(userInformation.shared.userFilterDictionary.minArea ?? "")"
        self.toMeterSquareTXT.text = "\(userInformation.shared.userFilterDictionary.maxArea ?? "")"
        
        
        
        // General Info
        
        //Added Date
        for i in 0..<self.viewDaysArray.count {
            
            if "\(userInformation.shared.userFilterDictionary.viewDays ?? "")".elementsEqual("\(self.viewDaysArray[i].key ?? "")") {
                
                if "\(self.viewDaysArray[i].key ?? "")".elementsEqual("") == false {
                    
                    self.viewDaysArray[i].isSelected = true
                    
                    self.viewDays = "\(self.viewDaysArray[i].key ?? "")"
                    
                    self.addedDateCollectionview.reloadData()
                    
                    break
                    
                }
                
            }
        }
        
        self.addedDateCollectionview.reloadData()
        
        
        //Added By
        for i in 0..<self.AddedByArray.count {
            
            if "\(userInformation.shared.userFilterDictionary.addedBy ?? "")".elementsEqual("\(self.AddedByArray[i].key ?? "")") {
                
                if "\(self.AddedByArray[i].key ?? "")".elementsEqual("") == false {
                    
                    self.AddedByArray[i].isSelected = true
                    
                    self.viewDays = "\(self.AddedByArray[i].key ?? "")"
                    
                    self.addedByCollectionview.reloadData()
                    
                    break
                    
                }
                
            }
        }
        
        self.addedByCollectionview.reloadData()
        
        
        //Negotiation Price
        for i in 0..<self.negotiablePriceArray.count {
            
            if "\(userInformation.shared.userFilterDictionary.negotiablePrice ?? "")".elementsEqual("\(self.negotiablePriceArray[i].key ?? "")") {
                
                if "\(self.negotiablePriceArray[i].key ?? "")".elementsEqual("") == false {
                    
                    self.negotiablePriceArray[i].isSelected = true
                    
                    self.viewDays = "\(self.negotiablePriceArray[i].key ?? "")"
                    
                    self.negotiablePriceColletionview.reloadData()
                    
                    break
                    
                }
                
                
            }
        }
        
        self.negotiablePriceColletionview.reloadData()
    }
    
    
    // MARK:- // Prefill Dynamic Filter Values
    
    func prefillDynamicFilterValues() {
        
        self.dynamicInfoFeaturesDataArray = userInformation.shared.userFilterFeatureDataArray
        self.dynamicInfoStatusDataArray = userInformation.shared.userFilterStatusDataArray
        
        self.createDynamicFeatureStackview()
        
        self.createDynamicStatusStackview()
        
        
        // Feature Section
        for i in 0..<self.dynamicInfoFeaturesDataArray.count {
            
            // MIN MAX Textfields
            if "\(self.dynamicInfoFeaturesDataArray[i].fieldTypeID ?? "")".elementsEqual("1") || "\(self.dynamicInfoFeaturesDataArray[i].fieldTypeID ?? "")".elementsEqual("3") {
                
                for j in 0..<self.minTXTArray.count {
                    
                    if (self.minTXTArray[j].restorationIdentifier?.elementsEqual("features"))! {
                        
                        if self.minTXTArray[j].tag == i {
                            
                            if self.dynamicInfoFeaturesDataArray[i].value.elementsEqual("") == false {
                                
                                let tempArray = self.dynamicInfoFeaturesDataArray[i].value.components(separatedBy: ",")
                                
                                self.minTXTArray[j].text = tempArray[0]
                                self.maxTXTArray[j].text = tempArray[1]
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
        
        // Status Section
        for i in 0..<self.dynamicInfoStatusDataArray.count {
            
            // MIN MAX Textfields
            if "\(self.dynamicInfoStatusDataArray[i].fieldTypeID ?? "")".elementsEqual("1") || "\(self.dynamicInfoStatusDataArray[i].fieldTypeID ?? "")".elementsEqual("3") {
                
                for j in 0..<self.minTXTArray.count {
                    
                    if (self.minTXTArray[j].restorationIdentifier?.elementsEqual("status"))! {
                        
                        if self.minTXTArray[j].tag == i {
                            
                            if self.dynamicInfoStatusDataArray[i].value.elementsEqual("") == false {
                                
                                let tempArray = self.dynamicInfoStatusDataArray[i].value.components(separatedBy: ",")
                                
                                self.minTXTArray[j].text = tempArray[0]
                                self.maxTXTArray[j].text = tempArray[1]
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }


       
    }
    
    
    
    
    // MARK:- // Buttons
    
    
    // MARK:- // Open District Picker Button
    
    @IBOutlet weak var openDistrictPickerButtonOutlet: UIButton!
    @IBAction func openDistrictPickerButton(_ sender: UIButton) {
        
        if self.districtArray.count == 0 {
            
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "property_post_city"))
            
        }
        else {
            
            self.view.bringSubviewToFront(self.viewOfPickerview)
            self.viewOfPickerview.isHidden = false
            self.districtPickerview.isHidden = false
            
            self.districtPickerview.selectRow(0, inComponent: 0, animated: false)
            
            self.selectedPickerview = self.districtPickerview
            self.selectedPickerIndex = 0
            self.doneClicked = false
        }
        
    }
    
    
    // MARK:- // Open City Picker Button
    
    @IBOutlet weak var openCityPickerButtonOutlet: UIButton!
    @IBAction func openCityPickerButton(_ sender: UIButton) {
        
        if self.cityArray.count == 0 {
            
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "please_select_province"))
        }
        else {
            
            self.view.bringSubviewToFront(self.viewOfPickerview)
            self.viewOfPickerview.isHidden = false
            self.cityPickerview.isHidden = false
            
            self.cityPickerview.selectRow(0, inComponent: 0, animated: false)
            
            self.selectedPickerview = self.cityPickerview
            self.selectedPickerIndex = 0
            self.doneClicked = false
        }
        
    }
    
    
    
    // MARK:- // Open Province Picker Button
    
    @IBOutlet weak var openProvincePickerButtonOutlet: UIButton!
    @IBAction func openProvincePickerButton(_ sender: UIButton) {
        
        if self.provinceArray.count == 0 {
            
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "my_profile_select_country"))
        }
        else {
            
            self.view.bringSubviewToFront(self.viewOfPickerview)
            self.viewOfPickerview.isHidden = false
            self.provincePickerview.isHidden = false
            
            self.provincePickerview.selectRow(0, inComponent: 0, animated: false)
            
            self.selectedPickerview = self.provincePickerview
            self.selectedPickerIndex = 0
            self.doneClicked = false
        }
        
    }
    
    
    
    
    // MARK:- // Open Period Pickerview
    
    @IBOutlet weak var openPeriodPickerButtonOutlet: UIButton!
    @IBAction func openPeriodPickerButton(_ sender: UIButton) {
        
        self.view.bringSubviewToFront(self.viewOfPickerview)
        self.viewOfPickerview.isHidden = false
        self.periodPickerview.isHidden = false
        
        self.periodPickerview.selectRow(0, inComponent: 0, animated: false)
        
        self.selectedPickerview = self.periodPickerview
        self.selectedPickerIndex = 0
        self.doneClicked = false
        
    }
    
    
    
    // MARK:- // Close Pickerview Button
    
    @IBOutlet weak var closePickerButtonOutlet: UIButton!
    @IBAction func closePickerButton(_ sender: UIButton) {
        
        self.view.sendSubviewToBack(self.viewOfPickerview)
        self.viewOfPickerview.isHidden = true
        self.periodPickerview.isHidden = true
        self.provincePickerview.isHidden = true
        self.cityPickerview.isHidden = true
        self.districtPickerview.isHidden = true
        
    }
    
    
    
    
    // MARK:- // Rent Button
    
    @IBOutlet weak var rentButtonOutlet: UIButton!
    @IBAction func rentButton(_ sender: UIButton) {
        
        if self.rentImageview.image == UIImage(named: "radio-button") {
            self.rentImageview.image = UIImage(named: "non select")
            self.saleImageview.image = UIImage(named: "non select")
            
            self.peroidView.isHidden = true
            
            self.searchCategory = ""
        }
        else {
            self.rentImageview.image = UIImage(named: "radio-button")
            self.saleImageview.image = UIImage(named: "non select")
            
            self.peroidView.isHidden = false
            
            self.searchCategory = "2"
        }
        
        
        
    }
    
    
    
    // MARK:- // Sale Button
    
    @IBOutlet weak var saleButtonOutlet: UIButton!
    @IBAction func saleButton(_ sender: UIButton) {
        
        if self.saleImageview.image == UIImage(named: "radio-button") {
            self.rentImageview.image = UIImage(named: "non select")
            self.saleImageview.image = UIImage(named: "non select")
            
            self.peroidView.isHidden = true
            
            self.searchCategory = ""
        }
        else {
            self.rentImageview.image = UIImage(named: "non select")
            self.saleImageview.image = UIImage(named: "radio-button")
            
            self.peroidView.isHidden = true
            
            self.searchCategory = "1"
        }
        
        
        
    }
    
    
    
    
    
    
    
    
    
    // MARK:- // Reset Button
    
    @IBOutlet weak var resetButtonOutlet: UIButton!
    @IBAction func resetButton(_ sender: UIButton) {
        
        self.filterDict = filterDictionary(parentCategory: "", propertyType: "", categorySub: "", period: "", country: userInformation.shared.searchCountryID, province: "", city: "", district: "", minPrice: "", maxPrice: "", minPriceID: "", maxPriceID: "", minArea: "", maxArea: "", viewDays: "", addedBy: "", negotiablePrice: "")
        
        userInformation.shared.setUserFilterDictionary(dict: self.filterDict)
        
        userInformation.shared.setUserDynamicFilterString(string: "")
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        
        navigate.fromFilter = true
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
    // MARK:- // Search Button
    
    @IBOutlet weak var searchButtonOutlet: UIButton!
    @IBAction func searchButton(_ sender: UIButton) {
        
        self.createFilterDictionary()
        
        userInformation.shared.setUserFilterDataArrays(featureArray: self.dynamicInfoFeaturesDataArray, statusArray: self.dynamicInfoStatusDataArray)
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        
        navigate.fromFilter = true
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
    
    
    // MARK:- // JSON Post Method to Load Real Estate Types
    
    func loadRealEstateTypes() {
        
        let parameters = "language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "parent_categories_list", param: parameters) {
            
            var tempFilterCategoriesArray = [filterCategoriesDataFormat]()
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                
                for j in 0..<(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["sub_category"] as! NSArray).count {
                    
                    let tempDict = (((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["sub_category"] as! NSArray)[j] as! NSDictionary
                    
                    tempFilterCategoriesArray.append(filterCategoriesDataFormat(id: "\(tempDict["sub_category_id"] ?? "")", categoryName: "\(tempDict["sub_category_name"] ?? "")", isSelected: false))
                    
                }
                
                self.userRealEstateTypeArray.append(filterDataFormat(parentID: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["parent_category_id"] ?? "")", parentName: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["parent_category_name"] ?? "")", childArray: tempFilterCategoriesArray, isselected: false))
                
                tempFilterCategoriesArray.removeAll()
                
            }
            
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.realEstateTypeTable.delegate = self
                self.realEstateTypeTable.dataSource = self
                self.realEstateTypeTable.reloadData()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    
    
    
    
    // MARK:- // JSON Post Method to fetch Province List
    
    func getProvinceList(countryID : String) {
        
        let parameters = "country_id=\(countryID)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "fetch_province_by_country", param: parameters) {
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                
                let tempDict = (self.globalJson["info"] as! NSArray)[i] as! NSDictionary
                
                self.provinceArray.append(tempDict)
                
            }
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.provincePickerview.delegate = self
                self.provincePickerview.dataSource = self
                self.provincePickerview.reloadAllComponents()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
        }
    }
    
    
    
    // MARK:- // JSON Post Method to get City List
    
    func getCityList(provinceID : String) {
        
        let parameters = "province_id=\(provinceID)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "fetch_city_by_province", param: parameters) {
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                
                let tempDict = (self.globalJson["info"] as! NSArray)[i] as! NSDictionary
                
                self.cityArray.append(tempDict)
                
            }
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.cityPickerview.delegate = self
                self.cityPickerview.dataSource = self
                self.cityPickerview.reloadAllComponents()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
            }
        }
        
    }
    
    
    // MARK:- // JSON Post Method to fetch District List
    
    func getDistrictList(cityID : String) {
        
        let parameters = "city_id=\(cityID)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "fetch_district_by_city", param: parameters) {
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                
                let tempDict = (self.globalJson["info"] as! NSArray)[i] as! NSDictionary
                
                self.districtArray.append(tempDict)
                
            }
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.districtPickerview.delegate = self
                self.districtPickerview.dataSource = self
                self.districtPickerview.reloadAllComponents()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
        }
        
    }
    
    
    
    // MARK:- // JSOn Post Method to get Dynamic Fields Data
    
    func getDynamicFieldsData(propertyType : String , completion : @escaping ()->()) {
        
        var parameters : String!
        
        parameters = "property_type=\(propertyType)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "service_form_field_new_listed", param: parameters) {
            
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                completion()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    
    // MARK:- // Create Dynamic Filter Arrays
    
    func createDynamicFilterArrays(array : NSArray) {
        
        self.dynamicInfoFeaturesDataArray.removeAll()
        
        self.dynamicInfoStatusDataArray.removeAll()
        
        var tempArray = (array[0] as! NSDictionary)["features"] as! NSArray
        
        for i in 0..<tempArray.count {
            
            var fieldOptionArray = [StaticDictDataFormat]()
            
            for j in 0..<((tempArray[i] as! NSDictionary)["field_option"] as! NSArray).count {
                
                fieldOptionArray.append(StaticDictDataFormat(key: "\((((tempArray[i] as! NSDictionary)["field_option"] as! NSArray)[j] as! NSDictionary)["field_value"] ?? "")", value: "\((((tempArray[i] as! NSDictionary)["field_option"] as! NSArray)[j] as! NSDictionary)["field_name"] ?? "")", isSelected: false))
                
            }
            
            self.dynamicInfoFeaturesDataArray.append(dynamicFilterDictionary(fieldID: "\((tempArray[i] as! NSDictionary)["field_id"] ?? "")", fieldName: "\((tempArray[i] as! NSDictionary)["field_name"] ?? "")", fieldType: "\((tempArray[i] as! NSDictionary)["field_type"] ?? "")", fieldTypeID: "\((tempArray[i] as! NSDictionary)["field_type_id"] ?? "")", parameter: "\((tempArray[i] as! NSDictionary)["parameter"] ?? "")", parameterSecond: "\((tempArray[i] as! NSDictionary)["parameter_second"] ?? "")", fieldOptionArray: fieldOptionArray, value: ""))
            
        }
        
        tempArray = (array[1] as! NSDictionary)["status"] as! NSArray
        
        for i in 0..<tempArray.count {
            
            var fieldOptionArray = [StaticDictDataFormat]()
            
            for j in 0..<((tempArray[i] as! NSDictionary)["field_option"] as! NSArray).count {
                
                fieldOptionArray.append(StaticDictDataFormat(key: "\((((tempArray[i] as! NSDictionary)["field_option"] as! NSArray)[j] as! NSDictionary)["field_value"] ?? "")", value: "\((((tempArray[i] as! NSDictionary)["field_option"] as! NSArray)[j] as! NSDictionary)["field_name"] ?? "")", isSelected: false))
                
            }
            
            self.dynamicInfoStatusDataArray.append(dynamicFilterDictionary(fieldID: "\((tempArray[i] as! NSDictionary)["field_id"] ?? "")", fieldName: "\((tempArray[i] as! NSDictionary)["field_name"] ?? "")", fieldType: "\((tempArray[i] as! NSDictionary)["field_type"] ?? "")", fieldTypeID: "\((tempArray[i] as! NSDictionary)["field_type_id"] ?? "")", parameter: "\((tempArray[i] as! NSDictionary)["parameter"] ?? "")", parameterSecond: "\((tempArray[i] as! NSDictionary)["parameter_second"] ?? "")", fieldOptionArray: fieldOptionArray, value: ""))
            
        }
        
    }
    
    
    // MARK:- // Create Dynamic Feature Stackview
    
    func createDynamicFeatureStackview() {
        
        let featuresStackview : dynamicStackview = {
            let featuresstackview = dynamicStackview()
            featuresstackview.translatesAutoresizingMaskIntoConstraints = false
            return featuresstackview
        }()
        
        self.dynamicStackView.addArrangedSubview(featuresStackview)
        
        
        for i in 0..<self.dynamicInfoFeaturesDataArray.count {
            
            
            // CHECKBOXES
            if "\(self.dynamicInfoFeaturesDataArray[i].fieldTypeID ?? "")".elementsEqual("4") {
                self.createViewWithCollectionview(addInView: featuresStackview, tag: i, restorationIdentifier: "features", dataDictionary: self.dynamicInfoFeaturesDataArray[i], accessibilityIdentifier: "checkbox")
                
            }
                
            // RADIO
            else if "\(self.dynamicInfoFeaturesDataArray[i].fieldTypeID ?? "")".elementsEqual("5") {
                    self.createViewWithCollectionview(addInView: featuresStackview, tag: i, restorationIdentifier: "features", dataDictionary: self.dynamicInfoFeaturesDataArray[i], accessibilityIdentifier: "radio")
                    
            }
                
            // MIN MAX TEXTFIELDS
            else if "\(self.dynamicInfoFeaturesDataArray[i].fieldTypeID ?? "")".elementsEqual("1") || "\(self.dynamicInfoFeaturesDataArray[i].fieldTypeID ?? "")".elementsEqual("3") {

                self.createViewWithMaxMinTextfields(addInView: featuresStackview, tag: i, restorationIdentifier: "features", dataDictionary: self.dynamicInfoFeaturesDataArray[i], accessibilityIdentifier: "textfield")

            }

            
        }
        
    }
    
    
    // MARK:- // Create Dynamic Status Stackview
    
    func createDynamicStatusStackview() {
        
        let statusStackview : dynamicStackview = {
            let statusstackview = dynamicStackview()
            statusstackview.translatesAutoresizingMaskIntoConstraints = false
            return statusstackview
        }()
        
        self.dynamicStackView.addArrangedSubview(statusStackview)
        
        for i in 0..<self.dynamicInfoStatusDataArray.count {
            
            // CHECKBOXES
            if "\(self.dynamicInfoStatusDataArray[i].fieldTypeID ?? "")".elementsEqual("4") {
                self.createViewWithCollectionview(addInView: statusStackview, tag: i, restorationIdentifier: "status", dataDictionary: self.dynamicInfoStatusDataArray[i], accessibilityIdentifier: "checkbox")
                
            }
                
            // RADIO
            else if "\(self.dynamicInfoStatusDataArray[i].fieldTypeID ?? "")".elementsEqual("4") {
                self.createViewWithCollectionview(addInView: statusStackview, tag: i, restorationIdentifier: "status", dataDictionary: self.dynamicInfoStatusDataArray[i], accessibilityIdentifier: "radio")
                
            }
            
            // MIN MAX TEXTFIELDS
            else if "\(self.dynamicInfoStatusDataArray[i].fieldTypeID ?? "")".elementsEqual("1") || "\(self.dynamicInfoStatusDataArray[i].fieldTypeID ?? "")".elementsEqual("3") {
                
                self.createViewWithMaxMinTextfields(addInView: statusStackview, tag: i, restorationIdentifier: "status", dataDictionary: self.dynamicInfoStatusDataArray[i], accessibilityIdentifier: "textfield")
                
            }
            
        }
            
        
        
    }
    
    
    
    
    
    
    // MARK:- // Section Button Action
    
    @objc func sectionButtonAction(sender: UIButton) {
        
        for i in 0..<(self.userRealEstateTypeArray.count) {
            self.userRealEstateTypeArray[i].isSelected = false
            
            for j in 0..<((self.userRealEstateTypeArray[i].childArray).count) {
                (self.userRealEstateTypeArray[i].childArray)[j].isSelected = false
            }
            
        }
        
        self.userRealEstateTypeArray[sender.tag].isSelected = !self.userRealEstateTypeArray[sender.tag].isSelected
        
        self.realEstateTypeTable.reloadData()
        
    }
    
    
    
    
    
    // MARK:- // Create Filter Dictionary
    
    func createFilterDictionary() {
        
        for i in 0..<self.userRealEstateTypeArray.count {
            
            if self.userRealEstateTypeArray[i].isSelected == true {
                
                self.parentCategory = "\(self.userRealEstateTypeArray[i].parentID ?? "")"
                
                for j in 0..<self.userRealEstateTypeArray[i].childArray.count {
                    
                    if (self.userRealEstateTypeArray[i].childArray)[j].isSelected == true {
                        
                        self.propertyType = "\(self.userRealEstateTypeArray[i].childArray[j].id ?? "")"
                        
                        break
                        
                    }
                }
            }
        }
        
        self.filterDict = filterDictionary(parentCategory: "\(self.parentCategory ?? "")", propertyType: "\(self.propertyType ?? "")", categorySub: "\(self.searchCategory ?? "")", period: "\(self.periodID ?? "")", country: userInformation.shared.searchCountryID, province: "\(self.provinceID ?? "")", city: "\(self.cityID ?? "")", district: "\(self.districtID ?? "")", minPrice: "\(self.fromCurrencyTXT.text ?? "")", maxPrice: "\(self.toCurrencyTXT.text ?? "")", minPriceID: "\(self.fromIDTXT.text ?? "")", maxPriceID: "\(self.toIDTXT.text ?? "")", minArea: "\(self.fromMeterSquareTXT.text ?? "")", maxArea: "\(self.toMeterSquareTXT.text ?? "")", viewDays: "\(self.viewDays ?? "")", addedBy: "\(self.addedBy ?? "")", negotiablePrice: "\(self.negotiablePrice ?? "")")
        
        userInformation.shared.setUserFilterDictionary(dict: self.filterDict)
        
        let tempString = self.createDynamicFieldString()
        
        userInformation.shared.setUserDynamicFilterString(string: tempString)
        
        
        
    }
    
    
    // MARK:- // Create Dynamic field String
    
    func createDynamicFieldString() -> String {
        
        var tempString : String! = ""
        
        // CHECKBOXES
        var tempArray = [String]()
        
        for i in 0..<self.dynamicInfoFeaturesDataArray.count {
            if (self.dynamicInfoFeaturesDataArray[i].fieldTypeID?.elementsEqual("4"))! {
                for j in 0..<self.dynamicInfoFeaturesDataArray[i].fieldOptionArray.count {
                    if self.dynamicInfoFeaturesDataArray[i].fieldOptionArray[j].isSelected == true {
                        tempArray.append("\(self.dynamicInfoFeaturesDataArray[i].fieldOptionArray[j].key ?? "")")
                    }
                }
                self.dynamicInfoFeaturesDataArray[i].value = tempArray.joined(separator: ",")
                
                if tempArray.count > 0 {
                    tempString = tempString + "&\(self.dynamicInfoFeaturesDataArray[i].parameter ?? "")" + "=\(self.dynamicInfoFeaturesDataArray[i].value ?? "")"
                }
                
            }
            tempArray.removeAll()
        }
        
        tempArray.removeAll()
        
        for i in 0..<self.dynamicInfoStatusDataArray.count {
            if (self.dynamicInfoStatusDataArray[i].fieldTypeID?.elementsEqual("4"))! {
                for j in 0..<self.dynamicInfoStatusDataArray[i].fieldOptionArray.count {
                    if self.dynamicInfoStatusDataArray[i].fieldOptionArray[j].isSelected == true {
                        tempArray.append("\(self.dynamicInfoStatusDataArray[i].fieldOptionArray[j].key ?? "")")
                    }
                }
                self.dynamicInfoStatusDataArray[i].value = tempArray.joined(separator: ",")
                
                if tempArray.count > 0 {
                    tempString = tempString + "&\(self.dynamicInfoStatusDataArray[i].parameter ?? "")" + "=\(self.dynamicInfoStatusDataArray[i].value ?? "")"
                }
                
            }
            tempArray.removeAll()
        }
        /////
        
        
        // MIN MAX FIELDS
        for i in 0..<self.minTXTArray.count {
            
            print(self.minTXTArray[i].text ?? "")
            print(self.maxTXTArray[i].text ?? "")
            
            if self.minTXTArray[i].text?.elementsEqual("") == false || self.maxTXTArray[i].text?.elementsEqual("") == false {
                
                if (self.minTXTArray[i].restorationIdentifier?.elementsEqual("features"))! {
                    
                    self.dynamicInfoFeaturesDataArray[minTXTArray[i].tag].value = "\(self.minTXTArray[i].text ?? "0"),\(self.maxTXTArray[i].text ?? "0")"
                    tempString = tempString + "&\(self.dynamicInfoFeaturesDataArray[minTXTArray[i].tag].parameter ?? "")=\(self.minTXTArray[i].text ?? "0")&\(self.dynamicInfoFeaturesDataArray[minTXTArray[i].tag].parameterSecond ?? "")=\(self.maxTXTArray[i].text ?? "0")"
                    
                }
                else {
                    
                    self.dynamicInfoStatusDataArray[minTXTArray[i].tag].value = "\(self.minTXTArray[i].text ?? "0"),\(self.maxTXTArray[i].text ?? "0")"
                    tempString = tempString + "&\(self.dynamicInfoStatusDataArray[minTXTArray[i].tag].parameter ?? "")=\(self.minTXTArray[i].text ?? "0")&\(self.dynamicInfoStatusDataArray[minTXTArray[i].tag].parameterSecond ?? "")=\(self.maxTXTArray[i].text ?? "0")"
                    
                }
                
            }
            
        }
        /////
        
        return tempString
        
    }
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.offerForTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "property_post_sub_category")
        self.rentLBL.text = globalFunctions.shared.getLanguageString(variable: "home_rent")
        self.saleLBL.text = globalFunctions.shared.getLanguageString(variable: "home_sale")
        self.period.text = globalFunctions.shared.getLanguageString(variable: "property_post_select") + " " + globalFunctions.shared.getLanguageString(variable: "extend_cost_text3")
        
        self.addressTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_address")
        self.province.text = globalFunctions.shared.getLanguageString(variable: "property_post_province")
        self.city.text = globalFunctions.shared.getLanguageString(variable: "property_post_city")
        self.district.text = globalFunctions.shared.getLanguageString(variable: "property_post_district")
        
        self.priceTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "home_price")
        //self.currencyLBL.text = globalFunctions.shared.getLanguageString(variable: "property_post_district")
        self.toStaticLBLCurrency.text = globalFunctions.shared.getLanguageString(variable: "home_to")
        self.idLBL.text = globalFunctions.shared.getLanguageString(variable: "app_IQD")
        self.toStaticLBLID.text = globalFunctions.shared.getLanguageString(variable: "home_to")
        self.areaTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "home_area")
        //self.meterSquareStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "home_m2")
        self.toStaticLBLMeter.text = globalFunctions.shared.getLanguageString(variable: "home_to")
        self.generalInfoTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "home_general_info")
        self.addedDateTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "home_added_date")
        self.addedByTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "home_added_by")
        self.negotiablePriceTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "property_post_negotiable_price")
        
        
        
        self.resetButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "reset"), for: .normal)
        self.searchButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "home_search"), for: .normal)
        
    }
    
    
    // MARK:- // Set Font
    
    func SetFont() {
        
        self.offerForTitleLBL.font = UIFont(name: self.offerForTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.rentLBL.font = UIFont(name: self.rentLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.saleLBL.font = UIFont(name: self.saleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.period.font = UIFont(name: self.period.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        
        self.addressTitleLBL.font = UIFont(name: self.addressTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.province.font = UIFont(name: self.province.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.city.font = UIFont(name: self.city.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.district.font = UIFont(name: self.district.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        
        self.priceTitleLBL.font = UIFont(name: self.priceTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.currencyLBL.font = UIFont(name: self.currencyLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.fromCurrencyTXT.font = UIFont(name: self.fromCurrencyTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.toStaticLBLCurrency.font = UIFont(name: self.toStaticLBLCurrency.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.toCurrencyTXT.font = UIFont(name: self.toCurrencyTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.idLBL.font = UIFont(name: self.idLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.fromIDTXT.font = UIFont(name: self.fromIDTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.toStaticLBLID.font = UIFont(name: self.toStaticLBLID.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.toIDTXT.font = UIFont(name: self.toIDTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.areaTitleLBL.font = UIFont(name: self.areaTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.meterSquareStaticLBL.font = UIFont(name: self.meterSquareStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.fromMeterSquareTXT.font = UIFont(name: self.fromMeterSquareTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.toStaticLBLMeter.font = UIFont(name: self.toStaticLBLMeter.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.toMeterSquareTXT.font = UIFont(name: self.toMeterSquareTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.generalInfoTitleLBL.font = UIFont(name: self.generalInfoTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.addedDateTitleLBL.font = UIFont(name: self.addedDateTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.addedByTitleLBL.font = UIFont(name: self.addedByTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.negotiablePriceTitleLBL.font = UIFont(name: self.negotiablePriceTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        
        
        
        self.resetButtonOutlet.titleLabel?.font = UIFont(name: (self.resetButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.searchButtonOutlet.titleLabel?.font = UIFont(name: (self.searchButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
    }
    
    
    // MARK:- // Set Collectionview Heights
    
    func setCollectionviewHeights() {
        
        self.addedDateCollectionviewHeightConstraint.constant = CGFloat((self.viewDaysArray.count / 2) + (self.viewDaysArray.count % 2)) * 40
        
        self.addedByCollectionviewHeightConstraint.constant = CGFloat((self.AddedByArray.count / 2) + (self.AddedByArray.count % 2)) * 40
        
        self.negotiablePriceCollectionviewHeightConstraint.constant = CGFloat((self.negotiablePriceArray.count / 2) + (self.negotiablePriceArray.count % 2)) * 40
        
    }
    
    
    
    
    
    // MARK:- // Create View with Checkboxes
    
    func createViewWithCollectionview(addInView : UIStackView , tag : Int , restorationIdentifier : String , dataDictionary : dynamicFilterDictionary , accessibilityIdentifier : String) {
        
        // MAIN VIEW
        let checkboxView : UIView = {
            let checkboxview = UIView()
            checkboxview.translatesAutoresizingMaskIntoConstraints = false
            return checkboxview
        }()
        
        addInView.addArrangedSubview(checkboxView)
        
        
        // HEADER LABEL
        let headerLBL : UILabel = {
            let headerlbl = UILabel()
            headerlbl.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
            headerlbl.text = "\(dataDictionary.fieldName ?? "")"
            headerlbl.translatesAutoresizingMaskIntoConstraints = false
            return headerlbl
        }()
        
        checkboxView.addSubview(headerLBL)
        
        headerLBL.anchor(top: checkboxView.topAnchor, leading: checkboxView.leadingAnchor, bottom: nil, trailing: checkboxView.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 10))
        
        
//        if restorationIdentifier.elementsEqual("features") {
//            self.featureHeaderArray.append(headerLBL)
//        }
//        else {
//            self.statusHeaderArray.append(headerLBL)
//        }
        
        
        
        // CHECKBOX COLLECTION VIEW
        let checkboxCollectionView : UICollectionView = {
            
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            
            let width = (UIScreen.main.bounds.size.width - 20) / 2
            layout.itemSize = CGSize(width: floor(width), height: 45)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            layout.scrollDirection = .vertical
            
            let listingcollectionview = UICollectionView(frame: .zero, collectionViewLayout: layout)
            
            listingcollectionview.backgroundColor = .white
            listingcollectionview.showsVerticalScrollIndicator = false
            listingcollectionview.showsHorizontalScrollIndicator = false
            listingcollectionview.translatesAutoresizingMaskIntoConstraints = false
            listingcollectionview.isScrollEnabled = true
            
            return listingcollectionview
            
        }()
        
        checkboxCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "checkboxCVC")
        
        checkboxCollectionView.dataSource = self
        checkboxCollectionView.delegate = self
        
        checkboxView.addSubview(checkboxCollectionView)
        checkboxCollectionView.tag = tag
        checkboxCollectionView.restorationIdentifier = restorationIdentifier
        checkboxCollectionView.accessibilityIdentifier = accessibilityIdentifier
        
        checkboxCollectionView.anchor(top: headerLBL.bottomAnchor, leading: checkboxView.leadingAnchor, bottom: nil, trailing: checkboxView.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 0, right: 0))
        
        let tempcount = (dataDictionary.fieldOptionArray.count / 2) + (dataDictionary.fieldOptionArray.count % 2)
        checkboxCollectionView.heightAnchor.constraint(equalToConstant: CGFloat(tempcount) * 40).isActive = true
        
        
        // BORDER VIEW
        let borderView : UIView = {
            let borderview = UIView()
            borderview.backgroundColor = .lightGray
            borderview.translatesAutoresizingMaskIntoConstraints = false
            return borderview
        }()
        
        checkboxView.addSubview(borderView)
        
        borderView.anchor(top: checkboxCollectionView.bottomAnchor, leading: checkboxView.leadingAnchor, bottom: checkboxView.bottomAnchor, trailing: checkboxView.trailingAnchor, padding: .init(top: 10, left: 10, bottom: 0, right: 10), size: .init(width: 0, height: 1))
        
    }
    
    
    // MARK:- // Create View with Max Min Textfields
    
    func createViewWithMaxMinTextfields(addInView : UIStackView , tag : Int , restorationIdentifier : String , dataDictionary : dynamicFilterDictionary , accessibilityIdentifier : String) {
        
        // MAIN VIEW
        let mainView : UIView = {
            let mainview = UIView()
            mainview.translatesAutoresizingMaskIntoConstraints = false
            return mainview
        }()
        
        addInView.addArrangedSubview(mainView)
        
        // HEADER LABEL
        let headerLBL : UILabel = {
            let headerlbl = UILabel()
            headerlbl.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
            headerlbl.text = "\(dataDictionary.fieldName ?? "")"
            headerlbl.translatesAutoresizingMaskIntoConstraints = false
            return headerlbl
        }()
        
        mainView.addSubview(headerLBL)
        
        headerLBL.anchor(top: mainView.topAnchor, leading: mainView.leadingAnchor, bottom: nil, trailing: mainView.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 10))
        
        // Min Max VIEW
        let minMaxView : UIView = {
            let minmaxview = UIView()
            minmaxview.translatesAutoresizingMaskIntoConstraints = false
            return minmaxview
        }()
        
        mainView.addSubview(minMaxView)
        
        minMaxView.anchor(top: headerLBL.bottomAnchor, leading: mainView.leadingAnchor, bottom: mainView.bottomAnchor, trailing: mainView.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 10, right: 0), size: .init(width: 0, height: 40))
        
        // Min LABEL
        let minLBL : UILabel = {
            let minlbl = UILabel()
            minlbl.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
            minlbl.textAlignment = .natural
            minlbl.text = "\(globalFunctions.shared.getLanguageString(variable: "home_min"))"
            minlbl.translatesAutoresizingMaskIntoConstraints = false
            return minlbl
        }()
        
        minMaxView.addSubview(minLBL)
        
        minLBL.anchor(top: minMaxView.topAnchor, leading: minMaxView.leadingAnchor, bottom: minMaxView.bottomAnchor, trailing: nil, size: .init(width: 35, height: 40))
        
        
        // Min Border VIEW
        let minViewBorder : borderView = {
            let minviewborder = borderView()
            minviewborder.translatesAutoresizingMaskIntoConstraints = false
            return minviewborder
        }()

        minMaxView.addSubview(minViewBorder)

        minViewBorder.anchor(top: minMaxView.topAnchor, leading: minLBL.trailingAnchor, bottom: minMaxView.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 10, bottom: 0, right: 0), size: .init(width: (self.FullWidth - 140)/2, height: 40))

        // Min Textfield
        let minTXT : UITextField = {
            let mintxt = UITextField()
            mintxt.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
            mintxt.keyboardType = .asciiCapableNumberPad
            mintxt.textAlignment = .center
            mintxt.translatesAutoresizingMaskIntoConstraints = false
            return mintxt
        }()

        minViewBorder.addSubview(minTXT)

        minTXT.anchor(top: minViewBorder.topAnchor, leading: minViewBorder.leadingAnchor, bottom: minViewBorder.bottomAnchor, trailing: minViewBorder.trailingAnchor, padding: .init(top: 10, left: 10, bottom: 10, right: 10))
        
        self.minTXTArray.append(minTXT)
        minTXT.restorationIdentifier = restorationIdentifier
        minTXT.tag = tag
        
        
        // Max LABEL
        let maxLBL : UILabel = {
            let maxlbl = UILabel()
            maxlbl.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
            maxlbl.textAlignment = .natural
            maxlbl.text = "\(globalFunctions.shared.getLanguageString(variable: "home_max"))"
            maxlbl.translatesAutoresizingMaskIntoConstraints = false
            return maxlbl
        }()
        
        minMaxView.addSubview(maxLBL)
        
        maxLBL.anchor(top: minMaxView.topAnchor, leading: minViewBorder.trailingAnchor, bottom: minMaxView.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 10, bottom: 0, right: 0), size: .init(width: 35, height: 40))
        
        
        // Max Border VIEW
        let maxViewBorder : borderView = {
            let maxviewborder = borderView()
            maxviewborder.translatesAutoresizingMaskIntoConstraints = false
            return maxviewborder
        }()
        
        minMaxView.addSubview(maxViewBorder)
        
        maxViewBorder.anchor(top: minMaxView.topAnchor, leading: maxLBL.trailingAnchor, bottom: minMaxView.bottomAnchor, trailing: minMaxView.trailingAnchor, padding: .init(top: 0, left: 10, bottom: 0, right: 0), size: .init(width: (self.FullWidth - 140)/2, height: 40))
        
        // Max Textfield
        let maxTXT : UITextField = {
            let maxtxt = UITextField()
            maxtxt.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
            maxtxt.keyboardType = .asciiCapableNumberPad
            maxtxt.textAlignment = .center
            maxtxt.translatesAutoresizingMaskIntoConstraints = false
            return maxtxt
        }()
        
        maxViewBorder.addSubview(maxTXT)
        
        maxTXT.anchor(top: maxViewBorder.topAnchor, leading: maxViewBorder.leadingAnchor, bottom: maxViewBorder.bottomAnchor, trailing: maxViewBorder.trailingAnchor, padding: .init(top: 10, left: 10, bottom: 10, right: 10))
        
        self.maxTXTArray.append(maxTXT)
        maxTXT.restorationIdentifier = restorationIdentifier
        maxTXT.tag = tag
        
        
    }
    
}



// MARK:- // Tableview Delegate Methods

extension FilterVC: UITableViewDelegate,UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.userRealEstateTypeArray.count
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.userRealEstateTypeArray[section].childArray.count
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let sectionView = UIView()
        
        let sectionImageview : UIImageView = {
            let sectionimageview = UIImageView()
            sectionimageview.contentMode = .scaleAspectFit
            sectionimageview.translatesAutoresizingMaskIntoConstraints = false
            return sectionimageview
        }()
        
        let sectionLBL : UILabel = {
            let sectionlbl = UILabel()
            sectionlbl.font = UIFont(name: self.headerView.headerTitle.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
            sectionlbl.translatesAutoresizingMaskIntoConstraints = false
            return sectionlbl
        }()
        
        let sectionButton : UIButton = {
            let sectionbutton = UIButton()
            sectionbutton.translatesAutoresizingMaskIntoConstraints = false
            return sectionbutton
        }()
        
        sectionView.addSubview(sectionImageview)
        sectionView.addSubview(sectionLBL)
        sectionView.addSubview(sectionButton)
        
        sectionImageview.anchor(top: sectionView.topAnchor, leading: sectionView.leadingAnchor, bottom: sectionView.bottomAnchor, trailing: nil, padding: .init(top: 5, left: 0, bottom: 5, right: 0), size: .init(width: 25, height: 25))
        
        sectionLBL.anchor(top: sectionView.topAnchor, leading: sectionImageview.trailingAnchor, bottom: sectionView.bottomAnchor, trailing: nil, padding: .init(top: 5, left: 5, bottom: 5, right: 0))
        
        sectionButton.anchor(top: sectionView.topAnchor, leading: sectionView.leadingAnchor, bottom: sectionView.bottomAnchor, trailing: sectionView.trailingAnchor)
        
        
        if self.userRealEstateTypeArray[section].isSelected == true {
            sectionImageview.image = UIImage(named: "radio-button")
        }
        else {
            sectionImageview.image = UIImage(named: "non select")
        }
        
        sectionLBL.text = "\((self.userRealEstateTypeArray[section]).parentName ?? "")"
        
        sectionButton.addTarget(self, action: #selector(self.sectionButtonAction(sender:)), for: .touchUpInside)
        sectionButton.tag = section
        
        return sectionView
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 35
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "filterRealEstateTypeTVC") as! filterRealEstateTypeTVC
        
        if ((self.userRealEstateTypeArray[indexPath.section]).childArray[indexPath.row]).isSelected == true {
            cell.boxImageview.image = UIImage(named: "radio-button")
        }
        else {
            cell.boxImageview.image = UIImage(named: "non select")
        }
        
        cell.cellLBL.text = "\(((self.userRealEstateTypeArray[indexPath.section]).childArray[indexPath.row]).categoryName ?? "")"
        
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        
        //Set Font
        cell.cellLBL.font = UIFont(name: cell.cellLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        for i in 0..<self.userRealEstateTypeArray.count {
            self.userRealEstateTypeArray[i].isSelected = false
            
            for j in 0..<self.userRealEstateTypeArray[i].childArray.count {
                self.userRealEstateTypeArray[i].childArray[j].isSelected = false
            }
        }
        
        self.userRealEstateTypeArray[indexPath.section].isSelected = true
        self.userRealEstateTypeArray[indexPath.section].childArray[indexPath.row].isSelected = true
        
        self.propertyType = "\(self.userRealEstateTypeArray[indexPath.section].childArray[indexPath.row].id ?? "")"
        
        print("Property Type --- \(self.propertyType ?? "")")
        
        for view in self.dynamicStackView.subviews {
            view.removeFromSuperview()
        }
        
        self.getDynamicFieldsData(propertyType: "\(self.propertyType ?? "")") {
            
            //creating dynamic filter arrays
            self.createDynamicFilterArrays(array: self.globalJson["dynamic_info"] as! NSArray)
            
            self.minTXTArray.removeAll()
            self.maxTXTArray.removeAll()
            
            self.createDynamicFeatureStackview()
            
            self.createDynamicStatusStackview()
            
        }
        
        self.realEstateTypeTable.reloadData()
        
    }
    
    
}




// MARK:- // Pickerview Delegate Methods

extension FilterVC: UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == self.periodPickerview {
            return self.periodArray.count
        }
        else if pickerView == self.provincePickerview {
            return self.provinceArray.count
        }
        else if pickerView == self.cityPickerview {
            return self.cityArray.count
        }
        else {
            return self.districtArray.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == self.periodPickerview {
            return "\((self.periodArray[row])["value"] ?? "")"
        }
        else if pickerView == self.provincePickerview {
            return "\((self.provinceArray[row])["title_eng"] ?? "")"
        }
        else if pickerView == self.cityPickerview {
            return "\((self.cityArray[row])["cityname"] ?? "")"
        }
        else {
            return "\((self.districtArray[row])["districtname"] ?? "")"
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.periodPickerview {
            
            if self.doneClicked {
                self.period.text = "\((self.periodArray[row])["value"] ?? "")"
                
                self.periodID = "\((self.periodArray[row])["key"] ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPickerview)
                self.viewOfPickerview.isHidden = true
                self.periodPickerview.isHidden = true
            }
            else {
                self.selectedPickerIndex = row
            }
            
        }
        else if pickerView == self.provincePickerview {
            
            if self.doneClicked {
                self.province.text = "\((self.provinceArray[row])["title_eng"] ?? "")"
                
                self.provinceID = "\((self.provinceArray[row])["id"] ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPickerview)
                self.viewOfPickerview.isHidden = true
                self.provincePickerview.isHidden = true
                
                self.cityArray.removeAll()
                self.districtArray.removeAll()
                
                self.city.text = globalFunctions.shared.getLanguageString(variable: "home_select_city")
                self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
                
                self.getCityList(provinceID: "\((self.provinceArray[row])["id"] ?? "")")
                
                self.cityID = ""
                self.districtID = ""
            }
            else {
                self.selectedPickerIndex = row
            }
            
        }
        else if pickerView == self.cityPickerview {
            
            if self.doneClicked {
                self.city.text = "\((self.cityArray[row])["cityname"] ?? "")"
                
                self.cityID = "\((self.cityArray[row])["id"] ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPickerview)
                self.viewOfPickerview.isHidden = true
                self.cityPickerview.isHidden = true
                
                self.districtArray.removeAll()
                
                self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
                
                self.getDistrictList(cityID: "\((self.cityArray[row])["id"] ?? "")")
                
                self.districtID = ""
            }
            else {
                self.selectedPickerIndex = row
            }
            
            
            
        }
        else {
            
            if self.doneClicked {
                self.district.text = "\((self.districtArray[row])["districtname"] ?? "")"
                
                self.districtID = "\((self.districtArray[row])["id"] ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPickerview)
                self.viewOfPickerview.isHidden = true
                self.districtPickerview.isHidden = true
            }
            else {
                self.selectedPickerIndex = row
            }
            
        }
        
        
        
    }
    
}




// MARK:- // Collectionview Delegate Methods

extension FilterVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.addedDateCollectionview {
            return self.viewDaysArray.count
        }
        else if collectionView == self.addedByCollectionview {
            return self.AddedByArray.count
        }
        else if collectionView == self.negotiablePriceColletionview {
            return self.negotiablePriceArray.count
        }
        else {
            
            
            if (collectionView.restorationIdentifier?.elementsEqual("features"))! {
                return self.dynamicInfoFeaturesDataArray[collectionView.tag].fieldOptionArray.count
            }
            else {
                return self.dynamicInfoStatusDataArray[collectionView.tag].fieldOptionArray.count
            }
            
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        if collectionView == self.addedDateCollectionview {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "filterGeneralInfoCVC", for: indexPath) as! filterGeneralInfoCVC
            
            if self.viewDaysArray[indexPath.item].isSelected == true {
                cell.cellBoxImage.image = UIImage(named: "radio-button")
            }
            else {
                cell.cellBoxImage.image = UIImage(named: "non select")
            }
            
            cell.cellLBL.text = "\(self.viewDaysArray[indexPath.row].value ?? "")"
            
            //Set Font
            cell.cellLBL.font = UIFont(name: cell.cellLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
            
            return cell
        }
        else if collectionView == self.addedByCollectionview {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "filterGeneralInfoCVC", for: indexPath) as! filterGeneralInfoCVC
            
            if self.AddedByArray[indexPath.item].isSelected == true {
                cell.cellBoxImage.image = UIImage(named: "radio-button")
            }
            else {
                cell.cellBoxImage.image = UIImage(named: "non select")
            }
            
            cell.cellLBL.text = "\(self.AddedByArray[indexPath.row].value ?? "")"
            
            //Set Font
            cell.cellLBL.font = UIFont(name: cell.cellLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
            
            return cell
        }
        else if collectionView == self.negotiablePriceColletionview {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "filterGeneralInfoCVC", for: indexPath) as! filterGeneralInfoCVC
            
            if self.negotiablePriceArray[indexPath.item].isSelected == true {
                cell.cellBoxImage.image = UIImage(named: "radio-button")
            }
            else {
                cell.cellBoxImage.image = UIImage(named: "non select")
            }
            
            cell.cellLBL.text = "\(self.negotiablePriceArray[indexPath.row].value ?? "")"
            
            //Set Font
            cell.cellLBL.font = UIFont(name: cell.cellLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
            
            return cell
        }
        else {
            
            var dataArray : [dynamicFilterDictionary]!
            
            if (collectionView.restorationIdentifier?.elementsEqual("features"))! {
                dataArray = self.dynamicInfoFeaturesDataArray
            }
            else {
                dataArray = self.dynamicInfoStatusDataArray
            }
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "checkboxCVC", for: indexPath)
            
            for view in cell.subviews {
                view.removeFromSuperview()
            }
            
            let checkboxImage : UIImageView = {
                let checkboximage = UIImageView()
                checkboximage.contentMode = .scaleAspectFit
                checkboximage.translatesAutoresizingMaskIntoConstraints = false
                return checkboximage
            }()
            
            cell.addSubview(checkboxImage)
            
            checkboxImage.anchor(top: cell.topAnchor, leading: cell.leadingAnchor, bottom: cell.bottomAnchor, trailing: nil, padding: .init(top: 10, left: 5, bottom: 10, right: 0), size: .init(width: 25, height: 25))
            
            
            let cellLBL : UILabel = {
                let celllbl = UILabel()
                celllbl.numberOfLines = 0
                celllbl.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 11)))
                celllbl.translatesAutoresizingMaskIntoConstraints = false
                return celllbl
            }()
            
            cell.addSubview(cellLBL)
            
            cellLBL.anchor(top: cell.topAnchor, leading: checkboxImage.trailingAnchor, bottom: cell.bottomAnchor, trailing: cell.trailingAnchor, padding: .init(top: 0, left: 5, bottom: 0, right: 10))
            
            
            
            if (collectionView.accessibilityIdentifier?.elementsEqual("checkbox"))! {
                
                if dataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected == true {
                    checkboxImage.image = UIImage(named: "checkbox2")
                }
                else {
                    checkboxImage.image = UIImage(named: "Rectangle 22")
                }
            }
            else {
                
                if dataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected == true {
                    checkboxImage.image = UIImage(named: "select black")
                }
                else {
                    checkboxImage.image = UIImage(named: "non select")
                }
            }
            
            cellLBL.text = "\(dataArray[collectionView.tag].fieldOptionArray[indexPath.item].value ?? "")"
            
            return cell
            
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
//        if collectionView == addedDateCollectionview || collectionView == addedByCollectionview || collectionView == negotiablePriceColletionview {
//
//            return CGSize(width: (self.FullWidth - 40)/2, height: 40)
//
//        }
//        else {
//
//            return CGSize(width: (self.FullWidth - 0)/2, height: 40)
//
//        }
        
        return CGSize(width: (self.FullWidth - 40)/2, height: 40)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.addedDateCollectionview {
            
            for i in 0..<self.viewDaysArray.count {
                self.viewDaysArray[i].isSelected = false
            }
            self.viewDaysArray[indexPath.item].isSelected = true
            
            self.viewDays = "\(self.viewDaysArray[indexPath.row].key ?? "")"
            
            self.addedDateCollectionview.reloadData()
        }
        else if collectionView == self.addedByCollectionview {
            
            for i in 0..<self.AddedByArray.count {
                self.AddedByArray[i].isSelected = false
            }
            self.AddedByArray[indexPath.item].isSelected = true
            
            self.addedBy = "\(self.AddedByArray[indexPath.row].key ?? "")"
            
            self.addedByCollectionview.reloadData()
        }
        else if collectionView == self.negotiablePriceColletionview {
            for i in 0..<self.negotiablePriceArray.count {
                self.negotiablePriceArray[i].isSelected = false
            }
            self.negotiablePriceArray[indexPath.item].isSelected = true
            
            self.negotiablePrice = "\(self.negotiablePriceArray[indexPath.row].key ?? "")"
            
            self.negotiablePriceColletionview.reloadData()
        }
        else {
            
            if (collectionView.accessibilityIdentifier?.elementsEqual("checkbox"))! {
                
                if (collectionView.restorationIdentifier?.elementsEqual("features"))! {
                    self.dynamicInfoFeaturesDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected = !self.dynamicInfoFeaturesDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected
                    print(self.dynamicInfoFeaturesDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected!)
                }
                else {
                    self.dynamicInfoStatusDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected = !self.dynamicInfoStatusDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected
                }
                
            }
            else {
                
                if (collectionView.restorationIdentifier?.elementsEqual("features"))! {
                    
                    for i in 0..<self.dynamicInfoFeaturesDataArray[collectionView.tag].fieldOptionArray.count {
                        self.dynamicInfoFeaturesDataArray[collectionView.tag].fieldOptionArray[i].isSelected = false
                    }
                    
                    self.dynamicInfoFeaturesDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected = !self.dynamicInfoFeaturesDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected
                }
                else {
                    
                    for i in 0..<self.dynamicInfoStatusDataArray[collectionView.tag].fieldOptionArray.count {
                        self.dynamicInfoStatusDataArray[collectionView.tag].fieldOptionArray[i].isSelected = false
                    }
                    
                    self.dynamicInfoStatusDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected = !self.dynamicInfoStatusDataArray[collectionView.tag].fieldOptionArray[indexPath.item].isSelected
                }
                
            }
            
            collectionView.reloadData()
        }
        
    }
    
    
}
