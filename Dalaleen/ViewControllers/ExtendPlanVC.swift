//
//  ExtendPlanVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 23/08/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import StoreKit

class ExtendPlanVC: GlobalViewController {

    
    @IBOutlet weak var adsCostView: UIView!
    @IBOutlet weak var adsCostTitleLBL: UILabel!
    @IBOutlet weak var adsCostTableview: UITableView!
    
    @IBOutlet weak var viewOfPickerview: UIView!
    @IBOutlet weak var planDurationPickerview: UIPickerView!
    
    
    @IBOutlet weak var currentPlanView: UIView!
    @IBOutlet weak var currentPlanTitle: UILabel!
    @IBOutlet weak var currentPlanName: UILabel!
    @IBOutlet weak var currentPlanPeriod: UILabel!
    @IBOutlet weak var currentPlanCost: UILabel!
    @IBOutlet weak var currentPlanExpiry: UILabel!
    
    
    @IBOutlet weak var changePlanStaticView: UIView!
    @IBOutlet weak var changePlanLBL: UILabel!
    
    @IBOutlet weak var changedPlanView: UIView!
    @IBOutlet weak var changedPlanTitle: UILabel!
    @IBOutlet weak var changedPlanName: UILabel!
    @IBOutlet weak var changedPlanPeriod: UILabel!
    @IBOutlet weak var changedPlanCost: UILabel!
    @IBOutlet weak var changedPlanExpiry: UILabel!
    
    @IBOutlet weak var totalCostView: UIView!
    @IBOutlet weak var totalCostStaticLBL: UILabel!
    @IBOutlet weak var totalCost: UILabel!
    
    
    
    
    var planDataArray = [planTypeDataFormat]()
    
    var planDurationArray = [planDurationDataForamt]()
    
    var planPriceID : String! = ""
    
    var propertyID : String! = ""
    
    var currentPlanDictionary : NSDictionary!
    
    var newPlanPriceID : String!
    
    
    var selectedPickerview : UIPickerView!
    var selectedPickerIndex : Int!
    var doneClicked : Bool!
    var subPlanId : String! = "999"
    
    var planIdArray = ["com.esolz.Dalaleen.gold30","com.esolz.Dalaleen.gold180","com.esolz.Dalaleen.gold365","com.esolz.Dalaleen.urgent30","com.esolz.Dalaleen.urgent180","com.esolz.Dalaleen.urgent365"]
    
    
    // MARK:- // Create Picker Toolbar
    
    func createToolbar() {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.tintColor = UIColor.white//"Done" button colour
        toolBar.barTintColor = UIColor.black// bar background colour
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "\(globalFunctions.shared.getLanguageString(variable: "ios_ok"))", style: UIBarButtonItem.Style.done, target: self, action: #selector(donePicker))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "\(globalFunctions.shared.getLanguageString(variable: "modal_button_text1"))", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelPicker))
        
        toolBar.setItems([cancelButton, spacer, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.viewOfPickerview.addSubview(toolBar)
        toolBar.anchor(top: nil, leading: nil, bottom: self.planDurationPickerview.topAnchor, trailing: nil, size: .init(width: UIScreen.main.bounds.size.width, height: 40))
        
    }
    
    
    // MARK:- // Done Picker Action Method
    
    @objc func donePicker() {
        
        print("Done Clicked")
        
        self.doneClicked = true
        
        self.pickerView(self.selectedPickerview, didSelectRow: self.selectedPickerIndex, inComponent: 0)
        
        self.view.sendSubviewToBack(self.viewOfPickerview)
        self.viewOfPickerview.isHidden = true
        self.selectedPickerview.isHidden = true
        
    }
    
    // MARK:- // Cancel Picker Action Method
    
    @objc func cancelPicker() {
        print("Cancel Clicked")
        
        self.view.sendSubviewToBack(self.viewOfPickerview)
        self.viewOfPickerview.isHidden = true
        self.selectedPickerview.isHidden = true
    }
    
    
    //MARK:- In app-purchase variabel
    var subscriptionContent : SubscriptionDataGroup!
    
    var products: [SKProduct] = []
    
    var purchaseIndex : Int!
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for transactionPending in SKPaymentQueue.default().transactions {
            SKPaymentQueue.default().finishTransaction(transactionPending)
        }
        
        globalFunctions.shared.loadPlaceholder(onView: self.view)
        
        self.creaeteSubscriptionFunc()
        
        self.setFont()
        
        self.localizeStrings()

        self.getPlanData()
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            self.getCurrentPlan()
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                self.fillPlanInformation()
                
                globalFunctions.shared.removePlaceholder(fromView: self.view)
                
            })
            
        }
        
        // Create toolbar
        self.createToolbar()
        
    }
    
    
    
    // MARK:- // Buttons
    
    
    // MARK:- // Cancel Button
    
    @IBOutlet weak var cancelButtonOutlet: UIButton!
    @IBAction func cancelButton(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    // MARK:- // Update Button
    
    @IBOutlet weak var updateButtonOutlet: UIButton!
    @IBAction func updateButton(_ sender: UIButton) {
        
        if self.subPlanId != "999" {
            let index = self.getIndexFromProductId(productId: self.subPlanId)
            self.purchaseItemIndex(index: index) {
                
                self.updateNewPlan()
                
                self.globalDispatchgroup.notify(queue: .main) {
                    
                    self.customAlert(title: "", message: "\(self.globalJson["message"] ?? "")", doesCancel: false, completion: {
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    })
                    
                }
            }
            
        }
        else {
            self.updateNewPlan()
            
            self.globalDispatchgroup.notify(queue: .main) {
                
                self.customAlert(title: "", message: "\(self.globalJson["message"] ?? "")", doesCancel: false, completion: {
                    
                    self.navigationController?.popViewController(animated: true)
                    
                })
                
            }
        }
        

        
    }
    
    
    
    
    // MARK:- // Close Pickerview Button
    
    @IBOutlet weak var closePickerviewButtonOutlet: UIButton!
    @IBAction func closePickerviewButton(_ sender: UIButton) {
        
        self.view.sendSubviewToBack(self.viewOfPickerview)
        self.viewOfPickerview.isHidden = true
        self.planDurationPickerview.isHidden = true
        
    }
    
    
    //MARK:- //Create subscription function
    func creaeteSubscriptionFunc(){
        // Creating Subscription Data
        //self.createSubscriptionData()
        
        // Rest of the work
        SubscriptionProducts.store.requestProducts { [weak self] success, products in
            guard let self = self else { return }
            guard success else {
                let alertController = UIAlertController(title: "Failed to load list of products",
                                                        message: "Check logs for details",
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .default))
                self.present(alertController, animated: true, completion: nil)
                return
            }
            self.products = products!
        }
        
        if SubscriptionProducts.store.isProductPurchased(SubscriptionProducts.oneMonthSubUrgent) {
            self.displaySubscriptionDetails(index: 0)
        }
        else if SubscriptionProducts.store.isProductPurchased(SubscriptionProducts.sixMonthsSubUrgent) {
            self.displaySubscriptionDetails(index: 1)
        }
        else if SubscriptionProducts.store.isProductPurchased(SubscriptionProducts.oneYearSubUrgent) {
            self.displaySubscriptionDetails(index: 2)
        }
        else if SubscriptionProducts.store.isProductPurchased(SubscriptionProducts.oneMonthSubGold) {
                self.displaySubscriptionDetails(index: 3)
        }
        else if SubscriptionProducts.store.isProductPurchased(SubscriptionProducts.sixMonthsSubGold) {
            self.displaySubscriptionDetails(index: 4)
        }
        else if SubscriptionProducts.store.isProductPurchased(SubscriptionProducts.oneYearSubGold) {
            self.displaySubscriptionDetails(index: 5)
        }
        else {
            self.displaySubscriptionAlert()
        }
    }
    
    // MARK:- // Create Subscription Data
    
    func createSubscriptionData() {
        
        self.subscriptionContent = SubscriptionDataGroup(subscriptionStrings: ["You have subscribed for 1 month.","You have subscribed for 1 year.","You have subscribed for 6 months."])
        
    }
    
    
    // MARK:- // Display Subscription Alert.
    
    func displaySubscriptionAlert() {
        
        //self.mainLBL.text = "You have not purchased any subscription yet."
        
    }
    
    // MARK:- // Display Subscription Details
    
    func displaySubscriptionDetails(index : Int) {
        
        //self.mainLBL.text = "\(self.subscriptionContent.subscriptionStrings[index])"
        
    }
    
    
    // MARK:- // Purchase Item Index
    
    func purchaseItemIndex(index: Int , completion : @escaping ()->()) {
        
        startAnimating(CGSize(width: ((30.0/320.0)*FullWidth), height: ((30.0/320.0)*FullWidth)), message: "", messageFont: UIFont(name: "Lato-Bold", size: 14), type: .ballClipRotateMultiple, color: UIColor(displayP3Red: 0/255, green: 135/255, blue: 228/255, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.4), textColor: .black, fadeInAnimation: nil)
    
        //print(indx)
        
        DispatchQueue.main.async {
            SubscriptionProducts.store.buyProduct(self.products[index]) { [weak self] success, productId in
                guard let self = self else { return }
                guard success else {
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    let alertController = UIAlertController(title: "Failed to purchase product",
                                                            message: "Check logs for details",
                                                            preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(alertController, animated: true, completion: nil)
                    return
                }
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                completion()
                
            }
        }
        
        
    }
    
    
    
    
    // MARK:- // JSON Post Method to get Plan Data
    
    func getPlanData() {
        
        var parameters : String!
        
        parameters = "type=new&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "propertyPlan", param: parameters) {
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                
                var tempArray = [planDurationDataForamt]()
                
                for j in 0..<(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["plan_price_list"] as! NSArray).count {
                    
                    let tempDictionary = (((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["plan_price_list"] as! NSArray)[j] as! NSDictionary
                    
                    tempArray.append(planDurationDataForamt(planPriceID: "\(tempDictionary["plan_price_id"] ?? "")", planCost: "\(tempDictionary["cost"] ?? "")", planDuration: "\(tempDictionary["duration"] ?? "")"))
                }
                
                let tempDict = (self.globalJson["info"] as! NSArray)[i] as! NSDictionary
                
                self.planDataArray.append(planTypeDataFormat(planID: "\(tempDict["id"] ?? "")", planName: "\(tempDict["plan_name"] ?? "")", planValidUnit: "\(tempDict["plan_valid_unit"] ?? "")", planPriceList: tempArray, planDuration: "", isSelected: false , planCost: "" , planPriceID: ""))
                
            }
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.adsCostTableview.delegate = self
                self.adsCostTableview.dataSource = self
                self.adsCostTableview.reloadData()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    
    
    
    // MARK:- // JSON Post Method to get Current Plan
    
    func getCurrentPlan() {
        
        var parameters : String!
        
        parameters = "user_id=\(userInformation.shared.userID ?? "")&property_id=\(self.propertyID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "extend_cost_info", param: parameters) {
            
            self.currentPlanDictionary = self.globalJson["info"] as? NSDictionary
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
            }
            
        }
        
    }
    
    
    
    // MARK:- // JSON Post Method to Update New plan
    
    func updateNewPlan() {
        
        var parameters : String!
        
        parameters = "property_id=\(self.propertyID ?? "")&dalaleen_user_id=\(userInformation.shared.userID ?? "")&plan_price=\(self.newPlanPriceID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "extend_cost_update", param: parameters) {
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
            }
            
        }
        
    }
    
    
    
    
    
    // MARK:- // Fill Plan Information
    
    func fillPlanInformation() {
        
        self.changePlanStaticView.isHidden = true
        self.changedPlanView.isHidden = true
        
        self.currentPlanName.text = "\(globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_plan")) : " + "\(self.currentPlanDictionary["plan_name"] ?? "")".uppercased()
        self.currentPlanPeriod.text = "\(globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_period")) : \(self.currentPlanDictionary["duration"] ?? "") Days"
        self.currentPlanCost.text = "\(globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_cost")) : \(self.currentPlanDictionary["cost"] ?? "") USD"
        self.currentPlanExpiry.text = "\(self.currentPlanDictionary["msg"] ?? "")"
        
        
        // Ads Cost Table Section
        for i in 0..<self.planDataArray.count {
            if (self.planDataArray[i].planID?.elementsEqual("\(self.currentPlanDictionary["plan_id"] ?? "")"))! {
                self.planDataArray[i].isSelected = true
                
                for j in 0..<self.planDataArray[i].planPriceList.count {
                    if (self.planDataArray[i].planPriceList[j].planPriceID?.elementsEqual("\(self.currentPlanDictionary["plan_price_id"] ?? "")"))! {
                        
                        self.planDataArray[i].planDuration = "\(self.planDataArray[i].planPriceList[j].planDuration ?? "")"  + " \(self.planDataArray[i].planValidUnit ?? "")"
                        self.planDataArray[i].planCost = "=\(self.planDataArray[i].planPriceList[j].planCost ?? "")$"
                        self.planDataArray[i].planPriceID = "\(self.planDataArray[i].planPriceList[j].planPriceID ?? "")"
                        
                        self.adsCostTableview.reloadData()
                        
                        break
                        
                    }
                }
            }
        }
        
    }
    
    
    // MARK:- // Set data for changed plan
    
    func setDataForChangedPlan(planArray : planDurationDataForamt , planIndex : Int , planPriceIndex : Int) {
        if planIndex == 1 {
            self.subPlanId = planIdArray[planPriceIndex]
        }
        else {
            self.subPlanId = planIdArray[planPriceIndex + 3]
        }
        self.changePlanLBL.text = "\(globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_text9")) " + "\(self.currentPlanDictionary["plan_name"] ?? "")".uppercased() + " \(globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_text10"))"
        
        self.changedPlanName.text = "\(globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_plan")) : " + "\(self.planDataArray[planIndex].planName ?? "")".uppercased()
        self.changedPlanPeriod.text = "\(globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_period")) : \(self.planDataArray[planIndex].planPriceList[planPriceIndex].planDuration ?? "")" + " \(self.planDataArray[planIndex].planValidUnit ?? "")"
        self.changedPlanCost.text = "\(globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_cost")) : \(self.planDataArray[planIndex].planPriceList[planPriceIndex].planCost ?? "") USD"
        
        
        // CALCULATING EXPIRY DATE
        let daysToAdd = Int("\(self.planDataArray[planIndex].planPriceList[planPriceIndex].planDuration ?? "")")
        let currentDate = Date()
        
        var dateComponent = DateComponents()
        dateComponent.day = daysToAdd
        
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
        
        let tempFormatter = DateFormatter()
        tempFormatter.dateFormat = "dd MMMM, yyyy"
        let dateString = tempFormatter.string(from: futureDate!)
        
        self.changedPlanExpiry.text = "\(globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_text11"))" + " \(self.planDataArray[planIndex].planName ?? "")".uppercased() + " \(globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_text12"))" + " \(dateString)."
        
        self.totalCost.text = "= \(self.planDataArray[planIndex].planPriceList[planPriceIndex].planCost ?? "")$"
        
        self.newPlanPriceID = "\(self.planDataArray[planIndex].planPriceList[planPriceIndex].planPriceID ?? "")"
        
    }
    
    
    
    
    // MARK:- // Plan Type Button Action
    
    @objc func planTypeButtonAction(sender : UIButton) {
        
        for i in 0..<self.planDataArray.count {
            self.planDataArray[i].isSelected = false
        }
        self.planDataArray[sender.tag].isSelected = true
        
        for i in 0..<self.planDataArray.count {
            if i != sender.tag {
                self.planDataArray[i].planDuration = ""
                self.planDataArray[i].planCost = ""
                self.planDataArray[i].planPriceID = ""
            }
            
        }
        
        if "\(self.planDataArray[sender.tag].planID ?? "")".elementsEqual("1") {
            
            self.planDurationArray = self.planDataArray[sender.tag].planPriceList
            
            self.planDataArray[sender.tag].planDuration = "\(self.planDurationArray[0].planDuration ?? "")"  + " \(self.planDataArray[sender.tag].planValidUnit ?? "")"
            self.planDataArray[sender.tag].planCost = "=\(self.planDurationArray[0].planCost ?? "")$"
            self.planDataArray[sender.tag].planPriceID = "\(self.planDurationArray[0].planPriceID ?? "")"
            
            //self.adsCostTableview.reloadData()
            
            
            self.setDataForChangedPlan(planArray: self.planDurationArray[0], planIndex: sender.tag, planPriceIndex: 0)
            
            if "\(self.currentPlanDictionary["plan_price_id"] ?? "")".elementsEqual("\(self.planDurationArray[0].planPriceID ?? "")") {
                self.changePlanStaticView.isHidden = true
                self.changedPlanView.isHidden = true
                
                self.totalCost.text = "= 0$"
                
                self.updateButtonOutlet.isHidden = true
            }
            else {
                self.changePlanStaticView.isHidden = false
                self.changedPlanView.isHidden = false
                
                self.updateButtonOutlet.isHidden = false
            }
            
        }
        
        self.adsCostTableview.reloadData()
        
    }
    
    // MARK:- // Plan Duration Button Action
    
    @objc func planDurationButtonAction(sender : UIButton) {
        
        self.view.bringSubviewToFront(self.viewOfPickerview)
        self.viewOfPickerview.isHidden = false
        self.planDurationPickerview.isHidden = false
        
        self.planDurationPickerview.selectRow(0, inComponent: 0, animated: false)
        
        self.selectedPickerview = self.planDurationPickerview
        self.selectedPickerIndex = 0
        self.doneClicked = false
        
        
        self.planDurationPickerview.tag = sender.tag
        
        self.planDurationArray = self.planDataArray[sender.tag].planPriceList
        
        self.planDurationPickerview.delegate = self
        self.planDurationPickerview.dataSource = self
        self.planDurationPickerview.reloadAllComponents()
        
    }
    
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.adsCostTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "app_msg8")
        self.currentPlanTitle.text = globalFunctions.shared.getLanguageString(variable: "extend_cost_text2")
        self.changedPlanTitle.text = globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_new_plan")
        
        self.totalCostStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "property_post_total_cost")
        self.totalCost.text = "= 0$"
        
        self.cancelButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "property_details_cost_extend_cancel"), for: .normal)
        self.updateButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "property_details_cost_extend_update"), for: .normal)
        
    }
    
    
    
    // MARK:- // Set Font
    
    func setFont() {
        
        self.adsCostTitleLBL.font = UIFont(name: self.adsCostTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.currentPlanTitle.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.currentPlanName.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.currentPlanPeriod.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.currentPlanCost.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.currentPlanExpiry.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.changedPlanTitle.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.changedPlanName.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.changedPlanPeriod.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.changedPlanCost.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.changedPlanExpiry.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
        self.changePlanLBL.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
        self.totalCostStaticLBL.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.totalCost.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
        self.cancelButtonOutlet.titleLabel!.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.updateButtonOutlet.titleLabel!.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
    }
    
    //MARK:- Get Index from product Id
    func getIndexFromProductId(productId: String) -> Int{
        var index: Int?
        for i in 0..<products.count{
            if self.subPlanId == products[i].productIdentifier{
                index = i
                break
            }
        }
        //print(index!)
        return index!
    }
    

}




// MARK:- // Tableview Delegate Methods

extension ExtendPlanVC: UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.planDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "addRealEstatePlanTypeTVC") as! addRealEstatePlanTypeTVC
        
        cell.planDaysView.layer.cornerRadius = 5
        cell.planDaysView.layer.borderColor = UIColor.lightGray.cgColor
        cell.planDaysView.layer.borderWidth = 0.5
        
        if self.planDataArray[indexPath.row].isSelected == true {
            cell.checkBoxButton.setImage(UIImage(named: "checkbox2"), for: .normal)
            cell.planDurationButton.isUserInteractionEnabled = true
        }
        else {
            cell.checkBoxButton.setImage(UIImage(named: "Rectangle 22"), for: .normal)
            cell.planDurationButton.isUserInteractionEnabled = false
        }
        
        if (self.planDataArray[indexPath.row].planID?.elementsEqual("1"))! {
            
            cell.planName.text = "\(self.planDataArray[indexPath.row].planName ?? "")" + " (" + "\(self.planDataArray[indexPath.row].planPriceList[0].planDuration ?? "") \(self.planDataArray[indexPath.row].planValidUnit ?? "")" + ")"
            
            cell.planDurationButton.isHidden = true
            cell.planDaysView.isHidden = true
            cell.planCost.isHidden = true
            
            cell.planNameWidthCOnstraint.constant = 100
            
        }
        else {
            
            cell.planName.text = "\(self.planDataArray[indexPath.row].planName ?? "")"
            
            cell.planDays.text = "\(self.planDataArray[indexPath.row].planDuration ?? "")"
            
            cell.planCost.text = "\(self.planDataArray[indexPath.row].planCost ?? "")"
            
            cell.planDurationButton.isHidden = false
            cell.planDaysView.isHidden = false
            cell.planCost.isHidden = false
            
            cell.planNameWidthCOnstraint.constant = 65
            
        }
        
        
        
        
        cell.checkBoxButton.addTarget(self, action: #selector(self.planTypeButtonAction(sender:)), for: .touchUpInside)
        cell.checkBoxButton.tag = indexPath.row
        
        cell.planDurationButton.addTarget(self, action: #selector(self.planDurationButtonAction(sender:)), for: .touchUpInside)
        cell.planDurationButton.tag = indexPath.row
        
        
        //
        cell.selectionStyle = .none
        
        //SET FONT
        cell.planName.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        cell.planDays.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        cell.planCost.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
        
    }
    
}




// MARK:- // Pickerview Delegate Methods

extension ExtendPlanVC: UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.planDurationArray.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return "\(self.planDurationArray[row].planDuration ?? "")"
       
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if self.doneClicked {
            
            self.planDataArray[pickerView.tag].planDuration = "\(self.planDurationArray[row].planDuration ?? "")"  + " \(self.planDataArray[pickerView.tag].planValidUnit ?? "")"
            self.planDataArray[pickerView.tag].planCost = "=\(self.planDurationArray[row].planCost ?? "")$"
            self.planDataArray[pickerView.tag].planPriceID = "\(self.planDurationArray[row].planPriceID ?? "")"
            
            self.view.sendSubviewToBack(self.viewOfPickerview)
            self.viewOfPickerview.isHidden = true
            self.planDurationPickerview.isHidden = true
            
            self.adsCostTableview.reloadData()
            
            
            self.setDataForChangedPlan(planArray: self.planDurationArray[row], planIndex: pickerView.tag, planPriceIndex: row)
            
            if "\(self.currentPlanDictionary["plan_price_id"] ?? "")".elementsEqual("\(self.planDurationArray[row].planPriceID ?? "")") {
                self.changePlanStaticView.isHidden = true
                self.changedPlanView.isHidden = true
                
                self.totalCost.text = "= 0$"
                
                self.updateButtonOutlet.isHidden = true
            }
            else {
                self.changePlanStaticView.isHidden = false
                self.changedPlanView.isHidden = false
                
                self.updateButtonOutlet.isHidden = false
            }
            
        }
        else {
            self.selectedPickerIndex = row
        }
        
        
    }
    
}
