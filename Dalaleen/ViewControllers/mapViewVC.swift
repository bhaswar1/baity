//
//  mapViewVC.swift
//  Dalaleen
//
//  Created by esolz on 30/01/20.
//  Copyright © 2020 esolz. All rights reserved.
//

import UIKit
import MapKit

class mapViewVC: GlobalViewController {

    @IBOutlet weak var openMapOut: UIButton!
    @IBOutlet weak var mapViewOut: GMSMapView!
    var marker = GMSMarker()
    
    let addressLat = "\(UserDefaults.standard.string(forKey: "lat") ?? "")"
    let addressLong = "\(UserDefaults.standard.string(forKey: "long") ?? "")"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showLocation()
        openMapOut.isHidden = true
        mapViewOut.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismissVCBtnAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func openMap(_ sender: Any) {
        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
            UIApplication.shared.open(NSURL(string: "comgooglemaps://?saddr=&daddr=\(addressLat),\(addressLat)&directionsmode=driving")! as URL)
        } else {
            NSLog("Can't use comgooglemaps://");
            
            let latitude: CLLocationDegrees = Double(addressLat)!
            let longitude: CLLocationDegrees = Double(addressLong)!
            
            let regionDistance:CLLocationDistance = 10000
            let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
            let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = ""
            mapItem.openInMaps(launchOptions: options)
            
        }
    }
    
    
    // MARK:- // Functions to show location
    
    
    func showLocation()
    {
        print(addressLat,addressLong)
        let camera = GMSCameraPosition.camera(withLatitude: Double(addressLat)!, longitude: Double(addressLong)!, zoom: 10.0)
        self.mapViewOut.animate(to: camera)
        self.mapViewOut.camera = camera
        
        //marker.position = CLLocationCoordinate2D(latitude: Double(addressLat)!, longitude: Double(addressLong)!)
        marker.position = self.mapViewOut.camera.target
        //        marker.title = "Selected Address"
        //        marker.snippet = "Description"
        marker.map = self.mapViewOut
        self.mapViewOut.mapType = .normal
        self.mapViewOut.isUserInteractionEnabled = true
    }

}
extension mapViewVC: GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        //let tempInt : Int = (marker.userData as! [String : Any])["tag"] as! Int
        
        //let nextItem: IndexPath = IndexPath(item: tempInt, section: 0)
        
        self.openMapOut.isHidden = false
        

        return true
        
    }
}
