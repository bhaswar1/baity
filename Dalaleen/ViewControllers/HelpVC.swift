//
//  HelpVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 16/10/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit

class HelpVC: GlobalViewController {

    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setFont()
        
        self.localizeStrings()
        
        
        
    }
    
    
    
    
    // MARK:- // Buttons
    
    //MARK:- // About US Button
    
    @IBOutlet weak var aboutUsButtonOutlet: UIButton!
    @IBAction func aboutUsButton(_ sender: UIButton) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "WebkitVC") as! WebkitVC
        
        navigate.staticParam = "about_us"
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    // MARK:- // Privacy Policy Button
    
    @IBOutlet weak var privacyPolicyButtonOutlet: UIButton!
    @IBAction func privacyPolicyButton(_ sender: UIButton) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "WebkitVC") as! WebkitVC
        
        navigate.staticParam = "privacy_policy"
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
    // MARK:- // Set Font
    
    func setFont() {
        
        self.aboutUsButtonOutlet.titleLabel!.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 16)))
        self.privacyPolicyButtonOutlet.titleLabel!.font = UIFont(name: "Lato-Bold", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 16)))
        
    }
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.aboutUsButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "footer_about_us"), for: .normal)
        self.privacyPolicyButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "footer_privacy_policy"), for: .normal)
        
    }

    

}



extension String{
    func applyPatternOnNumbers(pattern: String) -> String {
        let replacmentCharacter: Character = "#"
        let pureNumber = self//.replacingOccurrences( of: "[^۰-۹0-9]", with: "", options: .regularExpression)
        var result = ""
        var pureNumberIndex = pureNumber.startIndex
        for patternCharacter in pattern {
            if patternCharacter == replacmentCharacter {
                guard pureNumberIndex < pureNumber.endIndex else { return result }
                result.append(pureNumber[pureNumberIndex])
                pureNumber.formIndex(after: &pureNumberIndex)
            } else {
                result.append(patternCharacter)
            }
        }
        
        print("Result \(result)")
        return result
        
        
    }
}





extension String{
    func creatingPriceStringLikeDecimal() -> String {
        
        let priceString = self.replacingOccurrences(of: ",", with: "")
        
        var result : String! = ""
        
        let StringArray = Array(String(priceString.reversed()))
        
        for i in 0..<StringArray.count {
            
            if i == 0 {
                result = result + "\(StringArray[i])"
            }
            else {
                if i % 3 == 0 {
                    result = result + ",\(StringArray[i])"
                }
                else {
                    result = result + "\(StringArray[i])"
                }
            }
            
        }
        
        result = String(result.reversed())
        
        return result
        
    }
}
