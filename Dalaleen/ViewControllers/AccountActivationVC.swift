//
//  AccountActivationVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 08/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView
import FirebaseUI

class AccountActivationVC: GlobalViewController {
    
    @IBOutlet weak var accountActivationTitleLBL: UILabel!
    @IBOutlet weak var accountActivationSubtitleLBL: UILabel!
    @IBOutlet weak var successMessageView: UIView!
    @IBOutlet weak var successMessageLBL: UILabel!
    @IBOutlet weak var enterActivationCodeVIew: UIView!
    @IBOutlet weak var enterActivationCodeTXT: UITextField!
    @IBOutlet weak var enterActivationcodeShadowview: ShadowView!
    @IBOutlet weak var submitButtonShadowview: ShadowView!
    @IBOutlet weak var resendButtonShadowview: UIView!
    @IBOutlet weak var resendView: UIView!
    @IBOutlet weak var resendStaticLbl: UILabel!
    @IBOutlet weak var resendCounterLbl: UILabel!
    
    
    
    let VCDispatchGroup = DispatchGroup()
    
    var loginResponseDictionary : NSDictionary!
    
    var userInformationDictionary : NSDictionary!
    
    var verificationID : String!
    
    var signUpResponseDictionary : NSDictionary!
    
    var SignUpPhone : String? = ""
    var SignUpName : String? = ""
    var SignUpEmail : String? = ""
    var SignUpPhoneCode : String? = ""
    var SignUpPassword : String? = ""
    var SignUpType : String? = ""
    var SignUpCountryName : String? = ""
    
    var profileID : String? = ""
    var profileName : String? = ""
    var profileEmail : String? = ""
    var profileAccountType : String? = ""
    var profileCountry : String? = ""
    var profileProvince : String? = ""
    var profileCity : String? = ""
    var profileDistrict : String? = ""
    var profilePhoneNumber : String? = ""
    var profilePhoneCode : String? = ""
    var profileWorkPhone : String? = ""
    var profileFax : String? = ""
    var profileStreet : String? = ""
    var profileLat : String? = ""
    var profileLong : String? = ""
    var profileBannerImageData : String? = ""
    
    
    
    
    var activationResponseDictionary : NSDictionary!
    
    var phoneCode : String! = ""
    var phoneNumber : String! = ""
    
    
    // MARK:- // View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.PhoneVerify()
        print("VerificationID",self.verificationID ?? "")
        self.SetFont()
        self.localizeStrings()
        self.enterActivationCodeVIew.layer.cornerRadius = self.enterActivationCodeVIew.frame.size.height / 2
        self.enterActivationcodeShadowview.layer.cornerRadius = self.enterActivationcodeShadowview.frame.size.height / 2
        self.submitButtonOutlet.layer.cornerRadius = self.submitButtonOutlet.frame.size.height / 2
        self.submitButtonShadowview.layer.cornerRadius = self.submitButtonShadowview.frame.size.height / 2
        self.resendButtonOutlet.layer.cornerRadius = self.resendButtonOutlet.frame.size.height / 2
        self.resendView.layer.cornerRadius = self.resendView.frame.size.height / 2
        
        self.resendButtonOutlet.isUserInteractionEnabled = false
        self.resendStaticLbl.isHidden = true
        
        self.startTimer()
    }
    
    // MARK:- // Successful Login Actions
    
    func successfulLoginActions() {
        
        print("ProvinceID","\((self.userInformationDictionary["info"] as! NSDictionary)["province_id"] ?? "")")
        
        UserDefaults.standard.set(self.SignUpCountryName, forKey: "userCountryName")
        UserDefaults.standard.set(self.SignUpPhoneCode, forKey: "userPhoneCode")
        UserDefaults.standard.set(self.SignUpPhone, forKey: "userPhoneNumber")
        UserDefaults.standard.set(self.SignUpPassword, forKey: "userPassword")
        
        
        userInformation.shared.setUserID(id: "\((self.userInformationDictionary["info"] as! NSDictionary)["id"] ?? "")")
        userInformation.shared.setUserName(name: "\((self.userInformationDictionary["info"] as! NSDictionary)["name"] ?? "")")
        userInformation.shared.setUserImageString(imageString: "\((self.userInformationDictionary["info"] as! NSDictionary)["photo"] ?? "")")
        userInformation.shared.setUserEmail(email: "\((self.userInformationDictionary["info"] as! NSDictionary)["email"] ?? "")")
        userInformation.shared.setUserPhoneCode(phoneCode: "\((self.userInformationDictionary["info"] as! NSDictionary)["phone_code"] ?? "")")
        userInformation.shared.setUserPhoneNumber(phoneNumber: "\((self.userInformationDictionary["info"] as! NSDictionary)["phone"] ?? "")")
        userInformation.shared.setUserCountryID(countryID: "\((self.userInformationDictionary["info"] as! NSDictionary)["country_code"] ?? "")")
        userInformation.shared.setUserCountryName(countryName: "\((self.userInformationDictionary["info"] as! NSDictionary)["country"] ?? "")")
        userInformation.shared.setUserType(userType: "\((self.userInformationDictionary["info"] as! NSDictionary)["user_type"] ?? "")")
        userInformation.shared.setSuperAgent(superAgent: "\((self.userInformationDictionary["info"] as! NSDictionary)["super_agent"] ?? "")")
        userInformation.shared.setUserWorkPhone(workPhone: "\((self.userInformationDictionary["info"] as! NSDictionary)["work_phone"] ?? "")")
        userInformation.shared.setUserFaxNumber(faxNumber: "\((self.userInformationDictionary["info"] as! NSDictionary)["fax"] ?? "")")
        userInformation.shared.setUserAddress(address: "\((self.userInformationDictionary["info"] as! NSDictionary)["strt_address"] ?? "")")
        userInformation.shared.setUserProvinceID(provinceID: "\((self.userInformationDictionary["info"] as! NSDictionary)["province_id"] ?? "")")
        userInformation.shared.setUserProvinceName(provinceName: "\((self.userInformationDictionary["info"] as! NSDictionary)["province"] ?? "")")
        userInformation.shared.setUserDictrictID(dictrictID: "\((self.userInformationDictionary["info"] as! NSDictionary)["district_id"] ?? "")")
        userInformation.shared.setUserDictrictName(districtName: "\((self.userInformationDictionary["info"] as! NSDictionary)["district"] ?? "")")
        userInformation.shared.setUserCityID(cityID: "\((self.userInformationDictionary["info"] as! NSDictionary)["city_id"] ?? "")")
        userInformation.shared.setUserCityName(cityName: "\((self.userInformationDictionary["info"] as! NSDictionary)["city"] ?? "")")
        userInformation.shared.setUserLatitude(latitude: "\((self.userInformationDictionary["info"] as! NSDictionary)["latitude"] ?? "")")
        userInformation.shared.setUserLongitude(longitude: "\((self.userInformationDictionary["info"] as! NSDictionary)["longitude"] ?? "")")
        
        
        userInformation.shared.setUserInboxCount(inboxCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["inbox_count"] ?? "")")
        userInformation.shared.setUserRecyclebinCount(recyclebinCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["recyclebin_count"] ?? "")")
        userInformation.shared.setUserAdsCount(adsCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["my_real_estate"] ?? "")")
        userInformation.shared.setUserFavouritesCount(favouritesCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["my_fav_real_estate"] ?? "")")
        
        
        userInformation.shared.setUserCountryFlag(countryFlag: UserDefaults.standard.string(forKey: "userCountryFlag")!)
        
        
        UserDefaults.standard.set(true, forKey: "loginStatus")
        
        
        
        
    }
    
    // MARK: - // JSON Get Method to get User Profile Details
    
    
    func loadUserProfileDetails() {
        
        var parameters : String!
        
        parameters = "dalaleen_user_id=\(userInformation.shared.userID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "profile_details", param: parameters) {
            
            self.userInformationDictionary = self.globalJson
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
            }
        }
    }
    
    
    // MARK:- // Verify Phone Number
    
    func PhoneVerify()
    {
        print("number",("\(self.SignUpPhoneCode ?? "")" + "\(self.SignUpPhone ?? "")"))
        PhoneAuthProvider.provider().verifyPhoneNumber(("\(self.SignUpPhoneCode ?? "")" + "\(self.SignUpPhone ?? "")"), uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print(error.localizedDescription)
                //self.showMessagePrompt(error.localizedDescription)
                return
            }
            // Sign in using the verificationID and the code sent to the user
            // ...
            self.verificationID = verificationID
            print("veri",verificationID)
        }
        
    }
    
    
    // MARK:- // Buttons
    
    
    
    var counter = 150
    var resendTimer : Timer?
    
    func startTimer() {
        if resendTimer == nil {
            resendTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
            
        }
        
    }
    
    func stopTimer() {
        if resendTimer != nil {
            self.counter = 150
            resendTimer!.invalidate()
            resendTimer = nil
        }
    }
    
    // MARK:- // Resend Button
    
    @IBOutlet weak var resendButtonOutlet: UIButton!
    @IBAction func resendButton(_ sender: UIButton) {
        
        self.resendButtonOutlet.isUserInteractionEnabled = false
        
        self.PhoneVerify()
        
        self.startTimer()
        
    }
    
    
    // MARK:- // Update Counter
    
    @objc func updateCounter() {
        
        //example functionality
        if counter > 0 {
            self.resendStaticLbl.isHidden = false
            self.resendButtonOutlet.titleLabel!.font = UIFont(name: self.resendButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
            self.resendCounterLbl.text = "\(counter)"
            self.resendButtonOutlet.setTitleColor(.white, for: .normal)
            self.resendView.backgroundColor = .lightGray
            print("\(counter) seconds to the end of the world")
            counter -= 1
        }
        else if counter == 0 {
            self.resendButtonOutlet.titleLabel!.font = UIFont(name: self.resendButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
            self.resendCounterLbl.text = "Resend"
            self.resendButtonOutlet.setTitleColor(.white, for: .normal)
            self.resendView.backgroundColor = .orange
            self.resendButtonOutlet.isUserInteractionEnabled = true
            self.resendStaticLbl.isHidden = true
            self.stopTimer()
            
            
        }
    }
    
    
    // MARK:- // Submit Button
    
    @IBOutlet weak var submitButtonOutlet: UIButton!
    @IBAction func subMitButton(_ sender: UIButton) {
        
        self.submitButtonOutlet.isUserInteractionEnabled = false
        
        if (self.enterActivationCodeTXT.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "resetpass_otp_placeholder"))
            self.submitButtonOutlet.isUserInteractionEnabled = true
        }
        else {

            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: verificationID, verificationCode: "\(self.enterActivationCodeTXT.text ?? "")")
            
            Auth.auth().signIn(with: credential) { (authResult, error) in
                if let error = error {
                    self.ShowAlertMessage(title: "Alert", message: "Something went wrong! Try again later")
                    // ...
                    return
                }
                // User is signed in
                // ...
                self.signUp(completion: {
                    print("signup completed")
                    self.submitButtonOutlet.isUserInteractionEnabled = true
                })
                print("Signed In.....")
                print("UserID----\(authResult?.user.uid)")
            }
        }
    }
    
    
    
    // MARK:- // Back Button
    
    @IBOutlet weak var backButtonOutlet: UIButton!
    @IBAction func backButton(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    // MARK:- // JSON Post Method to Send Sign Up Date
    
    func signUp(completion : @escaping ()->()) {
        
        var parameters : String = ""
        
        parameters = "phone=\(SignUpPhone ?? "")&name=\(SignUpName ?? "")&email=\(SignUpEmail ?? "")&phone_code=\(SignUpPhoneCode ?? "")&password=\(SignUpPassword ?? "")&type=\(self.SignUpType ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "signup", param: parameters) {
            
            self.signUpResponseDictionary = self.globalJson
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                completion()
                
                self.customAlert(title: "", message: "\(self.globalJson["message"] ?? "")", doesCancel: false, completion: {
                    
                    UserDefaults.standard.set(self.SignUpPhone, forKey: "userPhoneNumber")

                    UserDefaults.standard.set(self.SignUpPhoneCode, forKey: "userPhoneCode")
                    
                    UserDefaults.standard.set(self.SignUpPassword, forKey: "userPassword")

                    self.SignInAfterActivation()
                    
                    //self.navigationController?.pushViewController(navigate, animated: true)
                    
                })
                
            }
            
        }
        
    }
    
    func SignInAfterActivation()
    {
        var parameters : String = ""
        
        parameters = "phone_code=\(UserDefaults.standard.string(forKey: "userPhoneCode") ?? "")&phone=\(UserDefaults.standard.string(forKey: "userPhoneNumber") ?? "")&password=\(UserDefaults.standard.string(forKey: "userPassword") ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "login", param: parameters) {
            
            self.loginResponseDictionary = self.globalJson.copy() as? NSDictionary
            
            if "\(self.loginResponseDictionary["response"] ?? "")".elementsEqual("TRUE") {
                
                userInformation.shared.setUserID(id: "\((self.loginResponseDictionary["info"] as! NSDictionary)["id"] ?? "")")
                
                DispatchQueue.main.async {
                    
                    self.globalDispatchgroup.leave()
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    self.globalDispatchgroup.notify(queue: .main, execute: {
                        
                        
                        if "\(self.loginResponseDictionary["response"] ?? "")".elementsEqual("FALSE") {
                            
                            if ("\(self.loginResponseDictionary["reason"]!)".elementsEqual("DEACTIVE")) {
                                
                                self.customAlert(title: "Warning!", message: "\(self.loginResponseDictionary["message"] ?? "")", doesCancel: false, completion: {
                                    
                                })
                            }
                            else if ("\(self.loginResponseDictionary["reason"]!)".elementsEqual("INVALID")) {
                                
                                self.ShowAlertMessage(title: "", message: "\(self.loginResponseDictionary["message"] ?? "")")
                                
                            }
                        }
                        else {
                            
                            self.loadUserProfileDetails()
                            
                            self.globalDispatchgroup.notify(queue: .main, execute: {
                                
                                self.successfulLoginActions()
                                
                                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                                
                                self.navigationController?.pushViewController(navigate, animated: true)
                            })
                            
                        }
                    })
                    
                    //SVProgressHUD.dismiss()
                    
                }
            }
            else {
                DispatchQueue.main.async {
                    
                    self.globalDispatchgroup.leave()
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    //SVProgressHUD.dismiss()
                    
                }
            }
            
        }
        
    }
    
    
    
    // MARK:- // JSON Post Method to Send Activation Code
    
    func sendActivationCode() {
        
        var parameters : String = ""
        
        let userPhoneNumber = self.phoneCode + self.phoneNumber
        
        parameters = "activation_code=\(self.enterActivationCodeTXT.text ?? "")&number=\(userPhoneNumber)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "account_activation", param: parameters) {
            
            self.activationResponseDictionary = self.globalJson
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                //SVProgressHUD.dismiss()
                
                self.customAlert(title: "", message: "\(self.globalJson["message"] ?? "")", doesCancel: false, completion: {
                    
                    let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                    
                    self.navigationController?.pushViewController(navigate, animated: true)
                    
                })
                
            }
            
        }
        
    }
    
    
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {

        
        self.accountActivationTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "ios_account_activation_title")
        self.accountActivationSubtitleLBL.text = globalFunctions.shared.getLanguageString(variable: "ios_account_activation_subtitle")
        self.successMessageLBL.text = globalFunctions.shared.getLanguageString(variable: "registration_success_msg")
        self.enterActivationCodeTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "activation_placeholder_msg")
        self.submitButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "ios_submit_capital"), for: .normal)
        //self.resendButtonOutlet.setTitle("Resend", for: .normal)
        self.resendCounterLbl.text = "Resend"
        self.resendStaticLbl.text = "\(globalFunctions.shared.getLanguageString(variable: "send_in_again")) :"
        
    }
    
    
    // MARK:- // Set Font
    
    func SetFont() {
        self.resendStaticLbl.font = UIFont(name: self.accountActivationTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        self.resendCounterLbl.font = UIFont(name: self.accountActivationTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        self.accountActivationTitleLBL.font = UIFont(name: self.accountActivationTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        self.accountActivationSubtitleLBL.font = UIFont(name: self.accountActivationSubtitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.successMessageLBL.font = UIFont(name: self.successMessageLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.enterActivationCodeTXT.font = UIFont(name: self.enterActivationCodeTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.submitButtonOutlet.titleLabel!.font = UIFont(name: self.submitButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        self.resendButtonOutlet.titleLabel!.font = UIFont(name: self.resendButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        
    }
    
    
    
}


// MARK:- // FUI Auth Delegate Functions

extension AccountActivationVC : FUIAuthDelegate {
    
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        
        // CHeck if there was an error
        guard error == nil else {
            // Login Error
            return
        }
        print("USERID ------\(authDataResult?.user.uid ?? "")")
        print("User Info -----\(authDataResult?.user.phoneNumber ?? "")")
        
    }
    
}
