//
//  AgentAllRealEstatesListingVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 13/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView

class AgentAllRealEstatesListingVC: GlobalViewController {

    
    @IBOutlet weak var realEstatesPublishedTitleLBL: UILabel!
    @IBOutlet weak var addressTitleLBL: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var cityCountryTitleLBL: UILabel!
    @IBOutlet weak var cityCountry: UILabel!
    @IBOutlet weak var phoneNumberTitleLBL: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var emailTitleLBL: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var agentName: UILabel!
    @IBOutlet weak var agentImageview: UIImageView!
    
    @IBOutlet weak var listingTable: UITableView!
    @IBOutlet weak var NoListFoundView: UIView!
    @IBOutlet weak var noRecordFoundLbl: UILabel!
    
    var startValue = 0
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var recordsArray = NSMutableArray()
    
    
    var agentID : String!
    
    var agentInformationDictionary : NSDictionary!
    
    var listingDataArray : NSArray!
    
    var isFromSuperAgentDtls: Bool?
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        globalFunctions.shared.loadPlaceholder(onView: self.view)
        
        let identifier = "RealEstateListingTVC"
        let nibName = UINib(nibName: "RealEstateListingTVC", bundle: nil)
        self.listingTable.register(nibName, forCellReuseIdentifier: identifier)
        
        self.SetFont()
        
        self.loadAgentProfileDetails()
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            self.localizeStrings()
            
            self.populateFieldsWithData()
            
            self.getListingData()
            
            self.globalDispatchgroup.notify(queue: .main) {
                
                globalFunctions.shared.removePlaceholder(fromView: self.view)
                if self.recordsArray.count == 0{
                    //app_no_result_found
                    self.NoListFoundView.layer.cornerRadius = 10
                    self.NoListFoundView.isHidden = false
                    self.noRecordFoundLbl.font = UIFont(name: self.noRecordFoundLbl.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 16.0)))
                    self.noRecordFoundLbl.text = globalFunctions.shared.getLanguageString(variable: "home_no_record_found")
                }
                
            }
            
        }
        
    }
    
    
    // MARK:- // View will Appear
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
    }
    
    
    
    // MARK: - // JSON Post Method to get Agent Profile Details
    
    
    func loadAgentProfileDetails() {
        
        var parameters : String!
        
        parameters = "dalaleen_user_id=\(self.agentID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "profile_details", param: parameters) {
            
            self.agentInformationDictionary = self.globalJson
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
            }
        }
    }
    
    
    
    
    // MARK:- // JSON Post Method to get Listing Data
    
    func getListingData() {
        
        var parameters : String!
        
        parameters = "created_by=\(self.agentID ?? "")&start_value=\(self.startValue)&per_load=\(self.perLoad)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)&sort_by="
        
        self.CallAPI(urlString: "home_all_property_listing", param: parameters) {
            
            self.listingDataArray = self.globalJson["info"] as? NSArray
            
            for i in 0..<self.listingDataArray.count {
                
                let tempDict = self.listingDataArray[i] as! NSDictionary
                
                self.recordsArray.add(tempDict)
            }
            
            self.nextStart = "\(self.globalJson["next_start"]!)"
            
            self.startValue = self.startValue + self.perLoad
            
            print("Next Start Value : " , self.startValue)
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.listingTable.delegate = self
                self.listingTable.dataSource = self
                self.listingTable.reloadData()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
            }
            
        }
        
    }
    
    
    
    
    // MARK:- // Populate Fields with Data
    
    func populateFieldsWithData() {
        
        self.address.text = "\((self.agentInformationDictionary["info"] as! NSDictionary)["strt_address"] ?? "")"
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnAddress(sender:)))
        self.address.isUserInteractionEnabled = true
        self.address.addGestureRecognizer(tap)
        self.cityCountry.text = "\((self.agentInformationDictionary["info"] as! NSDictionary)["city"] ?? "")/" + "\((self.agentInformationDictionary["info"] as! NSDictionary)["country"] ?? "")"
        self.phoneNumber.text = "\((self.agentInformationDictionary["info"] as! NSDictionary)["phone"] ?? "")"
        
        self.email.text = "\((self.agentInformationDictionary["info"] as! NSDictionary)["email"] ?? "")"
        if isFromSuperAgentDtls == true{
            let emailTap = UITapGestureRecognizer(target: self, action: #selector(tapOpenMail(sender:)))
            self.email.isUserInteractionEnabled = true
            self.email.addGestureRecognizer(emailTap)
            
            self.phoneNumber.text = "\((self.agentInformationDictionary["info"] as! NSDictionary)["phone"] ?? "")"
            let tapNo = UITapGestureRecognizer(target: self, action: #selector(callButtonAction(sender:)))
            self.phoneNumber.isUserInteractionEnabled = true
            self.phoneNumber.addGestureRecognizer(tapNo)
        }
        self.agentName.text = "\((self.agentInformationDictionary["info"] as! NSDictionary)["name"] ?? "")"
        self.agentImageview.sd_setImage(with: URL(string: "\((self.agentInformationDictionary["info"] as! NSDictionary)["photo"] ?? "")"))
        
    }
    
    //MARK:- Adress btn Click
    @objc func tapOnAddress(sender: UITapGestureRecognizer){
        UserDefaults.standard.set("\((self.agentInformationDictionary["info"] as! NSDictionary)["latitude"] ?? "")", forKey: "lat")
        UserDefaults.standard.set("\((self.agentInformationDictionary["info"] as! NSDictionary)["longitude"] ?? "")", forKey: "long")
        let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "mapViewVC") as! mapViewVC
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    //MARK:- Open mail Btn Click
    @objc func tapOpenMail(sender: UITapGestureRecognizer){
        let email = "\((self.agentInformationDictionary["info"] as! NSDictionary)["email"] ?? "")"
        if let url = URL(string: "mailto:\(email)") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
    }
    
    //MARK:- Tap On number
    @objc func callButtonAction(sender: UITapGestureRecognizer){
        let phoneNumber = "\((self.agentInformationDictionary["info"] as! NSDictionary)["phone_code"] ?? "")\((self.agentInformationDictionary["info"] as! NSDictionary)["phone"] ?? "")"
        
        if phoneNumber.elementsEqual("") == false {
            
            if let phoneURL = NSURL(string: ("tel://" + phoneNumber)) {
                
                self.customAlert(title: globalFunctions.shared.getLanguageString(variable: "ios_are_you_sure"), message: globalFunctions.shared.getLanguageString(variable: "property_details_call") + " " + phoneNumber + " ?", doesCancel: true) {
                    
                    UIApplication.shared.open(phoneURL as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    
                }
            }
        }
    }
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        if "\((self.agentInformationDictionary["info"] as! NSDictionary)["id"] ?? "")".elementsEqual("\(userInformation.shared.userID ?? "")") {
            self.realEstatesPublishedTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "all_real_estates_published_by_you")
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "left_side_my_real_estates")
        }
        else {
            self.realEstatesPublishedTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "all_real_estates_published")
            self.headerView.headerTitle.text = globalFunctions.shared.getLanguageString(variable: "super_agents_details_text")
        }
        
        self.addressTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_address")
        self.cityCountryTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "property_details_city") + "/" + globalFunctions.shared.getLanguageString(variable: "property_details_country")
        self.phoneNumberTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "property_details_telephone")
        self.emailTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "super_agents_details_email")
        
    }
    
    
    // MARK:- // Set Font
    
    func SetFont() {
        self.realEstatesPublishedTitleLBL.font = UIFont(name: self.realEstatesPublishedTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.addressTitleLBL.font = UIFont(name: self.addressTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.address.font = UIFont(name: self.address.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.cityCountryTitleLBL.font = UIFont(name: self.cityCountryTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.cityCountry.font = UIFont(name: self.cityCountry.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.phoneNumberTitleLBL.font = UIFont(name: self.phoneNumberTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.phoneNumber.font = UIFont(name: self.phoneNumber.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.emailTitleLBL.font = UIFont(name: self.emailTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.email.font = UIFont(name: self.email.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.agentName.font = UIFont(name: self.agentName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
    }
    
}







// MARK:- // Tableview Delegate Methods

extension AgentAllRealEstatesListingVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.recordsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RealEstateListingTVC") as! RealEstateListingTVC
        
        globalFunctions.shared.loadImage(inImageview: cell.propertyImage, url: "\((self.recordsArray[indexPath.row] as! NSDictionary)["property_image"] ?? "")", indicatorInView: cell.cellView)
        
        cell.imageCount.text = " \((self.recordsArray[indexPath.row] as! NSDictionary)["total_image"] ?? "") "
        
        cell.parentCategoryImage.sd_setImage(with: URL(string: "\((self.recordsArray[indexPath.row] as! NSDictionary)["parent_cat_image"] ?? "")"))
        
        
        //cell.propertyType.text = "\((recordsArray[indexPath.row] as! NSDictionary)["property_type"] ?? "")" + " (\((recordsArray[indexPath.row] as! NSDictionary)["area"] ?? "") m) "  + "\(globalFunctions.shared.getLanguageString(variable: "property_details_for"))" + "\((recordsArray[indexPath.row] as! NSDictionary)["ribbon_value_lang"] ?? "")"
        
        if (recordsArray[indexPath.row] as! NSDictionary)["area"] as! String == ""{
            cell.propertyType.text = "\((recordsArray[indexPath.row] as! NSDictionary)["property_type"] ?? "")"
            cell.propertyType.font = UIFont(name: cell.propertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        }
        else{
            cell.propertyType.text = "\((recordsArray[indexPath.row] as! NSDictionary)["property_type"] ?? "")" + "(\((recordsArray[indexPath.row] as! NSDictionary)["area"] ?? "") \(globalFunctions.shared.getLanguageString(variable: "app_home_m2"))2)"//  + "\((recordsArray[indexPath.row] as! NSDictionary)["ribbon_value_lang"] ?? "")"
            let font:UIFont? = UIFont(name: cell.propertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            let fontSuper:UIFont? = UIFont(name: cell.propertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 6)))
            let attString:NSMutableAttributedString = NSMutableAttributedString(string: cell.propertyType.text!, attributes: [.font:font!])
            attString.setAttributes([.font:fontSuper!,.baselineOffset:5], range: NSRange(location:(cell.propertyType.text!.count - 2),length:1))
            cell.propertyType.attributedText = attString
        }
        
        
        //cell.address.text = "\((recordsArray[indexPath.row] as! NSDictionary)["province"] ?? "")"
        cell.city.text = "\((recordsArray[indexPath.row] as! NSDictionary)["distric"] ?? "")"
        cell.district.text = "\((recordsArray[indexPath.row] as! NSDictionary)["city"] ?? "")"
        
        cell.bedCount.text = ((recordsArray[indexPath.row] as! NSDictionary)["bedroom"] as! String) == "" ? "0" : "\((recordsArray[indexPath.row] as! NSDictionary)["bedroom"] ?? "0")"
        cell.livingRoomCount.text = ((recordsArray[indexPath.row] as! NSDictionary)["livingroom"] as! String) == "" ? "0" : "\((recordsArray[indexPath.row] as! NSDictionary)["livingroom"] ?? "0")"//"\((recordsArray[indexPath.row] as! NSDictionary)["livingroom"] ?? "0")"
        cell.bathroomCount.text = ((recordsArray[indexPath.row] as! NSDictionary)["bathroom"] as! String) == "" ? "0" : "\((recordsArray[indexPath.row] as! NSDictionary)["bathroom"] ?? "0")"//"\((recordsArray[indexPath.row] as! NSDictionary)["bathroom"] ?? "0")"
        cell.diningCount.text = ((recordsArray[indexPath.row] as! NSDictionary)["kichen"] as! String) == "" ? "0" : "\((recordsArray[indexPath.row] as! NSDictionary)["kichen"] ?? "0")"
        
        //cell.bedCount.text = "\((recordsArray[indexPath.row] as! NSDictionary)["bedroom"] ?? "")"
        //cell.livingRoomCount.text = "\((recordsArray[indexPath.row] as! NSDictionary)["livingroom"] ?? "")"
        //cell.bathroomCount.text = "\((recordsArray[indexPath.row] as! NSDictionary)["bathroom"] ?? "")"
        //cell.diningCount.text = "\((recordsArray[indexPath.row] as! NSDictionary)["kichen"] ?? "")"
        
        cell.listingDate.text = "\((recordsArray[indexPath.row] as! NSDictionary)["created_on_date"] ?? "")".replacingOccurrences(of: "/", with: ".")
        
        cell.price.text = "\((recordsArray[indexPath.row] as! NSDictionary)["other_price"] ?? "")" + " " + "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"] ?? "")"
        
        if "\((recordsArray[indexPath.row] as! NSDictionary)["plan_id"] ?? "")".elementsEqual("2") {
            cell.urgentImageview.isHidden = false
            
            cell.urgentImageview.contentMode = .scaleAspectFit
            
            cell.urgentImageview.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSDictionary)["gold_logo"] ?? "")"))
        }
        else if "\((recordsArray[indexPath.row] as! NSDictionary)["plan_id"] ?? "")".elementsEqual("6") {
            cell.urgentImageview.isHidden = false
            
            cell.urgentImageview.contentMode = .scaleAspectFill
            cell.urgentImageview.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSDictionary)["urgent_logo"] ?? "")"))
        }
        else {
            cell.urgentImageview.isHidden = true
        }
        
        
        
        //
        cell.cellView.layer.cornerRadius = 8
        
        
        //
        
        cell.propertyType.font = UIFont(name: cell.propertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        cell.city.font = UIFont(name: cell.city.font.fontName, size: CGFloat(self.Get_fontSize(size: 7)))
        cell.address.font = UIFont(name: cell.address.font.fontName, size: CGFloat(self.Get_fontSize(size: 9)))
        cell.district.font = UIFont(name: cell.district.font.fontName, size: CGFloat(self.Get_fontSize(size: 7)))
        cell.price.font = UIFont(name: cell.price.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        cell.listingDate.font = UIFont(name: cell.listingDate.font.fontName, size: CGFloat(self.Get_fontSize(size: 9)))
        
        cell.bedCount.font = UIFont(name: cell.bedCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        cell.livingRoomCount.font = UIFont(name: cell.livingRoomCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        cell.bathroomCount.font = UIFont(name: cell.bathroomCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        cell.diningCount.font = UIFont(name: cell.diningCount.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let object : RealEstateListingTVC = cell as! RealEstateListingTVC
        
        if ("\((recordsArray[indexPath.row] as! NSDictionary)["plan_id"] ?? "")".elementsEqual("2"))
        {
            object.cellView.backgroundColor = UIColor(red:242/255, green:243/255, blue:193/255, alpha: 1)
        }
        else if ("\((recordsArray[indexPath.row] as! NSDictionary)["plan_id"] ?? "")".elementsEqual("6"))
        {
            object.cellView.backgroundColor = UIColor(red:242/255, green:243/255, blue:193/255, alpha: 1)
            //object.cellView.backgroundColor = UIColor(red:254/255, green:215/255, blue:218/255, alpha: 1)
        }
        else
        {
            object.cellView.backgroundColor = UIColor.white
        }
        
        
        if ("\((recordsArray[indexPath.row] as! NSDictionary)["ribbon_value"] ?? "")".elementsEqual("sale"))
        {
            object.cellSideView.backgroundColor = UIColor.green
        }
        else if ("\((recordsArray[indexPath.row] as! NSDictionary)["ribbon_value"] ?? "")".elementsEqual("rent"))
        {
            object.cellSideView.backgroundColor = UIColor.yellow
        }
        else if ("\((recordsArray[indexPath.row] as! NSDictionary)["ribbon_value"] ?? "")".elementsEqual("sold"))
        {
            object.cellSideView.backgroundColor = UIColor.red
        }
        else
        {
            object.cellSideView.backgroundColor = UIColor.white
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let object = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "PropertyDetailsVC") as! PropertyDetailsVC
        
        let mydict = recordsArray[indexPath.row] as! NSDictionary
        
        object.propertyID = "\(mydict["id"] ?? "")"
        
        self.navigationController?.pushViewController(object, animated: true)
        
    }
    
    
}



// MARK:- // Scrollview Delegate Methods

extension AgentAllRealEstatesListingVC: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {
            
        }
        else
        {
            print("next start : ",nextStart ?? "")
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    //SVProgressHUD.dismiss()
                }
            }
            else
            {
                self.getListingData()
            }
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
