//
//  UserProfilePhoneVerificationVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 10/12/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import FirebaseUI

class UserProfilePhoneVerificationVC: GlobalViewController {
    
    
    @IBOutlet weak var activationCodeTXT: UITextField!
    @IBOutlet weak var enterActivationCodeView: UIView!
    @IBOutlet weak var enterActivationCodeShadowView: ShadowView!
    
    @IBOutlet weak var submitButtonShadowView: ShadowView!
    @IBOutlet weak var resendButtonShadowview: UIView!
    
    @IBOutlet weak var resendView: UIView!
    @IBOutlet weak var resendStaticLbl: UILabel!
    @IBOutlet weak var resendCounterLbl: UILabel!
    
    var profileID : String? = ""
    var profileName : String? = ""
    var profileEmail : String? = ""
    var profileAccountType : String? = ""
    var profileCountry : String? = ""
    var profileProvince : String? = ""
    var profileCity : String? = ""
    var profileDistrict : String? = ""
    var profilePhoneNumber : String? = ""
    var profilePhoneCode : String? = ""
    var profileWorkPhone : String? = ""
    var profileFax : String? = ""
    var profileStreet : String? = ""
    var profileLat : String? = ""
    var profileLong : String? = ""
    var profileBannerImage : UIImage?
    var profileCountryFlagString : String? = ""
    
    var verificationID : String? = ""
    
    var newBannerUploaded : Bool! = false
    
    var userInformationDictionary : NSDictionary!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.verifyPhoneNumber()

        self.SetFont()
        self.localizeStrings()
        self.enterActivationCodeView.layer.cornerRadius = self.enterActivationCodeView.frame.size.height / 2
        self.submitButtonShadowView.layer.cornerRadius = self.submitButtonShadowView.frame.size.height / 2
        self.submitButtonOutlet.layer.cornerRadius = self.submitButtonOutlet.frame.size.height / 2
        self.submitButtonShadowView.layer.cornerRadius = self.submitButtonShadowView.frame.size.height / 2
        self.resendButtonOutlet.layer.cornerRadius = self.resendButtonOutlet.frame.size.height / 2
        self.resendButtonShadowview.layer.cornerRadius = self.resendButtonShadowview.frame.size.height / 2
        
        self.resendView.layer.cornerRadius = self.resendView.frame.size.height / 2
        self.resendButtonOutlet.isUserInteractionEnabled = false
        self.resendStaticLbl.isHidden = true
        
        self.startTimer()
    }
    
    
    
    
    var counter = 150
    var resendTimer : Timer?
    
    func startTimer() {
        if resendTimer == nil {
            resendTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
            
        }
        
    }
    
    func stopTimer() {
        if resendTimer != nil {
            self.counter = 150
            resendTimer!.invalidate()
            resendTimer = nil
        }
    }
    
    
    // MARK:- // Resend Button
    
    @IBOutlet weak var resendButtonOutlet: UIButton!
    @IBAction func resendButton(_ sender: UIButton) {
        
        self.resendButtonOutlet.isUserInteractionEnabled = false
        
        self.verifyPhoneNumber()
        
        self.startTimer()
        
    }
    
    // MARK:- // Update Counter
    
    @objc func updateCounter() {
        
        //example functionality
        if counter > 0 {
            self.resendStaticLbl.isHidden = false
            self.resendButtonOutlet.titleLabel!.font = UIFont(name: self.resendButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
            self.resendCounterLbl.text = "\(counter)"
            self.resendButtonOutlet.setTitleColor(.white, for: .normal)
            self.resendView.backgroundColor = .lightGray
            print("\(counter) seconds to the end of the world")
            counter -= 1
        }
        else if counter == 0 {
            self.resendButtonOutlet.titleLabel!.font = UIFont(name: self.resendButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
            self.resendCounterLbl.text = "Resend"
            self.resendButtonOutlet.setTitleColor(.white, for: .normal)
            self.resendView.backgroundColor = .orange
            self.resendButtonOutlet.isUserInteractionEnabled = true
            self.resendStaticLbl.isHidden = true
            self.stopTimer()
            
            
        }
    }
    
    
    
    // MARK:- // BAck Button
    
    @IBOutlet weak var backButtonOutlet: UIButton!
    @IBAction func backButton(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    // MARK:- // Submit Button
    
    @IBOutlet weak var submitButtonOutlet: UIButton!
    @IBAction func submitButton(_ sender: UIButton) {
        
        self.submitButtonOutlet.isUserInteractionEnabled = false
        
        if (self.activationCodeTXT.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "resetpass_otp_placeholder"))
            
            self.submitButtonOutlet.isUserInteractionEnabled = true
        }
        else {
            
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: self.verificationID ?? "", verificationCode: "\(self.activationCodeTXT.text ?? "")")
            
            Auth.auth().signIn(with: credential) { (authResult, error) in
                if error != nil {
                    self.ShowAlertMessage(title: "Alert", message: "Something went wrong! Try again later")
                    // ...
                    return
                }
                // User is signed in
                // ...
                self.saveUserProfile()
                
                
                self.globalDispatchgroup.notify(queue: .main) {
                    
                    self.loadUserProfileDetails()
                    
                    self.globalDispatchgroup.notify(queue: .main, execute: {
                        
                        self.submitButtonOutlet.isUserInteractionEnabled = true
                        
                        self.saveuserInformations()
                        
                        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        
                        self.navigationController?.pushViewController(navigate, animated: true)
                        
                        //
                        
                    })
                    
                }
            }
        }
        
    }
    
    
    
    
    // MARK:- // Verify Phone Number
    
    func verifyPhoneNumber()
    {
        print("number",("+\(self.profilePhoneCode ?? "")" + "\(self.profilePhoneNumber ?? "")"))
        PhoneAuthProvider.provider().verifyPhoneNumber(("+\(self.profilePhoneCode ?? "")" + "\(self.profilePhoneNumber ?? "")"), uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print(error.localizedDescription)
                //self.showMessagePrompt(error.localizedDescription)
                self.customAlert(title: "", message: "Could not verify Phone Number.", doesCancel: false, completion: {
                    
                    self.navigationController?.popViewController(animated: true)
                    
                })
                return
            }
            
            // Sign in using the verificationID and the code sent to the user
            // ...
            self.verificationID = verificationID
            print("veri",verificationID ?? "")
        }
        
    }
    
    
    
    
    
    
    
    
    // MARK:- // Alamofire Method to Save User Profile
    
    func saveUserProfile() {
        
        
        let bannerImageData = self.profileBannerImage!.jpegData(compressionQuality: 1)!
        
        self.globalDispatchgroup.enter()
        
        startAnimating(CGSize(width: ((30.0/320.0)*FullWidth), height: ((30.0/320.0)*FullWidth)), message: "", messageFont: UIFont(name: "Lato-Bold", size: 14), type: .ballClipRotateMultiple, color: UIColor(displayP3Red: 0/255, green: 135/255, blue: 228/255, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.4), textColor: .black, fadeInAnimation: nil)
        
        //        DispatchQueue.main.async {
        //
        //            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        //        }
        
        let parameters = [
            "dalaleen_user_id" : "\(self.profileID ?? "")" , "name" : "\(self.profileName ?? "")" , "email" : "\(self.profileEmail ?? "")" , "account_type" : "\(self.profileAccountType ?? "")" , "country" : "\(self.profileCountry ?? "")" , "province" : "\(self.profileProvince ?? "")" , "city" : "\(self.profileCity ?? "")" , "district" : "\(self.profileDistrict ?? "")" , "phone" : "\(self.profilePhoneNumber ?? "")" , "phone_code" : "\(self.profilePhoneCode ?? "")" , "work_phone" : "\(self.profileWorkPhone ?? "")" , "fax" : "\(self.profileFax ?? "")" , "strt_address" : "\(self.profileStreet ?? "")" , "verify_lat" : "\(self.profileLat ?? "")" , "verify_long" : "\(self.profileLong ?? "")" , "language_code" : "\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"]
        
        print("Parameters are---",parameters)
        
        Alamofire.upload(
            multipartFormData: { MultipartFormData in
                
                for (key, value) in parameters {
                    MultipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
                
                if self.newBannerUploaded == true {
                    MultipartFormData.append(bannerImageData, withName: "profile_image", fileName: "profileImage.jpeg", mimeType: "image/jpeg")
                }
                
                
                
                
                
        }, to: (GLOBALAPI + "profile_edit"))
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    //
                    print(response.request!)  // original URL request
                    print(response.response!) // URL response
                    print(response.data!)     // server data
                    print(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value {
                        
                        print("Image Search Response-----: \(JSON)")
                        
                        let tempdict = JSON as! NSDictionary
                        
                        userInformation.shared.setUserImageString(imageString: "\(tempdict["profileimg"] ?? "")")
                        
                        
                        
                        DispatchQueue.main.async {
                            
                            self.globalDispatchgroup.leave()
                            
                            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                            //SVProgressHUD.dismiss()
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            
                            self.globalDispatchgroup.leave()
                            
                            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                            //SVProgressHUD.dismiss()
                        }
                    }
                }
                
            case .failure(let encodingError):
                
                print(encodingError)
                
                DispatchQueue.main.async {
                    
                    self.globalDispatchgroup.leave()
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    //SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    
    
    // MARK: - // JSON Get Method to get User Profile Details
    
    
    func loadUserProfileDetails() {
        
        var parameters : String!
        
        parameters = "dalaleen_user_id=\(userInformation.shared.userID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "profile_details", param: parameters) {
            
            self.userInformationDictionary = self.globalJson
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
            }
        }
    }
    
    
    
    
    
    // MARK:- // Save user informations
    
    func saveuserInformations() {
        
        UserDefaults.standard.set(self.profileCountry, forKey: "userCountryName")
        UserDefaults.standard.set(self.profilePhoneCode, forKey: "userPhoneCode")
        UserDefaults.standard.set(self.profilePhoneNumber, forKey: "userPhoneNumber")
        UserDefaults.standard.set(self.profileCountryFlagString, forKey: "userCountryFlag")
        
        
        userInformation.shared.setUserID(id: "\((self.userInformationDictionary["info"] as! NSDictionary)["id"] ?? "")")
        userInformation.shared.setUserName(name: "\((self.userInformationDictionary["info"] as! NSDictionary)["name"] ?? "")")
        
        userInformation.shared.setUserImageString(imageString: "\((self.userInformationDictionary["info"] as! NSDictionary)["photo"] ?? "")")
        userInformation.shared.setUserEmail(email: "\((self.userInformationDictionary["info"] as! NSDictionary)["email"] ?? "")")
        userInformation.shared.setUserPhoneCode(phoneCode: "\((self.userInformationDictionary["info"] as! NSDictionary)["phone_code"] ?? "")")
        userInformation.shared.setUserPhoneNumber(phoneNumber: "\((self.userInformationDictionary["info"] as! NSDictionary)["phone"] ?? "")")
        userInformation.shared.setUserCountryID(countryID: "\((self.userInformationDictionary["info"] as! NSDictionary)["country_code"] ?? "")")
        userInformation.shared.setUserCountryName(countryName: "\((self.userInformationDictionary["info"] as! NSDictionary)["country"] ?? "")")
        userInformation.shared.setUserType(userType: "\((self.userInformationDictionary["info"] as! NSDictionary)["user_type"] ?? "")")
        userInformation.shared.setSuperAgent(superAgent: "\((self.userInformationDictionary["info"] as! NSDictionary)["super_agent"] ?? "")")
        userInformation.shared.setUserWorkPhone(workPhone: "\((self.userInformationDictionary["info"] as! NSDictionary)["work_phone"] ?? "")")
        userInformation.shared.setUserFaxNumber(faxNumber: "\((self.userInformationDictionary["info"] as! NSDictionary)["fax"] ?? "")")
        userInformation.shared.setUserAddress(address: "\((self.userInformationDictionary["info"] as! NSDictionary)["strt_address"] ?? "")")
        userInformation.shared.setUserProvinceID(provinceID: "\((self.userInformationDictionary["info"] as! NSDictionary)["province_id"] ?? "")")
        userInformation.shared.setUserProvinceName(provinceName: "\((self.userInformationDictionary["info"] as! NSDictionary)["province"] ?? "")")
        userInformation.shared.setUserDictrictID(dictrictID: "\((self.userInformationDictionary["info"] as! NSDictionary)["district_id"] ?? "")")
        userInformation.shared.setUserDictrictName(districtName: "\((self.userInformationDictionary["info"] as! NSDictionary)["district"] ?? "")")
        userInformation.shared.setUserCityID(cityID: "\((self.userInformationDictionary["info"] as! NSDictionary)["city_id"] ?? "")")
        userInformation.shared.setUserCityName(cityName: "\((self.userInformationDictionary["info"] as! NSDictionary)["city"] ?? "")")
        userInformation.shared.setUserLatitude(latitude: "\((self.userInformationDictionary["info"] as! NSDictionary)["latitude"] ?? "")")
        userInformation.shared.setUserLongitude(longitude: "\((self.userInformationDictionary["info"] as! NSDictionary)["longitude"] ?? "")")
        
        
        userInformation.shared.setUserInboxCount(inboxCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["inbox_count"] ?? "")")
        userInformation.shared.setUserRecyclebinCount(recyclebinCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["recyclebin_count"] ?? "")")
        userInformation.shared.setUserAdsCount(adsCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["my_real_estate"] ?? "")")
        userInformation.shared.setUserFavouritesCount(favouritesCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["my_fav_real_estate"] ?? "")")
        
        
        userInformation.shared.setUserCountryFlag(countryFlag: UserDefaults.standard.string(forKey: "userCountryFlag")!)
        
        
    }
    
    
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.activationCodeTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "activation_placeholder_msg")
        self.submitButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "ios_submit_capital"), for: .normal)
        //self.resendButtonOutlet.setTitle("RESEND", for: .normal)
        self.resendCounterLbl.text = "Resend"
        self.resendStaticLbl.text = "\(globalFunctions.shared.getLanguageString(variable: "send_in_again")) :"
        
    }
    
    
    // MARK:- // Set Font
    
    func SetFont() {
        
        self.resendStaticLbl.font = UIFont(name: self.resendStaticLbl.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        self.resendCounterLbl.font = UIFont(name: self.resendCounterLbl.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        
        self.activationCodeTXT.font = UIFont(name: self.activationCodeTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.submitButtonOutlet.titleLabel!.font = UIFont(name: self.submitButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        self.resendButtonOutlet.titleLabel!.font = UIFont(name: self.resendButtonOutlet.titleLabel!.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        
    }
}
