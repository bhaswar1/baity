//
//  PropertyDetailsVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 11/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView
import SwiftGoogleTranslate
import NaturalLanguage
import YouTubePlayer_Swift
import SDWebImage



class PropertyDetailsVC: GlobalViewController {
    
    @IBOutlet weak var translateStaticOut: UILabel!
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var propertyImageCollectionview: UICollectionView!
    @IBOutlet weak var propertyImagePagecontrol: UIPageControl!
    
    @IBOutlet weak var expireOnView: UIView!
    @IBOutlet weak var expireOnLBL: UILabel!
    @IBOutlet weak var breadcrumbFavouritePropertyTypeView: UIView!
    @IBOutlet weak var breadCrumbLBL: UILabel!
    @IBOutlet weak var favouriteView: UIView!
    @IBOutlet weak var favouriteImageview: UIImageView!
    @IBOutlet weak var propertyTypeImageview: UIImageView!
    
    @IBOutlet weak var propertyBriefDetailsview: UIView!
    @IBOutlet weak var propertyName: UILabel!
    @IBOutlet weak var propertyPrice: UILabel!
    @IBOutlet weak var propertyViews: UILabel!
    @IBOutlet weak var viewPerLBL: UILabel!
    
    @IBOutlet weak var roomDetailsView: UIView!
    @IBOutlet weak var roomTypesCollectionview: UICollectionView!
    
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var descriptionTitleLBL: UILabel!
    @IBOutlet weak var propertyDescription: UILabel!
    
    @IBOutlet weak var agentView: UIView!
    @IBOutlet weak var agentImage: UIImageView!
    @IBOutlet weak var offeredByStaticLBL: UILabel!
    @IBOutlet weak var agentName: UILabel!
    @IBOutlet weak var tapToShowLBL: UILabel!
    
    @IBOutlet weak var propertyDetailsView: UIView!
    @IBOutlet weak var detailsTitleLBL: UILabel!
    @IBOutlet weak var reportTitleLBL: UILabel!
    @IBOutlet weak var reportImageview: UIImageView!
    @IBOutlet weak var propertyDetailsTableview: UITableView!
    
    @IBOutlet weak var propertyDetailsTableHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var locationTitleLBL: UILabel!
    @IBOutlet weak var locationMapView: GMSMapView!
    @IBOutlet weak var callNowMessageView: UIView!
    
    
    @IBOutlet weak var popUpView: PopUpViewTypeOne!
    
    
    @IBOutlet weak var extendEditDeleteSoldView: UIView!
    @IBOutlet weak var extendEditDeleteSoldCollectionview: UICollectionView!
    
    
    @IBOutlet weak var callNowStaticLBL: UILabel!
    @IBOutlet weak var sendMessageStaticLBL: UILabel!
    @IBOutlet weak var sendEmailStaticLbl: UILabel!
    
    @IBOutlet weak var btnShowMoreOut: UIButton!
    @IBOutlet weak var addImage: UIImageView!
    
    @IBOutlet weak var addView: UIView!
    
    
    
    
    var propertyID : String! = ""
    
    var allDataDictionary : NSDictionary!
    var propertyImageArray : NSArray!
    var propertyFeatureArray = [[String : String]]()
    
    var roomTypesArray = [roomTypesDataFormat]()
    
    var favouriteStatus : Bool!
    
    var isShowMore: Bool = true
    
    var favouriteUnfavouriteStatus : String! = ""

    var marker = GMSMarker()
    
    var popupType : String! = "" // R = Report  M = Message
    
    var extendEditDelteSoldDataArray = [StaticDictDataFormat(key: "clock1", value: "\(globalFunctions.shared.getLanguageString(variable: "my_real_estate_extend"))", isSelected: false) , StaticDictDataFormat(key: "pencil", value: "\(globalFunctions.shared.getLanguageString(variable: "my_real_estate_edit"))", isSelected: false) , StaticDictDataFormat(key: "trash g", value: "\(globalFunctions.shared.getLanguageString(variable: "my_real_estate_delete"))", isSelected: false) , StaticDictDataFormat(key: "SideMenuHelp", value: "\(globalFunctions.shared.getLanguageString(variable: "my_real_estate_sold"))", isSelected: false)]
    
    var isTranslated : Bool = false
    
    var detectedLanguageCode : String = ""
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.zoominBtnOut.layer.cornerRadius = 1
        self.zoominBtnOut.layer.borderColor = UIColor.lightGray.cgColor
        self.zoominBtnOut.layer.borderWidth = 0.5
        
        self.zoomOutBtnOut.layer.cornerRadius = 1
        self.zoomOutBtnOut.layer.borderColor = UIColor.lightGray.cgColor
        self.zoomOutBtnOut.layer.borderWidth = 0.5
        
    }
    
    // MARK:- // View will appear
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        globalFunctions.shared.loadPlaceholder(onView: self.view)
        
//        for view in self.view.subviews {
//            view.isHidden = true
//        }
        
        self.propertyFeatureArray.removeAll()
        
        self.SetFont()
        
        self.localizeStrings()
        
        self.getPropertyDetails {
            
            self.populateFieldsWithData()
            
            //            for view in self.view.subviews {
            //                view.isHidden = false
            //            }
            
            globalFunctions.shared.removePlaceholder(fromView: self.view)
            
        }
        
        self.headerView.headerIconPositionOne.addTarget(self, action: #selector(self.shareClick(sender:)), for: .touchUpInside)
        
        self.propertyDetailsTableview.estimatedRowHeight = 500
        self.propertyDetailsTableview.rowHeight = UITableView.automaticDimension
        
        self.globalDispatchgroup.notify(queue: .main) {
            DispatchQueue.main.async {
                globalFunctions.shared.SetTableheight(table: self.propertyDetailsTableview, heightConstraint: self.propertyDetailsTableHeightConstraint)
            }
        }
        
    }
    
    @IBAction func clickOnAddButtonAction(_ sender: UIButton) {
        guard let url = URL(string: "\(self.allDataDictionary["advertisement_link"] ?? "")") else { return }
        UIApplication.shared.open(url)
    }
    
    // MARK:- // Buttons
    
    @IBAction func sendEmailButtonClick(_ sender: Any) {
        let email = "\((self.allDataDictionary["property_owner_details"] as! NSDictionary)["owner_email"] ?? "")"
        if let url = URL(string: "mailto:\(email)") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
    }
    
    //MARK:- Showmore button Click
    @IBAction func showMoreBtnAct(_ sender: UIButton) {
        if isShowMore{
            self.propertyDescription.numberOfLines = 0
            self.btnShowMoreOut.setTitle(globalFunctions.shared.getLanguageString(variable: "app_show_less"), for: .normal)
        }
        else{
            self.propertyDescription.numberOfLines = 4
            self.btnShowMoreOut.setTitle(globalFunctions.shared.getLanguageString(variable: "app_show_more"), for: .normal)
        }
        self.isShowMore = !self.isShowMore
    }
    
    // MARK:- // Translate Button
    
    @IBOutlet weak var translateButtonOutlet: UIButton!
    @IBAction func translateButton(_ sender: UIButton) {
        
        
        let translator = ROGoogleTranslate()
        translator.apiKey = "AIzaSyBWGigwjDYqBBAhvKCkldOz2q3MwsqFMow" // Add your API Key here
        var params = ROGoogleTranslateParams()
        
        if self.isTranslated == true {
            
            self.propertyName.text = "\(self.allDataDictionary["property_name"] ?? "")"
            self.propertyDescription.text = "\(self.allDataDictionary["property_description"] ?? "")"
            if self.propertyDescription.calculateMaxLines() > 4{
                self.btnShowMoreOut.isHidden = false
            }
            else{
                self.btnShowMoreOut.isHidden = true
            }
            self.isTranslated = false
            
            self.detectedLanguageCode = ""
            
        }
        else {
            
            self.detectedLangaugeCode(for: "\(self.allDataDictionary["property_name"] ?? "")")
            
            self.globalDispatchgroup.notify(queue: .main) {
                
                if self.detectedLanguageCode.elementsEqual("") {
                    
                    self.propertyName.text = "\(self.allDataDictionary["property_name"] ?? "")"
                    self.propertyDescription.text = "\(self.allDataDictionary["property_description"] ?? "")"
                    if self.propertyDescription.calculateMaxLines() > 4{
                        self.btnShowMoreOut.isHidden = false
                    }
                    else{
                        self.btnShowMoreOut.isHidden = true
                    }
                    
                    self.isTranslated = false
                    
                    self.detectedLanguageCode = ""
                    
                }
                else {
                    if self.detectedLanguageCode.elementsEqual("en") {
                        params.source = "en"//fromLanguage
                        params.target = "ar"//toLanguage
                    }
                    else {
                        params.source = "ar"//fromLanguage
                        params.target = "en"//toLanguage
                    }
                    
                    params.text = self.propertyName.text ?? ""
                    
                    translator.translate(params: params) { (result) in
                        DispatchQueue.main.async {
                            self.propertyName.text = "\(result)"
                        }
                    }
                    
                    params.text = self.propertyDescription.text ?? ""
                    
                    translator.translate(params: params) { (result) in
                        DispatchQueue.main.async {
                            self.propertyDescription.text = "\(result)"
                            if self.propertyDescription.calculateMaxLines() > 4{
                                self.btnShowMoreOut.isHidden = false
                            }
                            else{
                                self.btnShowMoreOut.isHidden = true
                            }
                        }
                    }
                    
                    self.isTranslated = true
                    
                }
                
            }
            
        }
        
    }
    
    //MARK:- Map zoom in and zoom out button function
    @IBOutlet weak var zoominBtnOut: UIButton!
    @IBOutlet weak var zoomOutBtnOut: UIButton!
    @IBAction func zoomMap(_ sender: UIButton) {
        if sender.tag == 0{
            //zoom in
            let zoomCamera = GMSCameraUpdate.zoomIn()
            self.locationMapView.animate(with: zoomCamera)
        }
        else{
            //zoom out
            let zoomCamera = GMSCameraUpdate.zoomOut()
            self.locationMapView.animate(with: zoomCamera)
        }
    }
    
    
    
    
    
    // MARK:- // Call Now Button
    
    @IBOutlet weak var callNowButtonOutlet: UIButton!
    @IBAction func callNowButton(_ sender: UIButton) {
       
        let phoneNumber = "\((self.allDataDictionary["property_owner_details"] as! NSDictionary)["owner_phone_no"] ?? "")"
        
        if phoneNumber.elementsEqual("") == false {
            
            if let phoneURL = NSURL(string: ("tel://" + phoneNumber)) {
                
                self.customAlert(title: globalFunctions.shared.getLanguageString(variable: "ios_are_you_sure"), message: globalFunctions.shared.getLanguageString(variable: "property_details_call") + " \((self.allDataDictionary["property_owner_details"] as! NSDictionary)["owner_phone_no"] ?? "") ?", doesCancel: true) {
                    
                    UIApplication.shared.open(phoneURL as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    
                }
            }
        }
    }
    
    
    // MARK:- // Send Message Button
    
    @IBOutlet weak var sendMessageButtonOutlet: UIButton!
    @IBAction func sendMessageButton(_ sender: UIButton) {
        
        if UserDefaults.standard.bool(forKey: "loginStatus") == true {
            //print((self.allDataDictionary["property_owner_details"] as! NSDictionary)["owner_phone_code"] ?? "")
            let urlWhats = "whatsapp://send?phone=+\((self.allDataDictionary["property_owner_details"] as! NSDictionary)["owner_phone_code"] ?? "")\(((self.allDataDictionary["property_owner_details"] as! NSDictionary)["owner_phone_no"] ?? ""))&text=Hello ! im interest in your ad : \(GLOBALAPI)" + "property-details/\(self.propertyID ?? "")"
            
            var characterSet = CharacterSet.urlQueryAllowed
            characterSet.insert(charactersIn: "?&")
            
            if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: characterSet){
                
                if let whatsappURL = NSURL(string: urlString) {
                    if UIApplication.shared.canOpenURL(whatsappURL as URL){
                        //UIApplication.shared.openURL(whatsappURL as URL)
                        UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: nil)
                    }
                    else {
                        print("Install Whatsapp")
                        
                        self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "whatsapp_not_installed"))
                        
                    }
                }
            }
            
            //        self.setupPopupView(type: "M")
            //
            //        self.view.bringSubviewToFront(self.popUpView)
            //        self.popUpView.isHidden = false
            
        }
        else {
            
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "you_have_to_login_first"))
            
        }
        
        
        
    }
    
    
    // MARK:- // Report Button
    
    @IBOutlet weak var reportButtonOutlet: UIButton!
    @IBAction func reportButton(_ sender: UIButton) {
        
        if userInformation.shared.userID.elementsEqual("") {
            
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "you_have_to_login_first"))
            
        }
        else {
            self.setupPopupView(type: "R")
            
            self.view.bringSubviewToFront(self.popUpView)
            self.popUpView.isHidden = false
        }
        
        
        
    }
    
    
    // MARK:- // Favourite Button
    
    @IBOutlet weak var favouriteButtonOutlet: UIButton!
    @IBAction func favouriteButton(_ sender: UIButton) {
        
        if userInformation.shared.userID.elementsEqual("") {
            
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "you_have_to_login_first"))
            
        }
        else {
            
            if self.favouriteUnfavouriteStatus.elementsEqual("Y") {
                
                self.favouriteUnfavourite(status: "N")
                
                self.globalDispatchgroup.notify(queue: .main) {
                    
                    self.favouriteImageview.image = UIImage(named: "heart_white")
                    
                    self.favouriteUnfavouriteStatus = "N"
                    
                }
            }
            else {
                
                self.favouriteUnfavourite(status: "Y")
                
                self.globalDispatchgroup.notify(queue: .main) {
                    
                    self.favouriteImageview.image = UIImage(named: "heart_red")
                    
                    self.favouriteUnfavouriteStatus = "Y"
                    
                }
            }
            
        }
        
    }
    
    
    
    // MARK:- // View Agent Listings Button
    
    @IBOutlet weak var viewAgentListingsButtonOutlet: UIButton!
    @IBAction func viewAgentListingsButton(_ sender: UIButton) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "AgentAllRealEstatesListingVC") as! AgentAllRealEstatesListingVC
        
        navigate.agentID = "\((self.allDataDictionary["property_owner_details"] as! NSDictionary)["owner_id"] ?? "")"
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
    
    
    // MARK:- // JSON Post Method to get Property Details Data from Web
    
    func getPropertyDetails(completion : @escaping ()->()) {
        
        var parameters : String! = ""
        
        parameters = "property_id=\(propertyID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "property_details", param: parameters) {
            
            self.allDataDictionary = (self.globalJson["info"] as! NSArray)[0] as? NSDictionary
            
            self.propertyImageArray = self.allDataDictionary["property_image"] as? NSArray
            
            self.propertyFeatureArray.append(["key" : globalFunctions.shared.getLanguageString(variable: "home_added_date") , "value" : "\(self.allDataDictionary["property_created_date"] ?? "")"])
            self.propertyFeatureArray.append(["key" : globalFunctions.shared.getLanguageString(variable: "home_district") , "value" : "\(self.allDataDictionary["property_district_name"] ?? "")"])
            
            for i in 0..<(self.allDataDictionary["property_feature"] as! NSArray).count {
                self.propertyFeatureArray.append(["key" : "\(((self.allDataDictionary["property_feature"] as! NSArray)[i] as! NSDictionary)["features_name"] ?? "")" , "value" : "\(((self.allDataDictionary["property_feature"] as! NSArray)[i] as! NSDictionary)["feature_value"] ?? "")"])
            }
            
            for i in 0..<(self.allDataDictionary["property_dynamic_status"] as! NSArray).count {
                self.propertyFeatureArray.append(["key" : "\(((self.allDataDictionary["property_dynamic_status"] as! NSArray)[i] as! NSDictionary)["status_name"] ?? "")" , "value" : "\(((self.allDataDictionary["property_dynamic_status"] as! NSArray)[i] as! NSDictionary)["status_value"] ?? "")"])
            }
            if (self.allDataDictionary["property_owner_details"] as! NSDictionary)["owner_type"] as! String == "User"{
                self.agentView.isHidden = true
            }
            
            self.createRoomTypesArray()
           
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                print(self.propertyFeatureArray)
                
                self.propertyImageCollectionview.delegate = self
                self.propertyImageCollectionview.dataSource = self
                self.propertyImageCollectionview.reloadData()
                
                DispatchQueue.main.async {
                    
                    self.propertyDetailsTableview.delegate = self
                    self.propertyDetailsTableview.dataSource = self
                    self.propertyDetailsTableview.reloadData()
                    
                    self.extendEditDeleteSoldCollectionview.delegate = self
                    self.extendEditDeleteSoldCollectionview.dataSource = self
                    self.extendEditDeleteSoldCollectionview.reloadData()
                }
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
                completion()
                
            }
            
        }
    }
    
    
    // MARK:- // JSON Post Method to Check if Property is in Favourite
    
    func checkIfFavourite() {
        
        var parameters : String! = ""
        
        parameters = "dalaleen_user_id=\(userInformation.shared.userID ?? "")&property_id=\(self.propertyID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "favorite_property", param: parameters) {
            
            if "\((self.globalJson["info"] as! NSDictionary)["favorite_status"]!)".elementsEqual("Y") {
                self.favouriteStatus = true
                
                self.favouriteUnfavouriteStatus = "Y"
            }
            else {
                self.favouriteStatus = false
                
                self.favouriteUnfavouriteStatus = "N"
            }
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
            
        }
        
    }
    
    
    // MARK:- // JSON Post Method to Report a Violation
    
    func report() {
        
        var parameters : String = ""
        
        parameters = "property_id=\(self.propertyID ?? "")&from_user_id=\(userInformation.shared.userID ?? "")&to_user_id=\((self.allDataDictionary["property_owner_details"] as! NSDictionary)["owner_id"] ?? "")&reason=Y&message=\(self.popUpView.contentTXTView.text ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "violation_message_save", param: parameters) {
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
                self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "success_msg"), message: "\(self.globalJson["message"] ?? "")")
                
            }
            
        }
        
    }
    
    
    
    // MARK:- // JSON Post Method to Send Message
    
    func sendMessage() {
        
        var parameters : String = ""
        
        parameters = "property_id=\(self.propertyID ?? "")&from_user_id=\(userInformation.shared.userID ?? "")&to_user_id=\((self.allDataDictionary["property_owner_details"] as! NSDictionary)["owner_id"] ?? "")&subject=\(globalFunctions.shared.getLanguageString(variable: "ios_property_query") )&message=\(self.popUpView.contentTXTView.text ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "send_message_save", param: parameters) {
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
                self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "success_msg"), message: "\(self.globalJson["message"] ?? "")")
                
            }
            
        }
        
    }
    
    
    
    
    
    // MARK:- // JSON Post Method to set property as Favourite Unfavourite
    
    func favouriteUnfavourite(status : String) {
        
        var parameters : String! = ""
        
        parameters = "user_id=\(userInformation.shared.userID ?? "")&property_id=\(self.propertyID ?? "")&status=\(status)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "favorite_property_save", param: parameters) {
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
                self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "success_msg"), message: "\(self.globalJson["message"] ?? "")")
                
            }
            
        }
    }
    
    
    
    // MARK:- // JSON Post Method to make property as Sold
    
    func makePropertyAsSold(completion : @escaping () -> ()) {
        
        var parameters : String!
        
        parameters = "property_id=\(self.propertyID ?? "")&dalaleen_user_id=\(userInformation.shared.userID ?? "")&sold_status=1&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "property_sold_save", param: parameters) {
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
                completion()
            }
            
        }
        
    }
    
    
    // MARK:- // JSON Post Method to Delete Property
    
    func deleteProperty(completion : @escaping () -> ()) {
        
        var parameters : String!
        
        parameters = "property_id=\(self.propertyID ?? "")&dalaleen_user_id=\(userInformation.shared.userID ?? "")&status=N&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "property_delete", param: parameters) {
            
            self.globalDispatchgroup.leave()
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            //SVProgressHUD.dismiss()
            
            completion()
            
        }
        
    }
    
    
    
    
    
    
    // MARK:- // Populate Fields with Data
    
    func populateFieldsWithData() {
        if "\(self.allDataDictionary["advertisement_link"] ?? "")" != ""{
           self.addImage.sd_setImage(with: URL(string: "\(self.allDataDictionary["advertisement_image"] ?? "")"))
        }
        else{
            self.addView.isHidden = true
        }
        if "\((self.allDataDictionary["property_owner_details"] as! NSDictionary)["owner_id"] ?? "")".elementsEqual("\(userInformation.shared.userID ?? "")") {
            self.callNowMessageView.isHidden = true
            self.extendEditDeleteSoldView.isHidden = false
            self.expireOnView.isHidden = false
            
            self.expireOnLBL.text = "\(globalFunctions.shared.getLanguageString(variable: "property_details_text1")) \(self.allDataDictionary["property_expiry_date"] ?? "")"
        }
        else {
            self.callNowMessageView.isHidden = false
            self.extendEditDeleteSoldView.isHidden = true
            
            self.expireOnView.isHidden = true
        }
        
        
        self.breadCrumbLBL.text = (self.allDataDictionary["breadcrumb_propertydetails"] as! NSArray).componentsJoined(by: " > ")
        
        if "\(self.allDataDictionary["plan_type"] ?? "")".elementsEqual("2") {
            self.propertyTypeImageview.sd_setImage(with: URL(string: "\(self.allDataDictionary["gold_icon"] ?? "")"))
            
            self.propertyTypeImageview.contentMode = .scaleAspectFit
        }
        else if "\(self.allDataDictionary["plan_type"] ?? "")".elementsEqual("6") {
            self.propertyTypeImageview.isHidden = true
            self.propertyTypeImageview.sd_setImage(with: URL(string: "\(self.allDataDictionary["urgent_icon"] ?? "")"))
            
            self.propertyTypeImageview.contentMode = .scaleAspectFill
        }
        
        //Check if Favourite
        if userInformation.shared.userID.elementsEqual("") {
            self.favouriteView.isHidden = false
        }
        else {
            
            self.favouriteView.isHidden = false
            
            self.checkIfFavourite()
            
            self.globalDispatchgroup.notify(queue: .main) {
                
                if self.favouriteStatus == true {
                    self.favouriteImageview.image = UIImage(named: "heart_red")
                }
                else {
                    self.favouriteImageview.image = UIImage(named: "heart_white")
                }
                
            }
        }
        
        
        
        
        self.propertyName.text = "\(self.allDataDictionary["property_name"] ?? "")"
        self.propertyPrice.text = "\(self.allDataDictionary["other_property_price"] ?? "")" + " \(self.allDataDictionary["property_currency"] ?? "")" + "/"
        
        self.propertyViews.text = globalFunctions.shared.getLanguageString(variable: "property_details_views") + " : \(self.allDataDictionary["property_view"] ?? "")"
        
        self.propertyDescription.text = "\(self.allDataDictionary["property_description"] ?? "")"
        if self.propertyDescription.calculateMaxLines() > 4{
            self.btnShowMoreOut.isHidden = false
        }
        else{
            self.btnShowMoreOut.isHidden = true
        }
        
        if "\(self.allDataDictionary["category_sub_type"] ?? "")".elementsEqual("Rent")
        {
            self.propertyPrice.text = "\(self.allDataDictionary["other_property_price"] ?? "") \(self.allDataDictionary["property_currency"] ?? "") / \(self.allDataDictionary["property_period"] ?? "")"
            //self.viewPerLBL.text =  "\(self.allDataDictionary["property_period"] ?? "")"
        }
        else
        {
            self.propertyPrice.text = "\(self.allDataDictionary["other_property_price"] ?? "") \(self.allDataDictionary["property_currency"] ?? "") / \(globalFunctions.shared.getLanguageString(variable: "app_mnth"))"
            //self.viewPerLBL.text = globalFunctions.shared.getLanguageString(variable: "per_month")
        }
        
        
        self.agentName.text = "\((self.allDataDictionary["property_owner_details"] as! NSDictionary)["owner_name"] ?? "")"
        self.agentImage.sd_setImage(with: URL(string: "\((self.allDataDictionary["property_owner_details"] as! NSDictionary)["owner_pic"] ?? "")"))
        
        self.offeredByStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "ios_property_details_offered_by_text1")
        self.tapToShowLBL.text = globalFunctions.shared.getLanguageString(variable: "tap_to_show_all") + " (\((self.allDataDictionary["property_owner_details"] as! NSDictionary)["total_property"] ?? "")) " + globalFunctions.shared.getLanguageString(variable: "ios_property_details_offered_by_text3")
        
        
        self.showLocation()
        
        
        if "\(self.allDataDictionary["property_sold_status"] ?? "")".elementsEqual("0") {
            self.extendEditDelteSoldDataArray[3].key = "SideMenuHelp"
        }
        else {
            self.extendEditDelteSoldDataArray[3].key = "checkbox2"
        }
        
        self.extendEditDeleteSoldCollectionview.reloadData()
    }
    
    
    // MARK:- // Create Room Types Array
    
    func createRoomTypesArray() {
        self.roomTypesArray.removeAll()
        self.roomTypesArray.append(roomTypesDataFormat(title: "Bedroom", count: "\((self.allDataDictionary["property_other_details"] as! NSDictionary)["bedroom"] ?? "")", image: "bedroom"))
        self.roomTypesArray.append(roomTypesDataFormat(title: "Livingroom", count: "\((self.allDataDictionary["property_other_details"] as! NSDictionary)["livingroom"] ?? "")", image: "livingroom"))
        self.roomTypesArray.append(roomTypesDataFormat(title: "Bathroom", count: "\((self.allDataDictionary["property_other_details"] as! NSDictionary)["bathroom"] ?? "")", image: "bathroom"))
        self.roomTypesArray.append(roomTypesDataFormat(title: "Kitchen", count: "\((self.allDataDictionary["property_other_details"] as! NSDictionary)["kichen"] ?? "")", image: "kitchen"))
        
        self.roomTypesCollectionview.delegate = self
        self.roomTypesCollectionview.dataSource = self
        self.roomTypesCollectionview.reloadData()
        
    }
    
    
    
    // MARK:- // Functions to show location
    
    
    func showLocation()
    {
        
        let addressLat = "\(self.allDataDictionary["property_latitude"]!)"
        
        let addressLong = "\(self.allDataDictionary["property_longitude"]!)"
        
        UserDefaults.standard.set(addressLat, forKey: "lat")
        UserDefaults.standard.set(addressLong, forKey: "long")
        
        let camera = GMSCameraPosition.camera(withLatitude: Double(addressLat)!, longitude: Double(addressLong)!, zoom: 10.0)
        
        marker.position = CLLocationCoordinate2D(latitude: Double(addressLat)!, longitude: Double(addressLong)!)
        //        marker.title = "Selected Address"
        //        marker.snippet = "Description"
        marker.map = self.locationMapView
        self.locationMapView.mapType = .normal
        self.locationMapView.setMinZoom(1.0, maxZoom: 20.0)
        self.locationMapView.isUserInteractionEnabled = true
        self.locationMapView.animate(to: camera)
        
        
    }
    
    //MARK:- Open Map Button Action
    @IBAction func openMapBtn(_ sender: UIButton) {
        let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "mapViewVC") as! mapViewVC
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    
    
    
    
    // MARK:- // Setup Popup View
    
    func setupPopupView(type: String) {
        
        if type.elementsEqual("R") {
            self.popUpView.titleLBL.text = globalFunctions.shared.getLanguageString(variable: "property_details_violation_report")
            
            self.popUpView.submitButton.addTarget(self, action: #selector(self.reportViolation(sender:)), for: .touchUpInside)
        }
        else {
            self.popUpView.titleLBL.text = globalFunctions.shared.getLanguageString(variable: "send_message")
            
            self.popUpView.submitButton.addTarget(self, action: #selector(self.sendMessage(sender:)), for: .touchUpInside)
        }
        
        self.popUpView.closeBackgroundButton.addTarget(self, action: #selector(self.closePopupView(sender:)), for: .touchUpInside)
        
        self.popUpView.crossButton.addTarget(self, action: #selector(self.closePopupView(sender:)), for: .touchUpInside)
        
    }
    
    
    // MARK:- // Report Violation
    
    @objc func reportViolation(sender: UIButton) {
        
        self.report()
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            self.view.sendSubviewToBack(self.popUpView)
            self.popUpView.isHidden = true
            
        }
        
    }
    
    // MARK:- // Send Message
    
    @objc func sendMessage(sender: UIButton) {
        
        self.sendMessage()
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            self.view.sendSubviewToBack(self.popUpView)
            self.popUpView.isHidden = true
            
        }
        
    }
    
    // MARK:- // Close Popup VIew
    
    @objc func closePopupView(sender: UIButton) {
        
        self.view.sendSubviewToBack(self.popUpView)
        self.popUpView.isHidden = true
        
    }
    
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.descriptionTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "home_description")
        self.detailsTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "app_details")
        self.reportTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "property_details_violation_report")
        self.locationTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "home_location")
        self.callNowStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "super_agents_details_call")
        self.sendMessageStaticLBL.text = globalFunctions.shared.getLanguageString(variable: "send_message")
        self.sendEmailStaticLbl.text = globalFunctions.shared.getLanguageString(variable: "app_send_mail")
        self.translateStaticOut.text = globalFunctions.shared.getLanguageString(variable: "app_new_translate")
        //self.viewPerLBL.text = globalFunctions.shared.getLanguageString(variable: "per_month")
        
    }
    
    
    // MARK:- // Set Font
    
//    func SetFont() {
//
//        self.expireOnLBL.font = UIFont(name: self.expireOnLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 10)))
//        self.breadCrumbLBL.font = UIFont(name: self.breadCrumbLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        self.propertyName.font = UIFont(name: self.propertyName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
//        self.propertyPrice.font = UIFont(name: self.propertyPrice.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
//        self.propertyViews.font = UIFont(name: self.propertyViews.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.viewPerLBL.font = UIFont(name: self.viewPerLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.descriptionTitleLBL.font = UIFont(name: self.descriptionTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.propertyDescription.font = UIFont(name: self.propertyDescription.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        self.offeredByStaticLBL.font = UIFont(name: self.offeredByStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
//        self.agentName.font = UIFont(name: self.agentName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        self.tapToShowLBL.font = UIFont(name: self.tapToShowLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
//        self.detailsTitleLBL.font = UIFont(name: self.detailsTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.reportTitleLBL.font = UIFont(name: self.reportTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
//        self.locationTitleLBL.font = UIFont(name: self.locationTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.callNowStaticLBL.font = UIFont(name: self.callNowStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.sendMessageStaticLBL.font = UIFont(name: self.sendMessageStaticLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//
//    }
    
    
    
    func SetFont() {
        self.btnShowMoreOut.titleLabel?.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))!
        self.translateStaticOut.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 10)))
        self.expireOnLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 10)))
        self.breadCrumbLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.propertyName.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.propertyPrice.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.propertyViews.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.viewPerLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.descriptionTitleLBL.font = UIFont(name: "verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.propertyDescription.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.offeredByStaticLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.agentName.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.tapToShowLBL.font = UIFont(name: "verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.detailsTitleLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.reportTitleLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.locationTitleLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.callNowStaticLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.sendMessageStaticLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.sendEmailStaticLbl.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
    }
    
    
    // MARK:- // Share Button Action
    
    @objc func shareClick(sender: UIButton) {
        
        if let myWebsite = NSURL(string: "http://www.ibaity.com/property-details/\(self.propertyID ?? "")") {
            let objectsToShare: [Any] = ["", myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = sender
            self.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    
    
    // MARK:- // Detect Language

    func detectedLangaugeCode(for string: String) {
        
        self.globalDispatchgroup.enter()
        
        startAnimating(CGSize(width: ((30.0/320.0)*FullWidth), height: ((30.0/320.0)*FullWidth)), message: "", messageFont: UIFont(name: "Lato-Bold", size: 14), type: .ballClipRotateMultiple, color: UIColor(displayP3Red: 0/255, green: 135/255, blue: 228/255, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.4), textColor: .black, fadeInAnimation: nil)
       
        
        let  urlstr = URL(string: "https://translation.googleapis.com/language/translate/v2/detect")!   //change the url
        
        let parameters = "key=AIzaSyBWGigwjDYqBBAhvKCkldOz2q3MwsqFMow&q=\(string)"
        
        print("Parameters are : " , parameters)
        print("URL : ", "\(urlstr)" + "?" + parameters)
        
        
        let session = URLSession.shared
        
        var request = URLRequest(url: urlstr)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                
                DispatchQueue.main.async {
                    
                    self.globalDispatchgroup.leave()
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    //SVProgressHUD.dismiss()
                    
                }
                
                return
            }
            
            guard let data = data else {
                
                DispatchQueue.main.async {
                    
                    self.globalDispatchgroup.leave()
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    //SVProgressHUD.dismiss()
                    
                }
                
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("JSon Response: " , json)
                    
                    
                    self.globalJson = json.copy() as? NSDictionary
                    
                    self.detectedLanguageCode = "\(((((self.globalJson["data"] as! NSDictionary)["detections"] as! NSArray)[0] as! NSArray)[0] as! NSDictionary)["language"] ?? "")"
                    
                    DispatchQueue.main.async {
                        
                        self.globalDispatchgroup.leave()
                        
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        
                    }
                }
                
            } catch let error {
                
                print(error.localizedDescription)
                
                DispatchQueue.main.async {
                    
                    self.globalDispatchgroup.leave()
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                }
            }
        })
        task.resume()
        
    }
    
}





// MARK:- // Collectionview Delegates

extension PropertyDetailsVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.propertyImageCollectionview {
            var count = self.propertyImageArray.count
            if "\(self.allDataDictionary["property_youtube_id"] ?? "")" != ""{
               count = count + 1
            }
            self.propertyImagePagecontrol.numberOfPages = count
            self.propertyImagePagecontrol.isHidden = !(count > 1)
            
            return count
        }
        else if collectionView == self.roomTypesCollectionview {
            return self.roomTypesArray.count
        }
        else {
            return self.extendEditDelteSoldDataArray.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.propertyImageCollectionview {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "propertyImageCVC", for: indexPath) as! propertyImageCVC
            if "\(self.allDataDictionary["property_youtube_id"] ?? "")" != "" && indexPath.row == 0{
                cell.videoView.isHidden = false
                cell.cellImage.isHidden = true
//                let myview = YouTubePlayerView(frame: CGRect(x: 0, y: 0, width: self.FullWidth, height: cell.cellView.frame.size.height - 40))
//                cell.cellView.addSubview(myview)
                let myVideoURL = NSURL(string: "https://www.youtube.com/watch?v=\(self.allDataDictionary["property_youtube_id"] as! String)")
                
                cell.videoView.loadVideoURL(myVideoURL! as URL)
            }
            else{
                cell.cellImage.isHidden = false
                cell.videoView.isHidden = true
                var index = indexPath.row
                if "\(self.allDataDictionary["property_youtube_id"] ?? "")" != ""{
                    index = indexPath.row - 1
                }
               cell.cellImage.sd_setImage(with: URL(string: "\(self.propertyImageArray[index])"))
            }
            return cell
        }
        else if collectionView == self.roomTypesCollectionview {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "roomTypesCVC", for: indexPath) as! roomTypesCVC
            
            cell.roomTypeImage.image = UIImage(named: self.roomTypesArray[indexPath.item].image ?? "")
            
            cell.roomCount.text = self.roomTypesArray[indexPath.item].count!
            
            if indexPath.item == (self.roomTypesArray.count - 1) {
                cell.separator.isHidden = true
            }
            else {
                cell.separator.isHidden = false
            }
            
            return cell
        }
        else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "extendEditDeleteSoldCVC", for: indexPath) as! extendEditDeleteSoldCVC
            
            if "\(self.allDataDictionary["property_sold_status"] ?? "")".elementsEqual("1") {
                
                if indexPath.item == 2 {
                    cell.isUserInteractionEnabled = true
                    cell.backgroundColor = UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
                }
                else {
                    cell.isUserInteractionEnabled = false
                    cell.backgroundColor = .red
                }
                
            }
            else {
                cell.isUserInteractionEnabled = true
                cell.backgroundColor = UIColor(displayP3Red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
            }
            
            
            cell.cellImage.image = UIImage(named: "\(self.extendEditDelteSoldDataArray[indexPath.item].key ?? "")")
            cell.cellLBL.text = "\(self.extendEditDelteSoldDataArray[indexPath.item].value ?? "")"
            
            
            //SET FONT
            //cell.cellLBL.font = UIFont(name: "Lato-Regular", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 10)))
            
            cell.cellLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 10)))
            
            return cell
            
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.propertyImageCollectionview {
            return CGSize(width: self.FullWidth, height: 320)
        }
        else if collectionView == self.roomTypesCollectionview {
            return CGSize(width: (self.roomTypesCollectionview.frame.size.width / 4), height: 46)
        }
        else {
            return CGSize(width: (self.FullWidth / 4) - 2.5, height: 36)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == self.extendEditDeleteSoldCollectionview {
            return 2
        }
        else {
            return 0
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.extendEditDeleteSoldCollectionview {
            
            
            if indexPath.item == 0 { // Extend
                
                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "ExtendPlanVC") as! ExtendPlanVC
                
                navigate.propertyID = self.propertyID
                
                self.navigationController?.pushViewController(navigate, animated: true)
            }
            else if indexPath.item == 1 {  // Edit
                
                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "AddNewAdVC") as! AddNewAdVC
                
                navigate.categoryNameID = "\(self.allDataDictionary["category_name_id"] ?? "")"
                navigate.categoryName = "\(self.allDataDictionary["category_name"] ?? "")"
                navigate.categoryTypeID = "\(self.allDataDictionary["category_type_id"] ?? "")"
                navigate.CategoryType = "\(self.allDataDictionary["category_type"] ?? "")"
                
                navigate.propertyID = self.propertyID
                
                self.navigationController?.pushViewController(navigate, animated: true)
                
            }
            else if indexPath.item == 2 {  // Delete
                
                self.customAlert(title: "", message: "\(globalFunctions.shared.getLanguageString(variable: "modal_tex5"))", doesCancel: true) {
                    
                    self.deleteProperty {
                        
                        self.globalDispatchgroup.notify(queue: .main, execute: {
                            
                            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                            
                            self.navigationController?.pushViewController(navigate, animated: true)
                            
                        })
                    }
                }
                
            }
            else if indexPath.item == 3 {  // Sold
                
                print("\(globalFunctions.shared.getLanguageString(variable: "modal_tex9"))")
                
                self.customAlert(title: "", message: "\(globalFunctions.shared.getLanguageString(variable: "modal_tex9"))", doesCancel: true) {
                    
                    self.makePropertyAsSold {
                        
                        self.getPropertyDetails {
                            
                            self.extendEditDelteSoldDataArray[indexPath.item].key = "checkbox2"
                            
                            self.extendEditDeleteSoldCollectionview.reloadData()
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
}




// MARK:- // Scrollview Delegates

extension PropertyDetailsVC: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        propertyImagePagecontrol?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
        propertyImagePagecontrol?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    
}



// MARK:- // Tableview Delegate Methods

extension PropertyDetailsVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.propertyFeatureArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "propertyDetailsTVC") as! propertyDetailsTVC
        
        cell.cellKey.text = "\((self.propertyFeatureArray[indexPath.row])["key"] ?? "")"
        cell.cellValue.text = "\((self.propertyFeatureArray[indexPath.row])["value"] ?? "")"
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        //
//        cell.cellKey.font = UIFont(name: cell.cellKey.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        cell.cellValue.font = UIFont(name: cell.cellValue.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        
        
        //
        cell.cellKey.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        cell.cellValue.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let cell : propertyDetailsTVC = cell as! propertyDetailsTVC
        
        if (indexPath.row % 2 != 0)
        {
            
            cell.backgroundColor = UIColor(red:239/255, green:239/255, blue:244/255, alpha: 1)
        }
        
    }
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

extension UILabel {
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font!], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}
