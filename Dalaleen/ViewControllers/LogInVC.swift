//
//  LogInVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 04/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView

class LogInVC: GlobalViewController {

    @IBOutlet weak var screenBackgroundImageview: UIImageView!
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var baityLogoImageview: UIImageView!
    @IBOutlet weak var whiteBackgroundImageview: UIImageView!
    @IBOutlet weak var avatarLogo: UIImageView!
    @IBOutlet weak var infoContentView: UIView!
    @IBOutlet weak var signInHeaderLBL: UILabel!
    @IBOutlet weak var selectCountryTitleLBL: UILabel!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var dropDownImageview: UIImageView!
    @IBOutlet weak var mobileNoView: UIView!
    @IBOutlet weak var countryCode: UITextField!
    @IBOutlet weak var mobileNoTXT: UITextField!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordTXT: UITextField!
    @IBOutlet weak var showPasswordLBL: UILabel!
    @IBOutlet weak var keepMeSignedInLBL: UILabel!
    @IBOutlet weak var dontHaveAnAccountLBL: UILabel!
    @IBOutlet weak var countryListView: UIView!
    @IBOutlet weak var chooseCountryTitleLBL: UILabel!
    @IBOutlet weak var countryListTable: UITableView!
    
    
    
    
    
    var CountryCodeArray = countryPhoneCodes.shared.countryPhnCodes
    
    var loginResponseDictionary : NSDictionary!
    
    var userInformationDictionary : NSDictionary!
    
    var rememberMeChecked : Bool! = false
    
    var phoneCode : String! = ""
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        for view in self.view.subviews {
            view.isHidden = true
        }

        if UserDefaults.standard.bool(forKey: "loginStatus") == true { // Auto Login
            
            self.countryName.text = UserDefaults.standard.string(forKey: "userCountryName")
            self.countryCode.text = UserDefaults.standard.string(forKey: "userPhoneCode")
            self.mobileNoTXT.text = UserDefaults.standard.string(forKey: "userPhoneNumber")
            self.passwordTXT.text = UserDefaults.standard.string(forKey: "userPassword")
            
            //self.signIn()
            userInformation.shared.setUserID(id: UserDefaults.standard.string(forKey: "userId")!)
            
           // self.globalDispatchgroup.notify(queue: .main) {
                
                if "\(userInformation.shared.userID!)".elementsEqual("") {
                    let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                    
                    UserDefaults.standard.set(false, forKey: "loginStatus")
                    
                    self.navigationController?.pushViewController(navigate, animated: true)
                }
                else
                {
                    self.loadUserProfileDetails()
                    
                    self.globalDispatchgroup.notify(queue: .main, execute: {
                        
                        self.successfulLoginActions()
                        
                        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        
                        self.navigationController?.pushViewController(navigate, animated: true)
                        
                    })
                }
                
                
                //globalFunctions.shared.removePlaceholder(fromView: self.view)
           // }
            
        }
        else {
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                if self.SignInActivation == true
                {
                    let navigating = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    self.navigationController?.pushViewController(navigating, animated: true)
                }
            })

            for view in self.view.subviews {
                view.isHidden = false
            }

            self.countryView.layer.borderWidth = 0.5
            self.countryView.layer.borderColor = UIColor.lightGray.cgColor
            self.countryView.layer.cornerRadius = 5
            
            self.SetFont()
            
            self.localizeStrings()
            
            self.countryListTable.delegate = self
            self.countryListTable.dataSource = self
            self.countryListTable.reloadData()
            
            if globalFunctions.shared.isKeyPresentInUserDefaults(key: "rememberMe") == true {
               
                
                if UserDefaults.standard.bool(forKey: "rememberMe") == true {
                    self.keepMeSignedInButtonOutlet.isSelected = true
                    
                    self.countryCode.text = UserDefaults.standard.string(forKey: "userPhoneCode")
                    self.mobileNoTXT.text = UserDefaults.standard.string(forKey: "userPhoneNumber")
                    self.passwordTXT.text = UserDefaults.standard.string(forKey: "userPassword")
                    
                    self.loadCountryCodes()
                    
                    self.globalDispatchgroup.notify(queue: .main) {
                        
                        
                        for i in 0..<countryPhoneCodes.shared.countryPhnCodes.count {
                            
                            if (countryPhoneCodes.shared.countryPhnCodes[i].phoneCode ?? "").elementsEqual("\(UserDefaults.standard.string(forKey: "userPhoneCode") ?? "")") {
                                
                                self.countryName.text = "\(countryPhoneCodes.shared.countryPhnCodes[i].countryName ?? "")"
                                
                                UserDefaults.standard.set("\(countryPhoneCodes.shared.countryPhnCodes[i].countryName ?? "")", forKey: "userCountryName")
                                
                                break
                                
                            }
                        }
                        
                    }
                    
                }
                else {
                    self.keepMeSignedInButtonOutlet.isSelected = false
                }
            }
            else {
                self.keepMeSignedInButtonOutlet.isSelected = false
            }
            
            
        }
        
    }
    
    
    // MARK:- // View will Appear
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //globalFunctions.shared.loadPlaceholder(onView: self.view)
        
        self.countryListTable.estimatedRowHeight = 100
        self.countryListTable.rowHeight = UITableView.automaticDimension
        
        self.countryListView.isHidden = true
        
    }
    
    
    
    // MARK:- // Buttons
    
    
    // MARK:- // Skip Login Button
    
    @IBOutlet weak var skipLoginButtonOutlet: UIButton!
    @IBAction func skipLoginButton(_ sender: UIButton) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        
        userInformation.shared.setValuesForGuest()
        
        self.navigationController?.pushViewController(navigate, animated: false)
        
    }
    
    
    
    
    // MARK:- // Country Dropdown Button
    
    @IBOutlet weak var countryDropdownButtonOutlet: UIButton!
    @IBAction func countryDropdownButton(_ sender: UIButton) {
        
        if self.countryListView.isHidden == true {
            self.view.bringSubviewToFront(self.countryListView)
            self.countryListView.isHidden = false
        }
        else {
            self.view.sendSubviewToBack(self.countryListView)
            self.countryListView.isHidden = true
        }
        
    }
    
    
    // MARK:- // Show Password Button
    
    @IBOutlet weak var showPasswordButtonOutlet: UIButton!
    @IBAction func showPasswordButton(_ sender: UIButton) {
        
        self.buttonTransformIdentity(button: self.showPasswordButtonOutlet)
        
        if sender.isSelected == true {
            self.passwordTXT.isSecureTextEntry = true
        }
        else {
            self.passwordTXT.isSecureTextEntry = false
        }
        
    }
    
    
    
    // MARK:- // Keep Me Signed In Button
    
    @IBOutlet weak var keepMeSignedInButtonOutlet: UIButton!
    @IBAction func keepMeSignedInButton(_ sender: UIButton) {
        
        self.buttonTransformIdentity(button: self.keepMeSignedInButtonOutlet)
        
        if sender.isSelected == true {
            UserDefaults.standard.set(false, forKey: "rememberMe")
        }
        else {
            UserDefaults.standard.set(true, forKey: "rememberMe")
        }
        
    }
    
    
    // MARK:- // Forgot Password Button
    
    @IBOutlet weak var forgotPasswordButtonOutlet: UIButton!
    @IBAction func forgotPasswordButton(_ sender: UIButton) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK:- // Login Button
    
    @IBOutlet weak var loginButtonOutlet: UIButton!
    @IBAction func loginButton(_ sender: UIButton) {
        
        if (self.countryName.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "please_select_country"))
        }
        else if (self.mobileNoTXT.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "registration_enter_mobile_number"))
        }
        else if self.passwordTXT.text == ""{ //globalFunctions.shared.validatePassword(enteredPassword: self.passwordTXT.text!) == false {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "registration_enter_your_password"))
        }
//        else if (self.passwordTXT.text?.elementsEqual(""))! {
//            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "registration_enter_your_password"))
//        }
        else {
            signIn()
            
            self.globalDispatchgroup.notify(queue: .main) {
                

                if "\(self.loginResponseDictionary["response"] ?? "")".elementsEqual("FALSE") {
                    
                    if ("\(self.loginResponseDictionary["reason"]!)".elementsEqual("DEACTIVE")) {
                        
                        self.customAlert(title: "Warning!", message: "\(self.loginResponseDictionary["message"] ?? "")", doesCancel: false, completion: {
                            
//                            let navigate = self.storyboard?.instantiateViewController(withIdentifier: "AccountActivationVC") as! AccountActivationVC
//
//                            navigate.phoneCode = self.countryCode.text?.deletingPrefix("+ ")
//                            navigate.phoneNumber = self.mobileNoTXT.text ?? ""
//
//                            self.navigationController?.pushViewController(navigate, animated: true)
                            
                        })
                    }
                    else if ("\(self.loginResponseDictionary["reason"]!)".elementsEqual("INVALID")) {
                        
                        self.ShowAlertMessage(title: "", message: "\(self.loginResponseDictionary["message"] ?? "")")
                        
                    }
                }
                else {
                    
                    self.loadUserProfileDetails()
                    
                    self.globalDispatchgroup.notify(queue: .main, execute: {
                        
                        self.successfulLoginActions()
                        
                        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        
                        self.navigationController?.pushViewController(navigate, animated: true)
                        
                    })
                    
                    
                }
            }
        }
        
        
    }
    
    
    
    
    // MARK:- // Dont Have An Account Button
    
    @IBOutlet weak var dontHaveAnAccountButtonOutlet: UIButton!
    @IBAction func dontHaveAnAccount(_ sender: UIButton) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    
    // MARK:- // Close Country List VIew Button
    
    @IBOutlet weak var closeCountryListViewButtonOutlet: UIButton!
    @IBAction func closeCountryListViewButton(_ sender: UIButton) {
        self.view.sendSubviewToBack(self.countryListView)
        self.countryListView.isHidden = true
    }
    
    
    
    
    
    
    // MARK: - // JSON POST Method to submit SignIn Data
    
    func signIn()
        
    {
        var parameters : String = ""
        
        parameters = "phone_code=\(countryCode.text ?? "")&phone=\(mobileNoTXT.text ?? "")&password=\(passwordTXT.text ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)&ios_device_token=\(UserDefaults.standard.string(forKey: "DeviceToken") ?? "")"
        
        self.CallAPI(urlString: "login", param: parameters) {
            
            self.loginResponseDictionary = self.globalJson.copy() as? NSDictionary
            
            if "\(self.loginResponseDictionary["response"] ?? "")".elementsEqual("TRUE") {
                
                userInformation.shared.setUserID(id: "\((self.loginResponseDictionary["info"] as! NSDictionary)["id"] ?? "")")
                UserDefaults.standard.set("\((self.loginResponseDictionary["info"] as! NSDictionary)["id"] ?? "")", forKey: "userId")
                
                DispatchQueue.main.async {
                    
                    self.globalDispatchgroup.leave()
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    //SVProgressHUD.dismiss()
                    
                }
            }
            else {
                DispatchQueue.main.async {
                    
                    self.globalDispatchgroup.leave()
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    //SVProgressHUD.dismiss()
                    
                }
            }
            
        }
        
    }
    
        
    
    
    
    
    // MARK: - // JSON Get Method to get User Profile Details
    
    
    func loadUserProfileDetails() {
        
        var parameters : String!
        
        parameters = "dalaleen_user_id=\(userInformation.shared.userID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "profile_details", param: parameters) {
            
            self.userInformationDictionary = self.globalJson
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
            }
        }
    }
    
    // MARK: - // JSON Get Method to get PhoneCode and Country Data from Web
    
    func loadCountryCodes()
        
    {
        
        let parameters = "language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "country", param: parameters) {
            
            var countryCodeArray = [countryAndPhoneCodes]()
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                countryCodeArray.append(countryAndPhoneCodes(countryFlag: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["counrty_flag"] ?? "")", countryName: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["country_name"] ?? "")", countryCode: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["country_code"] ?? "")", id: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["id"] ?? "")", languageCode: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["language_code"] ?? "")", phoneCode: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["phone_code"] ?? "")", appSearchStatus: "\(((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)["appsearch_status"] ?? "")"))
            }
            
            countryPhoneCodes.shared.storeCountryPhoneCodesData(countryPhoneCodeData: countryCodeArray)
            
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
            }
            
        }
    }
    
    
    
    
    
    // MARK:- // Successful Login Actions
    
    func successfulLoginActions() {
        
        UserDefaults.standard.set(self.countryName.text, forKey: "userCountryName")
        UserDefaults.standard.set(self.countryCode.text, forKey: "userPhoneCode")
        UserDefaults.standard.set(self.mobileNoTXT.text, forKey: "userPhoneNumber")
        UserDefaults.standard.set(self.passwordTXT.text, forKey: "userPassword")
        UserDefaults.standard.set("\((self.userInformationDictionary["info"] as! NSDictionary)["id"] ?? "")", forKey: "userId")
        
        userInformation.shared.setUserID(id: "\((self.userInformationDictionary["info"] as! NSDictionary)["id"] ?? "")")
        userInformation.shared.setUserName(name: "\((self.userInformationDictionary["info"] as! NSDictionary)["name"] ?? "")")
        userInformation.shared.setUserImageString(imageString: "\((self.userInformationDictionary["info"] as! NSDictionary)["photo"] ?? "")")
        userInformation.shared.setUserEmail(email: "\((self.userInformationDictionary["info"] as! NSDictionary)["email"] ?? "")")
        userInformation.shared.setUserPhoneCode(phoneCode: "\((self.userInformationDictionary["info"] as! NSDictionary)["phone_code"] ?? "")")
        userInformation.shared.setUserPhoneNumber(phoneNumber: "\((self.userInformationDictionary["info"] as! NSDictionary)["phone"] ?? "")")
        userInformation.shared.setUserCountryID(countryID: "\((self.userInformationDictionary["info"] as! NSDictionary)["country_code"] ?? "")")
        userInformation.shared.setUserCountryName(countryName: "\((self.userInformationDictionary["info"] as! NSDictionary)["country"] ?? "")")
        userInformation.shared.setUserType(userType: "\((self.userInformationDictionary["info"] as! NSDictionary)["user_type"] ?? "")")
        userInformation.shared.setSuperAgent(superAgent: "\((self.userInformationDictionary["info"] as! NSDictionary)["super_agent"] ?? "")")
        userInformation.shared.setUserWorkPhone(workPhone: "\((self.userInformationDictionary["info"] as! NSDictionary)["work_phone"] ?? "")")
        userInformation.shared.setUserFaxNumber(faxNumber: "\((self.userInformationDictionary["info"] as! NSDictionary)["fax"] ?? "")")
        userInformation.shared.setUserAddress(address: "\((self.userInformationDictionary["info"] as! NSDictionary)["strt_address"] ?? "")")
        userInformation.shared.setUserProvinceID(provinceID: "\((self.userInformationDictionary["info"] as! NSDictionary)["province_id"] ?? "")")
        userInformation.shared.setUserProvinceName(provinceName: "\((self.userInformationDictionary["info"] as! NSDictionary)["province"] ?? "")")
        userInformation.shared.setUserDictrictID(dictrictID: "\((self.userInformationDictionary["info"] as! NSDictionary)["district_id"] ?? "")")
        userInformation.shared.setUserDictrictName(districtName: "\((self.userInformationDictionary["info"] as! NSDictionary)["district"] ?? "")")
        userInformation.shared.setUserCityID(cityID: "\((self.userInformationDictionary["info"] as! NSDictionary)["city_id"] ?? "")")
        userInformation.shared.setUserCityName(cityName: "\((self.userInformationDictionary["info"] as! NSDictionary)["city"] ?? "")")
        userInformation.shared.setUserLatitude(latitude: "\((self.userInformationDictionary["info"] as! NSDictionary)["latitude"] ?? "")")
        userInformation.shared.setUserLongitude(longitude: "\((self.userInformationDictionary["info"] as! NSDictionary)["longitude"] ?? "")")
        
        
        userInformation.shared.setUserInboxCount(inboxCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["inbox_count"] ?? "")")
        userInformation.shared.setUserRecyclebinCount(recyclebinCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["recyclebin_count"] ?? "")")
        userInformation.shared.setUserAdsCount(adsCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["my_real_estate"] ?? "")")
        userInformation.shared.setUserFavouritesCount(favouritesCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["my_fav_real_estate"] ?? "")")
        
        
        userInformation.shared.setUserCountryFlag(countryFlag: UserDefaults.standard.string(forKey: "userCountryFlag")!)
        
        
        UserDefaults.standard.set(true, forKey: "loginStatus")
        
       
        
        
    }
    

    
    // MARK:- // Set Font
    
//    func SetFont() {
//        self.signInHeaderLBL.font = UIFont(name: self.latoBoldLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
//        self.selectCountryTitleLBL.font = UIFont(name: self.latoRegularLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
//        self.countryName.font = UIFont(name: self.latoRegularLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.countryCode.font = UIFont(name: self.latoRegularLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.mobileNoTXT.font = UIFont(name: self.latoRegularLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.passwordTXT.font = UIFont(name: self.latoRegularLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        self.showPasswordLBL.font = UIFont(name: self.latoRegularLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        self.keepMeSignedInLBL.font = UIFont(name: self.latoRegularLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        self.forgotPasswordButtonOutlet.titleLabel?.font = UIFont(name: self.latoRegularLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
//        self.loginButtonOutlet.titleLabel?.font = UIFont(name: self.latoRegularLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//        self.dontHaveAnAccountLBL.font = UIFont(name: self.latoRegularLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
//    }
    
    func SetFont() {
        self.signInHeaderLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 17)))
        self.selectCountryTitleLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.countryName.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.countryCode.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.mobileNoTXT.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.passwordTXT.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.showPasswordLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.keepMeSignedInLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.forgotPasswordButtonOutlet.titleLabel?.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 12)))
        self.loginButtonOutlet.titleLabel?.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        self.dontHaveAnAccountLBL.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
    }
    
    // MARK:- // Locatize Strings
    
    func localizeStrings() {
        
        self.signInHeaderLBL.text = globalFunctions.shared.getLanguageString(variable: "header_sign_in")
        self.selectCountryTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_select_country")
        self.mobileNoTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "registration_enter_mobile_number")
        self.passwordTXT.placeholder = globalFunctions.shared.getLanguageString(variable: "registration_enter_your_password")
        self.showPasswordLBL.text = globalFunctions.shared.getLanguageString(variable: "registration_show_password")
        self.keepMeSignedInLBL.text = globalFunctions.shared.getLanguageString(variable: "login_remember_user")
        self.forgotPasswordButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "forgotpass_text1"), for: .normal)
        self.loginButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "login_page_log_in"), for: .normal)
        
        self.dontHaveAnAccountLBL.halfTextColorChange(fullText: globalFunctions.shared.getLanguageString(variable: "login_text2") + "? " + globalFunctions.shared.getLanguageString(variable: "header_sign_up") + "?", changeText: globalFunctions.shared.getLanguageString(variable: "header_sign_up") + "?", color: .darkGray)
        
    }
    
}




// MARK:- // Tableview Delegate Functions

extension LogInVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return CountryCodeArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "countryAndPhoneCodeTVC") as! countryAndPhoneCodeTVC
        
        let mydict = CountryCodeArray[indexPath.row]
        
        cell.countryName.text = "\(mydict.countryName ?? "")"
        cell.countryPhoneCode.text = "+ " + "\(mydict.phoneCode ?? "")"
        
        cell.countryImage.sd_setImage(with: URL(string: "\(mydict.countryFlag ?? "")"))
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        // Set Font
//        cell.countryName.font = UIFont(name: cell.countryName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
//        cell.countryPhoneCode.font = UIFont(name: cell.countryPhoneCode.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
        cell.countryName.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        cell.countryPhoneCode.font = UIFont(name: "Verdana", size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let mydict = CountryCodeArray[indexPath.row]
        self.countryName.text = "\(mydict.countryName ?? "")"
        self.countryCode.text = "+ " + "\(mydict.phoneCode ?? "")"
        UserDefaults.standard.set("\(mydict.countryFlag ?? "")", forKey: "userCountryFlag")
        self.phoneCode = "\(mydict.phoneCode ?? "")"
        self.view.sendSubviewToBack(self.countryListView)
        self.countryListView.isHidden = true
    }
    
}







