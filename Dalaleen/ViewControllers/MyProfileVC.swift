//
//  MyProfileVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 24/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import MapKit
import NVActivityIndicatorView
import GoogleMaps
import GooglePlaces
import FirebaseUI

class MyProfileVC: GlobalViewController {

    
    @IBOutlet weak var viewOfPicker: UIView!
    @IBOutlet weak var typeOfAccountPickerview: UIPickerView!
    @IBOutlet weak var countryPickerview: UIPickerView!
    @IBOutlet weak var provincePickerview: UIPickerView!
    @IBOutlet weak var cityPickerview: UIPickerView!
    @IBOutlet weak var districtPickerview: UIPickerView!
    
    
    
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var accountTypeAndNameView: UIView!
    @IBOutlet weak var typeOfAccountTitleLBL: UILabel!
    @IBOutlet weak var typeOfAccount: UILabel!
    @IBOutlet weak var profileNameTitleLBL: UILabel!
    @IBOutlet weak var profileNameTXT: UITextField!
    @IBOutlet weak var profileLogoAndCardView: UIView!
    @IBOutlet weak var profileLogoAndCardTitleLBL: UILabel!
    @IBOutlet weak var profileLogoImageview: UIImageView!
    @IBOutlet weak var regInfoSectionOneView: UIView!
    @IBOutlet weak var registrationInfoTitleLBL: UILabel!
    @IBOutlet weak var countryTitleLBL: UILabel!
    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var phoneNumberTitleLBL: UILabel!
    @IBOutlet weak var phoneCode: UILabel!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var emailTitleLBL: UILabel!
    @IBOutlet weak var emailTXT: UITextField!
    @IBOutlet weak var regInfoSectionTwoView: UIView!
    @IBOutlet weak var officePhoneTitleLBL: UILabel!
    @IBOutlet weak var officePhoneCode: UILabel!
    @IBOutlet weak var officePhoneNumberTXT: UITextField!
    @IBOutlet weak var faxTitleLBL: UILabel!
    @IBOutlet weak var faxPhoneCode: UILabel!
    @IBOutlet weak var faxNumberTXT: UITextField!
    @IBOutlet weak var regInfoThreeView: UIView!
    @IBOutlet weak var provinceTitleLBL: UILabel!
    @IBOutlet weak var province: UILabel!
    @IBOutlet weak var cityTitleLBL: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var districtTitleLBL: UILabel!
    @IBOutlet weak var district: UILabel!
    @IBOutlet weak var addressTitleLBL: UILabel!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var locationTitleLBL: UILabel!
    @IBOutlet weak var locationMapView: GMSMapView!
    
    
//    pod 'DropDown', '2.3.4'
    
    var accountType : String! = "\(userInformation.shared.userType ?? "")"
    
    var countryID : String! = "\(userInformation.shared.userCountryID ?? "")"
    
    var provinceID : String! = "\(userInformation.shared.userProvinceID ?? "")"
    
    var districtID = "\(userInformation.shared.userDistrictID ?? "")"
    
    var cityID = "\(userInformation.shared.userCityID ?? "")"
    
    var marker = GMSMarker()
    
    var typeOfAccountArray = [["key" : "A" , "value" : "Agent"] , ["key" : "U" , "value" : "User"]]
    
    var countryArray = [countryAndPhoneCodes]()
    
    var provinceArray = [NSDictionary]()
    
    var cityArray = [NSDictionary]()
    
    var districtArray = [NSDictionary]()
    
    var userInformationDictionary : NSDictionary!
    
    var userPhoneCode : String! = "\(userInformation.shared.userPhoneCode ?? "")"
    
    var locationManager = CLLocationManager()
    
    var userLat : String!
    var userLong : String!
    
    var chosenPlace: MyPlace?
    
    var newBannerUploaded : Bool! = false
    
    var doneClicked : Bool!
    
    var selectedPickerIndex : Int!
    
    var selectedPickerview : UIPickerView!
    
    var verificationID : String!
    
    var userCountryFlagString : String? = ""
    
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        globalFunctions.shared.loadPlaceholder(onView: self.view)
        
        locationManager.requestWhenInUseAuthorization()
        
        self.localizeStrings()
        
        self.setFont()
        
        self.showOrHideSections()
        
        
        self.typeOfAccountPickerview.delegate = self
        self.typeOfAccountPickerview.dataSource = self
        
        self.address.delegate = self
        
        self.loadUserProfileDetails()
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            self.populateFieldsWithData()
            
            self.saveuserInformations()
            
            self.getCountryList()
            
            self.globalDispatchgroup.notify(queue: .main) {
                
                self.getProvinceList(countryID: "102", completion: {
                    
                    print(self.provinceArray.count)
                    
                    for i in 0..<self.provinceArray.count {
                        
                        if "\((self.userInformationDictionary["info"] as! NSDictionary)["province_id"] ?? "")".elementsEqual("\((self.provinceArray[i])["id"] ?? "")") {
                            
                            self.provinceID = "\((self.provinceArray[i])["id"] ?? "")"
                            
                        }
                        
                    }
                    
                    if self.cityID.elementsEqual("0") == false {
                        
                        self.getCityList(provinceID: self.provinceID, completion: {
                            
                            for i in 0..<self.cityArray.count {
                                
                                if "\((self.userInformationDictionary["info"] as! NSDictionary)["city_id"] ?? "")".elementsEqual("\((self.cityArray[i])["id"] ?? "")") {
                                    
                                    self.cityID = "\((self.cityArray[i])["id"] ?? "")"
                                    
                                }
                                
                            }
                            
                            self.getDistrictList(cityID: self.cityID)
                            
                            self.globalDispatchgroup.notify(queue: .main, execute: {
                                
                                
                                
                            })
                            
                        })
                        
                    }
                    
                    
                })
                
            }
            
            if "\(userInformation.shared.userLatitute ?? "")".elementsEqual("") {
                
                self.locationManager.requestWhenInUseAuthorization()
                
                if CLLocationManager.locationServicesEnabled() {
                    
                    self.locationManager.delegate = self
                    self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                    self.locationManager.startUpdatingLocation()
                    
                }
                
            }
            else {
                self.showLocation(addressLat: "\(userInformation.shared.userLatitute!)", addressLong: "\(userInformation.shared.userLongitude!)")
                
                self.userLat = "\(userInformation.shared.userLatitute!)"
                self.userLong = "\(userInformation.shared.userLongitude!)"
            }
            
            globalFunctions.shared.removePlaceholder(fromView: self.view)
            
        }
        self.createToolbar()
        self.locationMapView.delegate = self
        
        
    }
    
    // MARK:- // Create Picker Toolbar
    
    func createToolbar() {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.tintColor = UIColor.white//"Done" button colour
        toolBar.barTintColor = UIColor.black// bar background colour
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "\(globalFunctions.shared.getLanguageString(variable: "ios_ok"))", style: UIBarButtonItem.Style.done, target: self, action: #selector(donePicker))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "\(globalFunctions.shared.getLanguageString(variable: "modal_button_text1"))", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelPicker))
        
        toolBar.setItems([cancelButton, spacer, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.viewOfPicker.addSubview(toolBar)
        toolBar.anchor(top: nil, leading: nil, bottom: self.provincePickerview.topAnchor, trailing: nil, size: .init(width: UIScreen.main.bounds.size.width, height: 40))
        
    }
    
    // MARK:- // Done Picker Action Method
    
    @objc func donePicker() {
        
        print("Done Clicked")
        
        self.doneClicked = true
        
        self.pickerView(self.selectedPickerview, didSelectRow: self.selectedPickerIndex, inComponent: 0)
        
        self.view.sendSubviewToBack(self.viewOfPicker)
        self.viewOfPicker.isHidden = true
        self.selectedPickerview.isHidden = true
        
    }
    
    // MARK:- // Cancel Picker Action Method
    
    @objc func cancelPicker() {
        print("Cancel Clicked")
        
        self.view.sendSubviewToBack(self.viewOfPicker)
        self.viewOfPicker.isHidden = true
        self.selectedPickerview.isHidden = true
    }
    
    
    
    /*
    func storeAllUserDetails() {
        
        
        userInformation.shared.setUserName(name: "\((self.userInformationDictionary["info"] as! NSDictionary)["name"] ?? "")")
        userInformation.shared.setUserImageString(imageString: "\((self.userInformationDictionary["info"] as! NSDictionary)["photo"] ?? "")")
        userInformation.shared.setUserEmail(email: "\((self.userInformationDictionary["info"] as! NSDictionary)["email"] ?? "")")
        userInformation.shared.setUserPhoneCode(phoneCode: "\((self.userInformationDictionary["info"] as! NSDictionary)["phone_code"] ?? "")")
        userInformation.shared.setUserPhoneNumber(phoneNumber: "\((self.userInformationDictionary["info"] as! NSDictionary)["phone"] ?? "")")
        userInformation.shared.setUserCountryID(countryID: "\((self.userInformationDictionary["info"] as! NSDictionary)["country_code"] ?? "")")
        userInformation.shared.setUserCountryName(countryName: "\((self.userInformationDictionary["info"] as! NSDictionary)["country"] ?? "")")
        userInformation.shared.setUserType(userType: "\((self.userInformationDictionary["info"] as! NSDictionary)["user_type"] ?? "")")
        userInformation.shared.setSuperAgent(superAgent: "\((self.userInformationDictionary["info"] as! NSDictionary)["super_agent"] ?? "")")
        userInformation.shared.setUserWorkPhone(workPhone: "\((self.userInformationDictionary["info"] as! NSDictionary)["work_phone"] ?? "")")
        userInformation.shared.setUserFaxNumber(faxNumber: "\((self.userInformationDictionary["info"] as! NSDictionary)["fax"] ?? "")")
        userInformation.shared.setUserAddress(address: "\((self.userInformationDictionary["info"] as! NSDictionary)["strt_address"] ?? "")")
        userInformation.shared.setUserProvinceID(provinceID: "\((self.userInformationDictionary["info"] as! NSDictionary)["province_id"] ?? "")")
        userInformation.shared.setUserProvinceName(provinceName: "\((self.userInformationDictionary["info"] as! NSDictionary)["province"] ?? "")")
        userInformation.shared.setUserDictrictID(dictrictID: "\((self.userInformationDictionary["info"] as! NSDictionary)["district_id"] ?? "")")
        userInformation.shared.setUserDictrictName(districtName: "\((self.userInformationDictionary["info"] as! NSDictionary)["district"] ?? "")")
        userInformation.shared.setUserCityID(cityID: "\((self.userInformationDictionary["info"] as! NSDictionary)["city_id"] ?? "")")
        userInformation.shared.setUserCityName(cityName: "\((self.userInformationDictionary["info"] as! NSDictionary)["city"] ?? "")")
        userInformation.shared.setUserLatitude(latitude: "\((self.userInformationDictionary["info"] as! NSDictionary)["latitude"] ?? "")")
        userInformation.shared.setUserLongitude(longitude: "\((self.userInformationDictionary["info"] as! NSDictionary)["longitude"] ?? "")")
        
        
        userInformation.shared.setUserInboxCount(inboxCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["inbox_count"] ?? "")")
        userInformation.shared.setUserRecyclebinCount(recyclebinCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["recyclebin_count"] ?? "")")
        userInformation.shared.setUserAdsCount(adsCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["my_real_estate"] ?? "")")
        userInformation.shared.setUserFavouritesCount(favouritesCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["my_fav_real_estate"] ?? "")")
        
        
        userInformation.shared.setUserCountryFlag(countryFlag: UserDefaults.standard.string(forKey: "userCountryFlag")!)
        
    }
    */
    
    
    // MARK:- // Show or Hide Sections
    
    func showOrHideSections() {
        
        if accountType.elementsEqual("U") {
            self.regInfoSectionTwoView.isHidden = true
            self.locationView.isHidden = true
        }
        else {
            self.regInfoSectionTwoView.isHidden = false
            self.locationView.isHidden = false
        }
        
    }
    
    
    
    
    // MARK:- // Buttons
    
    
    // MARK:- // Validation
    
    func validation(completion : @escaping ()->()) {
        
        if self.accountType.elementsEqual("") {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "my_profile_account_type"))
        }
        else if (self.profileNameTXT.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "registration_enter_your_name"))
        }
        else if self.profileLogoImageview.image == nil {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "please_select_profile_image"))
        }
        else if (self.countryName.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "please_select_country"))
        }
        else if (self.phoneNumber.text?.elementsEqual(""))! {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "registration_enter_mobile_number"))
        }
        else if globalFunctions.shared.validateEmail(enteredEmail: self.emailTXT.text ?? "") == false {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "registration_enter_your_email"))
        }
        else if self.provinceID.elementsEqual("") //|| self.provinceID.elementsEqual("0")
        {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "property_post_province"))
        }
        else if self.cityID.elementsEqual("")  //|| self.cityID.elementsEqual("0")
        {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "property_post_city"))
        }
        else if self.districtID.elementsEqual("") //|| self.districtID.elementsEqual("0")
    {
            self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "property_post_district"))
        }
        else {
            if self.accountType.elementsEqual("U") {
                completion()
            }
            else {
                if (self.officePhoneNumberTXT.text?.elementsEqual(""))! {
                    self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "my_profile_office_phone"))
                }
                else if (self.faxNumberTXT.text?.elementsEqual(""))! {
                    self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "my_profile_fax"))
                }
                else if (self.address.text?.elementsEqual(""))! {
                    self.ShowAlertMessage(title: "", message: globalFunctions.shared.getLanguageString(variable: "property_post_map_address_validation"))
                }
                else {
                    completion()
                }
            }
        }
        
    }
    
    //7877 7544 2539
    
    
    // MARK:- // Save Button
    
    @IBOutlet weak var saveButtonOutlet: UIButton!
    @IBAction func saveButton(_ sender: UIButton) {
        
        
        self.validation {
            
            if "\(self.phoneNumber.text ?? "")".elementsEqual("\(userInformation.shared.userPhoneNumber ?? "")") {
                
                self.saveUserProfile()
                
                self.globalDispatchgroup.notify(queue: .main) {
                    
                    self.loadUserProfileDetails()
                    
                    self.globalDispatchgroup.notify(queue: .main, execute: {
                        
                        self.saveuserInformations()
                        
                        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        
                        self.navigationController?.pushViewController(navigate, animated: true)
                        
                        //                    self.navigationController?.popViewController(animated: true)
                        
                    })
                    
                }
                
            }
            else {
                
                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "UserProfilePhoneVerificationVC") as! UserProfilePhoneVerificationVC
                
                navigate.profileID = "\(userInformation.shared.userID ?? "")"
                navigate.profileName = "\(self.profileNameTXT.text ?? "")"
                navigate.profileEmail = "\(self.emailTXT.text ?? "")"
                navigate.profileAccountType = "\(self.accountType ?? "")"
                navigate.profileCountry = "\(self.countryID ?? "")"
                navigate.profileProvince = "\(self.provinceID ?? "")"
                navigate.profileCity = "\(self.cityID)"
                navigate.profileDistrict = "\(self.districtID)"
                navigate.profilePhoneNumber = "\(self.phoneNumber.text ?? "")"
                navigate.profilePhoneCode = "\(self.userPhoneCode ?? "")"
                navigate.profileWorkPhone = "\(self.officePhoneNumberTXT.text ?? "")"
                navigate.profileFax = "\(self.faxNumberTXT.text ?? "")"
                navigate.profileStreet = "\(self.address.text ?? "")"
                navigate.profileLat = "\(self.userLat ?? "")"
                navigate.profileLong = "\(self.userLong ?? "")"
                navigate.profileBannerImage = self.profileLogoImageview.image
                navigate.newBannerUploaded = self.newBannerUploaded
                navigate.profileCountryFlagString = self.userCountryFlagString
                
                self.navigationController?.pushViewController(navigate, animated: true)
                
            }
            
        }
        
    }
    
    
    
    
    // MARK:- // District Button
    
    @IBOutlet weak var districtButtonOutlet: UIButton!
    @IBAction func districtButton(_ sender: UIButton) {
        
        if self.districtArray.count == 0 {
            
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "please_select_city"))
        }
        else {
            
            self.view.bringSubviewToFront(self.viewOfPicker)
            self.viewOfPicker.isHidden = false
            self.districtPickerview.isHidden = false
            self.selectedPickerview = self.districtPickerview
            self.selectedPickerIndex = 0
            self.doneClicked = false
        }
        
    }
    
    
    
    // MARK:- // City Button
    
    @IBOutlet weak var cityButtonOutlet: UIButton!
    @IBAction func cityButton(_ sender: UIButton) {
        
        if self.cityArray.count == 0 {
            
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "please_select_province"))
        }
        else {
            
            self.view.bringSubviewToFront(self.viewOfPicker)
            self.viewOfPicker.isHidden = false
            self.cityPickerview.isHidden = false
            self.selectedPickerview = self.cityPickerview
            self.selectedPickerIndex = 0
            self.doneClicked = false
        }
        
    }
    
    
    
    
    
    // MARK:- // Province Button
    
    @IBOutlet weak var provinceButtonOutlet: UIButton!
    @IBAction func provinceButton(_ sender: UIButton) {
        
        if self.provinceArray.count == 0 {
            
            self.ShowAlertMessage(title: globalFunctions.shared.getLanguageString(variable: "error_msg"), message: globalFunctions.shared.getLanguageString(variable: "my_profile_select_country"))
        }
        else {
            
            self.view.bringSubviewToFront(self.viewOfPicker)
            self.viewOfPicker.isHidden = false
            self.provincePickerview.isHidden = false
            self.selectedPickerview = self.provincePickerview
            self.selectedPickerIndex = 0
            self.doneClicked = false
        }
        
    }
    
    
    
    
    // MARK:- // Country Button
    
    @IBOutlet weak var countryButtonOutlet: UIButton!
    @IBAction func countryButton(_ sender: UIButton) {
        
        self.view.bringSubviewToFront(self.viewOfPicker)
        self.viewOfPicker.isHidden = false
        self.countryPickerview.isHidden = false
        self.selectedPickerview = self.countryPickerview
        self.selectedPickerIndex = 0
        self.doneClicked = false
        
    }
    
    
    
    
    
    // MARK:- // Profile Logo Button
    
    @IBOutlet weak var profileLogoButtonOutlet: UIButton!
    @IBAction func profileLogoButton(_ sender: UIButton) {
        
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a Source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    
    
    
    // MARK:- // Type of Account Button
    
    @IBOutlet weak var typeOfAccountButtonOutlet: UIButton!
    @IBAction func typeOfAccountButton(_ sender: UIButton) {
        
        self.view.bringSubviewToFront(self.viewOfPicker)
        self.viewOfPicker.isHidden = false
        self.typeOfAccountPickerview.isHidden = false
        self.selectedPickerview = self.typeOfAccountPickerview
        self.selectedPickerIndex = 0
        self.doneClicked = false
        
    }
    
    
    // MARK:- // Close Pickerview Button
    
    @IBOutlet weak var closePickerviewButtonOutlet: UIButton!
    @IBAction func closePickerviewButton(_ sender: UIButton) {
        
        self.view.sendSubviewToBack(self.viewOfPicker)
        self.viewOfPicker.isHidden = true
        self.typeOfAccountPickerview.isHidden = true
        self.countryPickerview.isHidden = true
        self.provincePickerview.isHidden = true
        self.cityPickerview.isHidden = true
        self.districtPickerview.isHidden = true
       
        
    }
    
    
    
    // MARK:- // JSON Post Method to fetch Country List
    
    func getCountryList() {
        
        let parameters = "language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "country", param: parameters) {
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                
                let tempDict = (self.globalJson["info"] as! NSArray)[i] as! NSDictionary
                
                self.countryArray.append(countryAndPhoneCodes(countryFlag: "\(tempDict["counrty_flag"] ?? "")", countryName: "\(tempDict["country_name"] ?? "")", countryCode: "\(tempDict["country_code"] ?? "")", id: "\(tempDict["id"] ?? "")", languageCode: "\(tempDict["language_code"] ?? "")", phoneCode: "\(tempDict["phone_code"] ?? "")", appSearchStatus: "\(tempDict["appsearch_status"] ?? "")"))
            }
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.countryPickerview.delegate = self
                self.countryPickerview.dataSource = self
                self.countryPickerview.reloadAllComponents()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
        }
    }
    
    
    // MARK:- // JSON Post Method to fetch Province List
    
    func getProvinceList(countryID : String , completion : @escaping ()->()) {
        
        let parameters = "country_id=\(countryID)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "fetch_province_by_country", param: parameters) {
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                
                let tempDict = (self.globalJson["info"] as! NSArray)[i] as! NSDictionary
                
                self.provinceArray.append(tempDict)
                
            }
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.provincePickerview.delegate = self
                self.provincePickerview.dataSource = self
                self.provincePickerview.reloadAllComponents()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
                completion()
                
            }
        }
    }
    
    
    
    // MARK:- // JSON Post Method to get City List
    
    func getCityList(provinceID : String , completion : @escaping ()->()) {
        
        let parameters = "province_id=\(provinceID)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "fetch_city_by_province", param: parameters) {
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                
                let tempDict = (self.globalJson["info"] as! NSArray)[i] as! NSDictionary
                
                self.cityArray.append(tempDict)
                
            }
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.cityPickerview.delegate = self
                self.cityPickerview.dataSource = self
                self.cityPickerview.reloadAllComponents()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
                completion()
                
            }
        }
        
    }
    
    
    // MARK:- // JSON Post Method to fetch District List
    
    func getDistrictList(cityID : String) {
        
        let parameters = "city_id=\(cityID)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "fetch_district_by_city", param: parameters) {
            
            for i in 1..<(self.globalJson["info"] as! NSArray).count {
                
                let tempDict = (self.globalJson["info"] as! NSArray)[i] as! NSDictionary
                
                self.districtArray.append(tempDict)
                
            }
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.districtPickerview.delegate = self
                self.districtPickerview.dataSource = self
                self.districtPickerview.reloadAllComponents()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
                
            }
        }
        
    }
    
    
    
    // MARK:- // Alamofire Method to Save User Profile
    
    func saveUserProfile() {
        
        
        let bannerImageData = self.profileLogoImageview.image!.jpegData(compressionQuality: 1)!
        
        self.globalDispatchgroup.enter()
        
        startAnimating(CGSize(width: ((30.0/320.0)*FullWidth), height: ((30.0/320.0)*FullWidth)), message: "", messageFont: UIFont(name: "Lato-Bold", size: 14), type: .ballClipRotateMultiple, color: UIColor(displayP3Red: 0/255, green: 135/255, blue: 228/255, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.4), textColor: .black, fadeInAnimation: nil)
        
//        DispatchQueue.main.async {
//
//            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
//        }
        
        let parameters = [
            "dalaleen_user_id" : "\(userInformation.shared.userID ?? "")" , "name" : "\(self.profileNameTXT.text ?? "")" , "email" : "\(self.emailTXT.text ?? "")" , "account_type" : "\(self.accountType ?? "")" , "country" : "\(self.countryID ?? "")" , "province" : "\(self.provinceID ?? "")" , "city" : "\(self.cityID)" , "district" : "\(self.districtID)" , "phone" : "\(self.phoneNumber.text ?? "")" , "phone_code" : "\(self.userPhoneCode ?? "")" , "work_phone" : "\(self.officePhoneNumberTXT.text ?? "")" , "fax" : "\(self.faxNumberTXT.text ?? "")" , "strt_address" : "\(self.address.text ?? "")" , "verify_lat" : "\(self.userLat ?? "")" , "verify_long" : "\(self.userLong ?? "")" , "language_code" : "\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"]
        
        print("Parameters are---",parameters)
        
        Alamofire.upload(
            multipartFormData: { MultipartFormData in
                
                for (key, value) in parameters {
                    MultipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
                
                if self.newBannerUploaded == true {
                    MultipartFormData.append(bannerImageData, withName: "profile_image", fileName: "profileImage.jpeg", mimeType: "image/jpeg")
                }
                
                
                
                
                
        }, to: (GLOBALAPI + "profile_edit"))
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    //
                    print(response.request!)  // original URL request
                    print(response.response!) // URL response
                    print(response.data!)     // server data
                    print(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value {
                        
                        print("Image Search Response-----: \(JSON)")
                        
                        let tempdict = JSON as! NSDictionary
                        
                    userInformation.shared.setUserImageString(imageString: "\(tempdict["profileimg"] ?? "")")
                        
                        
                        
                        DispatchQueue.main.async {
                            
                            self.globalDispatchgroup.leave()
                            
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                            //SVProgressHUD.dismiss()
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            
                            //self.globalDispatchgroup.leave()
                            
                            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                            //SVProgressHUD.dismiss()
                        }
                    }
                }
                
            case .failure(let encodingError):
                
                print(encodingError)
                
                DispatchQueue.main.async {
                    
                    //self.globalDispatchgroup.leave()
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    //SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    
    
    // MARK: - // JSON Get Method to get User Profile Details
    
    
    func loadUserProfileDetails() {
        
        var parameters : String!
        
        parameters = "dalaleen_user_id=\(userInformation.shared.userID ?? "")&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "profile_details", param: parameters) {
            
            self.userInformationDictionary = self.globalJson
            
            DispatchQueue.main.async {
                
                if ((self.userInformationDictionary["info"] as! NSDictionary)["user_type"] as! String) == "U"{
                    self.setCurrentLocation()
                }
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
            }
        }
    }
    
    
    // MARK:- // JSON Post Method to Check if the No Exists
    
    func checkNumberExisis(completion : @escaping ()->()) {
        
        var parameters : String = ""
        
        parameters = "phone_code=\(self.userPhoneCode ?? "")&phone=\(self.phoneNumber.text ?? "")&email=\(self.emailTXT.text ?? "")&user_type=\(self.accountType ?? "")"
        
        self.CallAPI(urlString: "Phone_no_check", param: parameters) {
            
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                completion()
                
            }
            
        }
        
    }
    
    
    
    
    
    // MARK:- // Save user informations
    
    func saveuserInformations() {
        
        UserDefaults.standard.set(self.countryName.text, forKey: "userCountryName")
        UserDefaults.standard.set(self.userPhoneCode, forKey: "userPhoneCode")
        UserDefaults.standard.set(self.phoneNumber.text, forKey: "userPhoneNumber")
        
        
        userInformation.shared.setUserID(id: "\((self.userInformationDictionary["info"] as! NSDictionary)["id"] ?? "")")
        userInformation.shared.setUserName(name: "\((self.userInformationDictionary["info"] as! NSDictionary)["name"] ?? "")")
        
        userInformation.shared.setUserImageString(imageString: "\((self.userInformationDictionary["info"] as! NSDictionary)["photo"] ?? "")")
        userInformation.shared.setUserEmail(email: "\((self.userInformationDictionary["info"] as! NSDictionary)["email"] ?? "")")
        userInformation.shared.setUserPhoneCode(phoneCode: "\((self.userInformationDictionary["info"] as! NSDictionary)["phone_code"] ?? "")")
        userInformation.shared.setUserPhoneNumber(phoneNumber: "\((self.userInformationDictionary["info"] as! NSDictionary)["phone"] ?? "")")
        userInformation.shared.setUserCountryID(countryID: "\((self.userInformationDictionary["info"] as! NSDictionary)["country_code"] ?? "")")
        userInformation.shared.setUserCountryName(countryName: "\((self.userInformationDictionary["info"] as! NSDictionary)["country"] ?? "")")
        userInformation.shared.setUserType(userType: "\((self.userInformationDictionary["info"] as! NSDictionary)["user_type"] ?? "")")
        userInformation.shared.setSuperAgent(superAgent: "\((self.userInformationDictionary["info"] as! NSDictionary)["super_agent"] ?? "")")
        userInformation.shared.setUserWorkPhone(workPhone: "\((self.userInformationDictionary["info"] as! NSDictionary)["work_phone"] ?? "")")
        userInformation.shared.setUserFaxNumber(faxNumber: "\((self.userInformationDictionary["info"] as! NSDictionary)["fax"] ?? "")")
        userInformation.shared.setUserAddress(address: "\((self.userInformationDictionary["info"] as! NSDictionary)["strt_address"] ?? "")")
        userInformation.shared.setUserProvinceID(provinceID: "\((self.userInformationDictionary["info"] as! NSDictionary)["province_id"] ?? "")")
        userInformation.shared.setUserProvinceName(provinceName: "\((self.userInformationDictionary["info"] as! NSDictionary)["province"] ?? "")")
        userInformation.shared.setUserDictrictID(dictrictID: "\((self.userInformationDictionary["info"] as! NSDictionary)["district_id"] ?? "")")
        userInformation.shared.setUserDictrictName(districtName: "\((self.userInformationDictionary["info"] as! NSDictionary)["district"] ?? "")")
        userInformation.shared.setUserCityID(cityID: "\((self.userInformationDictionary["info"] as! NSDictionary)["city_id"] ?? "")")
        userInformation.shared.setUserCityName(cityName: "\((self.userInformationDictionary["info"] as! NSDictionary)["city"] ?? "")")
        userInformation.shared.setUserLatitude(latitude: "\((self.userInformationDictionary["info"] as! NSDictionary)["latitude"] ?? "")")
        userInformation.shared.setUserLongitude(longitude: "\((self.userInformationDictionary["info"] as! NSDictionary)["longitude"] ?? "")")
        
        
        userInformation.shared.setUserInboxCount(inboxCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["inbox_count"] ?? "")")
        userInformation.shared.setUserRecyclebinCount(recyclebinCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["recyclebin_count"] ?? "")")
        userInformation.shared.setUserAdsCount(adsCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["my_real_estate"] ?? "")")
        userInformation.shared.setUserFavouritesCount(favouritesCount: "\((self.userInformationDictionary["count_all_info"] as! NSDictionary)["my_fav_real_estate"] ?? "")")
        
        
        //userInformation.shared.setUserCountryFlag(countryFlag: UserDefaults.standard.string(forKey: "userCountryFlag")!)
        
        
    }
    
    
    
    
    
    
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.typeOfAccountTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_account_type")
        self.profileNameTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_profile_name")
        self.profileLogoAndCardTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "app_msg3")
        self.registrationInfoTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "ios_gegistration_info")
        self.countryTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_country")
        self.phoneNumberTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "phone")
        self.emailTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_email")
        self.officePhoneTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_office_phone")
        self.faxTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_fax")
        self.provinceTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "home_province")
        self.cityTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "home_city")
        self.districtTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "home_district")
        self.addressTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "my_profile_address")
        self.provinceTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "property_post_location")
        
        self.locationTitleLBL.text = globalFunctions.shared.getLanguageString(variable: "home_location")
        self.saveButtonOutlet.setTitle(globalFunctions.shared.getLanguageString(variable: "property_details_violation_report_save"), for: .normal)
        
        
    }
    
    
    // MARK:- // Set Font
    
    func setFont() {
        
        self.typeOfAccountTitleLBL.font = UIFont(name: self.typeOfAccountTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.typeOfAccount.font = UIFont(name: self.typeOfAccount.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.profileNameTitleLBL.font = UIFont(name: self.profileNameTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.profileNameTXT.font = UIFont(name: self.profileNameTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.profileLogoAndCardTitleLBL.font = UIFont(name: self.profileLogoAndCardTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.registrationInfoTitleLBL.font = UIFont(name: self.registrationInfoTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.countryTitleLBL.font = UIFont(name: self.countryTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.countryName.font = UIFont(name: self.countryName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.phoneNumberTitleLBL.font = UIFont(name: self.phoneNumberTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.phoneCode.font = UIFont(name: self.phoneCode.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.phoneNumber.font = UIFont(name: self.phoneNumber.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.emailTitleLBL.font = UIFont(name: self.emailTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.emailTXT.font = UIFont(name: self.emailTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.officePhoneTitleLBL.font = UIFont(name: self.officePhoneTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.officePhoneCode.font = UIFont(name: self.officePhoneCode.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.officePhoneNumberTXT.font = UIFont(name: self.officePhoneNumberTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.faxTitleLBL.font = UIFont(name: self.faxTitleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.faxPhoneCode.font = UIFont(name: self.faxPhoneCode.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.faxNumberTXT.font = UIFont(name: self.faxNumberTXT.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.provinceTitleLBL.font = UIFont(name: self.provinceTitleLBL.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.province.font = UIFont(name: self.province.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.cityTitleLBL.font = UIFont(name: self.cityTitleLBL.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.city.font = UIFont(name: self.city.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.districtTitleLBL.font = UIFont(name: self.districtTitleLBL.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.district.font = UIFont(name: self.district.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.addressTitleLBL.font = UIFont(name: self.addressTitleLBL.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.address.font = UIFont(name: self.address.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.locationTitleLBL.font = UIFont(name: self.locationTitleLBL.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.saveButtonOutlet.titleLabel!.font = UIFont(name: self.saveButtonOutlet.titleLabel!.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
        
        
    }
    
    
    
    // MARK:- // Functions to show location
    
    
    func showLocation(addressLat : String , addressLong : String)
    {
        
        let camera = GMSCameraPosition.camera(withLatitude: Double(addressLat)!, longitude: Double(addressLong)!, zoom: 10.0)
        
        marker.position = CLLocationCoordinate2D(latitude: Double(addressLat)!, longitude: Double(addressLong)!)
        marker.map = self.locationMapView
        marker.isDraggable = true
        locationMapView.setMinZoom(5, maxZoom: 20)
        
        self.locationMapView.animate(to: camera)
        
    }
    
    
    
    
    // MARK:- // Populate Fields with Data
    
    func populateFieldsWithData() {
        
        if "\(userInformation.shared.userType ?? "")".elementsEqual("A") {
            self.typeOfAccount.text = globalFunctions.shared.getLanguageString(variable: "home_agent")
        }
        else {
            self.typeOfAccount.text = globalFunctions.shared.getLanguageString(variable: "my_profile_user")
        }
        
        self.profileNameTXT.text = userInformation.shared.userName
        
        
        globalFunctions.shared.loadImage(inImageview: self.profileLogoImageview, url: "\(userInformation.shared.userImageString ?? "")", indicatorInView: self.profileLogoImageview)
        
        //self.profileLogoImageview.sd_setImage(with: URL(string: "\(userInformation.shared.userImageString ?? "")"))
        //globalFunctions.shared.loadImage(inImageview: self.profileLogoImageview, url: "\(userInformation.shared.userImageString ?? "")", indicatorInView: self.profileLogoAndCardView)
        
        globalFunctions.shared.loadImage(inImageview: self.countryImage, url: "\(UserDefaults.standard.string(forKey: "userCountryFlag") ?? "")", indicatorInView: self.countryImage)
        self.countryName.text = userInformation.shared.userCountryName
        self.phoneCode.text = "+(" + "\(userInformation.shared.userPhoneCode ?? "")" + ")"
        self.phoneNumber.text = userInformation.shared.userPhoneNumber
        self.emailTXT.text = userInformation.shared.userEmail
        self.officePhoneCode.text = "(+" + "\(userInformation.shared.userPhoneCode ?? "")" + ")"
        self.officePhoneNumberTXT.text = userInformation.shared.userWorkPhone
        self.faxPhoneCode.text = "(+" + "\(userInformation.shared.userPhoneCode ?? "")" + ")"
        self.faxNumberTXT.text = userInformation.shared.userFaxNumber
        self.province.text = userInformation.shared.userProvinceName
        self.city.text = userInformation.shared.userCityName
        self.district.text = userInformation.shared.userDistrictName
        self.address.text = userInformation.shared.userAddress
        
    }
    
    
    
    
    
    // MARK:- // Reverse Geocoding to get address from Entered Lat Long
    
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if let pm = placemarks {
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print("Country---\(pm.country ?? "")")
                        print("Locality---\(pm.locality ?? "")")
                        print("Sublocality---\(pm.subLocality ?? "")")
                        print("Throughfate---\(pm.thoroughfare ?? "")")
                        print("PostalCode---\(pm.postalCode ?? "")")
                        print("SubThroughfare---\(pm.subThoroughfare ?? "")")
                        print("AdministrativeArea---\(pm.administrativeArea ?? "")")
                        
                        var addressString : String = ""
                        if pm.subThoroughfare != nil {    // Street No
                            addressString = addressString + pm.subThoroughfare! + ", "
                        }
                        if pm.thoroughfare != nil {    //Street Name
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.subLocality != nil {   //Area
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        
                        if pm.locality != nil {     //City
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.administrativeArea != nil {   //State
                            addressString = addressString + pm.administrativeArea! + ", "
                        }
                        if pm.country != nil {    //Country
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {   //ZipCode
                            addressString = addressString + pm.postalCode! + " "
                        }
                        
                        self.address.text = addressString
                        
                    }
                }
                

        })
        
    }
    
    //MARK:- Set Current Location
    func setCurrentLocation(){
        locationManager.requestWhenInUseAuthorization()
        var currentLocation: CLLocation!

        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
           CLLocationManager.authorizationStatus() ==  .authorizedAlways{
            currentLocation = locationManager.location
        }
        if let locValue = currentLocation?.coordinate{
            self.showLocation(addressLat: "\(locValue.latitude)", addressLong: "\(locValue.longitude)")
            
            self.userLat = "\(locValue.latitude)"
            self.userLong = "\(locValue.longitude)"
            
            self.getAddressFromLatLon(pdblLatitude: self.userLat!, withLongitude: self.userLong!)
        }
    }
    
}




// MARK:- // Pickerview Delegate Methods

extension MyProfileVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == self.typeOfAccountPickerview {
            return self.typeOfAccountArray.count
        }
        else if pickerView == self.countryPickerview {
            return self.countryArray.count
        }
        else if pickerView == self.provincePickerview {
            return self.provinceArray.count
        }
        else if pickerView == self.cityPickerview {
            return self.cityArray.count
        }
        else {
            return self.districtArray.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == self.typeOfAccountPickerview {
            return "\((self.typeOfAccountArray[row])["value"] ?? "")"
        }
        else if pickerView == self.countryPickerview {
            return "\(self.countryArray[row].countryName ?? "")"
        }
        else if pickerView == self.provincePickerview {
            return "\((self.provinceArray[row])["title_eng"] ?? "")"
        }
        else if pickerView == self.cityPickerview {
            return "\((self.cityArray[row])["cityname"] ?? "")"
        }
        else {
            return "\((self.districtArray[row])["districtname"] ?? "")"
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.typeOfAccountPickerview {
            
            if self.doneClicked
            {
                self.typeOfAccount.text = "\((self.typeOfAccountArray[row])["value"] ?? "")"
                
                self.accountType = "\((self.typeOfAccountArray[row])["key"] ?? "")"
                
                self.showOrHideSections()
                
                self.view.sendSubviewToBack(self.viewOfPicker)
                self.viewOfPicker.isHidden = true
                self.typeOfAccountPickerview.isHidden = true
            }
            else
            {
                self.selectedPickerIndex = row
            }
            
        }
        else if pickerView == self.countryPickerview {
            
            if self.doneClicked
            {
                self.countryName.text = "\(self.countryArray[row].countryName ?? "")"
                
                self.countryID = "\(self.countryArray[row].id ?? "")"
                
                globalFunctions.shared.loadImage(inImageview: self.countryImage, url: "\(self.countryArray[row].countryFlag ?? "")", indicatorInView: self.countryImage)
                
                self.userCountryFlagString = "\(self.countryArray[row].countryFlag ?? "")"
                
                self.userPhoneCode = "\(self.countryArray[row].phoneCode ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPicker)
                self.viewOfPicker.isHidden = true
                self.countryPickerview.isHidden = true
                
                self.phoneCode.text = "+(\(self.countryArray[row].phoneCode ?? ""))"
                self.officePhoneCode.text = "+(\(self.countryArray[row].phoneCode ?? ""))"
                self.faxPhoneCode.text = "+(\(self.countryArray[row].phoneCode ?? ""))"
//
//                self.provinceArray.removeAll()
//                self.cityArray.removeAll()
//                self.districtArray.removeAll()
//
//                self.province.text = globalFunctions.shared.getLanguageString(variable: "home_select_province")
//                self.city.text = globalFunctions.shared.getLanguageString(variable: "home_select_city")
//                self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
//
//                self.getProvinceList(countryID: "\(self.countryArray[row].id ?? "")") {
//
//                    print("Provinces Listed.")
//            }
            }
            else
            {
               self.selectedPickerIndex = row
            }
            
            
            
        }
        else if pickerView == self.provincePickerview {
            
            if self.doneClicked
            {
                self.province.text = "\((self.provinceArray[row])["title_eng"] ?? "")"
                
                self.provinceID = "\((self.provinceArray[row])["id"] ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPicker)
                self.viewOfPicker.isHidden = true
                self.provincePickerview.isHidden = true
                
                self.cityArray.removeAll()
                self.districtArray.removeAll()
                
                self.city.text = globalFunctions.shared.getLanguageString(variable: "home_select_city")
                self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
                
                self.getCityList(provinceID: "\((self.provinceArray[row])["id"] ?? "")") {
                    
                    print("Cities Listed.")
                    
                }
            }
            else
            {
                self.selectedPickerIndex = row
            }
            
        }
        else if pickerView == self.cityPickerview {
            
            if self.doneClicked
            {
                self.city.text = "\((self.cityArray[row])["cityname"] ?? "")"
                
                self.cityID = "\((self.cityArray[row])["id"] ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPicker)
                self.viewOfPicker.isHidden = true
                self.cityPickerview.isHidden = true
                
                self.districtArray.removeAll()
                
                self.district.text = globalFunctions.shared.getLanguageString(variable: "home_select_district")
                
                self.getDistrictList(cityID: "\((self.cityArray[row])["id"] ?? "")")
            }
            else
            {
                self.selectedPickerIndex = row
            }
           
        }
        else {
            if self.doneClicked
            {
                self.district.text = "\((self.districtArray[row])["districtname"] ?? "")"
                
                self.districtID = "\((self.districtArray[row])["id"] ?? "")"
                
                self.view.sendSubviewToBack(self.viewOfPicker)
                self.viewOfPicker.isHidden = true
                self.districtPickerview.isHidden = true
            }
            else
            {
                self.selectedPickerIndex = row
            }
        }
        
    }
    
}


// MARK:- // Imagepicker Delegate Methods

extension MyProfileVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            self.profileLogoImageview.image = image
            
            self.newBannerUploaded = true
        }
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
}




// MARK:- // CLLocationmanager Delegate

extension MyProfileVC: CLLocationManagerDelegate {
    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//
//        let locValue : CLLocationCoordinate2D = manager.location!.coordinate
//
//        self.showLocation(addressLat: "\(locValue.latitude)", addressLong: "\(locValue.longitude)")
//
//        self.userLat = "\(locValue.latitude)"
//        self.userLong = "\(locValue.longitude)"
//
//        self.getAddressFromLatLon(pdblLatitude: self.userLat!, withLongitude: self.userLong!)
//
//    }
    
}




// MARK:- // Textfield Delegate Functions

extension MyProfileVC: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.address {
            let autoCompleteController = GMSAutocompleteViewController()
            autoCompleteController.delegate = self
            
            let filter = GMSAutocompleteFilter()
            autoCompleteController.autocompleteFilter = filter
            
            self.locationManager.startUpdatingLocation()
            self.present(autoCompleteController, animated: true, completion: nil)
            
        }
        
        return false
    }
    
}



// MARK:- // GMS Autocomplete Delegate Functions

extension MyProfileVC: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        locationMapView.clear()
        
        let lat = place.coordinate.latitude
        let long = place.coordinate.longitude
        
        self.userLat = "\(lat)"
        self.userLong = "\(long)"
        
        //self.showLocation(addressLat: self.userLat, addressLong: self.userLong)
        self.address.text = place.formattedAddress
        
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 10.0)
        self.locationMapView.camera = camera
        self.address.text=place.formattedAddress
        chosenPlace = MyPlace(name: place.formattedAddress!, lat: lat, long: long)
        let marker=GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
//        marker.title = "\(place.name)"
//        marker.snippet = "\(place.formattedAddress!)"
        marker.map = self.locationMapView
        marker.isDraggable = true
        locationMapView.setMinZoom(5, maxZoom: 20)
        
        self.dismiss(animated: true, completion: nil) // dismiss after place selected
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("ERROR AUTO COMPLETE \(error)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }

    
}

// MARK:- //

extension MyProfileVC: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        
        self.getAddressFromLatLon(pdblLatitude: "\(marker.position.latitude)", withLongitude: "\(marker.position.longitude)")
        
        self.userLat = "\(marker.position.latitude)"
        self.userLong = "\(marker.position.longitude)"
        
        //self.showLocation(addressLat: self.userLat, addressLong: self.userLong)
    }
    
}


//// MARK:- // FUI Auth Delegate Functions
//
//extension MyProfileVC : FUIAuthDelegate {
//
//    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
//
//        // CHeck if there was an error
//        guard error == nil else {
//            // Login Error
//            return
//        }
//        print("USERID ------\(authDataResult?.user.uid ?? "")")
//        print("User Info -----\(authDataResult?.user.phoneNumber ?? "")")
//
//    }
//
//}










// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}






