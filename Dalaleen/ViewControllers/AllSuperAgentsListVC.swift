//
//  AllSuperAgentsListVC.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 22/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView

class AllSuperAgentsListVC: GlobalViewController {
    
    @IBOutlet weak var allSuperAgentsListTable: UITableView!
    
    var startValue = 0
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var recordsArray = NSMutableArray()
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        globalFunctions.shared.loadPlaceholder(onView: self.view)
        
        self.getAllAgentsList()
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            self.allSuperAgentsListTable.estimatedRowHeight = 150
            self.allSuperAgentsListTable.rowHeight = UITableView.automaticDimension
            
            globalFunctions.shared.removePlaceholder(fromView: self.view)
            
        }
        
    }
    
    
    
    
    // MARK:- // JSON Post Method to get all Super Agents' List
    
    func getAllAgentsList() {
        
        var parameters : String!
        
        parameters = "start_value=\(startValue)&per_load=\(perLoad)&language_code=\(UserDefaults.standard.string(forKey: "searchLanguageCode")!)"
        
        self.CallAPI(urlString: "super_agent", param: parameters) {
            
            for i in 0..<(self.globalJson["info"] as! NSArray).count {
                self.recordsArray.add((self.globalJson["info"] as! NSArray)[i] as! NSDictionary)
            }
            
            self.nextStart = "\(self.globalJson["next_start"]!)"
            
            self.startValue = self.startValue + self.recordsArray.count
            
            print("Next Start Value : " , self.startValue)
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.allSuperAgentsListTable.delegate = self
                self.allSuperAgentsListTable.dataSource = self
                self.allSuperAgentsListTable.reloadData()
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //SVProgressHUD.dismiss()
            }
            
        }
        
    }

   

}




// MARK:- // Tableview Delegate Methods

extension AllSuperAgentsListVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.recordsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "allSuperAgentsListTVC") as! allSuperAgentsListTVC
        
        cell.advertisementImage.sd_setImage(with: URL(string: "\((self.recordsArray[indexPath.row] as! NSDictionary)["card_image"] ?? "")"), placeholderImage: UIImage(named: "user-image-with-black-background"))
        
        //cell.advertisementImage.sd_setImage(with: URL(string: "\((self.recordsArray[indexPath.row] as! NSDictionary)["card_image"] ?? "")"))
        
        cell.agentImage.sd_setImage(with: URL(string: "\((self.recordsArray[indexPath.row] as! NSDictionary)["profile_image"] ?? "")"), placeholderImage: UIImage(named: "user-image-with-black-background"))

        //cell.agentImage.sd_setImage(with: URL(string: "\((self.recordsArray[indexPath.row] as! NSDictionary)["profile_image"] ?? "")"))

        cell.advertisementTitle.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["business_title"] ?? "")"
        cell.advertisementDescription.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["business_description"] ?? "")"
        cell.agentName.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["name"] ?? "")"
        cell.agentLocation.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["country_name"] ?? "")" + " - \((self.recordsArray[indexPath.row] as! NSDictionary)["city_name"] ?? "")"
        
        
        cell.cellView.layer.cornerRadius = 8
        cell.cellShadowview.layer.cornerRadius = 8
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        // SEt Font
        cell.advertisementTitle.font = UIFont(name: cell.advertisementTitle.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        cell.advertisementDescription.font = UIFont(name: cell.advertisementDescription.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 11)))
        cell.agentName.font = UIFont(name: cell.agentName.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 13)))
        cell.agentLocation.font = UIFont(name: cell.agentLocation.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 11)))
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "SuperAgentDetailsVC") as! SuperAgentDetailsVC
        
        navigate.agentID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["super_agent_id"] ?? "")"
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
}




// MARK:- // Scrollview Delegate Methods

extension AllSuperAgentsListVC: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {
            
        }
        else
        {
            print("next start : ",nextStart ?? "")
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    //SVProgressHUD.dismiss()
                }
            }
            else
            {
                self.getAllAgentsList()
            }
        }
    }
}
