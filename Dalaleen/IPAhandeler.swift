//
//  IPAhandeler.swift
//  Dalaleen
//
//  Created by esolz on 08/01/20.
//  Copyright © 2020 esolz. All rights reserved.
//

import Foundation
import StoreKit

public struct SubscriptionDataGroup {
    public let subscriptionStrings: [String]
}

public struct SubscriptionProducts {
    
    public static let oneMonthSubUrgent = "com.esolz.Dalaleen.urgent30"
    public static let sixMonthsSubUrgent = "com.esolz.Dalaleen.urgent180"
    public static let oneYearSubUrgent = "com.esolz.Dalaleen.urgent365"
    
    public static let oneMonthSubGold = "com.esolz.Dalaleen.gold30"
    public static let sixMonthsSubGold = "com.esolz.Dalaleen.gold180"
    public static let oneYearSubGold = "com.esolz.Dalaleen.gold365"
    
    public static let tenDaysSubPremium = "com.esolz.Dalaleen.10dPrem"
    public static let oneMonthPremium = "com.esolz.Dalaleen.30dPrem"
    public static let twoMonthPremium = "com.esolz.Dalaleen.60dPrem"
    
     public static let oneMonthSeasonal = "com.esolz.Dalaleen.Seasonal30Days"
     public static let sixMonthSeasonal = "com.esolz.Dalaleen.Seasonal180Days"
    
     public static let oneYearAnnual  = "com.esolz.Dalaleen.Annual1Year"
    
    
    //public static let nonRenewableOneYear = "NonRenew1Year"
    
    public static let store = IAPManager(productIDs: SubscriptionProducts.productIDs)
    
    //private static let productIDs: Set<ProductID> =
//        [SubscriptionProducts.oneMonthSubUrgent, SubscriptionProducts.oneYearSubGold, SubscriptionProducts.oneYearSubUrgent,SubscriptionProducts.oneMonthSubGold, SubscriptionProducts.sixMonthsSubGold, SubscriptionProducts.sixMonthsSubUrgent]
    private static let productIDs: Set<ProductID> =
        [SubscriptionProducts.oneMonthSubUrgent,
         SubscriptionProducts.sixMonthsSubUrgent,
         SubscriptionProducts.oneYearSubUrgent,
         SubscriptionProducts.oneMonthSubGold,
         SubscriptionProducts.sixMonthsSubGold,
         SubscriptionProducts.oneYearSubGold,
         SubscriptionProducts.tenDaysSubPremium,
         SubscriptionProducts.oneMonthPremium,
         SubscriptionProducts.twoMonthPremium,
         SubscriptionProducts.oneMonthSeasonal,
         SubscriptionProducts.sixMonthSeasonal,
         SubscriptionProducts.oneYearAnnual
            
    ]
//    private static let productIDs: Set<ProductID> =
//        [SubscriptionProducts.nonRenewableOneYear]
    
}

public func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
    return productIdentifier.components(separatedBy: ".").last
}
