//
//  TableviewCells.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 05/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import Foundation

// MARK:- // Login Country List Table Cell

class countryAndPhoneCodeTVC: UITableViewCell {
    
    @IBOutlet weak var cellVIew: UIView!
    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var countryPhoneCode: UILabel!
    @IBOutlet weak var countryName: UILabel!
    
    
}


// MARK:- // New Home Screen Listing Table View Cell

class RealEstateListingTVC: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var propertyImage: UIImageView!
    @IBOutlet weak var parentCategoryImage: UIImageView!
    @IBOutlet weak var imageCount: UILabel!
    @IBOutlet weak var listingDate: UILabel!
    @IBOutlet weak var propertyType: UILabel!
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var propertyFeaturesView: UIView!
    @IBOutlet weak var bedCount: UILabel!
    @IBOutlet weak var livingRoomCount: UILabel!
    @IBOutlet weak var bathroomCount: UILabel!
    @IBOutlet weak var diningCount: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var district: UILabel!
    
    
    @IBOutlet weak var cellSideView: UIView!
    
    @IBOutlet weak var urgentImageview: UIImageView!
    
    
    
}



// MARK:- // Property Details Tableview Cell

class propertyDetailsTVC: UITableViewCell {
    
    @IBOutlet weak var cellKey: UILabel!
    @IBOutlet weak var cellValue: UILabel!
    
}




// MARK:- // Inbox Listing Tableview Cell

class inboxListingTVC: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var readUnreadImageview: UIImageView!
    @IBOutlet weak var messageSubject: UILabel!
    @IBOutlet weak var messageDate: UILabel!
    @IBOutlet weak var messageTime: UILabel!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var senderEmail: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var viewMessageButton: UIButton!
    
}



// MARK:- // All Super Agents' List Tableview Cell

class allSuperAgentsListTVC: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellShadowview: ShadowView!
    @IBOutlet weak var advertisementImage: UIImageView!
    @IBOutlet weak var advertisementTitle: UILabel!
    @IBOutlet weak var advertisementDescription: UILabel!
    @IBOutlet weak var agentImage: UIImageView!
    @IBOutlet weak var agentName: UILabel!
    @IBOutlet weak var agentLocation: UILabel!
    
    
}



// MARK:- // Recycle Bin Listing Tableview Cell

class RecyclebinTVC: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var messageSubject: UILabel!
    @IBOutlet weak var messageDate: UILabel!
    @IBOutlet weak var messageTime: UILabel!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var senderEmail: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var deleteMessageButton: UIButton!
    @IBOutlet weak var restoreMessageButton: UIButton!
    @IBOutlet weak var viewMessageButton: UIButton!
    
}



// MARK:- // Filter Tableview Cell

class filterRealEstateTypeTVC: UITableViewCell {
    
    @IBOutlet weak var boxImageview: UIImageView!
    @IBOutlet weak var cellLBL: UILabel!
    
}



// MARK:- // Add real Estate Plan type Tableview Cell

class addRealEstatePlanTypeTVC: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var planName: UILabel!
    @IBOutlet weak var planDaysView: UIView!
    @IBOutlet weak var planDays: UILabel!
    @IBOutlet weak var planCost: UILabel!
    @IBOutlet weak var planDurationButton: UIButton!
    @IBOutlet weak var planNameWidthCOnstraint: NSLayoutConstraint!
    
}

class subscriptionTVC: UITableViewCell{
    @IBOutlet weak var checkBtnOut: UIButton!
    @IBOutlet weak var planLbl: UILabel!
    
}

