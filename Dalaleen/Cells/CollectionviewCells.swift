//
//  CollectionviewCells.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 11/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import Foundation
import YouTubePlayer_Swift


// MARK:- // New Home Screen Listing Collectionview Cell

class NewHomeListingCVCCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var parentCategoryImage: UIImageView!
    
    @IBOutlet weak var cellSideView: UIView!
    @IBOutlet weak var propertyImage: UIImageView!
    @IBOutlet weak var propertyType: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var propertyFeaturesView: UIView!
    @IBOutlet weak var bedCount: UILabel!
    @IBOutlet weak var livingRoomCount: UILabel!
    @IBOutlet weak var bathroomCount: UILabel!
    @IBOutlet weak var diningCount: UILabel!
    @IBOutlet weak var listingDate: UILabel!
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var district: UILabel!
    
   
    
    @IBOutlet weak var urgentImageview: UIImageView!
    @IBOutlet weak var imageCount: UILabel!
    
    
}



// MARK:- // Property Details Screen Property Image Collectionview Cell

class propertyImageCVC: UICollectionViewCell {
    
    @IBOutlet weak var videoView: YouTubePlayerView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellImage: UIImageView!
    
}


// MARK:- // Extend Edit Delete Sold Collectionview Cell

class extendEditDeleteSoldCVC: UICollectionViewCell {
    
    @IBOutlet weak var cellImage : UIImageView!
    @IBOutlet weak var cellLBL : UILabel!
    
}


// MARK:- // Property Details Room Types Collectionview Cell

class roomTypesCVC: UICollectionViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var roomTypeImage: UIImageView!
    @IBOutlet weak var roomCount: UILabel!
    @IBOutlet weak var separator: UIView!
    
    
}



// MARK:- // Super Agent Profile Information Image Slide Collectionview Cell

class imageSlideCVC: UICollectionViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var deleteCheckImage: UIImageView!
    
}



// MARK:- // Super Agent Plans Collectionview Cell

class SuperAgentPlansCVC: UICollectionViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellButton: UIButton!
    @IBOutlet weak var cellLBL: UILabel!
    
}


// MARK:- // Super Agent Details Image Slideshow Collectionview

class imageSlideshowCVC
: UICollectionViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    
}



// MARK:- // Filter General Info Collectionview Cell

class filterGeneralInfoCVC: UICollectionViewCell {
    
    @IBOutlet weak var cellBoxImage: UIImageView!
    @IBOutlet weak var cellLBL: UILabel!
    
    
}

