//
//  AppDelegate.swift
//  Dalaleen
//
//  Created by admin on 12/06/18.
//  Copyright © 2018 esolz. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import Firebase
import UserNotificationsUI
import UserNotifications
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // Debojyoti Chowdhury gave this.
        //        GMSServices.provideAPIKey("AIzaSyDhSiMkUDx_PVnzxrRMuAfxaeLAObKuskM")
        //        GMSPlacesClient.provideAPIKey("AIzaSyDhSiMkUDx_PVnzxrRMuAfxaeLAObKuskM")
        
        //Client Gave this
        GMSServices.provideAPIKey("AIzaSyDGMy2SkOJlEUmDBSS_1XN8nbXqgen_1cw")
        GMSPlacesClient.provideAPIKey("AIzaSyDGMy2SkOJlEUmDBSS_1XN8nbXqgen_1cw")
        
        // MY Generated Key
        //        GMSServices.provideAPIKey("AIzaSyDZom7DmbweIrVRbygX9mOh442JyYlyXzM")
        //        GMSPlacesClient.provideAPIKey("AIzaSyDZom7DmbweIrVRbygX9mOh442JyYlyXzM")
        
        IQKeyboardManager.shared.enable = true
        Messaging.messaging().isAutoInitEnabled = true
        
        // Firebase
        FirebaseApp.configure()
        
        
        if #available(iOS 10, *)
        {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                
                guard error == nil
                    else
                {
                    UserDefaults.standard.set(false, forKey: "allowpushnotifaction")
                    return
                }
                
                if granted
                {
                    
                    DispatchQueue.main.async(execute: {
                        UserDefaults.standard.set(true, forKey: "allowpushnotifaction")
                        //    application.registerForRemoteNotifications()
                        UIApplication.shared.registerForRemoteNotifications()
                        
                    })
                }
                else
                {
                    //Handle user denying permissions..
                }
            }
            //
            //  application.registerForRemoteNotifications()
            UIApplication.shared.registerForRemoteNotifications()
        }
        else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            UserDefaults.standard.set(true, forKey: "allowpushnotifaction")
            //  application.registerForRemoteNotifications()
            UIApplication.shared.registerForRemoteNotifications()
        }
        HTTPCookieStorage.shared.cookieAcceptPolicy = .always
        Messaging.messaging().delegate = self
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        ConnectivityManager.shared().startListening()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
        ConnectivityManager.shared().stopListening()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Dalaleen")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
     //MARK:- FireBase Messaging
        
        func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
            print("Firebase registration token: \(fcmToken)")
            let defaults = UserDefaults.standard
            defaults.set(fcmToken, forKey:"DeviceToken")
            
            let dataDict:[String: String] = ["token": fcmToken]
            NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
            // TODO: If necessary send token to application server.
            // Note: This callback is fired at each app startup and whenever a new token is generated.
        }
        func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
            print("Received data message: \(remoteMessage.appData)")
        }
        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
          // If you are receiving a notification message while your app is in the background,
          // this callback will not be fired till the user taps on the notification launching the application.
          // TODO: Handle data of notification

          // With swizzling disabled you must let Messaging know about the message, for Analytics
          // Messaging.messaging().appDidReceiveMessage(userInfo)
          // Print message ID.
            //NotificationCenter.default.post(name: Notification.Name("changeRootVC"), object: nil, userInfo: ["type":"1"])
            //}
            
          if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
          }

          // Print full message.
          print(userInfo)
        }

        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                         fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
          // If you are receiving a notification message while your app is in the background,
          // this callback will not be fired till the user taps on the notification launching the application.
          // TODO: Handle data of notification

          // With swizzling disabled you must let Messaging know about the message, for Analytics
          // Messaging.messaging().appDidReceiveMessage(userInfo)
    //        if ((self.topViewController()?.isKind(of: StatusDetailsVC.self))!){
    //        print("Inside StatusDetailsVC")
          // NotificationCenter.default.post(name: Notification.Name("reloadPage"), object: nil)
    //        }
    //        else{
                
          // Print message ID.
          if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
          }

          // Print full message.
          print(userInfo)

          completionHandler(UIBackgroundFetchResult.newData)
        }

        func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
            print("i am not available in simulator \(error)")
            
            let defaults = UserDefaults.standard
            defaults.set("cb8143a8becd78c317b8e0c722c9177a4b9579ab25f5e2f5f4fe806dc2937a3e", forKey:"DeviceToken")
        }

        // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
        // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
        // the FCM registration token.
        func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
                let tokenParts = deviceToken.map { data -> String in
                    return String(format: "%02.2hhx", data)
                }
                let token = tokenParts.joined()
                // 2. Print device token to use for PNs payloads
                print("Device Token: \(token)")
                
        //        let defaults = UserDefaults.standard
        //        defaults.set(token, forKey:"DeviceToken")
                Messaging.messaging().apnsToken = deviceToken
              
            }
        

        
        func requestNotificationAuthorization(application: UIApplication) {
               if #available(iOS 10.0, *) {
                UNUserNotificationCenter.current().delegate = self
                   let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                   UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
               } else {
                   let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                   application.registerUserNotificationSettings(settings)
               }
           }

        // Receive displayed notifications for iOS 10 devices.

        func userNotificationCenter(_ center: UNUserNotificationCenter,

                                    willPresent notification: UNNotification,

                                    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

            let userInfo = notification.request.content.userInfo

            //NotificationCenter.default.post(name: Notification.Name("changeRootVC"), object: nil, userInfo: nil)

            if let messageID = userInfo[gcmMessageIDKey] {

                print("Message ID: \(messageID)")

            }

            // Print full message.

            print(userInfo)
            if let messageInfo = (userInfo["gcm.notification.info"]){
                print("\(messageInfo)".contains("logout"))
            }

            completionHandler([.alert,.sound,.badge])

        }

        func userNotificationCenter(_ center: UNUserNotificationCenter,

                                    didReceive response: UNNotificationResponse,

                                    withCompletionHandler completionHandler: @escaping () -> Void) {

            let userInfo = response.notification.request.content.userInfo

            // Print message ID.

            if let messageID = userInfo[gcmMessageIDKey] {

                print("Message ID: \(messageID)")

            }
            if let messageInfo = (userInfo["gcm.notification.info"]){
                print("\(messageInfo)".contains("logout"))
                if ("\(messageInfo)".contains("logout")){
                   UserDefaults.standard.set(false, forKey: "isClickOnPush")
                }
                else{
                    UserDefaults.standard.set(true, forKey: "isClickOnPush")
                    NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                }
            }
            
            

            // Print full message.

            print(userInfo)

            completionHandler()

        }

}

