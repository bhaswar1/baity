//
//  HeaderView.swift
//  Dalaleen
//
//  Created by admin on 21/06/18.
//  Copyright © 2018 esolz. All rights reserved.
//

import UIKit

class HeaderView: UIView {
    
    
    @IBOutlet var contentVIew: UIView!
    
    @IBOutlet weak var sideMenuButton: UIButton!
    @IBOutlet weak var headerBackButton: UIButton!
    
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var headerViewOfImage: UIView!
    @IBOutlet weak var headerImage: UIImageView!
    
    
    @IBOutlet weak var iconImageOne: UIImageView!
    @IBOutlet weak var headerIconPositionOne: UIButton!
    @IBOutlet weak var iconImageTwo: UIImageView!
    @IBOutlet weak var headerIconPositionTwo: UIButton!
    
    
    @IBOutlet weak var searchResultsLBL: UILabel!
    
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)
        addSubview(contentVIew)
        contentVIew.frame = self.bounds
        contentVIew.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        
        self.headerTitle.font = UIFont(name: self.headerTitle.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.searchResultsLBL.font = UIFont(name: self.searchResultsLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        
        
        if (userInformation.shared.searchLanguageCode.elementsEqual("ar")) {
            self.headerBackButton.transform = CGAffineTransform(rotationAngle: .pi)
        }
        
        
    }

}
