//
//  SideMenuView.swift
//  Dalaleen
//
//  Created by admin on 20/06/18.
//  Copyright © 2018 esolz. All rights reserved.
//

import UIKit
protocol Side_menu_delegate: class {
    func action_method(sender: NSInteger?)
    func guest_action_method(sender: NSInteger?)
}

class SideMenuView: UIView {

    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var sideMenuTable: UITableView!
    
    @IBOutlet weak var closeButtonView: UIView!
    @IBOutlet weak var closeSideMenu: UIButton!
    
    
    @IBOutlet weak var userInfoView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var clickOnProfile: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var userPhoneNumber: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    
    @IBOutlet weak var separator: UIView!
    
    weak var Side_delegate: Side_menu_delegate?
    
    var sideMenuUserDataArray = [sideMenuDataFormat]()
    
    var sideMenuGuestDataArray = [sideMenuDataFormat]()
    
    // MARK:- // Create Data Arrays
    
    //sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "app_where_to_search"), imageString: userInformation.shared.searchCountryFlag, notificationShow: false),
    
    func createDataArrays() {
        sideMenuUserDataArray = [
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "left_side_home_page"), imageString: "SideMenuHome", notificationShow: false),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "app_select_language"), imageString: "SideMenuSelectLanguage", notificationShow: false),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "left_side_inbox"), imageString: "SideMenuInbox", notificationShow: true),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "left_side_recycle_bin"), imageString: "SideMenuRecycleBin", notificationShow: true),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "app_ads"), imageString: "SideMenuMyAds", notificationShow: true),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "left_side_my_favorite"), imageString: "SideMenuMyFavourite", notificationShow: true),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "app_new_ad"), imageString: "SideMenuAddNewAd", notificationShow: false),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_page_title"), imageString: "SideMenuMySuperAgentProfile",  notificationShow: false),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "app_show_all_super_agents"), imageString: "SideMenuShowAllSuperAgents", notificationShow: false),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "footer_help_text"), imageString: "SideMenuHelp", notificationShow: false),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "my_profile_change_password"), imageString: "SideMenuChangePassword",  notificationShow: false),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "footer_contact_us"), imageString: "SideMenuContactUs", notificationShow: false),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "left_side_logout"), imageString: "SideMenuLogOut", notificationShow: false)]
        
        sideMenuGuestDataArray = [
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "left_side_home_page"), imageString: "SideMenuHome", notificationShow: false),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "app_select_language"), imageString: "SideMenuSelectLanguage", notificationShow: false),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "app_new_ad"), imageString: "SideMenuAddNewAd", notificationShow: false),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "app_show_all_super_agents"), imageString: "SideMenuShowAllSuperAgents", notificationShow: false),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "footer_help_text"), imageString: "SideMenuHelp", notificationShow: false),
            sideMenuDataFormat(title: globalFunctions.shared.getLanguageString(variable: "footer_contact_us"), imageString: "SideMenuContactUs", notificationShow: false)]
    }
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        Bundle.main.loadNibNamed("SideMenuView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        self.signInButton.setTitle(globalFunctions.shared.getLanguageString(variable: "header_sign_in"), for: .normal)
        self.signUpButton.setTitle(globalFunctions.shared.getLanguageString(variable: "header_sign_up"), for: .normal)
        
        
        let cellIdentifier = "SideViewCell"
        let nibName = UINib(nibName: "SideViewCell", bundle:nil)
        self.sideMenuTable.register(nibName, forCellReuseIdentifier: cellIdentifier)
        
        self.setUserInformation()
        
        self.userImage.layer.cornerRadius = self.userImage.frame.size.height / 2
        
        self.createDataArrays()
        
        self.sideMenuTable.delegate = self
        self.sideMenuTable.dataSource = self
        
        if "\(userInformation.shared.superAgent ?? "")".elementsEqual("Y") {
            self.sideMenuUserDataArray[7].title = globalFunctions.shared.getLanguageString(variable: "upgradetosuperagent_page_title")
        }
        else {
            self.sideMenuUserDataArray[7].title = globalFunctions.shared.getLanguageString(variable: "left_side_upgrade_to_super_agent")
        }
        
        self.sideMenuTable.isScrollEnabled = false
    }
    
    
    
    // MARK:- // Set User Information
    
    func setUserInformation() {
        
        if userInformation.shared.userPhoneNumber.elementsEqual("") == false {
            self.signInButton.isHidden = true
            self.separator.isHidden = true
            self.signUpButton.isHidden = true
            
            self.userImage.sd_setImage(with: URL(string: "\(userInformation.shared.userImageString ?? "")"), placeholderImage: UIImage(named: "user-image-with-black-background"))
            
        }
        else {
            self.userImage.image = UIImage(named: "user")
        }
        
        self.userPhoneNumber.text = "\(userInformation.shared.userName ?? "")"
        self.userEmail.text = "\(userInformation.shared.userEmail ?? "")"
        
    }
    
}


// MARK:- // Tableview Delegate Methods

extension SideMenuView: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if userInformation.shared.userPhoneNumber.elementsEqual("") {
            return self.sideMenuGuestDataArray.count
        }
        else {
            return self.sideMenuUserDataArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cellIdentifier = "SideViewCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SideViewCell
        
        var dataArray = [sideMenuDataFormat]()
        
        if userInformation.shared.userPhoneNumber.elementsEqual("") {
            dataArray = self.sideMenuGuestDataArray
        }
        else {
            dataArray = sideMenuUserDataArray
        }
        
        
        
        cell.labelOne.text = "\(dataArray[indexPath.row].title ?? "")"
        
        if "\(dataArray[indexPath.row].title!)".elementsEqual(globalFunctions.shared.getLanguageString(variable: "app_where_to_search")) {
            cell.labelTwo.isHidden = false
            cell.labelTwo.text = globalFunctions.shared.getLanguageString(variable: "my_profile_select_country")
            
            cell.cellImage.sd_setImage(with: URL(string: "\(dataArray[indexPath.row].imageString ?? "")"))
        }
        else {
            cell.labelTwo.isHidden = true
            cell.cellImage.image = UIImage(named: "\(dataArray[indexPath.row].imageString ?? "")")
        }
        
        if dataArray[indexPath.row].notificationShow == true {
            cell.notificationCountView.isHidden = false
            
            if indexPath.row == 2 { // Inbox
                cell.notificationCountLBL.text = userInformation.shared.userInboxCount
            }
            else if indexPath.row == 3 { // Recycle Bin
                cell.notificationCountLBL.text = userInformation.shared.userRecyclebinCount
            }
            else if indexPath.row == 4 { // My Ads
                cell.notificationCountLBL.text = userInformation.shared.myAdsCount
            }
            else if indexPath.row == 5 { // My Favourites
                cell.notificationCountLBL.text = userInformation.shared.myFavouritesCount
            }
        }
        else {
            cell.notificationCountView.isHidden = true
        }
        
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        // Set Font
        cell.labelOne.font = UIFont(name: cell.labelOne.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        cell.labelTwo.font = UIFont(name: cell.labelTwo.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 11)))
        cell.notificationCountLBL.font = UIFont(name: cell.notificationCountLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 11)))
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 36
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let cell = cell as! SideViewCell
        
        cell.notificationCountView.layer.cornerRadius = cell.notificationCountView.frame.size.height / 2
        cell.notificationCountShadowView.layer.cornerRadius = cell.notificationCountShadowView.frame.size.height / 2
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if userInformation.shared.userPhoneNumber.elementsEqual("")
        {
            Side_delegate?.guest_action_method(sender: indexPath.row)
        }
        else
        {
            Side_delegate?.action_method(sender: indexPath.row)
        }
        
    }
    
    
}
