//
//  PopUpViewTypeOne.swift
//  Dalaleen
//
//  Created by Shirsendu Sekhar Paul on 12/07/19.
//  Copyright © 2019 esolz. All rights reserved.
//

import Foundation

class PopUpViewTypeOne: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var whiteBackgroundview: UIView!
    @IBOutlet weak var closeBackgroundButton: UIButton!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var viewOfTextview: UIView!
    @IBOutlet weak var contentTXTView: UITextView!
    @IBOutlet weak var submitButton: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        Bundle.main.loadNibNamed("PopUpViewTypeOne", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        self.SetFont()
        
        self.localizeStrings()
        
        self.viewOfTextview.layer.borderWidth = 0.5
        self.viewOfTextview.layer.borderColor = UIColor.lightGray.cgColor
        
        self.submitButton.setTitle(globalFunctions.shared.getLanguageString(variable: "resetpass_submit_msg"), for: .normal) 
        
        self.submitButton.layer.cornerRadius = self.submitButton.frame.size.height / 2
        
    }
    
    
    // MARK:- // Set Font
    
    func SetFont() {
        
        self.titleLBL.font = UIFont(name: self.titleLBL.font.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        self.contentTXTView.font = UIFont(name: self.contentTXTView.font!.fontName, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 14)))
        self.submitButton.titleLabel?.font = UIFont(name: (self.submitButton.titleLabel?.font.fontName)!, size: CGFloat(globalFunctions.shared.Get_fontSize(size: 15)))
        
    }
    
    
    // MARK:- // Localize Strings
    
    func localizeStrings() {
        
        self.submitButton.titleLabel?.text = "    " + globalFunctions.shared.getLanguageString(variable: "ios_submit_capital") + "    "

    }
    
}



