//
//  SideViewCell.swift
//  Dalaleen
//
//  Created by admin on 20/06/18.
//  Copyright © 2018 esolz. All rights reserved.
//

import UIKit

class SideViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var labelOne: UILabel!
    @IBOutlet weak var labelTwo: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var notificationCountView: UIView!
    @IBOutlet weak var notificationCountShadowView: ShadowView!
    @IBOutlet weak var notificationCountLBL: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
