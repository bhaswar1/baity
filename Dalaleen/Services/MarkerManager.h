//
//  MarkerManager.h
//  ROAD SENSOR
//
//  Created by Priyanka on 24/01/18.
//  Copyright © 2018 Esolz. All rights reserved.
//

#ifndef MarkerManager_h
#define MarkerManager_h

#import <Foundation/Foundation.h>
@import CoreLocation;
#import "GMUClusterItem.h"
#import <GoogleMaps/GoogleMaps.h>


@interface MarkerManager: NSObject

@property (nonatomic) CLLocationCoordinate2D location;
@property (nonatomic, strong) GMSMarker *marker;

@end

#endif /* MarkerManager_h */
